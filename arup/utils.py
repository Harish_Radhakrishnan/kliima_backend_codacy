""" 
Utility module for settings
"""
import json
import boto3
from datetime import datetime

class LogUtils():
    """
    Log utility class
    """
    @staticmethod
    def load_dict(primary_key: int) -> dict:
        """
        getting log details
        """
        return {"rid":primary_key,
                "time":datetime.strftime(datetime.now(),
                                         "%Y:%m:%d %H:%M:%S.%f")
                }
    
    @staticmethod
    def load_message(rid: int, time: str, message: str) -> str:
        """
        getting log message
        """
        return f"{time}_{rid}_{message}"
    
class AwsSecretManager():
    """
    secrets
    """
    __instance = None
    
    @staticmethod
    def load():
        """
        load key value pair
        """
        if AwsSecretManager.__instance == None:
            secret_id = "kliima-secret"
            client = boto3.client('secretsmanager',region_name="eu-west-2")
            response = client.get_secret_value(SecretId=secret_id)
            data = json.loads(response["SecretString"])
            AwsSecretManager.__instance = data
        return AwsSecretManager.__instance
