"""
logging module
"""
import logging
from django.conf import settings

class Logger():
    """
    Logger class
    """
    __instance = None
    
    @staticmethod
    def get_logger(name: str):
        """
        loading logger
        """
        if Logger.__instance == None:
            logger = logging.getLogger(name)
            logger.setLevel(logging.DEBUG)
            handler = logging.FileHandler(f"{settings.LOG_DIR}/{name}.LOG")
            # handler = logging.StreamHandler()
            formatter = logging.\
                            Formatter("%(levelname)s-%(asctime)s-%(message)s")
            handler.setFormatter(formatter)
            handler.setLevel(logging.INFO)
            logger.addHandler(handler)
            Logger.__instance = logger
        return Logger.__instance
