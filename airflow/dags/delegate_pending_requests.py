import os
from airflow import DAG
import datetime
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from handlers.get_request_row import get_request_row_handler
from handlers.new_request_branch import new_request_branch_handler
from handlers.update_request_row_status import update_request_row_status_handler
from handlers.sendgrid_notify_user import sendgrid_notify_user_handler



default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime.datetime(2021, 2, 10),
    'retries': 0
}



schedule_interval = '* * * * *'

query_folder_path = os.path.join(os.path.dirname(__file__), '../data/sql')
with open('{}/get_request_row.sql'.format(query_folder_path), 'r') as f:
    select_request_row_query = f.read()

with open('{}/update_request_row_status.sql'.format(query_folder_path), 'r') as f:
    update_request_row_status_query = f.read()


with DAG('delegate_pending_requests', schedule_interval=schedule_interval, catchup=False, max_active_runs=1, concurrency=1, default_args=default_args) as dag:

    get_request_row = PythonOperator(
        task_id='get_request_row',
        python_callable=get_request_row_handler,
        provide_context=True,
        op_kwargs={'select_query': select_request_row_query,
                    'mysql_conn_id': 'mysql_conn_id',
                    'xcom_row_variable': 'pending_row_data'}
    )

    branch_for_new_request = BranchPythonOperator(
        task_id='branch_for_new_request',
        python_callable=new_request_branch_handler,
        provide_context=True,
        op_kwargs={'mysql_conn_id': 'mysql_conn_id',
                    'xcom_row_variable': 'pending_row_data'}
    )

    no_pending_requests = DummyOperator(
        task_id='no_pending_requests'
    )

    update_request_row_status = PythonOperator(
        task_id='update_request_row_status',
        python_callable=update_request_row_status_handler,
        provide_context=True,
        op_kwargs={'xcom_row_variable': 'pending_row_data',
                    'update_query': update_request_row_status_query,
                    'mysql_conn_id': 'mysql_conn_id'}
    )

    queued_notification = PythonOperator(
        task_id='queued_notification',
        python_callable=sendgrid_notify_user_handler,
        provide_context=True,
        op_kwargs={'xcom_row_variable': 'pending_row_data',
                    'status': 'queued'}
    )

    trigger_cordex_data_extraction = TriggerDagRunOperator(
        task_id="trigger_cordex_data_extraction",
        trigger_dag_id="cordex_data_extraction",
        conf={"pending_row_data": "{{ ti.xcom_pull(key='pending_row_data') }}"}
    )


get_request_row >> branch_for_new_request >> [no_pending_requests, update_request_row_status, queued_notification]
update_request_row_status >> trigger_cordex_data_extraction


