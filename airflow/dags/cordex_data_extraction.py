import os
import time
import json
import ast
from airflow import DAG
import datetime
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from operators.cordex_search_operator import CordexSearchOperator
from hooks.datasearch_hook import DatasearchHook
from hooks.custom_mysql_hook import CustomMysqlHook
from handlers.sendgrid_notify_user import sendgrid_notify_user_handler
from handlers.update_row import update_row_handler
from handlers.response_status_branch import response_status_branch_handler


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime.datetime(2021, 5, 26),
    'retries': 0
}


schedule_interval = None



query_folder_path = os.path.join(os.path.dirname(__file__), '../data/sql')
with open('{}/check_status.sql'.format(query_folder_path), 'r') as f:
    check_status_query = f.read()

with open('{}/update_row.sql'.format(query_folder_path), 'r') as f:
    update_row_query = f.read()




def get_request_row_data(**context):
    task_instance = context['task_instance']
    received_value = context['dag_run'].conf['pending_row_data']
    received_value = ast.literal_eval(received_value)
    print("Received Value: {}".format(received_value))
    task_instance.xcom_push(key='pending_row_data',value=received_value)



def check_updation(mysql_conn_id, xcom_row_variable, **context):
    task_instance = context['task_instance']
    pending_row_data = task_instance.xcom_pull(key=xcom_row_variable)
    primary_key = pending_row_data.get("request_details").get("primary_key")
    num_coordinates = pending_row_data.get("num_coordinates")
    print("Num Coordinates: {} | {}".format(type(num_coordinates), num_coordinates))

    timeout_datetime = datetime.datetime.now() + datetime.timedelta(minutes=60*int(num_coordinates))
    print("Timeout period: {}".format(timeout_datetime))
    
    has_completed = False
    while datetime.datetime.now() < timeout_datetime:
        time.sleep(60)
        mysql_conn = CustomMysqlHook(mysql_conn_id)
        status = mysql_conn.get_row(check_status_query.replace('__primary_key__', str(primary_key)))[0]
        print(status)
        mysql_conn.close_connection()
        if status == 'in-process':
            continue
        else:
            task_instance.xcom_push(key="status", value=status)
            has_completed = True
            break
    if not has_completed:
        task_instance.xcom_push(key="status", value='stuck')






def update_mysql_row(mysql_conn_id, xcom_row_variable, **context):
    task_instance = context['task_instance']
    mysql_conn = CustomMysqlHook(mysql_conn_id)
    request_id = task_instance.xcom_pull(key=xcom_row_variable).get("request_id")
    update_values = (json.dumps({}), json.dumps({}), json.dumps({}), 'failed', request_id)
    print("Update Values: {}".format(update_values))
    mysql_conn.execute_query(update_row_query, update_values)




with DAG('cordex_data_extraction', schedule_interval=schedule_interval, catchup=False, max_active_runs=1, concurrency=1, default_args=default_args) as dag:

    tasks = list()

    get_request_row_data = PythonOperator(
        task_id='get_request_row_data',
        python_callable=get_request_row_data,
        provide_context=True
    )

    in_process_notification = PythonOperator(
        task_id='in_process_notification',
        python_callable=sendgrid_notify_user_handler,
        provide_context=True,
        op_kwargs={
                    'xcom_row_variable': 'pending_row_data',
                    'status': 'in-process'}
    )

    cordex_data_search = CordexSearchOperator(
        task_id='cordex_data_search',
        mysql_conn_id='mysql_conn_id',
        datasearch_api_conn_id='datasearch_api_conn_id',
        xcom_row_variable='pending_row_data'
    )

    wait_for_results = PythonOperator(
        task_id='wait_for_results',
        python_callable=check_updation,
        provide_context=True,
        op_kwargs={'mysql_conn_id': 'mysql_conn_id',
                    'xcom_row_variable': 'pending_row_data'}

    )

    branch_notification = BranchPythonOperator(
        task_id='branch_notification',
        python_callable=response_status_branch_handler,
        provide_context=True,
        op_kwargs={'xcom_status_variable': 'status'}
    )

    request_stuck = PythonOperator(
        task_id='request_stuck',
        python_callable=update_mysql_row,
        provide_context=True,
        op_kwargs={'mysql_conn_id': 'mysql_conn_id',
                    'xcom_row_variable': 'pending_row_data'}
    )

    success_notification = PythonOperator(
        task_id='success_notification',
        python_callable=sendgrid_notify_user_handler,
        provide_context=True,
        op_kwargs={
                    'xcom_row_variable': 'pending_row_data',
                    'status': 'success'}
    )

    failure_notification = PythonOperator(
        task_id='failure_notification',
        python_callable=sendgrid_notify_user_handler,
        provide_context=True,
        op_kwargs={
                    'xcom_row_variable': 'pending_row_data',
                    'status': 'failure'}
    )

    stuck_notification = PythonOperator(
        task_id='stuck_notification',
        python_callable=sendgrid_notify_user_handler,
        provide_context=True,
        op_kwargs={
                    'xcom_row_variable': 'pending_row_data',
                    'status': 'stuck'}
    )



get_request_row_data >> in_process_notification >> cordex_data_search >> wait_for_results >> branch_notification >> [request_stuck, failure_notification, success_notification]
request_stuck >> stuck_notification
