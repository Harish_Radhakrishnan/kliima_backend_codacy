import os
import ast
from airflow import DAG
import datetime
from airflow.operators.python_operator import PythonOperator
from operators.cordex_file_upload_operator import CordexFileUploadOperator
from handlers.truncate_tables import truncate_tables_handler


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime.datetime(2021, 6, 17),
    'retries': 0
}


schedule_interval = None


with DAG('cordex_file_upload', schedule_interval=schedule_interval, catchup=False, default_args=default_args) as dag:

    truncate_db_tables = PythonOperator(
        task_id='truncate_db_table',
        python_callable=truncate_tables_handler,
        provide_context=True,
        op_kwargs={'tables_to_truncate': ['dataApp_cordexs3files', 'dataApp_cordexsearch'],
                   'mysql_conn_id': 'mysql_conn_id'}
    )


    historical_batch_file_upload = CordexFileUploadOperator(
        task_id='historical_batch_file_upload',
        datasearch_api_conn_id='datasearch_api_conn_id',
        experiment_name='historical'
    )

    truncate_db_tables_historical = PythonOperator(
        task_id='truncate_db_tables_historical',
        python_callable=truncate_tables_handler,
        provide_context=True,
        op_kwargs={'tables_to_truncate': ['dataApp_cordexs3files', 'dataApp_cordexsearch'],
                   'mysql_conn_id': 'mysql_conn_id'}
    )

    rcp45_batch_file_upload = CordexFileUploadOperator(
        task_id='rcp45_batch_file_upload',
        datasearch_api_conn_id='datasearch_api_conn_id',
        experiment_name='rcp45'
    )

    truncate_db_tables_45 = PythonOperator(
        task_id='truncate_db_tables_45',
        python_callable=truncate_tables_handler,
        provide_context=True,
        op_kwargs={'tables_to_truncate': ['dataApp_cordexs3files', 'dataApp_cordexsearch'],
                   'mysql_conn_id': 'mysql_conn_id'}
    )

    rcp85_batch_file_upload = CordexFileUploadOperator(
        task_id='rcp85_batch_file_upload',
        datasearch_api_conn_id='datasearch_api_conn_id',
        experiment_name='rcp85'
    )

truncate_db_tables >> historical_batch_file_upload >> truncate_db_tables_historical >> rcp45_batch_file_upload >> truncate_db_tables_45 >> rcp85_batch_file_upload
