from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import shutil
import os
import datetime


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime.datetime(2021, 6, 8),
    'retries': 0
}

def remove_previous_log_files():
    start_date = datetime.date.today() - datetime.timedelta(days=9)
    while(start_date <= datetime.date.today() - datetime.timedelta(days=1)):
        # Remove Scheduler Logs
        try:
            location = '/home/airflow/airflow/logs/scheduler/'
            dir = start_date.strftime('%Y-%m-%d')
            path = os.path.join(location, dir)
            shutil.rmtree(path)
            print("SCHEDULER: {} removed successfully.".format(path))
        except FileNotFoundError:
            print("SCHEDULER - NOT FOUND: {} does not exist.".format(path))
            
        try:
            # Remove DAGs Logs
            logs_root_path = '/home/airflow/airflow/logs/'
            dags_list = os.listdir(logs_root_path)
            for dag_name in dags_list:
                if dag_name.__contains__('dag_processor_manager') or dag_name.__contains__('scheduler') or dag_name.__contains__('free_memory'):
                    continue
                if dag_name.__contains__('cordex_data_extraction') and start_date > datetime.date.today() - datetime.timedelta(days=7):
                    continue
                tasks_list = os.listdir("{}{}/".format(logs_root_path, dag_name))
                for task_name in tasks_list:
                    dates = os.listdir("{}{}/{}/".format(logs_root_path, dag_name, task_name))
                    for date_dir in dates:
                        try:
                            if date_dir.split('T')[0] == dir:
                                path = os.path.join("{}{}/{}/".format(logs_root_path, dag_name, task_name), date_dir)
                                shutil.rmtree(path)
                        except:
                            print("NOT FOUND: {} does not exist.".format(path))
                            continue
                print("DAGs: {} logs removed successfully.".format(dag_name))
           
        except Exception as e:
            print("ERROR: {} ".format(e))
            
        start_date += datetime.timedelta(days=1)
       


schedule_interval = '0 */4 * * *'



with DAG('free_memory', schedule_interval=schedule_interval, catchup=False, default_args=default_args, template_searchpath='/home/airflow/airflow') as dag:
    t1 = PythonOperator(
        task_id='remove_previous_log_files',
        python_callable=remove_previous_log_files
    )



