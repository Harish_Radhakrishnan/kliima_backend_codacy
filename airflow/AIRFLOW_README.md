# kliima-airflow-orchestration
Airflow Documentation to Kliima Orchestration Pipeline

### dataApp_cordexresponsetracker - MySQL Table
* This table is updated by backend Django
* New row is added for each new request made on the UI with status = `pending`
* Each row contains request details and current status

### Email Notification Flow
Queued > In-progress > Success/Failed

## Cordex Search Architecture

1. ### DAGs

    * **delegate_pending_requests**
        * Scheduled for every minute with concurrency = 1
        * DAG responsible to query out oldest record with 'Pending' Status from the mysql table
        * If not found: skips all tasks and finishes successfully
        * If found:
            * Updates status to `queued` and sends out queued notification to the extracted pending records
            * triggers `cordex_data_extraction` using TriggerDAGRunOperator if no other record is currently in-progress else waits for all currently running tasks to finish
        * Can serve as a master DAG i.e. can be used to delegate requests for multiple data sources
    

    * **cordex_data_extraction**
        * DAG responsible to carry out cordex search
        * max_active_dag_runs = 1
        * schedule = None, can only be triggered externally
        * Updates status to `in-progress` and sends out queued notification to the extracted pending records
        * sets a timeout period based on number of coordinates in the request
        * calls cordex search api enpoint '/cordex/analyse' without waiting for response
        * since api is responsible to update status in database table, keeps checking the status of current row in the table every minute for the set timeout period
        * if status changed to success: sends success notification
        * if status changed to failure: sends success failure
        * if status remains unchanged and timeout period exceeds: notify support team (stuck record) and sends out failure notification to the user

2. ### Hooks
    * **custom_email_hook**
        * connects with current email account to send out notifications
    * **custom_mysql_hook**
        * connects with backend mysql database and execute queries
    * **datasearch_hook**
        * connects with backend cordex search api


3. ### Operators
    * **cordex_file_upload_operator**
        * takes care of batch upload for cordex data region wise
    * **cordex_search_operator**
        * calls backend cordex search api for passed request



## Directory Structure

1. ### Directory Structure
```bash
│__ airflow
    ├── dags
    │   ├── cordex_data_extraction.py
    │   ├── cordex_file_upload.py
    │   ├── delegate_pending_requests.py
    │   ├── free_memory.py
    ├── data
    │   ├── sql
    │__ plugins
        ├── handlers
        ├── hooks
        │__ operators
```

