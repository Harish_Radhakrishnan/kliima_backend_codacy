UPDATE dataApp_cordexresponsetracker
SET 
    display_data = %s,
    history_display_data = %s,
    model_used = %s,
    status = %s
WHERE id = %s;

