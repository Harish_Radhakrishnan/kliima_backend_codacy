SELECT
    id, user_name, region_long_name, lat, lon,
    threshold_value_less_than, threshold_value_more_than,
    variable_long_name, 'CORDEX', from_date, to_date, his_from_date, his_to_date,
    project_name, job_code, status
FROM dataApp_cordexresponsetracker 
WHERE status='pending' or status='queued'
ORDER BY current_item_status_date;

