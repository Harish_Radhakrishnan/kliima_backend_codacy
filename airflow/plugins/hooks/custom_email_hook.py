import email_to
from airflow.hooks.base_hook import BaseHook
from airflow.exceptions import AirflowException


class CustomEmailHook(BaseHook):
    def __init__(self, email_conn_id: str):
        connection = self.get_connection(email_conn_id)
        self.server = email_to.EmailServer(connection.host, connection.port, connection.login, connection.password)
        self.log.info("Email Authentication Successful!")

    def send_message(self, subject_text: str, message_text: str, to_email: str):
        message = self.server.message()
        message.add(message_text)
        message.send(to_email, subject_text)
        self.log.info("Email Successfully Sent to: {}".format(to_email))

