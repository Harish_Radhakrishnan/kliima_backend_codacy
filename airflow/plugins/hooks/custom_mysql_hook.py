import ast
import json
import mysql.connector
from airflow.hooks.base_hook import BaseHook
from airflow.exceptions import AirflowException


class CustomMysqlHook(BaseHook):
    def __init__(self, mysql_conn_id: str):
        connection = self.get_connection(mysql_conn_id)
        self.conn = mysql.connector.connect(
            host=connection.host,
            user=connection.login,
            password=connection.password,
            database=connection.schema
            )
        self.cursor = self.conn.cursor()
        self.log.info("Successfully connected to MySQL Database {} as {}".format(connection.schema, connection.login))


    def execute_query(self, query: str, values=None):
        if values:
            self.cursor.execute(query, values)
        else:
            self.cursor.execute(query)
        self.conn.commit()
        self.log.info("Query Executed Successfully: {}".format(query))
    

    def get_row(self, query: str):
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

    def close_connection(self):
        self.cursor.close()
        self.conn.close()

