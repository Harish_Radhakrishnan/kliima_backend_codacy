import json
import requests
from airflow.hooks.base_hook import BaseHook
from airflow.exceptions import AirflowException


class DatasearchHook(BaseHook):
    def __init__(self, datasearch_api_conn_id: str):
        self.datasearch_api_conn_id = datasearch_api_conn_id
        self.connection = self.get_connection(datasearch_api_conn_id)
        auth_body = {
                        "email" : self.connection.login,
                        "password" : self.connection.password
                    }
        auth_url = self.connection.host + "/auth/login"
        auth_response = requests.request("POST", auth_url, data=auth_body)
        if auth_response.status_code == 200:
            self.token = auth_response.json()["token"]
            self.log.info("Datasearch API Login Successful")
        else:
            raise AirflowException("Error creating a token while logging in!\nStatus Code: {}\nResponse: {}".format(auth_response.status_code, auth_response.text))


    def set_token(self):
        self.connection = self.get_connection(self.datasearch_api_conn_id)
        auth_body = {
                        "email" : self.connection.login,
                        "password" : self.connection.password
                    }
        auth_url = self.connection.host + "/auth/login"
        auth_response = requests.request("POST", auth_url, data=auth_body)
        if auth_response.status_code == 200:
            self.token = auth_response.json()["token"]

    def get_token(self):
        return self.token

    def extract_data(self, request_details: dict):
        extract_url = self.connection.host + "/cordex/analyse"
        request_details = json.dumps(request_details)
        headers = {
        'Authorization': "Token {}".format(self.token),
        "Content-Type": "application/json"
        }
        print("Dumped: ", request_details)
        try:
            requests.request("GET", extract_url, headers=headers, data=request_details, timeout=30.0)
        except requests.exceptions.ReadTimeout:
            pass

