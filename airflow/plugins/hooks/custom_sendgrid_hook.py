import sendgrid
from sendgrid.helpers.mail import Mail, Email, To, Content
from airflow.hooks.base_hook import BaseHook
from airflow.exceptions import AirflowException


class CustomSendgridHook(BaseHook):
    def __init__(self, api_key: str, from_email: str):
        self.sg = sendgrid.SendGridAPIClient(api_key=api_key)
        self.from_email = from_email
        
    def send_message(self, subject_text: str, message_text: str, to_email: str):
        from_email = Email(self.from_email)
        to_email = To(to_email)
        subject = subject_text
        content = Content("text/plain", message_text)
        mail = Mail(from_email, to_email, subject, content)
        response = self.sg.client.mail.send.post(request_body=mail.get())
        self.log.info("Email Successfully Sent to: {} | {}".format(to_email, response.status_code))
