import json
import boto3
from hooks.custom_sendgrid_hook import CustomSendgridHook

def get_aws_secret(secret, key):
    secret_name = secret
    region_name = "eu-west-2"
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )
    get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    api_key = json.loads(get_secret_value_response['SecretString']).get(key)
    return api_key

def sendgrid_notify_user_handler(xcom_row_variable, status, **context):
    task_instance = context['task_instance']
    api_key = get_aws_secret('SendGrid', 'api_key')
    email_conn = CustomSendgridHook(api_key, 'noreplykliima@arup.com')
    pending_row_data = task_instance.xcom_pull(key=xcom_row_variable)
    request_details = pending_row_data.get("request_details")
    message_text = "Hi,\n\n"

    if status == 'queued':
        subject_text = "Kliima - Task Queued: {}".format(request_details.get("primary_key"))
        message_text += "Thank you for using Kliima! Your data request is in queue, once the process starts we'll let you know!\n"
        message_text += "Summary\n"
        message_text += "Project Name: {}\n".format(request_details.get("project_name"))
        message_text += "Job Number: {}\n".format(request_details.get("job_code"))
        message_text += "Datasource: {}\n".format(request_details.get("datasource"))
        message_text += "Climate Variable: {}\n".format(request_details.get("user_var_"))
        message_text += "Historical Period: {}-{}\n".format(request_details.get("history_from"), request_details.get("history_to"))
        message_text += "Future Period: {}-{}\n".format(request_details.get("future_from"), request_details.get("future_to"))
        message_text += "Locations:\n"

        latitudes = list(request_details.get("lat"))
        longitudes = list(request_details.get("lon"))
        for index, latitude in enumerate(latitudes):
            message_text += "\t{}/{}\n".format(round(latitude, 2), round(longitudes[index], 2))
        message_text += "\n"

    if status == 'in-process':
        subject_text = "Kliima - Task In Progress: {}".format(request_details.get("primary_key"))
        message_text += "Thank you for using Kliima! Your data has been requested, once it is ready we'll let you know!\n"
        message_text += "Summary\n"
        message_text += "Project Name: {}\n".format(request_details.get("project_name"))
        message_text += "Job Number: {}\n\n".format(request_details.get("job_code"))
        message_text += "Datasource: {}\n".format(request_details.get("datasource"))
        message_text += "Climate Variable: {}\n".format(request_details.get("user_var_"))
        message_text += "Historical Period: {}-{}\n".format(request_details.get("history_from"), request_details.get("history_to"))
        message_text += "Future Period: {}-{}\n".format(request_details.get("future_from"), request_details.get("future_to"))
        message_text += "Locations:\n"

        latitudes = list(request_details.get("lat"))
        longitudes = list(request_details.get("lon"))
        for index, latitude in enumerate(latitudes):
            message_text += "\t{}/{}\n".format(round(latitude, 2), round(longitudes[index], 2))
        message_text += "\n"
        
    elif status == 'success':
        subject_text = "Kliima - Task Success: {}".format(request_details.get("primary_key"))
        message_text += 'Your data has now been processed and your outputs are ready. Click <a href="https://kliima.arup.com"> here </a> to log back into Kliima!\nhttps://kliima.arup.com\n\n'
    
    elif status == 'failure' or status == 'stuck':
        subject_text = "Kliima - Task Failed: {}".format(request_details.get("primary_key"))
        message_text += "Unfortunately, your request has been failed. Kindly try again after some time.\n\n"
        

    message_text += "\nThank You!"
    email_conn.send_message(subject_text, message_text, pending_row_data.get("user_name"))
    if status == 'stuck':
        email_conn.send_message('CRITICAL: Kliima Server Stuck - {}'.format(request_details.get("primary_key")), 'Hi, \nServer is stuck hence no response recorded for id={}\nKindly do the needful.'.format(request_details.get("primary_key")), 'karthik.p@digiryte.com')
        email_conn.send_message('CRITICAL: Kliima Server Stuck - {}'.format(request_details.get("primary_key")), 'Hi, \nServer is stuck hence no response recorded for id={}\nKindly do the needful.'.format(request_details.get("primary_key")), 'samrat@digiryte.com')
        email_conn.send_message('CRITICAL: Kliima Server Stuck - {}'.format(request_details.get("primary_key")), 'Hi, \nServer is stuck hence no response recorded for id={}\nKindly do the needful.'.format(request_details.get("primary_key")), 'zaheen@digiryte.com')

