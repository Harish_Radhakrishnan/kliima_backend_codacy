from hooks.custom_email_hook import CustomEmailHook


def notify_user_handler(email_conn_id, xcom_row_variable, status, **context):
    task_instance = context['task_instance']
    email_conn = CustomEmailHook(email_conn_id)
    pending_row_data = task_instance.xcom_pull(key=xcom_row_variable)
    request_details = pending_row_data.get("request_details")
    message_text = "Hi,\n\n\n\n"

    if status == 'queued':
        subject_text = "Kliima - Task Queued: {}".format(request_details.get("primary_key"))
        message_text += "Thank you for using Kliima! Your data request is in queue, once the process starts we'll let you know!\n\n"
        message_text += "Summary\n\n"
        message_text += "Project Name: {}\n\n".format(request_details.get("project_name"))
        message_text += "Job Number: {}\n\n\n\n".format(request_details.get("job_code"))
        message_text += "Datasource: {}\n\n".format(request_details.get("datasource"))
        message_text += "Climate Variable: {}\n\n".format(request_details.get("user_var_"))
        message_text += "Historical Period: {}-{}\n\n".format(request_details.get("history_from"), request_details.get("history_to"))
        message_text += "Future Period: {}-{}\n\n".format(request_details.get("future_from"), request_details.get("future_to"))
        message_text += "Locations:\n\n"

        latitudes = list(request_details.get("lat"))
        longitudes = list(request_details.get("lon"))
        for index, latitude in enumerate(latitudes):
            message_text += "\t{}/{}\n\n".format(round(latitude, 2), round(longitudes[index], 2))
        message_text += "\n\n"

    if status == 'in-process':
        subject_text = "Kliima - Task In Progress: {}".format(request_details.get("primary_key"))
        message_text += "Thank you for using Kliima! Your data has been requested, once it is ready we'll let you know!\n\n"
        message_text += "Summary\n\n"
        message_text += "Project Name: {}\n\n".format(request_details.get("project_name"))
        message_text += "Job Number: {}\n\n\n\n".format(request_details.get("job_code"))
        message_text += "Datasource: {}\n\n".format(request_details.get("datasource"))
        message_text += "Climate Variable: {}\n\n".format(request_details.get("user_var_"))
        message_text += "Historical Period: {}-{}\n\n".format(request_details.get("history_from"), request_details.get("history_to"))
        message_text += "Future Period: {}-{}\n\n".format(request_details.get("future_from"), request_details.get("future_to"))
        message_text += "Locations:\n\n"

        latitudes = list(request_details.get("lat"))
        longitudes = list(request_details.get("lon"))
        for index, latitude in enumerate(latitudes):
            message_text += "\t{}/{}\n\n".format(round(latitude, 2), round(longitudes[index], 2))
        message_text += "\n\n"
        
    elif status == 'success':
        subject_text = "Kliima - Task Success: {}".format(request_details.get("primary_key"))
        message_text += 'Your data has now been processed and your outputs are ready. Click <a href="https://kliima.arup.com"> here </a> to log back into Kliima!\n\nhttps://kliima.arup.com\n\n\n\n'
    
    elif status == 'failure' or status == 'stuck':
        subject_text = "Kliima - Task Failed: {}".format(request_details.get("primary_key"))
        message_text += "Unfortunately, your request has been failed. Kindly try again after some time.\n\n\n\n"
        

    message_text += "\n\nThank You!"
    email_conn.send_message(subject_text, message_text, pending_row_data.get("user_name"))
    if status == 'stuck':
        email_conn.send_message('CRITICAL: Kliima Server Stuck - {}'.format(request_details.get("primary_key")), 'Hi, \nServer is stuck hence no response recorded for id={}\nKindly do the needful.'.format(request_details.get("primary_key")), 'karthik.p@digiryte.com')
        email_conn.send_message('CRITICAL: Kliima Server Stuck - {}'.format(request_details.get("primary_key")), 'Hi, \nServer is stuck hence no response recorded for id={}\nKindly do the needful.'.format(request_details.get("primary_key")), 'samrat@digiryte.com')
        email_conn.send_message('CRITICAL: Kliima Server Stuck - {}'.format(request_details.get("primary_key")), 'Hi, \nServer is stuck hence no response recorded for id={}\nKindly do the needful.'.format(request_details.get("primary_key")), 'zaheen@digiryte.com')

