import logging

logging.getLogger().setLevel(logging.INFO)

def status_branch_handler(record_id, **context):
    task_instance = context['task_instance']
    record_status = task_instance.xcom_pull(key="record_status_{}".format(record_id))
    logging.info("Record Status: {}".format(record_status))
    if record_status == 'completed':
        return 'mark_completed_{}'.format(record_id)
    elif record_status == 'failed':
        return 'mark_failed_{}'.format(record_id)
