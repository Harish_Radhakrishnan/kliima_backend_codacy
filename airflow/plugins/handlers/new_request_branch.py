from hooks.custom_mysql_hook import CustomMysqlHook



def new_request_branch_handler(mysql_conn_id, xcom_row_variable, **context):
    task_instance = context['task_instance']
    mysql_conn = CustomMysqlHook(mysql_conn_id)
    in_process_count = int(mysql_conn.get_row("SELECT COUNT(*) FROM dataApp_cordexresponsetracker WHERE status='in-process';")[0])
    request_row = task_instance.xcom_pull(key=xcom_row_variable)
    pending_exists = request_row.get("pending_exists")
    status = request_row.get("status")
    request_id = request_row.get("request_id")
    print("{} | {} | {} | {}".format(pending_exists, status, request_id, in_process_count))
    if not pending_exists:
        return "no_pending_requests"
    else:
        if status == 'pending' and in_process_count != 0:
            print("UPDATE dataApp_cordexresponsetracker SET status = 'queued' WHERE id={}:".format(request_id))
            mysql_conn.execute_query("UPDATE dataApp_cordexresponsetracker SET status = 'queued' WHERE id={};".format(request_id))
            return "queued_notification"
        elif status == 'pending' and in_process_count == 0:
            return "update_request_row_status"
        elif status == 'queued' and in_process_count == 0:
            return "update_request_row_status"
        elif status == 'queued' and in_process_count != 0:
            return "no_pending_requests"
