from hooks.custom_mysql_hook import CustomMysqlHook


def update_request_row_status_handler(xcom_row_variable, update_query, mysql_conn_id, **context):
    task_instance = context['task_instance']
    pending_row_data = task_instance.xcom_pull(key=xcom_row_variable)
    update_query = update_query.replace('__request_id__', str(pending_row_data.get("request_id")))
    mysql_conn = CustomMysqlHook(mysql_conn_id)
    mysql_conn.execute_query(update_query)

