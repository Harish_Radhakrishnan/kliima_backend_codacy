

def response_status_branch_handler(xcom_status_variable, **context):
    task_instance = context['task_instance']
    response_status = task_instance.xcom_pull(key=xcom_status_variable)
    print(response_status)
    if response_status == 'success':
        return "success_notification"
    elif response_status == 'failed':
        return "failure_notification"
    elif response_status == 'stuck':
        return "request_stuck"
    
    

