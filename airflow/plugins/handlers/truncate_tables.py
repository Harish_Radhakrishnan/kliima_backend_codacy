from hooks.custom_mysql_hook import CustomMysqlHook


def truncate_tables_handler(tables_to_truncate, mysql_conn_id, **context):
    mysql_conn = CustomMysqlHook(mysql_conn_id)
    truncate_query = """ TRUNCATE TABLE {};"""
    for table_to_truncate in tables_to_truncate:
        mysql_conn.execute_query(truncate_query.format(table_to_truncate))
        print("Table: {} truncated successfully!\n".format(table_to_truncate))
