import ast
from hooks.custom_mysql_hook import CustomMysqlHook



def get_request_row_handler(select_query, mysql_conn_id, xcom_row_variable, **context):
    task_instance = context['task_instance']
    mysql_conn = CustomMysqlHook(mysql_conn_id)
    pending_row_tuple = mysql_conn.get_row(select_query)
    
    if pending_row_tuple:
        request_details = dict()
        request_details.__setitem__("primary_key", int(pending_row_tuple[0]))
        request_details.__setitem__("user_region", pending_row_tuple[2])
        request_details.__setitem__("user_var", ast.literal_eval(pending_row_tuple[7]))
        request_details.__setitem__("future_from", int(pending_row_tuple[9]))
        request_details.__setitem__("future_to", int(pending_row_tuple[10]))
        request_details.__setitem__("history_from", int(pending_row_tuple[11]))
        request_details.__setitem__("history_to", int(pending_row_tuple[12]))
        request_details.__setitem__("lat", ast.literal_eval(pending_row_tuple[3]))
        request_details.__setitem__("lon", ast.literal_eval(pending_row_tuple[4]))
        request_details.__setitem__("threshold_value_less_than", float(pending_row_tuple[5]))
        request_details.__setitem__("threshold_value_more_than", float(pending_row_tuple[6]))
        request_details.__setitem__("datasource", pending_row_tuple[8])
        request_details.__setitem__("project_name", pending_row_tuple[13])
        request_details.__setitem__("job_code", pending_row_tuple[14])
        pending_row_data = dict()
        pending_row_data.__setitem__("status", pending_row_tuple[15])
        pending_row_data.__setitem__("pending_exists", True)
        pending_row_data.__setitem__("num_coordinates", len(ast.literal_eval(pending_row_tuple[3])))
        pending_row_data.__setitem__("request_id", pending_row_tuple[0])
        pending_row_data.__setitem__("user_name", pending_row_tuple[1])
        pending_row_data.__setitem__("request_details", request_details)
    else:
        print("No pending requests")
        pending_row_data = dict()
        pending_row_data.__setitem__("pending_exists", False)

    task_instance.xcom_push(key=xcom_row_variable,value=pending_row_data)





