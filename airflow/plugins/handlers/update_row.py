import json
from hooks.custom_mysql_hook import CustomMysqlHook


def update_row_handler(update_row_query, mysql_conn_id, **context):
    task_instance = context['task_instance']
    mysql_conn = CustomMysqlHook(mysql_conn_id)
    request_id = task_instance.xcom_pull(key="pending_row_data").get("request_id")
    display_data = task_instance.xcom_pull(key="display_data")
    history_display_data = task_instance.xcom_pull(key="history_display_data")
    status = task_instance.xcom_pull(key="status")
    model_used = task_instance.xcom_pull(key="model_used")
    
    update_values = (json.dumps(display_data), json.dumps(history_display_data), json.dumps(model_used), status, request_id)
    print("Update Query: {}".format(update_row_query))
    print("Updated Values: {}".format(update_values))
    mysql_conn = CustomMysqlHook(mysql_conn_id)
    mysql_conn.execute_query(update_row_query, update_values)
    print("Row updated successfully!")
    

