from airflow.models.baseoperator import BaseOperator
from airflow.utils.decorators import apply_defaults
from hooks.custom_mysql_hook import CustomMysqlHook
# from hooks.cordex_search_hook import CordexSearchHook
from hooks.datasearch_hook import DatasearchHook


class CordexSearchOperator(BaseOperator):

    def __init__(self,
                 mysql_conn_id,
                 datasearch_api_conn_id,
                 xcom_row_variable,
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)

        self.mysql_conn_id = mysql_conn_id
        self.datasearch_api_conn_id = datasearch_api_conn_id
        self.xcom_row_variable = xcom_row_variable
        

    def execute(self, context):
        task_instance = context['task_instance']
        pending_row_data = task_instance.xcom_pull(key=self.xcom_row_variable)
        request_details = pending_row_data.get("request_details")
        request_body = {key: val for key, val in request_details.items() if key not in ["datasource", "project_name", "job_code"]}
        cordex_search_client = DatasearchHook(self.datasearch_api_conn_id)
        cordex_search_client.extract_data(request_body)
    
