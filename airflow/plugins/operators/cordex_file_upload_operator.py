import requests
import json
from airflow.models.baseoperator import BaseOperator
from hooks.datasearch_hook import DatasearchHook


class CordexFileUploadOperator(BaseOperator):

    def __init__(self,
                 datasearch_api_conn_id,
                 experiment_name,
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)

        self.datasearch_api_conn_id = datasearch_api_conn_id
        self.experiment_name = experiment_name
        

    def execute(self, context):
        api_client = DatasearchHook(self.datasearch_api_conn_id)
        user_token = api_client.get_token()

        status_of_upload={}
        file_upload_status=[]
        domain = "https://18.135.149.141/"
        headers = {
                    'Content-Type': 'application/json',
                    'Authorization':'Token {}'.format(user_token)
                    }

        region=[  "CAS-22", "CAS-44", "EAS-22", "EAS-44", "SEA-22", "WAS-22", "WAS-44"]

        variable=["tasmax","tas","tasmin","pr"]

        if self.experiment_name !="historical":
            for region_list in region:
                for variable_list in variable:
                    for batch in range(2006,2100,5):
                        cordex_parameters={"region":{
                            "name":region_list,
                            },
                            "variable":[variable_list],
                            "datasource":"CORDEX",
                            "from_date":batch,
                            "to_date":batch+4,
                            "experiment":self.experiment_name
                        }
                        data = json.dumps(cordex_parameters)
                        print(" {} | {} | {} ".format(domain+"/batch-file-upload", data, headers))
                        batch_upload = requests.post(domain+"/batch-file-upload",data=data,headers = headers)
                        print(batch_upload.status_code, batch_upload.text)
                        file_upload_status.append(batch_upload)
                        print(batch,batch+5)

        else:
            for region_list in region:
                for variable_list in variable:
                    for batch in range(1976,2005,5):
                        cordex_parameters={"region":{
                            "name":region_list,
                            },
                            "variable":[variable_list],
                            "datasource":"CORDEX",
                            "from_date":batch,
                            "to_date":batch+4,
                            "experiment":self.experiment_name
                        }
                        data = json.dumps(cordex_parameters)
                        print(" {} | {} | {} ".format(domain+"/batch-file-upload", data, headers))
                        batch_upload = requests.post(domain+"/batch-file-upload",data=data,headers = headers)
                        print(batch_upload.status_code, batch_upload.text)
                        file_upload_status.append(batch_upload)
                        print(batch,batch+5)
                        
        status_of_upload["status_of_upload"]=file_upload_status
        message={"status":status_of_upload}
        self.log.info("Message: {}".format(message))

