from django.db import models

# Create your models here.

class CordexWgetData(models.Model):
    """ 
    Cordex Wget Data Model
    """
    project = models.JSONField(null = True,default=None)
    product = models.JSONField(null = True,default=None)
    domain = models.JSONField(null = True,default=None)
    institute = models.JSONField(null = True,default=None)
    driving_model = models.JSONField(null = True,default=None)
    experiment = models.JSONField(null = True,default=None)
    experiment_family = models.JSONField(null = True,default=None)
    ensemble = models.JSONField(null = True,default=None)
    rcm_model = models.JSONField(null = True,default=None)
    rcm_version = models.JSONField(null = True,default=None)
    time_frequency = models.JSONField(null = True,default=None)
    variable = models.JSONField(null = True,default=None)
    variable_long_name = models.JSONField(null = True,default=None)
    cf_standard_name = models.JSONField(null = True,default=None)
    datanode = models.JSONField(null = True,default=None)
    from_date = models.IntegerField(null = True,default=None)
    to_date = models.IntegerField(null = True,default=None)
    file_name = models.CharField(null = True,default=None,max_length=1000)
    file_url = models.CharField(null = True,default=None,max_length=1000)
    en_hash_fn = models.CharField(null = True,default=None,max_length=1000)
    token = models.CharField(null = True,default=None,max_length=1000)
    search_url = models.CharField(null = True,default=None,max_length=1000)

class CordexParametersList(models.Model):
    """ 
    Cordex Parameters Model
    """
    project = models.JSONField(null = True,default=None)
    product = models.JSONField(null = True,default=None)
    domain = models.JSONField(null = True,default=None)
    institute = models.JSONField(null = True,default=None)
    driving_model = models.JSONField(null = True,default=None)
    experiment = models.JSONField(null = True,default=None)
    experiment_family = models.JSONField(null = True,default=None)
    ensemble = models.JSONField(null = True,default=None)
    rcm_model = models.JSONField(null = True,default=None)
    rcm_version = models.JSONField(null = True,default=None)
    time_frequency = models.JSONField(null = True,default=None)
    variable = models.JSONField(null = True,default=None)
    variable_long_name = models.JSONField(null = True,default=None)
    cf_standard_name = models.JSONField(null = True,default=None)
    datanode = models.JSONField(null = True,default=None)

class CordexSearch(models.Model):
    """ 
    Cordex Search Model
    """
    region = models.CharField(default=None,null=True,max_length=20)
    variable = models.JSONField(null=True,default=None)
    from_date = models.IntegerField(null = True,default=None)
    to_date = models.IntegerField(null = True,default=None)
    records = models.BooleanField(default=None,null=True)
    file_download_status = models.BooleanField(default=0,null=True)
    file_upload_status = models.BooleanField(default=0,null=True)


class CordexResponseTracker(models.Model):
    """ 
    Cordex Response Model
    """
    user_name = models.CharField(default=None,null=True,max_length=50)
    project_name = models.CharField(default=None,null=True,max_length=100)
    job_code = models.CharField(default=None,null=True,max_length=100)
    datasource = models.CharField(default=None,null=True,max_length=100)
    region_long_name = models.CharField(default=None,null=True,max_length=100)
    region = models.CharField(default=None,null=True,max_length=20)
    lat = models.JSONField(null=True,default=None)
    lon = models.JSONField(null=True,default=None) 
    country = models.CharField(default=None,null=True,max_length=40)
    main_variable = models.CharField(default=None,null=True,max_length=100)
    variable_long_name = models.JSONField(null=True,default=None)
    variable = models.JSONField(null=True,default=None)
    to_date = models.IntegerField(null = True,default=None)
    from_date=models.IntegerField(null = True,default=None)
    his_from_date=models.IntegerField(null = True,default=None)
    his_to_date=models.IntegerField(null = True,default=None)
    threshold_value_less_than=models.FloatField(null = True,default=None)
    threshold_value_more_than=models.FloatField(null = True,default=None)
    history_display_data= models.JSONField(null=True,default=None)
    display_data= models.JSONField(null=True,default=None)
    model_used= models.JSONField(null=True,default=None)
    text_display_data= models.JSONField(null=True,default=None)
    current_item_status_date=models.DateTimeField(null=True,default=None)
    status= models.CharField(default=None,null=True,max_length=50)
    gcmDrivingModel = models.CharField(default=None,null=True,max_length=100)
    institution = models.CharField(default=None,null=True,max_length=100)
    rcmModel = models.CharField(default=None,null=True,max_length=100)
    region_short = models.CharField(default=None,null=True,max_length=100)
def geo_file_user_path(instance, filename):
    """ 
    geo json file name
    """
    return 'geo_files/user_{0}_{1}'.format(instance.token, filename)


class GeoFileUpload(models.Model):
    """ 
    Geo json file Model
    """
    token = models.CharField(default=None,null=True,max_length=100)
    geo_file = models.FileField(upload_to=geo_file_user_path)

class CordexS3Files(models.Model):
    """ 
    Cordex S3 Files Model
    """
    region = models.CharField(default=None,null=True,max_length=20)
    variable = models.JSONField(null=True,default=None)
    start_year = models.IntegerField(null=True,default=None)
    end_year = models.IntegerField(null=True,default=None)
    s3_upload = models.BooleanField(default=0,null=True)
    
class requestTracker(models.Model):
    """ 
    request tracker model
    """
    user_name = models.CharField(default=None,null=True,max_length=50)
    tracker_id = models.IntegerField(null=True,default=None)
    datasource = models.CharField(default=None,null=True,max_length=100)
    current_item_status_date=models.DateTimeField(null=True,default=None)
    status = models.CharField(default=None,null=True,max_length=50)



    
