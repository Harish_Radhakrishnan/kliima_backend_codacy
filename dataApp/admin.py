from django.contrib import admin
from .models import CordexWgetData,CordexParametersList,\
                    CordexSearch,GeoFileUpload,\
                    CordexS3Files,CordexResponseTracker,\
                    requestTracker

# Register your models here.
admin.site.register(CordexWgetData)
admin.site.register(CordexParametersList)
admin.site.register(CordexSearch)
admin.site.register(GeoFileUpload)
admin.site.register(CordexS3Files)
admin.site.register(CordexResponseTracker)
admin.site.register(requestTracker)
