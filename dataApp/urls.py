from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("batch-file-upload",
         views.dataset_search,
         name="batch_file_upload"),
    path("dataset/cordex/add/parameters/auto",
         views.cordex_add_parameters_list_auto,
         name="cordex_add_parameters_list_auto"),
    path("dataset/cordex/add/parameters",
         views.cordex_add_parameters_list,
         name="cordex_add_parameters_list"),
    path("dataset/cordex/get/parameters",
         views.cordex_get_parameters_list,
         name="cordex_get_parameters_list"),
    path("dataset/cordex/search",
         views.cordex_search,name="cordex_search"),
    path("dataset/cordex/add",
         views.cordex_add,name="cordex_add"),
    path("dataset/cordex/list",
         views.cordex_list,name="cordex_list"),
    # path(".well-known/pki-validation/F5F68EE95B1213B575A47B2946B1F5ED.txt",
    #      views.zero_ssl,name='zero_ssl'),
    path("geodata.geojson",views.geo_file,name="geo_file"),
    path("history",views.history_detail,name="history"),
    path("table-detail",views.table_detail,name="table-detail"),
    path("submit-request",views.request_details,name="submit-request"),
    path("model/selection",views.models_used,name="model_used"),
    path("cordex/analyse",
         views.cordex_exist_approch,name="cordex_exist_approch"),
    path("files/csv-gis",views.csv_gis_files,name="csv-gis-files"),
    path("files/csv-gis/resolutions",
         views.csv_gis_files_dropdown,name="csv_gis_files_dropdown")
]

if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
