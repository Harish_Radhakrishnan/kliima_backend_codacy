from boto3.s3.transfer import S3Transfer
import boto3
from django.conf import settings
import os
import subprocess

class CordexFileUpload():
    """ 
    Upload file to the s3 from server/(local)
    machine
    """
    def __init__(self):
        self.path = os.path.join(settings.MEDIA_ROOT,
                                 'wget_for_download/')

    def run(self):
        """ 
        running the pipeline
        """
        try:
            list_of_file=[]
            os.chdir(self.path)
            list_files = os.listdir()
            for file_name in list_files:
                if file_name.endswith(".nc"):
                    list_of_file.append(file_name)
            # for files in list_of_file:
            #     stats = os.stat(files)
            #     if stats.st_size == 0:
            #         pass
                    # for nc_files in list_files:
                    #     subprocess.run(['rm',nc_files])
                    # os.chdir(settings.BASE_DIR)
                    #raise ValueError("break for 0 byte files")
            for files in list_of_file:
               # file_path=files.replace("_","/")
              #  file_path=file_path.replace(".nc","")
                stats = os.stat(files)
                file_split=files.split("_")
                region_split=file_split[1]
                region_split=region_split[0:3]
                file_path=region_split +"/"+file_split[0]
                client = boto3.client('s3')
                transfer = S3Transfer(client)
                transfer.upload_file(self.path+files, "klima-datasets","Cordex"+"/"+ file_path+"/"+files)
            message = {"message":"File are uploaded successfully"}
            for nc_files in list_files:
                subprocess.run(['rm',nc_files])
            os.chdir(settings.BASE_DIR)
        except:
            message = {"message":"File upload failed"}
            os.chdir(settings.BASE_DIR)
        return message
