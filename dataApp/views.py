import requests
import json
import os
from email import message
from django.shortcuts import render
from .serializers import CordexWgetDataSerializer,\
                        CordexParametersListSerializer,\
                        CordexSearchSerializer,GeoFileUploadSerializer,\
                        CordexS3FilesSerializer,\
                        CordexResponseTrackerSerializer,\
                        requestTrackerSerializer
from .models import CordexWgetData,CordexParametersList,\
                    CordexSearch,GeoFileUpload,\
                    CordexS3Files,CordexResponseTracker
from .utils import RequestUtils, CsvDownload
from rest_framework.decorators import api_view,permission_classes
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny,IsAuthenticated
from rest_framework import status
from .data import GlobalSearch
from .cordex_data import CordexWgetScript,GetParametersList
from .file_download import CordexFileDownload
from .file_upload import CordexFileUpload
from . geo_file import GeoJsonFile
from . search import CordexNcSearch, CordexNcSearchs
from django.conf import settings
from django.http import FileResponse
from django.core.files.base import ContentFile
from django.db.models import Count, Max
import datetime
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.core.serializers import serialize
import pandas as pd
from io import StringIO 
import boto3
from .main_cordex_search import MainCordexSearch
from .models_selections import ModelSelect
from .s3_hit_download import download_files_from_s3,run_master
from .climateprojections.tool_master_cloud_test import run_tool_master
import sys
from .utils import GetHistory, TableDetails 
from arup.utils import LogUtils
from dataApp import logger

# Create your views here.

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def dataset_search(request):
    """ 
    POST Method: data download api
    """    
    region=request.data["region"]["name"]
    variable=request.data["variable"]
    from_date = request.data["from_date"]
    to_date = request.data["to_date"]
    try:
        obj_ = CordexSearch.objects.get(region=region,variable=variable)
        search_flag = True
        script = GlobalSearch(request.data,
                              request.META["HTTP_AUTHORIZATION"],search_flag)
        url_status,data = script.run()
        if obj_.records == False:
            message = {"upload_status":url_status["message"],
                       "records":"No records found for this parameters"}
        message = {"upload_status":url_status["message"],
                   "records":"records are available"}
        serializer = CordexSearchSerializer(obj_)
    except CordexSearch.DoesNotExist:
        search_flag = False
        script = GlobalSearch(request.data,
                              request.META["HTTP_AUTHORIZATION"],search_flag)
        url_status,data = script.run()
        if url_status['message'] == "No files were found that matched the query":
            records = False
            message = {"upload_status":url_status["message"],"records":"No records found for this parameters"}
            return Response(message,status = status.HTTP_200_OK)
        records = True
        message = {"upload_status":url_status["message"],
                   "records":"records are available"}
        serializer_data = {"region":region,
                           "variable":variable,
                           "records":records,
                           "from_date":from_date,
                           "to_date":to_date}
        serializer = CordexSearchSerializer(data =serializer_data)
        if serializer.is_valid():
            serializer.save()
    obj_ = CordexSearch.objects.get(region=region,variable=variable)
    try:
        cordex_s3_obj = CordexS3Files.objects.get(region=region,variable = variable,start_year=from_date,end_year=to_date)
        message["s3_status"] = "Files are already available in s3"
    except CordexS3Files.DoesNotExist:
        cordex_s3_obj,_ = CordexS3Files.objects.get_or_create(region=region,variable = variable,start_year=from_date,end_year=to_date)
        message["s3_status"] = "Files are uploaded in s3"
    upload = False
    if data != None and cordex_s3_obj.s3_upload == False:
        if obj_.file_download_status == False or (obj_.from_date != from_date or obj_.to_date != to_date):
            message["file_download_status"]="Files download failed try again"
            script = CordexFileDownload(data)
            file_download_status = script.run()
            if file_download_status["message"] == "Files are downloaded successfully":
                obj_.file_download_status = True
                obj_.from_date = from_date
                obj_.to_date = to_date
                obj_.save()
                upload = True            
                message["file_download_status"]=file_download_status["message"]
    print(data)
    #zero_bytes = True
    if obj_.file_upload_status == False or upload == True:
        script = CordexFileUpload()
        file_upload_status = script.run()
        message["file_upload_status"]=file_upload_status["message"]
        if file_upload_status['message'] == "File are uploaded successfully":
            obj_.file_upload_status =True
            obj_.save()
            cordex_s3_obj.s3_upload = True
            cordex_s3_obj.save()
            message["file_upload_status"]=file_upload_status["message"]
            #zero_bytes = False
    print(data)
    nc_files = []
    for files in data:
        nc_files.append(files["file_name"])
    print(nc_files)
    message['variable'] = variable
    message['region'] = region
    #message['zero_bytes'] = zero_bytes
    return Response(message,status = status.HTTP_200_OK)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def cordex_analyse(request):
    """ 
    POST Method: cordex search api
    """
    user_region_=request.data["user_region"]
    user_var_=request.data["user_var"]
    history_from=request.data["history_from"]
    history_to=request.data["history_to"]
    future_from=request.data["future_from"]
    future_to=request.data["future_to"]
    lat=request.data["lat"]
    lon=request.data["lon"]
    primary_key=request.data["primary_key"]
    threshold_value_less_than=request.data["threshold_value_less_than"]
    threshold_value_more_than=request.data["threshold_value_more_than"]
    cordex = MainCordexSearch(user_region_,user_var_,
                              history_from,history_to,future_from,
                              future_to,primary_key,threshold_value_less_than,
                              threshold_value_more_than,lat,lon)
    display_data,final_result,model_used=cordex.searchs()
    data = {"display_data":display_data,
            "final_result":final_result,
            "model_used_result":model_used}
    return Response(data,status = status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def cordex_exist_approch(request):
    """ 
    GET Method: cordex search api using
    existing approch
    """
    try:
        request.log_utils = LogUtils.load_dict(request.data['primary_key'])
        logger.info(LogUtils.\
            load_message(**request.log_utils, message="started"))
        user_region_=request.data["user_region"]
        user_var_=request.data["user_var"]
        history_from=request.data["history_from"]
        history_to=request.data["history_to"]
        future_from=request.data["future_from"]
        future_to=request.data["future_to"]
        lat=request.data["lat"]
        lon=request.data["lon"]
        primary_key=request.data["primary_key"]
        threshold_value_less_than=request.data["threshold_value_less_than"]
        threshold_value_more_than=request.data["threshold_value_more_than"]
        #Model, institute and rcm based filters
        model_based_filter = False
        institute_based_filter = False
        rcm_based_filter = False
        region_short_filter =False
        for key in request.data.keys():
            if key == "gcmDrivingModel":
                model_based_filter = True
            elif key == "institution":
                institute_based_filter = True
            elif key == 'rcmModel':
                rcm_based_filter = True
            elif key == 'region_short':
                region_short_filter =True
        if model_based_filter and institute_based_filter and rcm_based_filter:
            model = request.data["gcmDrivingModel"]
            institute = request.data["institution"]
            rcm = request.data['rcmModel']
            region_short=request.data['region_short']
        else:
            model = None
            institute = None
            rcm = None
            region_short=None
        track_obj = CordexResponseTracker.objects.get(id=primary_key)
    except Exception as exception:
        message = "{}:{}".format(type(exception).__name__,exception)
        data = {"error":message}
        logger.exception(LogUtils.\
            load_message(**request.log_utils, message=message))
        return Response(data,status = status.HTTP_200_OK)
    try:
        output,table_outputs,error=run_master(user_region_,user_var_,
                                              history_from,history_to,
                                              future_from,future_to,
                                              primary_key,
                                              threshold_value_less_than,
                                              threshold_value_more_than,lat,
                                              lon,model,institute,rcm,region_short,request)
        models_used = []
        for out in output:
            region = out['region']
            data = out['data'][0]
            key = list(data.keys())[0]
            models = data[key][1]["Hits"]["model"]
            models = list(set(models.keys()))
            models_used.append({"region":region,
                               "models":models})
        # table_outputs = [tb for tb in table_outputs]
        track_obj.history_display_data = output
        track_obj.display_data = table_outputs
        track_obj.model_used = models_used
        track_obj.status = "success"
        track_obj.save()
        data = {"display_data":table_outputs,"final_result":output,"model_used_result":models_used}
        logger.info(LogUtils.\
            load_message(**request.log_utils, message="completed"))
        return Response(data,status = status.HTTP_200_OK)
    except Exception as exception:
        track_obj.status = "failed"
        track_obj.save()
        message = "{}:{}".format(type(exception).__name__,exception)
        logger.exception(LogUtils.\
            load_message(**request.log_utils, message=message))
        data = {"error":message}
        return Response(data,status = status.HTTP_200_OK)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def request_details(request):
    """ 
    POST Method: getting request details
    """
    if request.data['datasource'].lower() == "cordex":
        id = RequestUtils.cordex_request(request)
    elif request.data['datasource'].lower() in ("ukcp18-prob",
                                                "ukcp18-high-res",
                                                "haduk",
                                                "loca"):
        id = RequestUtils.ukcp_request(request)
    data = {"user_name":request.user.username,
            "tracker_id":id.id,
            "datasource":request.data['datasource'],
            "current_item_status_date":datetime.datetime.now(),
            "status":"pending"}
    serializer = requestTrackerSerializer(data = data)
    if serializer.is_valid():
        serializer.save()
    message={"status":"Task added successfully wait for email response!!!"}
    return Response(message,status = status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def csv_gis_files_dropdown(request):
    """ 
    GET Method: 
    """
    primary_key=request.GET["id"]
    file_format=request.GET["file_format"]
    if file_format =="csv":
        s3 = boto3.resource('s3')
        my_bucket = s3.Bucket('kliima-raw-data')
        file_name=[]
        for object_summary in my_bucket.objects.filter(Prefix="raw-data-dev/Cordex/csv/{}/".format(primary_key)):
            list_objects=object_summary.key
            file_name.append(list_objects)
        #csv_file_path="raw-data/Cordex/csv/{}/{}".format(primary_key,file_name[0])
        region_list = []
        for file_key in file_name:
            region_dict = {}
            file = file_key.split("_")
            file = file[0].split("/")[-1]
            region_dict["region"] = file
            region_dict["id"] = primary_key
            region_list.append(region_dict)
        message = {"available_resoltions":region_list}
    return Response(message,status = status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def csv_gis_files(request):
    """ 
    GET Method: getting raw time series csv url
    """
    primary_key=request.GET["id"]
    # resolution = request.GET['resolution']
    file_format=request.GET["file_format"]
    #raw-data-dev
    if file_format =="csv":
        raw_data_signed_url = CsvDownload.extract("raw-data-dev",
                                                  primary_key)
        per_model_signed_url = CsvDownload.extract("per-model-data-dev",
                                                  primary_key)
        message={"time_series_data":raw_data_signed_url,
                 "per_model_data":per_model_signed_url}
    return Response(message,status = status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def models_used(request):
    """ 
    GET Method: model details
    """
    user_region_=request.GET["user_region"]
    user_var_=request.GET["user_var"]
    user_var_ = [user_var_]
    cordex = ModelSelect(user_region_,user_var_)
    models=cordex.run()
    return Response(models,status = status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def history_detail(request):
    """ 
    GET Method: history details
    """
    history = GetHistory.load(request.user.username)
    return Response(history, status=status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def table_detail(request):
    """ 
    GET Method: table data details
    """
    if request.GET['datasource'].lower() == "cordex":
        results = TableDetails.cordex(request)
    elif request.GET['datasource'].lower() in ("ukcp-prob",
                                                "ukcp-high-res"): 
        results = TableDetails.ukcp(request)
    return Response(results,status=status.HTTP_200_OK)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def cordex_search(request):
    """ 
    POST Method: validating cordex download
    """
    script = CordexWgetScript(request.data,request.META["HTTP_AUTHORIZATION"])
    message = script.run()
    data = {"message":message}
    return Response(data,status = status.HTTP_200_OK)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def cordex_add(request):
    """ 
    POST Method: validating cordex download
    """
    serializer = CordexWgetDataSerializer(data = request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(request.data,status = status.HTTP_200_OK)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def cordex_add_parameters_list_auto(request):
    """ 
    POST Method: validating cordex download
    """
    script = GetParametersList(request.META["HTTP_AUTHORIZATION"])
    script.run()
    return Response(status = status.HTTP_200_OK)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def cordex_add_parameters_list(request):
    """ 
    POST Method: Cordex Parameters scraping api
    """
    try:
        parameters_list = CordexParametersList.objects.get(id=1)
    except CordexParametersList.DoesNotExist:
        parameters_list = None
    if parameters_list is None:
        serializer = CordexParametersListSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
    else:
        serializer = CordexParametersListSerializer(parameters_list,
                                                    data= request.data)
        if serializer.is_valid():
            serializer.save()
    return Response(request.data,status = status.HTTP_200_OK)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def cordex_get_parameters_list(request):
    """ 
    POST Method: List Cordex Parameters
    """
    try:
        parameters_list = CordexParametersList.objects.get(id=1)
    except CordexParametersList.DoesNotExist:
        parameters_list = None
    if parameters_list is None:
        data = {'error':"object doesn't exist"}
        return Response(data,status = status.HTTP_404_NOT_FOUND)
    serializer = CordexParametersListSerializer(parameters_list)
    return Response(serializer.data,status = status.HTTP_200_OK)

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def cordex_list(request):
    """ 
    POST Method:  validating cordex download
    """
    if request.method == "POST":
        unique_fields = ['file_name']
        duplicates = (
            CordexWgetData.objects.values(*unique_fields)
            .order_by()
            .annotate(max_id=Max('id'), count_id=Count('id'))
            .filter(count_id__gt=1)
        )
        for duplicate in duplicates:
            (
                CordexWgetData.objects
                .filter(**{x: duplicate[x] for x in unique_fields})
                .exclude(id=duplicate['max_id'])
                .delete()
            )
        instance = CordexWgetData.objects.filter(
                                            from_date__gte = request.data.get('from_date',None),
                                            to_date__lte = request.data.get('to_date',None),
                                            project = request.data.get('project',None),
                                            domain = request.data.get('domain',None),
                                          #  experiment_family=request.data.get('experiment_family',None),
                                            experiment = request.data.get('experiment',None),
                                            #ensemble = request.data.get('ensemble',None),
                                            time_frequency = request.data.get('time_frequency',None),
                                            variable = request.data.get('variable',None)
                                            )
        serializer = CordexWgetDataSerializer(instance,many=True)
        return Response(serializer.data,status = status.HTTP_200_OK)

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def geo_file(request):
    """ 
    POST Method: generating multiple lat lon
    in geo json format
    """
    token = request.META["HTTP_AUTHORIZATION"].split(" ")[1]
    try:
        obj_ = GeoFileUpload.objects.get(token=token)
        obj_.delete()
    except GeoFileUpload.DoesNotExist:
        pass
    serializer = GeoFileUploadSerializer(data= request.data)
    if serializer.is_valid():
        serializer.save(token= token)
    script = GeoJsonFile(serializer.data["geo_file"],token)
    message = script.run()
    path = os.path.join(settings.MEDIA_ROOT, 'geo_files/')
    if message == "warning":
        csv_file = serializer.data["geo_file"].split("/")[-1]
        os.remove(path+csv_file)
        return Response({"detail":message},status = status.HTTP_200_OK)
    file = open(path+token+"geodata.geojson",'rb')
    response = FileResponse(file,status = status.HTTP_200_OK)
    csv_file = serializer.data["geo_file"].split("/")[-1]
    os.remove(path+csv_file)
    os.remove(path+token+"geodata.geojson")
    return response
