import os
import requests
import re
from bs4 import BeautifulSoup
import json
from django.conf import settings

class CordexWgetScript():
    """ 
    Get data using Wget Script
    """
    def __init__(self, parameters,token):
        self.path = os.path.join(settings.MEDIA_ROOT, 'wget_for_upload/')
        self.parameters = parameters
        self.token = token
        self.headers = {
                        'Content-Type': 'application/json',
                        'Authorization':'{}'.format(self.token)
                        }
        self.domain = settings.CORDEX_DATA_DOMAIN
        
    def create_wget_script_url(self):
        """ 
        creating wget script url to get 
        script
        """
        base_url = "https://esgf-data.dkrz.de/esg-search/wget?limit=10000"
        for key in list(self.parameters.keys()):
            if self.parameters[key] != None:
                var = ",".join(self.parameters[key])
                base_url += "&"+key+'='+var
       # base_url=base_url.replace("&experiment=rcp45","")
       # base_url=base_url.replace("mon","day")
        return base_url
 
    def download_wget_script(self): 
        """ 
        downloading wget script
        """ 
        url = self.create_wget_script_url()
        response = requests.get(url)
        file = self.path+"wget.sh"
        with open(file,"wb") as sh: 
            sh.write(response.content)  
        return url,file
     
    def get_urls_from_wget_script(self): 
        """ 
        gathering required fields from the
        wget script (search url, file and urls)
        """
        search_url,file = self.download_wget_script()
        with open(file,"r") as sh:
            lines = sh.readlines()
        urls = []
        start = False
        if lines[0] == "No files were found that matched the query":
            urls = False
            return search_url,file,urls
        end_line = "EOF--dataset.file.url.chksum_type.chksum\n"
        start_line = 'download_files="$(cat <<EOF--dataset.file.url.chksum_type.chksum\n'
        for line in lines:
            if start_line == line:
                start = True
            elif end_line == line:
                break
            elif start == True:
                urls.append(line)
            else:
                pass
        return search_url,file,urls
    
    def scraping_details_from_urls(self):
        """ 
        gathering required details from the
        urls
        """   
        search_url,_,urls = self.get_urls_from_wget_script()
        if urls != False:       
            response = requests.post(self.domain+"/dataset/cordex/get/parameters",headers = self.headers)           
            parameters_dict = response.json()           
            for url in urls:               
                data = {}               
                url = url.replace("'","")                
                content = url.split(" ")               
                token = content[3].replace("\n","")
                date_pattern = re.compile(r'\d\d\d\d\d\d\d\d\d\d\d\d-\d\d\d\d\d\d\d\d\d\d\d\d')
                date = date_pattern.findall(content[0])
                if date == []:  
                    date_pattern = re.compile(r'\d\d\d\d\d\d\d\d-\d\d\d\d\d\d\d\d')  
                    date = date_pattern.findall(content[0])     
                date = date[0].split("-")     
                data['from_date'],data['to_date'] = date[0][0:4],date[1][0:4]     
                data['file_name'] = content[0]     
                data['file_url'] = content[1]        
                data['en_hash_fn'] = content[2]               
                data['token'] = token               
                data['search_url'] = search_url               
                variables = content[0].split("_")[:-1]              
                for var in variables:                  
                    for key in list(self.parameters.keys()):                     
                        data.setdefault(key,[])                  
                        if var in parameters_dict[key]:                      
                            data[key].append(var)                            
                        else:                           
                            pass                       
                data['project'] = self.parameters['project']        
                data['product'] = self.parameters['product']
                for key in list(data.keys()):             
                    if data[key] == []:                  
                        data[key] = None
                data = json.dumps(data)
                response = requests.post(self.domain+"/dataset/cordex/add",
                        data = data,
                        headers = self.headers)
            message = "urls are uploaded successfully"
        else:
            message = "No files were found that matched the query"
        return message
     
    def run(self):
        """ 
        run the pipeline
        """    
        message = self.scraping_details_from_urls()
        return message

class GetParametersList():
    """ 
    Getting the parameters from the
    esgf website like domain names,
    institute and etc.,
    """
    def __init__(self,token):
        self.token = token
        self.headers = {
                    'Content-Type': 'application/json',
                    'Authorization':'{}'.format(self.token)
                  }
        self.domain = settings.CORDEX_DATA_DOMAIN

    def run(self):
        """ 
        running the entire pipeline
        """
        parameters_dict = {}
        response = requests.\
                    get("https://esgf-data.dkrz.de/search/cordex-dkrz/")
        soup = BeautifulSoup(response.content,"html.parser")
        tags = soup.find_all('div',class_='facet-panel')
        keys = ["project","product","domain","institute",
                  "driving_model","experiment","experiment_family",
                  "ensemble","rcm_model","rcm_version",
                  "time_frequency","variable","variable_long_name",
                  "cf_standard_name","datanode"]
        for tag,key in zip(tags,keys):
            lis = tag.find_all("li")
            for li in lis:
                parameters_dict.setdefault(key,[])
                text = " ".join(li.text.strip().split(" ")[:-1])
                parameters_dict[key].append(text)
        data = json.dumps(parameters_dict)
        response = requests.post(self.domain+"/dataset/cordex/add/parameters",
                                 data = data,
                                 headers = self.headers)
