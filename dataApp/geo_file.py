import csv
import json
import os
from collections import OrderedDict
import json
from geopy.geocoders import Nominatim
import pycountry_convert as pc
from django.conf import settings


class GeoJsonFiles():

    def __init__(self,filename,token):
        self.path = os.path.join(settings.MEDIA_ROOT,'geo_files/')
        self.file = filename.split("/")[-1]
        self.token = token

    def run(self):
        with open(self.path+self.file, 'r') as csvfile:
            length = len(csvfile.readlines())
            csvfile.close()
        if (length - 1) > 30:
            return "warning"
        features = []
        with open(self.path+self.file, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            index=0
            for  title, short_name,latitude, longitude,region,region_short in reader:
                if index != 0:
                    ord_dict = OrderedDict()
                    ord_dict['type'] = 'Feature'
                    ord_dict['properties'] = {
                        'title': title,
                        'short_name': short_name,
                        'region':region,
                        'region_short':region_short
                    }
                    ord_dict['geometry'] = {
                        'type': 'Point',
                        'coordinates': [float(latitude), float(longitude)]
                    }
                    features.append(ord_dict)
                index=index+1
        ord_dict = OrderedDict()
        ord_dict['type'] = 'FeatureCollection'
        ord_dict['features'] = features
        with open(self.path+self.token+"geodata.geojson", 'w') as f:
            f.write(json.dumps(ord_dict, sort_keys=False, indent=4))
            f.close()
        return "success"

class GeoJsonFile():
    """ 
    Loading Geo location to the DB
    from csv file
    """
    def __init__(self,filename,token):
        self.path = os.path.join(settings.MEDIA_ROOT,'geo_files/')
        self.file = filename.split("/")[-1]
        self.token = token
        
    def get_geo_data(self,lat,lon):
        """ 
        get the geo location details
        """
        try:
            geolocator = Nominatim(user_agent="my-applicatione")
            co_ordinates=str(lat)+","+" "+str(lon)
            location = geolocator.reverse(co_ordinates)
            ddress = location.raw['address']
            country = ddress.get('country', '')
            country_code = ddress.get('country_code', '')
            country_code=country_code.upper()
            country_continent_code = pc.country_alpha2_to_continent_code(country_code)
            country_continent_name = pc.convert_continent_code_to_continent_name(country_continent_code)
            country_name=pc.country_alpha2_to_country_name(country_code,cn_name_format="default")
            co_ord=[float(lat),float(lon)]
            country_list=[country_name,country_code,country_continent_name,country_continent_code]
        except:
            co_ord = None
            country_list = None
        return co_ord,country_list

    def geojson_conversion(self,coordinates,properties):
        """ 
        converting geo json format
        """
        geojson_dict = OrderedDict()
        geojson_dict["type"]="FeatureCollection"
        for coor,properti in zip(coordinates,properties):
            geojson_dict.setdefault("features",[])
            pr = OrderedDict()
            pr["type"] = "Feature"
            pr["properties"] =  OrderedDict()
            pr["geometry"] = OrderedDict()
            pr["properties"]["title"] = properti[0]
            pr["properties"]["short_name"] = properti[1]
            pr["properties"]["region"] = properti[2]
            pr["properties"]["region_short"] = properti[3]
            pr["geometry"]["type"] = "Point"
            pr["geometry"]["coordinates"] = [coor[0],coor[1]]
            geojson_dict['features'].append(pr)

        return geojson_dict

    def run(self):
        """ 
        running the pipeline
        """
        with open(self.path+self.file, 'r') as csvfile:
            length = len(csvfile.readlines())
            csvfile.close()
        if (length - 1) > 30:
            return "warning"
        coordinates = []
        properties = []
        with open(self.path+self.file, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            next(reader)
            for latitude,longitude in reader:
                co,pr = self.get_geo_data(latitude,longitude)
                if co != None and pr != None:
                    coordinates.append(co)
                    properties.append(pr)
        geojson = self.geojson_conversion(coordinates,properties)
        with open(self.path+self.token+"geodata.geojson", 'w') as f:
            f.write(json.dumps(geojson, sort_keys=False, indent=4))
            f.close()
        return "success"
