from joblib import Parallel, delayed
import s3fs
import xarray as xr
from io import StringIO 
import requests
import numpy as np
import pandas as pd 
import string
import random
import boto3
from collections import Counter
import os
import copy

def file_filter(user_region_,user_var_,history_from,history_to,future_from,future_to):
    dicts={"Africa":"AFR","Antarctica":"ANT","Arctic":"ARC","Australasia":"AUS","Central America":"CAM","Central Asia":"CAS","East Asia":"EAS","Europe":"EUR","Mediterranean":"MED","Middle East North Africa":"MNA","North America":"NAM","South America":"SAM","South Asia":"WAS","South-East Asia":"SEA"}
    user_region=dicts.get(user_region_)
    temp_user_var=user_var_[0]
    var_dicts={"Maximum temperature":"tasmax","Temperature threshold more than":"tasmax","Temperature threshold less than":"tasmin","Average daily temperature":"tas","Average daily maximum temperature":"tasmax","Average daily minimum temperature":"tasmin","Daily max gust":"wsgsmax","Gust threshold more than":"wsgsmax","Wind threshold more than":"sfcWindmax","Daily average wind speed":"sfcWindmax","Specfic humidity":"huss","Relative humidity":"hurs","Snow depth":"snd","Sea level Rise":"snd","Snow threshold more than":"snd","Average daily precipitation":"pr","Precipitation threshold more than":"pr","Precipitation threshold less than":"pr","Maximum 1 day rainfall":"pr","Maximum 5 day rainfall":"pr","Cumulative dry days":"pr"}
    user_var=var_dicts.get(user_var_[0]) 
    file_path=user_region+"/"+user_var

    region_filter={"Africa":"AFR-44","Antarctica":"ANT-44","Arctic":"ARC-44","Australasia":"AUS-44","Central America":"CAM-44","Central Asia":"CAS-22","East Asia":"EAS-22","Europe":"EUR-11","Mediterranean":"MED-11","Middle East North Africa":"MNA-44","North America":"NAM-22","South America":"SAM-22","South Asia":"WAS-22","South-East Asia":"SEA-22"}
    temp_region_filter=region_filter.get(user_region_)

    s3 = boto3.resource('s3')
    my_bucket = s3.Bucket('klima-datasets')
    filter_file=[]
    for object_summary in my_bucket.objects.filter(Prefix="Cordex"+"/"+ file_path+"/"):
        list_objects=object_summary.key
        filter_file.append(list_objects)

    temp_file_data=[]
    for required_region in filter_file:
        if temp_region_filter in required_region:
            temp_file_data.append(required_region)
        else:
            pass

    file_data=[]
    for files_s3 in temp_file_data:
        s3 = s3fs.S3FileSystem(anon=False)
        val=boto3.resource('s3').Bucket('klima-datasets').Object(files_s3).content_length
        if val !=0:
            s3path = 's3://klima-datasets/'+files_s3
            file_data.append(s3path)
        else:
            pass
    file_format_split={}
    all_experiment=["historical", "rcp45", "rcp85"]
    for exp_class in all_experiment:
        temp_files=[]
        for file_classify in file_data:
            if exp_class in file_classify:
                file_drops=file_classify.replace(".nc","")
                year_seperate=file_drops[-17:]
                year_list=year_seperate.split("-")
                temp=[]
                for divide_year in year_list:
                    temp_value=divide_year[:-4]
                    temp.append(temp_value)
                for i in range(0, len(temp)):
                    temp[i] = int(temp[i])
                if exp_class=="historical":
                    range_val= list(range(history_from,history_to+1))
                else:
                    range_val= list(range(future_from,future_to+2))
                set_temp=set(temp)
                set_range_val=set(range_val)
                difference=list(set_temp-set_range_val)
                if len(difference) ==0:
                    temp_files.append(file_classify)
                else:
                    pass
        file_format_split[exp_class]=temp_files
    return file_format_split


def search(arg,coords,variable_pasing):
    print(coords,variable_pasing)
    latitude_longitude_temp=coords
    variable_pasing_temp=variable_pasing
    all_dataframe=[]
    model_used=[]
    try:
        s3 = s3fs.S3FileSystem(anon=False)
        fileset =s3.open(arg) 
        with xr.open_dataset(fileset) as data:
            # data = xr.open_dataset(fileset)
            latvar=data.lat.values
            lonvar= data.lon.values
            # print(latvar,"lon values")
            lat0=latitude_longitude_temp[0]
            lon0=latitude_longitude_temp[1]
            latvals = latvar[:]
            lonvals = lonvar[:]
            dist_sq = (latvals-lat0)**2 + (lonvals-lon0)**2
            minindex_flattened = dist_sq.argmin()  # 1D index of min element
            iy_min,ix_min = np.unravel_index(minindex_flattened, latvals.shape)
            temp = data.variables[variable_pasing_temp]
            # Creating an empty pandas dataframe
            try:
                data['time'] = data.indexes['time'].normalize()
            except:
                data['time'] = data.indexes['time'].to_datetimeindex()
            index_time = data["time"].to_series()
            res_val = ''.join(random.choices(string.ascii_uppercase, k = 7))
            df = pd.DataFrame(0, columns = [res_val], index = index_time)
            dt = np.arange(0, data.variables['time'].size)
            for time_index in dt:
                df.iloc[time_index] = temp[time_index,iy_min ,ix_min]
            df.index = pd.to_datetime(df.index).strftime('%Y-%m-%d')
            random_df_name = ''.join(random.choices(string.ascii_uppercase+string.digits, k = 7))
            random_df_name=df
            # print(df,"the df value------------------------------------")
            all_dataframe.append(random_df_name)
            model_used.append(fileset)
            # print(all_dataframe)
    except:
        pass

        
    return all_dataframe ,model_used

def run(user_region_,user_var_,history_from,history_to,future_from,future_to,primary_key,threshold_value_less_than,threshold_value_more_than,latitude_longitude):
    file_format_split=file_filter(user_region_,user_var_,history_from,history_to,future_from,future_to)
    print(file_format_split)
    tempvar_dicts={"Maximum temperature":"tasmax","Temperature threshold more than":"tasmax","Temperature threshold less than":"tasmin","Average daily temperature":"tas","Average daily maximum temperature":"tasmax","Average daily minimum temperature":"tasmin","Daily max gust":"wsgsmax","Gust threshold more than":"wsgsmax","Wind threshold more than":"sfcWindmax","Daily average wind speed":"sfcWindmax","Specfic humidity":"huss","Relative humidity":"hurs","Snow depth":"snd","Sea level Rise":"snd","Snow threshold more than":"snd","Average daily precipitation":"pr","Precipitation threshold more than":"pr","Precipitation threshold less than":"pr","Maximum 1 day rainfall":"pr","Maximum 5 day rainfall":"pr","Cumulative dry days":"pr"}
    temp_mag_pass=tempvar_dicts.get(user_var_[0]) 
    file_check=file_format_split.keys()
    dummy={}
    models_of_file=[]

    for temp_file_check in file_check:
        list_of_files= file_format_split.get(temp_file_check)
        user_lat_lon= [round(num, 4) for num in (latitude_longitude)]
        coords = [user_lat_lon]
       # coords = [latitude_longitude]
        variable_pasing = [temp_mag_pass]
        search_result = Parallel(n_jobs=-1, backend="loky")(map(delayed(search),list_of_files,coords*len(list_of_files),variable_pasing*len(list_of_files)))
        results_temp=[i[0] for i in search_result]
        model_used=[i[1] for i in search_result]
        results = [ele for ele in results_temp if ele != []]
        models_of_file.append(model_used)
        class_result=[]
        for tem_rest in results:
            temp_user_result=tem_rest[0]
            class_result.append(temp_user_result)

        if temp_file_check =="historical":
            dummy["historical"]=class_result
        elif temp_file_check =="rcp45":
            dummy["rcp45"]=class_result
        elif temp_file_check =="rcp85":
            dummy["rcp85"]=class_result

    temp_scene_combine=dummy.keys()
    dicts={"Africa":"AFR","Antarctica":"ANT","Arctic":"ARC","Australasia":"AUS","Central America":"CAM","Central Asia":"CAS","East Asia":"EAS","Europe":"EUR","Mediterranean":"MED","Middle East North Africa":"MNA","North America":"NAM","South America":"SAM","South Asia":"WAS","South-East Asia":"SEA"}
    user_region=dicts.get(user_region_)
    temp_user_var=user_var_[0]
    var_dicts={"Maximum temperature":"tasmax","Temperature threshold more than":"tasmax","Temperature threshold less than":"tasmin","Average daily temperature":"tas","Average daily maximum temperature":"tasmax","Average daily minimum temperature":"tasmin","Daily max gust":"wsgsmax","Gust threshold more than":"wsgsmax","Wind threshold more than":"sfcWindmax","Daily average wind speed":"sfcWindmax","Specfic humidity":"huss","Relative humidity":"hurs","Snow depth":"snd","Sea level Rise":"snd","Snow threshold more than":"snd","Average daily precipitation":"pr","Precipitation threshold more than":"pr","Precipitation threshold less than":"pr","Maximum 1 day rainfall":"pr","Maximum 5 day rainfall":"pr","Cumulative dry days":"pr"}
    user_var=var_dicts.get(user_var_[0])
    variable_pasing=user_var
    display_data={}
    final_result={}
    historical_metrics=[]
    rcp45_metrics=[]
    rcp85_metrics=[]
    for scene_name in temp_scene_combine:
        try:
            Multiple_data_with_metrics={}
            Multiple_data_with_lat_lon={}
            all_dataframe=dummy.get(scene_name)
            combing_data = pd.concat(all_dataframe,axis=1)
            combing_data[user_var]=combing_data.iloc[:,:].mean(axis=1)
            combing_data=combing_data.filter([user_var])
            combing_data['Time'] = combing_data.index
            if "tas" in user_var:
                combing_data[user_var]=combing_data[user_var] - 273.15 #kelvin_to_celcius
            elif "pr" in user_var:
                combing_data[user_var]=combing_data[user_var]*86400 #units to mm

            combing_data.reset_index(drop=True, inplace=True)
            co_ordinates=str(latitude_longitude[0])+"/"+str(latitude_longitude[1])
            value_of_dict={'{}'.format(user_var):combing_data[user_var].values.tolist(),'time':combing_data["Time"].values.tolist()}
            Multiple_data_with_lat_lon[co_ordinates]=value_of_dict
            #threshold and 5 day rainfall
            
            if "threshold more than" in temp_user_var:
                if threshold_value_more_than != 0:
                    # df[0] = df[df[0] > 5][0]
                    # df=df.dropna()
                    combing_data[user_var]=combing_data[combing_data[user_var] >= threshold_value_more_than][user_var]
                    combing_data=combing_data.dropna()
                    value_of_metrics={"min":combing_data[user_var].min(),"mean":combing_data[user_var].mean(),"max":combing_data[user_var].max()}
                    Multiple_data_with_metrics[co_ordinates]=value_of_metrics
                    if scene_name =="historical":
                        temp_metrics=combing_data[user_var].values.tolist()
                        historical_metrics.append(temp_metrics)
                    elif scene_name =="rcp45":
                        temp_metrics=combing_data[user_var].values.tolist()
                        rcp45_metrics.append(temp_metrics)
                    elif scene_name =="rcp85":
                        temp_metrics=combing_data[user_var].values.tolist()
                        rcp85_metrics.append(temp_metrics)
                else:
                    pass
            elif "threshold less than" in temp_user_var:
                if threshold_value_less_than != 0:
                    combing_data[user_var]=combing_data[combing_data[user_var] <= threshold_value_more_than][user_var]
                    combing_data=combing_data.dropna()
                    value_of_metrics={"min":combing_data[user_var].min(),"mean":combing_data[user_var].mean(),"max":combing_data[user_var].max()}
                    Multiple_data_with_metrics[co_ordinates]=value_of_metrics
                    if scene_name =="historical":
                        temp_metrics=combing_data[user_var].values.tolist()
                        historical_metrics.append(temp_metrics)
                    elif scene_name =="rcp45":
                        temp_metrics=combing_data[user_var].values.tolist()
                        rcp45_metrics.append(temp_metrics)
                    elif scene_name =="rcp85":
                        temp_metrics=combing_data[user_var].values.tolist()
                        rcp85_metrics.append(temp_metrics)
                else:
                    pass
            else:
                if "Maximum 5 day rainfall" in temp_user_var:
                    vals5day = combing_data[user_var].values.tolist()
                    count=[]
                    all5day=[]
                    x=len(vals5day)-4
                    for j in range (x):
                        count=vals5day[j]+vals5day[j+1]+vals5day[j+2]+vals5day[j+3]+vals5day[j+4]
                        all5day.append(count)
                    # prmax5day=np.max(all5day,axis=0)*86400 
                    value_of_metrics={"min":np.min(all5day),"mean":np.mean(all5day),"max":np.max(all5day)}
                    Multiple_data_with_metrics[co_ordinates]=value_of_metrics
                    if scene_name =="historical":
                        temp_metrics=combing_data[user_var].values.tolist()
                        historical_metrics.append(temp_metrics)
                    elif scene_name =="rcp45":
                        temp_metrics=combing_data[user_var].values.tolist()
                        rcp45_metrics.append(temp_metrics)
                    elif scene_name =="rcp85":
                        temp_metrics=combing_data[user_var].values.tolist()
                        rcp85_metrics.append(temp_metrics)
                else:
                    value_of_metrics={"min":combing_data[user_var].min(),"mean":combing_data[user_var].mean(),"max":combing_data[user_var].max()}
                    Multiple_data_with_metrics[co_ordinates]=value_of_metrics
                    if scene_name =="historical":
                        temp_metrics=combing_data[user_var].values.tolist()
                        historical_metrics.append(temp_metrics)
                    elif scene_name =="rcp45":
                        temp_metrics=combing_data[user_var].values.tolist()
                        rcp45_metrics.append(temp_metrics)
                    elif scene_name =="rcp85":
                        temp_metrics=combing_data[user_var].values.tolist()
                        rcp85_metrics.append(temp_metrics)
        except:
            pass
        final_result[scene_name]=Multiple_data_with_lat_lon
        final_result[scene_name+"_metrics"]=Multiple_data_with_metrics
    final_result['variable'] = user_var
    final_result['region'] = user_region
    final_output={}
    final_output[str(latitude_longitude[0])+"/"+str(latitude_longitude[1])]=final_result
    table_result={}
    # historical = np.array(historical_metrics) 
    # rcp45 = np.array(rcp45_metrics)  #Convert list to np array 
    # rcp85 = np.array(rcp85_metrics) 
    historical=[ item for elem in historical_metrics for item in elem]
    rcp45=[ item for elem in rcp45_metrics for item in elem]
    rcp85=[ item for elem in rcp85_metrics for item in elem]
    temp_historical_rcp45=copy.deepcopy(historical)
    temp_historical_rcp85=copy.deepcopy(historical)
    if len(temp_historical_rcp45) < len(rcp45):
        diff = len(rcp45) - len(temp_historical_rcp45)
        for i in range(diff):
            rcp45.pop()
    elif len(temp_historical_rcp45) > len(rcp45):
        diff = len(temp_historical_rcp45) - len(rcp45)
        for i in range(diff):
            temp_historical_rcp45.pop()
    if len(temp_historical_rcp85) < len(rcp85):
        diff = len(rcp85) - len(temp_historical_rcp85)
        for i in range(diff):
            rcp85.pop()
    elif len(temp_historical_rcp85) > len(rcp85):
        diff = len(temp_historical_rcp85) - len(rcp85)

        for i in range(diff):
            temp_historical_rcp85.pop()
    temp_historical_rcp45 = np.array(temp_historical_rcp45) 
    temp_historical_rcp85 = np.array(temp_historical_rcp85) 
    rcp45 = np.array(rcp45)  #Convert list to np array 
    rcp85 = np.array(rcp85) 
    if len(temp_historical_rcp45)!=0:
        if len(rcp45)!=0:
            if user_var=="pr":
                temp_value_rcp45=temp_historical_rcp45/rcp45
                # temp_value_rcp45 = [ item for elem in temp_value_rcp45 for item in elem]
                mean_temp=sum(temp_value_rcp45) / len(temp_value_rcp45)
                table_result["rcp45"]={"min":min(temp_value_rcp45),"mean":mean_temp,"max":max(temp_value_rcp45)}
            else:
                temp_value_rcp45=temp_historical_rcp45-rcp45
                # temp_value_rcp45 = [ item for elem in temp_value_rcp45 for item in elem]
                mean_temp=sum(temp_value_rcp45) / len(temp_value_rcp45)
                table_result["rcp45"]={"min":min(temp_value_rcp45),"mean":mean_temp,"max":max(temp_value_rcp45)}
        else:
            table_result["rcp45"]={"min":"N/A","mean":"N/A","max":"N/A"}
    else:
        table_result["rcp45"]={"min":"N/A","mean":"N/A","max":"N/A"}
    if len(temp_historical_rcp85)!=0:
        if len(rcp85)!=0:
            if user_var=="pr":
                temp_value_rcp85=temp_historical_rcp85/rcp85
                # temp_value_rcp85 = [ item for elem in temp_value_rcp85 for item in elem]
                mean_temp=sum(temp_value_rcp85) / len(temp_value_rcp85)
                table_result["rcp85"]={"min":min(temp_value_rcp85),"mean":mean_temp,"max":max(temp_value_rcp85)}
            else:
                temp_value_rcp85=temp_historical_rcp85-rcp85
                # temp_value_rcp85 = [ item for elem in temp_value_rcp85 for item in elem]
                mean_temp=sum(temp_value_rcp85) / len(temp_value_rcp85)
                table_result["rcp85"]={"min":min(temp_value_rcp85),"mean":mean_temp,"max":max(temp_value_rcp85)}
        else:
            table_result["rcp85"]={"min":"N/A","mean":"N/A","max":"N/A"}
    else:
        table_result["rcp85"]={"min":"N/A","mean":"N/A","max":"N/A"}
    display_data[str(latitude_longitude[0])+"/"+str(latitude_longitude[1])]=table_result
    return display_data,final_output,models_of_file
