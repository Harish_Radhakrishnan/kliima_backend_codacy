import joblib
import s3fs
import boto3
from joblib  import Parallel,delayed 

class ModelSelect():
    
    def __init__(self,user_region_,user_var_) :
        self.user_region_=user_region_
        self.user_var_=user_var_

    def file_filter(self,file_path,get_models):
        temp_region_filter=get_models
        s3 = boto3.resource('s3')
        my_bucket = s3.Bucket('klima-datasets')
        filter_file=[]
        for object_summary in my_bucket.\
            objects.filter(Prefix="Cordex"+"/"+ file_path+"/"):
            list_objects=object_summary.key
            filter_file.append(list_objects)
        temp_file_data=[]
        for required_region in filter_file:
            if (temp_region_filter in required_region) and \
            ("r1i1p1" in required_region):
                temp_file_data.append(required_region)
            else:
                pass
        # print(temp_file_data)
        def content_check(files_s3):
            file_data=[]
            # print(files_s3)
            # for files_s3 in temp_file_data1:
            s3 = s3fs.S3FileSystem(anon=False)
            val=boto3.resource('s3').Bucket('klima-datasets').\
                Object(files_s3).content_length
            if val !=0:
                s3path = 's3://klima-datasets/'+files_s3
                file_data.append(s3path)
            else:
                pass
            return file_data
        file_data = Parallel(n_jobs=-1,backend="loky")\
            (map(delayed(content_check),temp_file_data))
        return file_data,temp_region_filter

    def getting_model_names(self,file_path,get_models):
        data,temp_region_filter=self.file_filter(file_path,get_models)
        temp_region_filter=[temp_region_filter]
        x = str(data)
        x = x.replace("[","")
        x = x.replace("]","")
        x = x.replace("'","")
        output = []
        for file_ in x.split(","):
            if file_ == "" or file_ == " ":
                pass
            else:
                output.append(file_)
        models = []
        for file_ in set(output):
            file_ = file_.split("_")
            model = "_".join(file_[2:7])
            models.append(model)
        result = []
        for i in set(models):
            names = i.split("_")
            names.pop(1)
            result.append("_".join(names))
        result = set(result)
        driving_model = []
        institute = []
        rcm_model = []
        institute_names = ["AWI", "BCCR", "BOM", "BOUN", 
                           "CCCMA", "CEC", "CLMcom", "CLMcom-BTU",
                           "CLMcom-ETH", "CLMcom-HZG", "CLMcom-KIT", 
                           "CNRM", "CSIRO", "CYI", "DHMZ", "DMI", "DWD",
                           "ETH", "GERICS", "HMS", "ICTP", "IITM", "INPE",
                           "IPSL", "IPSL-INERIS", "ISU", "KNMI", "KNU", "MGO",
                           "MOHC", "MPI-CSC", "NCAR", "NIMS-KMA", "NUIM",
                           "ORNL", "OURANOS", "PIK", "PNU", "POSTECH",
                           "RMIB-UGent", "RU-CORE", "SMHI", "UA", "UCAN",
                           "UHOH", "ULg", "UNIST", "UNSW", "UQAM"]
        for i in result:
            names = i.split("_")
            driving_model.append(names[0])
            institute_checking = names[2].split("-")
            last_index = 2
            checking_ = False
            while checking_ == False:
                for ins in institute_names:
                    if ins.lower() == "-".\
                        join(institute_checking[0:last_index]).lower():
                        institute.append("-".\
                            join(institute_checking[0:last_index]))
                        rcm_model.append("-".\
                            join(institute_checking[last_index:]))
                        checking_ = True
                last_index -=1
        return driving_model,institute,rcm_model,temp_region_filter

    def run(self):
        dicts={"Africa":"AFR","Antarctica":"ANT",
               "Arctic":"ARC","Australasia":"AUS",
               "Central America":"CAM","Central Asia":"CAS",
               "East Asia":"EAS","Europe":"EUR",
               "Mediterranean":"MED","Middle East North Africa":"MNA",
               "North America":"NAM","South America":"SAM",
               "South Asia":"WAS","South-East Asia":"SEA"}
        user_region=dicts.get(self.user_region_)
        var_dicts={"Maximum temperature":"tasmax",
                   "Temperature threshold more than":"tasmax",
                   "Temperature threshold less than":"tasmin",
                   "Average daily temperature":"tas",
                   "Average daily maximum temperature":"tasmax",
                   "Average daily minimum temperature":"tasmin",
                   "Daily max gust":"wsgsmax",
                   "Gust threshold more than":"wsgsmax",
                   "Wind threshold more than":"sfcWindmax",
                   "Daily average wind speed":"sfcWindmax",
                   "Specfic humidity":"huss","Relative humidity":"hurs",
                   "Snow depth":"snd","Sea level Rise":"snd",
                   "Snow threshold more than":"snd",
                   "Average daily precipitation":"pr",
                   "Precipitation threshold more than":"pr",
                   "Precipitation threshold less than":"pr",
                   "Maximum 1 day rainfall":"pr","Maximum 5 day rainfall":"pr",
                   "Cumulative dry days":"pr"}
        user_var=var_dicts.get( self.user_var_[0]) 
        file_path=user_region+"/"+user_var
        model_split=[]
        region_filter={"Africa":["AFR-44"],
                       "Antarctica":["ANT-44"],
                       "Artic":["ARC-22","ARC-44"],
                       "Australasia":["AUS-22","AUS-44"],
                       "Central America":["CAM-22","CAM-44"],
                       "Central Asia":["CAS-22","CAS-44"],
                       "East Asia":["EAS-22","EAS-44"],
                       "Europe":["EUR-11","EUR-22","EUR-44"],
                       "Mediterranean":["MED-11"],
                       "Middle East North Africa":["MNA-22","MNA-44"],
                       "North America":["NAM-22","NAM-44"],
                       "South America":["SAM-20","SAM-22","SAM-44"],
                       "South Asia":["WAS-22","WAS-44"],
                       "South-East Asia":["SEA-22"]}
        temp_region_filters=region_filter.get(self.user_region_)
        region_temp=[ "0.{} (~{}km)".format(i[-2:],i[-2:]) for i in temp_region_filters]
        for get_models in temp_region_filters:
            model_split_result={}
            driving_model,institute,rcm_model,temp_region_filter=self.getting_model_names(file_path,get_models)
            rows = []
            for dm,ins,rm in zip(driving_model,institute,rcm_model):
                row_dict = {"institution":ins,
                "gcmDrivingModel":dm,
                "rcmModel":rm}
                rows.append(row_dict)
            model_split_result["region"]=get_models
            model_split_result["models"]=rows
            model_split.append(model_split_result)
        model_count=[]
        for models_output in model_split:
            model_count_dict={}
            model_count_dict["region"]=models_output["region"]
            model_count_dict["model_count"]=str(len(models_output["models"]))+" models available for " +models_output["region"]
            model_count.append(model_count_dict)
        result= {
            "last_updated":"N/A",
            "covered_emission_scenarios":[
                "RCP 4.5 (Medium)",
                "RCP 8.5 (High)"
            ],
            "spatial_resolution":region_temp,
            "temporal_resolution":"Daily",
            "time_period_coverage":[
                "Historical (1976-2005)",
                "Future (2010-2100)"
            ],
            "model_table":{
                "headers":[
                "Institution",
                "GSM Driving Model",
                "RCM Model"
                ],
                "model_details":model_count,
                "rows":model_split
            }
            }
        return result
