from django.conf import settings
import os
import subprocess

class CordexFileDownload():
    """ 
    Downloading files from the Cordex
    """
    def __init__(self,data):
        self.old_file_path = os.path.join(settings.MEDIA_ROOT,
                                          'wget_for_upload/')
        self.new_file_path = os.path.join(settings.MEDIA_ROOT,
                                          'wget_for_download/')
        self.data = data

    def wget_script_creation(self):
        """ 
        creating the wget script for 
        bash download
        """
        with open(self.old_file_path+'script.sh','r') as old_sh, open(self.new_file_path+'script_download.sh','w') as new_sh:
            for line in old_sh:
                if line == 'download_files="$(cat <<EOF--dataset.file.url.chksum_type.chksum\n':
                    new_sh.write('download_files="$(cat <<EOF--dataset.file.url.chksum_type.chksum\n')
                    for obj in self.data:
                        content = "'"+obj["file_name"]+"'"+" "+"'"+obj["file_url"]+"'"+" "\
                                +"'"+obj["en_hash_fn"]+"'"+" "+"'"+obj["token"]+"'"+"\n"
                        new_sh.write(content)
                elif line == "search_url=\n":
                    new_sh.write("search_url="+'"'+self.data[0]["search_url"]+'"'+"\n")
                else:
                    new_sh.write(line)

    def file_download(self):
        """ 
        file downloading using bash command
        """
        os.chdir(settings.MEDIA_ROOT+"/wget_for_download/")
        subprocess.run(["chmod","+x","script_download.sh"])
        result = subprocess.run(['./script_download.sh','-H'])
        if result.returncode == 0:
            subprocess.run(['rm','script_download.sh'])
            list_nc = os.listdir()
            for i in range(0,1):
                if os.path.getsize(list_nc[i]) == 0:
                    message = {"message":"Files download failed try again"}
            message = {"message":"Files are downloaded successfully"}
            os.chdir(settings.BASE_DIR)
        else:
            subprocess.run(['rm','script_download.sh'])
            os.chdir(settings.BASE_DIR)
            message = {"message":"Something went wrong please try again"}
        return message

    def run(self):
        """ 
        running the pipeline
        """
        self.wget_script_creation()
        messege = self.file_download()
        return messege
