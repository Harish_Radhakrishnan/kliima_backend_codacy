from joblib import Parallel, delayed
import itertools
from .sub_cordex_search import run
import boto3
import pandas as pd
from io import StringIO 

class MainCordexSearch():

    def __init__(self,user_region_,user_var_,history_from,
                 history_to,future_from,future_to,primary_key,
                 threshold_value_less_than,threshold_value_more_than,
                 lat,lon):
        self.user_region_ = user_region_
        self.user_var_ = user_var_
        self.history_from = history_from
        self.history_to = history_to
        self.future_from = future_from
        self.future_to = future_to
        self.primary_key = primary_key
        self.threshold_value_less_than = threshold_value_less_than
        self.threshold_value_more_than = threshold_value_more_than
        self.lat = lat
        self.lon = lon

    def mutiple_lat_lon(self,args,temp_user_region_,temp_user_var_,
                        temp_history_from,temp_history_to,temp_future_from,
                        temp_future_to,temp_primary_key,
                        temp_threshold_value_less_than,
                        temp_threshold_value_more_than):
        display_metrics=[]
        final_raw_data=[]
        models_of_file=[]
        temp_display_metrics,temp_final_raw_data,temp_models_of_file=run(temp_user_region_,temp_user_var_,temp_history_from,temp_history_to,temp_future_from,temp_future_to,temp_primary_key,temp_threshold_value_less_than,temp_threshold_value_more_than,args)
        display_metrics.append(temp_display_metrics)
        final_raw_data.append(temp_final_raw_data)
        models_of_file.append(temp_models_of_file)
        print(display_metrics)
        return display_metrics,final_raw_data,models_of_file

    def getting_model_names(self,data):
        x = str(data)
        x = x.replace("[","")
        x = x.replace("]","")
        x = x.replace("'","")
        output = []
        for file_ in x.split(","):
            if file_ == "" or file_ == " ":
                pass
            else:
                output.append(file_)
        models = []
        for file_ in set(output):
            file_ = file_.split("_")
            model = "_".join(file_[4:7])
            models.append(model)
        return list(set(models))

    def searchs(self):
        temp_user_region_,temp_user_var_,temp_history_from,\
        temp_history_to,temp_future_from,temp_future_to,\
        temp_primary_key,temp_threshold_value_less_than,\
        temp_threshold_value_more_than=\
        [self.user_region_],[self.user_var_],[self.history_from],\
        [self.history_to],[self.future_from],[self.future_to],\
        [self.primary_key],[self.threshold_value_less_than],\
        [self.threshold_value_more_than]
        #user_lat = [round(num, 4) for num in (self.lat)]
        #user_lon= [round(num, 4) for num in (self.lon)]
        dummy=[]
        for lat_one,lon_one in zip(self.lat,self.lon):
            k_both=[lat_one,lon_one]
            dummy.append(k_both)
        dummy.sort()
        co_ordinats_latand_lon=list(dummy for dummy,_ in \
                                itertools.groupby(dummy))
        display_output = Parallel(n_jobs=-1, backend="loky")(
                map(delayed(self.mutiple_lat_lon),
                co_ordinats_latand_lon,
                temp_user_region_*len(co_ordinats_latand_lon),
                temp_user_var_*len(co_ordinats_latand_lon),
                temp_history_from*len(co_ordinats_latand_lon),
                temp_history_to*len(co_ordinats_latand_lon),
                temp_future_from*len(co_ordinats_latand_lon),
                temp_future_to*len(co_ordinats_latand_lon),
                temp_primary_key*len(co_ordinats_latand_lon),
                temp_threshold_value_less_than*len(co_ordinats_latand_lon),
                temp_threshold_value_more_than*len(co_ordinats_latand_lon)))
        display_metrics=[i[0] for i in display_output]
        final_raw_data= [i[1] for i in display_output]
        models_of_file= [i[2] for i in display_output]
        models_of_file_list=self.getting_model_names(models_of_file)
        if not bool(final_raw_data) is False:
            df = pd.DataFrame(final_raw_data)
            csv_buffer = StringIO()
            df.to_csv(csv_buffer)
            s3_resource = boto3.resource('s3')
            csv_file_path='cordex-csv-gis-files/primarykey/'+ \
                str(self.primary_key)+'/csv/'+ \
                str(self.primary_key)+'.csv'
            s3_resource.Object('arup-climate-global-656165081472-aws-config',
                               csv_file_path).put(Body=csv_buffer.getvalue())
        else:
            pass
        return display_metrics,final_raw_data,models_of_file_list
