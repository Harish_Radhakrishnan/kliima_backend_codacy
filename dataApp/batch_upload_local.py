import requests
import json

def batch_file_upload(user_token,experiment_name):
    status_of_upload={}
    file_upload_status=[]
    domain = "http://127.0.0.1:8000"
    headers = {
                'Content-Type': 'application/json',
                'Authorization':'{}'.format(user_token)
                }
    # region=["NAM-11", "NAM-22", "NAM-44"]
    #region=["AFR-22", "AFR-44"]
    # region=["EUR-11", "EUR-22", "EUR-25", "EUR-44", "MED-11", 
    # "MNA-22",  "MNA-44", "SAM-20", "SAM-22", "SAM-44", "SEA-22", "WAS-22", "WAS-44"]
    region=[ "NAM-22"]
    variable = ['tasmax']
    #region=["AFR-22", "AFR-44",  "ANT-44",  "ARC-22", "ARC-44",  "AUS", "AUS-22", "AUS-44", "CAM-22", "CAM-44",  "CAS-22", "CAS-44", "EAS-22", "EAS-44",  "EUR-11", "EUR-22", "EUR-25", "EUR-44", "MED-11", 
    #"MNA-22",  "MNA-44",  "NAM-11", "NAM-22", "NAM-44",  "SAM-20", "SAM-22", "SAM-44", "SEA-22", "WAS-22", "WAS-44"]
    #variable=["tasmax","tas","tasmin","pr","wsgsmax","sfcWindmax","huss","hurs","snd"]
    if experiment_name != "historical":
        for region_list in region:
            for variable_list in variable:
                for batch in range(2006,2100,5):
                    cordex_parameters={"region":{
                        "name":region_list,
                        },
                        "variable":[variable_list],
                        "datasource":"CORDEX",
                        "from_date":batch,
                        "to_date":batch+4,
                        "experiment":experiment_name
                    }
                    
                    data = json.dumps(cordex_parameters)

                    zero_bytes = False
                    
                    while zero_bytes == False:

                        batch_upload = requests.post(domain+"/batch-file-upload",data=data,headers = headers)

                        if batch_upload['zero_bytes'] == False:

                            zero_bytes == True

                    file_upload_status.append(batch_upload)
                    print("*****************************Future*****************************")
                    print("batch year : {}-{}".format(batch,batch+5))
                    print("batch upload details: {}".format(batch_upload))
                    print(batch,batch+5)
    else:
        for region_list in region:
            for variable_list in variable:
                for batch in range(1976,2005,5):
                    cordex_parameters={"region":{
                        "name":region_list,
                        },
                        "variable":[variable_list],
                        "datasource":"CORDEX",
                        "from_date":batch,
                        "to_date":batch+4,
                        "experiment":experiment_name
                    }
                    
                    data = json.dumps(cordex_parameters)

                    zero_bytes = False
                    
                    while zero_bytes == False:

                        batch_upload = requests.post(domain+"/batch-file-upload",data=data,headers = headers)

                        if batch_upload['zero_bytes'] == False:

                            zero_bytes == True

                    #batch_upload = requests.post(domain+"/batch-file-upload",data=data,headers = headers)
                    file_upload_status.append(batch_upload)
                    print("*****************************Historical*****************************")
                    print("batch year : {}-{}".format(batch,batch+5))
                    print("batch upload details: {}".format(batch_upload))
    status_of_upload["status_of_upload"]=file_upload_status
    message={"status":status_of_upload}
    return message
if __name__ == "__main__":

    user_token='Token 5a9a87d38c4b9a10839e2657049a95c330f17340'
    #["historical", "rcp45", "rcp85"]
    experiment_name="historical"
    # CordexS3Files, CordexSearch clear data before new experiment apply
    run = batch_file_upload(user_token,experiment_name)
    print(run)
