from .models import CordexResponseTracker, requestTracker
from adpApp.models import UkcpResponseTracker
import datetime
import boto3
from django.forms.models import model_to_dict

class RequestUtils:
    
    @staticmethod
    def cordex_request(request):
        """ 
        store request to the cordex response
        tracker
        """
        project_name=request.data["project_name"]
        job_code=request.data["job_code"]
        datasource=request.data["datasource"]
        region_long_name=request.data["region"]["name"]
        lat = request.data["region"]["lat"]
        lon = request.data["region"]["lon"]
        country=request.data["country"]
        main_variable=request.data["main_variable"]
        variable_long_name=request.data["variable"]
        from_date = request.data["projectional_from_year"]
        to_date = request.data["projectional_to_year"]
        his_from_date = request.data["historical_from_year"]
        his_to_date = request.data["historical_to_year"]
        threshold_value_less_than=request.data["threshold_value_less_than"]
        threshold_value_more_than=request.data["threshold_value_more_than"]
        user_name_request_user = request.user.username
        if "gcmDrivingModel" in  request.data.keys():
            gcmDrivingModel=request.data["gcmDrivingModel"]
            institution=request.data["institution"]
            rcmModel=request.data["rcmModel"]
            region_short=request.data["region_short"]
        else:
            gcmDrivingModel=None
            institution=None
            rcmModel=None
            region_short=None

        current_item_status_date=datetime.datetime.now()
        add_data_to_db=CordexResponseTracker(user_name=user_name_request_user,
                        datasource=datasource,his_from_date=his_from_date,
                        his_to_date=his_to_date,
                        current_item_status_date=current_item_status_date,
                        project_name=project_name,job_code=job_code,
                        region_long_name=region_long_name,lat=lat,lon=lon,
                        country=country,main_variable=main_variable,
                        variable_long_name=variable_long_name,
                        from_date=from_date,to_date=to_date,
                        threshold_value_less_than=threshold_value_less_than,
                        threshold_value_more_than=threshold_value_more_than,
                        gcmDrivingModel=gcmDrivingModel,institution=institution , rcmModel=rcmModel,region_short=region_short,
                        status="pending")
        add_data_to_db.save()
        return CordexResponseTracker.objects.latest("id")
    
    @staticmethod
    def ukcp_request(request):
        """ 
        store request to the cordex response
        tracker
        """
        # backend mapping to get variable short form
        backend_var = {"Maximum temperature":"tasmax",
         "Average daily temperature":"tas",
         "Average daily maximum temperature":"tasmax",
         "Average daily minimum temperature":"tasmin",
         "Temperature threshold more than":"tasmax",
         "Temperature threshold less than":"tasmin",
         "Average summer temperature":"tas",
         "Average winter temperature":"tas",
         "Average summer maximum temperature":"tasmax",
         "Average winter minimum temperature":"tasmin",
         "Average daily precipitation":"pr",
         "Precipitation threshold more than":"pr",
         "Precipitation threshold less than":"pr",
         "Maximum 1 day rainfall":"pr",
         "Maximum 5 day rainfall":"pr",
         "Consecutive dry days":"pr",
         "Average summer precipitation": "pr",
         "Average winter precipitation": "pr"}
        # ui mapping to get backend named variables
        ui_var = {"Maximum temperature":"Maximum temperature",
         "Average annual temperature":"Average daily temperature",
         "Average annual daily maximum temperature":"Average daily maximum temperature",
         "Average annual daily minimum temperature":"Average daily minimum temperature",
         "Temperature threshold more than":"Temperature threshold more than",
         "Temperature threshold less than":"Temperature threshold less than",
         "Average summer temperature":"Average summer temperature",
         "Average winter temperature":"Average winter temperature",
         "Average summer daily maximum temperature":"Average summer maximum temperature",
         "Average winter daily minimum temperature":"Average winter minimum temperature",
         "Average annual precipitation":"Average daily precipitation",
         "Precipitation threshold more than":"Precipitation threshold more than",
         "Precipitation threshold less than":"Precipitation threshold less than",
         "Maximum 1 day rainfall":"Maximum 1 day rainfall",
         "Maximum 5 day rainfall":"Maximum 5 day rainfall",
         "Consecutive dry days":"Consecutive dry days",
         "Average summer precipitation":"Average summer precipitation",
         "Average winter precipitation":"Average winter precipitation"}
        back_var = ui_var[request.data["variable_long_name"][0]]
        variable = backend_var[back_var]
        param = RequestUtils.get_param(request, back_var, variable)
        dataset_id = RequestUtils.get_dataset_id(request)
        add_data_to_db=UkcpResponseTracker(user_name=request.user.username,
                    project_name = request.data['project_name'],
                    project_code=request.data["project_code"],
                    consumer_id=request.data["consumer_id"],
                    datasource = request.data['datasource'],
                    region_long_name = request.data["region"]["name"],
                    region ="EUR",
                    country = request.data["country"],
                    dataset_id=dataset_id,
                    dataset_version="1.0",
                    geometry_type="Point",
                    lat=request.data["region"]["lat"],
                    lon=request.data["region"]["lon"],
                    main_variable=request.data["main_variable"],
                    variable_long_name=[back_var],
                    variable=variable,
                    future_from_date = request.data["projectional_from_date"],
                    future_to_date = request.data["projectional_to_date"],
                    his_from_date = request.data["historical_from_date"],
                    his_to_date = request.data["historical_to_date"],
                    current_item_status_date=datetime.datetime.now(),
                    param= param,
                    threshold_value_less_than = request.\
                                            data["threshold_value_less_than"],
                    threshold_value_more_than = request.\
                                            data["threshold_value_more_than"],
                    hist_file_download_status = "pending",
                    future_file_download_status = "pending",
                    status="pending")
        add_data_to_db.save()
        return UkcpResponseTracker.objects.latest("id")
    
    @staticmethod
    def get_dataset_id(request):
        """
        getting adp dataset id
        """
        dataset_id = None
        if request.data['datasource'].lower() == "ukcp18-prob":
            dataset_id = "ukcp18"
        elif request.data['datasource'].lower() == "ukcp18-high-res":
            dataset_id = "ukcp18"
        elif request.data['datasource'].lower() == "haduk":
            if request.data['resolution'].lower() == "5km":
                dataset_id = "haduk_5km"    
            elif request.data['resolution'].lower() == "12km":
                dataset_id = "haduk_12km"
            elif request.data['resolution'].lower() == "25km":
                dataset_id = "haduk_25km"
        elif request.data['datasource'].lower() == "loca":
            dataset_id = "loca"
        return dataset_id
    
    @staticmethod
    def get_param(request, back_var, variable):
        """
        getting neccessary params for request body
        """
        param = list()
        # ukcp18-prob
        if (request.data['datasource'].lower() == "ukcp18-prob")\
            and ("winter" in back_var or "summer" in back_var):
            param.append("collection-land-prob")
            param.extend(['frequency-seas','scenario-all'])
            variable = f"{variable}Anom"
        elif request.data['datasource'].lower() == "ukcp18-prob":
            param.append("collection-land-prob")
            param.extend(['frequency-ann','scenario-all'])
            variable = f"{variable}Anom"
        # ukcp18-high-res
        elif (request.data['datasource'].lower() == "ukcp18-high-res") \
            and (request.data['resolution'].lower() == "5km"):
            param.append("collection-land-cpm")
            param.extend(["frequency-day", "scenario-all"])  
        elif (request.data['datasource'].lower() == "ukcp18-high-res") \
            and (request.data['resolution'].lower() == "12km"):
            param.append("collection-land-rcm")
            param.extend(["frequency-day", "scenario-all"])
        # haduk
        elif (request.data['datasource'].lower() == "haduk") \
            and (variable in ("tas")):
            param.append("collection-all")
            param.extend(["frequency-mon", "scenario-all"])
        elif (request.data['datasource'].lower() == "haduk") \
            and (variable in ("tasmin","tasmax","pr")):
            param.append("collection-all")
            param.extend(["frequency-day", "scenario-all"])   
        # loca
        elif (request.data['datasource'].lower() == "loca"):
            #TODO: need to change collection-miroc5 to collection-all in the deployment
            param.append("collection-miroc5")
            param.extend(["frequency-day", "scenario-all"])
        return param

class CsvDownload():
    """ 
    Download time series and per model
    csv data
    """
    @staticmethod
    def extract(data_type,primary_key):
        """
        extract aws signed url

        Args:
            data_type (str): s3 folder of a raw data or per model data
            primary_key (int): primary key of a folder
        """
        signed_url = []
        s3 = boto3.resource('s3')
        my_bucket = s3.Bucket('kliima-raw-data')
        file_name=[]
        for object_summary in \
        my_bucket.objects.filter(Prefix="{}/Cordex/csv/{}/".\
        format(data_type,primary_key)):
            list_objects=object_summary.key
            file_name.append(list_objects)
        s3Client = boto3.client('s3')
        for file_key in file_name:
            file = file_key.split("_")
            file = file[0].split("/")[-1]
            csv_file_url=s3Client.generate_presigned_url('get_object',
                Params = {'Bucket': 'kliima-raw-data', 'Key': file_key}, 
                ExpiresIn = 300)
            signed_url.append(csv_file_url)
        return signed_url
    
class GetHistory():
    """ 
    getting history data
    """
    @staticmethod
    def load(username):
        """
        username (str): user name to filter the records
        """
        request_tracker = requestTracker.objects.filter(user_name = username)
        history = list()
        for req in request_tracker:
            try:
                if req.datasource.lower() == "cordex":
                    model = CordexResponseTracker.objects.get(id = req.tracker_id)
                elif req.datasource.lower() in ("ukcp-prob","ukcp-high-res"):
                    model = UkcpResponseTracker.objects.get(id = req.tracker_id)
                history.append(model_to_dict(model))
            except: pass
        return history

class TableDetails():
    """ 
    GET table details
    """
    @staticmethod
    def cordex(request):
        primary_key=request.GET["id"]
        model_data=CordexResponseTracker.objects.filter(id=primary_key)
        model_to_dict=[model for model in model_data.values()]
        dict_of_values=model_to_dict[0]
        lat = dict_of_values.get('lat')
        lon = dict_of_values.get('lon')
        region_long_name = dict_of_values.get('region_long_name')
        regions_dict={"Africa":["AFR-44"],
                    "Antarctica":["ANT-44"],
                    "Artic":["ARC-22","ARC-44"],
                    "Australasia":["AUS-22","AUS-44"],
                    "Central America":["CAM-22","CAM-44"],
                    "Central Asia":["CAS-22","CAS-44"],
                    "East Asia":["EAS-22","EAS-44"],
                    "Europe":["EUR-11","EUR-22","EUR-44"],
                    "Mediterranean":["MED-11"],
                    "Middle East North Africa":["MNA-22","MNA-44"],
                    "North America":["NAM-22","NAM-44"],
                    "South America":["SAM-20","SAM-22","SAM-44"],
                    "South Asia":["WAS-22","WAS-44"],
                    "South East Asia":["SEA-22"]}
        dummy_location = {"value": [None, None]}
        location = [dummy_location]
        for loc in zip(lat,lon):
            loc_dict = dict()
            loc_dict['value'] = list(loc)
            location.append(loc_dict)
        rcp_45_data, rcp_85_data = [],[]
        for data in dict_of_values.get("display_data"):
            rcp45_dict, rcp85_dict= dict(), dict()
            rcp45_dict['region'],rcp85_dict['region'] = data['region'],data['region']
            rcp45_dict['data'], rcp85_dict['data']= [],[]
            for loc_data in data['data']:
                dict_45, dict_85 = dict(),dict()
                key = list(loc_data.keys())[0] 
                rcp_data = loc_data[key]
                rcp_45, rcp_85 = rcp_data['rcp45'],rcp_data['rcp85']
                dict_45['min_model_result_RCP_one'] = rcp_45['min']
                dict_45['mean_model_result_RCP_one'] = rcp_45['mean']
                dict_45['median_model_result_RCP_one'] = rcp_45['median']
                dict_45['max_model_result_RCP_one'] = rcp_45['max']
                dict_85['min_model_result_RCP_one'] = rcp_85['min']
                dict_85['mean_model_result_RCP_one'] = rcp_85['mean']
                dict_85['median_model_result_RCP_one'] = rcp_85['median']
                dict_85['max_model_result_RCP_one'] = rcp_85['max']
                rcp45_dict['data'].append(dict_45)
                rcp85_dict['data'].append(dict_85)
            rcp_45_data.append(rcp45_dict)
            rcp_85_data.append(rcp85_dict)
        # adding N/A for missing resolutions
        if len(rcp_45_data) != len(regions_dict[region_long_name]):
            resolutions = []
            for resol in rcp_45_data:
                resolutions.append(resol['region'])
            for re in regions_dict[region_long_name]:
                if re in resolutions:
                    pass
                else:
                    null_data = [{
                    "region": re,
                    "data": [{
                            "min_model_result_RCP_one": "N/A",
                            "mean_model_result_RCP_one": "N/A",
                            "median_model_result_RCP_one": "N/A",
                            "max_model_result_RCP_one": "N/A"
                        }]*len(rcp_45_data[0]['data'])
                        }]
                    rcp_45_data.extend(null_data)
                    rcp_85_data.extend(null_data)
        if ("threshold" in dict_of_values.get("variable_long_name")[0].\
        lower()) and (dict_of_values.get("threshold_value_less_than") != 0):
            main_table_name = "Change in "+ \
                str(dict_of_values.get("variable_long_name")[0])+" "+\
            f'({str(dict_of_values.get("threshold_value_less_than"))})'+ " "+\
                str(dict_of_values.get("from_date"))+"-"+\
                str(dict_of_values.get("to_date"))
        elif ("threshold" in dict_of_values.get("variable_long_name")[0]\
        .lower()) and (dict_of_values.get("threshold_value_more_than") != 0):
            main_table_name = "Change in "+ \
                str(dict_of_values.get("variable_long_name")[0])+" "+\
            f'({str(dict_of_values.get("threshold_value_more_than"))})'+ " "+\
                str(dict_of_values.get("from_date"))+"-"+\
                str(dict_of_values.get("to_date"))
        else:
            main_table_name = "Change in "+ \
                str(dict_of_values.get("variable_long_name")[0])+" "+\
                str(dict_of_values.get("from_date"))+"-"+\
                str(dict_of_values.get("to_date"))
        results={
            "project_name": dict_of_values.get("project_name"),
            "job_number": dict_of_values.get("job_code"),
            "requested_time": dict_of_values.get("current_item_status_date"),
            "data_source": dict_of_values.get("datasource"),
            "modelsUsed": dict_of_values.get("model_used"),
            "region_name": dict_of_values.get("region_long_name"),
            "country_name": dict_of_values.get("country"),
            "selected_latlon": "("+str(len(location)-1)+" Selected)",
            "variable": dict_of_values.get("main_variable"),
            "rx5": dict_of_values.get("variable_long_name"),
            "historical_Period": str(dict_of_values.get("his_from_date"))+"-"+\
                                str(dict_of_values.get("his_to_date")),
            "future_period": str(dict_of_values.get("from_date"))+"-"+\
                            str(dict_of_values.get("to_date")),
            "main_table_name": main_table_name,
            "table_structure_location": {
                "name": "Location",
                "header": ["Lat / Lon"],
                "row": location*len(rcp_45_data)
            },
            "table_data": [
                {
                "table_structure": {
                    "name": "RCP 4.5",
                    "headerName": [
                    "Min Model Result",
                    "Mean Model Result",
                    "Median Model Result",
                    "Max Model Result"
                    ],
                    "row": rcp_45_data
                }
                },
                {
                "table_structure": {
                    "name": "RCP 8.5",
                    "headerName": [
                    "Min Model Result",
                    "Mean Model Result",
                    "Median Model Result",
                    "Max Model Result"
                    ],
                    "row": rcp_85_data
                }
                }
            ]
            }
        return results
    
    @staticmethod
    def ukcp(request):
        primary_key=request.GET["id"]
        model=UkcpResponseTracker.objects.get(id=primary_key)
        model = model_to_dict(model)
        dummy_location = {"value": [None, None]}
        location = [dummy_location]
        for loc in zip(model.get('lat'),model.get('lon')):
            loc_dict = dict()
            loc_dict['value'] = list(loc)
            location.append(loc_dict)
        rcp_45_data, rcp_85_data = [],[]
        data = model.get("display_data")
        rcp45_dict, rcp85_dict= dict(), dict()
        rcp45_dict['region'],rcp85_dict['region'] = "EUR", "EUR"
        rcp45_dict['data'], rcp85_dict['data']= [],[]
        for key in data.keys():
            dict_45, dict_85 = dict(),dict()
            rcp_data = data[key]
            rcp_85 = rcp_data['rcp85']
            dict_45['min_model_result_RCP_one'] = "N/A"
            dict_45['mean_model_result_RCP_one'] = "N/A"
            dict_45['median_model_result_RCP_one'] = "N/A"
            dict_45['max_model_result_RCP_one'] = "N/A"
            dict_85['min_model_result_RCP_one'] = rcp_85['min']
            dict_85['mean_model_result_RCP_one'] = rcp_85['mean']
            dict_85['median_model_result_RCP_one'] = rcp_85['median']
            dict_85['max_model_result_RCP_one'] = rcp_85['max']
            rcp45_dict['data'].append(dict_45)
            rcp85_dict['data'].append(dict_85)
        rcp_45_data.append(rcp45_dict)
        rcp_85_data.append(rcp85_dict)
        from_hist_year = model.get("his_from_date").split("-")[0]
        to_hist_year = model.get("his_to_date").split("-")[0]
        from_proj_year = model.get("future_from_date").split("-")[0]
        to_proj_year = model.get("future_to_date").split("-")[0]
        if ("threshold" in model.get("variable_long_name")[0].lower()) and \
            (model.get("threshold_value_less_than") != 0):
            main_table_name = "Change in "+ \
                str(model.get("variable_long_name")[0])+" "+\
                f'({str(model.get("threshold_value_less_than"))})'+ " "+\
                str(from_proj_year)+"-"+\
                str(to_proj_year)
        elif ("threshold" in model.get("variable_long_name")[0].lower()) and \
            (model.get("threshold_value_more_than") != 0):
            main_table_name = "Change in "+ \
                str(model.get("variable_long_name")[0])+" "+\
                f'({str(model.get("threshold_value_more_than"))})'+ " "+\
                str(from_proj_year)+"-"+\
                str(to_proj_year)
        else:
            main_table_name = "Change in "+ \
                str(model.get("variable_long_name")[0])+" "+\
                str(from_proj_year)+"-"+\
                str(to_proj_year)
        results={
            "project_name": model.get("project_name"),
            "job_number": model.get("project_code"),
            "requested_time": model.get("current_item_status_date"),
            "data_source": model.get("datasource"),
            "modelsUsed": [],
            "region_name": model.get("region_long_name"),
            "country_name": model.get("country"),
            "selected_latlon": "("+str(len(location)-1)+" Selected)",
            "variable": model.get("main_variable"),
            "rx5": model.get("variable_long_name"),
            "historical_Period": str(from_hist_year)+"-"+\
                                str(to_hist_year),
            "future_period": str(from_proj_year)+"-"+\
                            str(to_proj_year),
            "main_table_name": main_table_name,
            "table_structure_location": {
                "name": "Location",
                "header": ["Lat / Lon"],
                "row": location*len(rcp_45_data)
            },
            "table_data": [
                {
                "table_structure": {
                    "name": "RCP 4.5",
                    "headerName": [
                    "Min Model Result",
                    "Mean Model Result",
                    "Median Model Result",
                    "Max Model Result"
                    ],
                    "row": rcp_45_data
                }
                },
                {
                "table_structure": {
                    "name": "RCP 8.5",
                    "headerName": [
                    "Min Model Result",
                    "Mean Model Result",
                    "Median Model Result",
                    "Max Model Result"
                    ],
                    "row": rcp_85_data
                }
                }
            ]
            }
        return results