from rest_framework import serializers
from .models import CordexWgetData,CordexParametersList,\
                    CordexSearch,GeoFileUpload,CordexS3Files,\
                    CordexResponseTracker, requestTracker

class CordexWgetDataSerializer(serializers.ModelSerializer):
    """ 
    Validating the Cordex Wget Data Model fields
    """
    class Meta:
        model = CordexWgetData
        fields = "__all__"

class CordexParametersListSerializer(serializers.ModelSerializer):
    """ 
    Validating the CordexParametersList Model fields
    """
    class Meta:
        model = CordexParametersList
        fields = "__all__"

class CordexSearchSerializer(serializers.ModelSerializer):
    """ 
    Validating the CordexSearch Model fields
    """
    class Meta:
        model = CordexSearch
        fields = "__all__"


class CordexResponseTrackerSerializer(serializers.ModelSerializer):
    """ 
    Validating the CordexResponseTracker Model fields
    """
    class Meta:
        model = CordexResponseTracker
        fields = "__all__"

class GeoFileUploadSerializer(serializers.ModelSerializer):
    """ 
    Validating the GeoFileUpload Model fields
    """
    geo_file = serializers.FileField(max_length= None,use_url= True)

    class Meta:
        model = GeoFileUpload
        fields = "__all__"

class CordexS3FilesSerializer(serializers.ModelSerializer):
    """ 
    Validating the CordexS3Files Model fields
    """
    class Meta:
        model = CordexS3Files
        fields = "__all__"
        
class requestTrackerSerializer(serializers.ModelSerializer):
    """ 
    Validating the requestTrackerSerializer Model fields
    """
    class Meta:
        model = requestTracker
        fields = "__all__"
