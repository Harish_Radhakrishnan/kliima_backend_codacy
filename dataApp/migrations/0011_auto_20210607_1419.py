# Generated by Django 3.2 on 2021-06-07 14:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dataApp', '0010_cordexresponsetracker_datasource'),
    ]

    operations = [
        migrations.AddField(
            model_name='cordexresponsetracker',
            name='his_from_date',
            field=models.IntegerField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='cordexresponsetracker',
            name='his_to_date',
            field=models.IntegerField(default=None, null=True),
        ),
    ]
