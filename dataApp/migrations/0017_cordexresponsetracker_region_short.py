# Generated by Django 3.2 on 2022-11-04 12:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dataApp', '0016_auto_20221104_0939'),
    ]

    operations = [
        migrations.AddField(
            model_name='cordexresponsetracker',
            name='region_short',
            field=models.CharField(default=None, max_length=100, null=True),
        ),
    ]
