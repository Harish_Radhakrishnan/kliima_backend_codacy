import requests
import json

def batch_file_upload(user_token,experiment_name):
    status_of_upload={}
    file_upload_status=[]
    domain = "https://18.133.109.38"
    headers = {
                'Content-Type': 'application/json',
                'Authorization':'{}'.format(user_token)
                }
    #region=[ "SEA-22"]
    #variable = ['tasmax']
    region=["AFR-22", "AFR-44",  "ANT-44",  "ARC-22", "ARC-44",  "AUS", "AUS-22", "AUS-44", "CAM-22", "CAM-44",  "CAS-22", "CAS-44", "EAS-22", "EAS-44",  "EUR-11", "EUR-22", "EUR-25", "EUR-44", "MED-11", 
    "MNA-22",  "MNA-44",  "NAM-11", "NAM-22", "NAM-44",  "SAM-20", "SAM-22", "SAM-44", "SEA-22", "WAS-22", "WAS-44"]
    variable=["tasmax","tas","tasmin","pr"]
    if experiment_name !="historical":
        for region_list in region:
            for variable_list in variable:
                for batch in range(2006,2100,5):
                    cordex_parameters={"region":{
                        "name":region_list,
                        },
                        "variable":[variable_list],
                        "datasource":"CORDEX",
                        "from_date":batch,
                        "to_date":batch+4,
                        "experiment":experiment_name
                    }
                    data = json.dumps(cordex_parameters)

                    batch_upload = requests.post(domain+"/batch-file-upload",data=data,headers = headers)
                    file_upload_status.append(batch_upload)
                    print(batch,batch+5)
    else:
        for region_list in region:
            for variable_list in variable:
                for batch in range(1976,2005,5):
                    cordex_parameters={"region":{
                        "name":region_list,
                        },
                        "variable":[variable_list],
                        "datasource":"CORDEX",
                        "from_date":batch,
                        "to_date":batch+4,
                        "experiment":experiment_name
                    }
                    
                    data = json.dumps(cordex_parameters)

                    batch_upload = requests.post(domain+"/batch-file-upload",data=data,headers = headers)
                    #batch_upload = requests.post(domain+"/batch-file-upload",data=data,headers = headers)
                    file_upload_status.append(batch_upload)
                    print(batch,batch+5)
                    
    status_of_upload["status_of_upload"]=file_upload_status
    message={"status":status_of_upload}
    return message
user_token='Token 53b1e7335fb3480ac8bf9843a667e78l6BAAUw'
#["historical", "rcp45", "rcp85"]
experiment_name="rcp85"
# CordexS3Files, CordexSearch clear data before new experiment apply
run=batch_file_upload(user_token,experiment_name)
print(run)