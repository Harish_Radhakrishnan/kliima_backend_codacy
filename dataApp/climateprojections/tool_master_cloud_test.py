import subprocess
import os
import time
import joblib 
from joblib import Parallel,delayed
import argparse
from django.conf import settings
from  .tool_master_cloud import processSingleLocation
import multiprocessing as mp
from multiprocessing import Process, Queue,freeze_support
import pandas as pd
import boto3
from boto3.s3.transfer import S3Transfer
from dataApp import logger
from arup.utils import LogUtils


# custom functions

def raw_file_generation(csv_folder):
    raw_data_dict = {}
    directory_files = [file for file in os.listdir(csv_folder) \
                        if file.startswith("raw-data")]
    for file in directory_files:
        if file != 'raw-data.csv':
            key = file[:-4].split("_")
            lat = key[1]
            lon = key [2]
            path = csv_folder+'/'+file
            key = lat+"/"+lon
            raw_data_dict[key] = pd.read_csv(path)
            if 'Unnamed: 0' in list(raw_data_dict[key].columns):
                raw_data_dict[key].drop('Unnamed: 0',axis=1,inplace=True)
                raw_data_dict[key].columns = [lat+"/"+lon+"_"+column for column in raw_data_dict[key].columns]
        else:
            pass
    print("raw data in tool cloud test ---------------->",
          list(raw_data_dict.keys()))
    list_df = [raw_data_dict[key] for key in list(raw_data_dict.keys())]
    data = pd.concat(list_df,axis=1)
    for file in os.listdir(csv_folder):
        key = file.split(".")[0]
        path = csv_folder+'/'+file
        os.remove(path)
    print("raw data in tool cloud ----------------> completed")
    data.to_csv(csv_folder+'/raw-data.csv')

def run_tool_master(user_region_,user_var_,history_from,history_to,future_from,
                    future_to,primary_key,threshold_value_less_than,
                    threshold_value_more_than,lat,lon,check_value, request):
    error="no error"
    raw_csv_folder = settings.RAW_DATA_FILE_PATH
    per_model_csv_path = settings.PER_MODEL_DATA_FILE_PATH
    try:
        logger.info(LogUtils.\
            load_message(**request.log_utils,message="exist cordex approch started"))
        start=time.time()
        threshold=[]
        value={}
        if threshold_value_less_than != 0:
            threshold.append(threshold_value_less_than)
        elif threshold_value_more_than != 0:
            threshold.append(threshold_value_more_than)
        threshold_val= str(threshold[0] if len(threshold) !=0 else "")
        #output_format,table_output=processSingleLocation(user_region_,user_var_,history_from,history_to,future_from,future_to,threshold_val,lat,lon)
        search_result = Parallel(n_jobs=-1, backend="sequential")(map(delayed(processSingleLocation),[user_region_]*len(lat),[user_var_]*len(lat),[history_from]*len(lat),[history_to]*len(lat),[future_from]*len(lat),[future_to]*len(lat),[threshold_val]*len(lat),lat,lon))
        output_format=[i[0] for i in search_result]
        table_output=[i[1] for i in search_result]
        print(output_format ,"output_format *****************")
        print(table_output ,'table_output *************')
        print(search_result ,'search_result ************')
        raw_file_generation(raw_csv_folder)
        logger.info(LogUtils.\
            load_message(**request.log_utils,
                         message="raw data has been generated"))
        raw_file_generation(per_model_csv_path)
        logger.info(LogUtils.\
            load_message(**request.log_utils,
                         message="per model data has been generated"))
        client = boto3.client('s3')
        transfer = S3Transfer(client)
        transfer.upload_file(raw_csv_folder+"raw-data.csv", "kliima-raw-data","raw-data-dev/Cordex/csv/{}/{}_{}_{}_{}_{}_{}.csv".format(primary_key,check_value,user_var_,history_from,history_to,future_from,future_to))
        transfer.upload_file(per_model_csv_path+"raw-data.csv", "kliima-raw-data","per-model-data-dev/Cordex/csv/{}/{}_{}_{}_{}_{}_{}.csv".format(primary_key,check_value,user_var_,history_from,history_to,future_from,future_to))

    except Exception as exception:
        error = "{}:{}".format(type(exception).__name__,exception)
        logger.exception(LogUtils.\
            load_message(**request.log_utils,message=error))
        print(error, 'error of **************')
        output_format,table_output=[0],[0]
    if table_output[0]==0 or output_format[0] ==0:
        error="internal no matching netcdf files"
        logger.info(LogUtils.\
            load_message(**request.log_utils,
                         message=error))
    return output_format,table_output,error


# def run_tool_master(user_region_,user_var_,history_from,history_to,future_from,future_to,primary_key,threshold_value_less_than,threshold_value_more_than,lat,lon):
#     start=time.time()
#     threshold=[]
#     value={}
#     if threshold_value_less_than != 0:
#         threshold.append(threshold_value_less_than)
#     elif threshold_value_more_than != 0:
#         threshold.append(threshold_value_more_than)
#     threshold_val= str(threshold[0] if len(threshold) !=0 else "")
#     #output_format,table_output=processSingleLocation(user_region_,user_var_,history_from,history_to,future_from,future_to,threshold_val,lat,lon)
#     search_result = Parallel(n_jobs=-1, backend="sequential")(map(delayed(processSingleLocation),[user_region_]*len(lat),[user_var_]*len(lat),[history_from]*len(lat),[history_to]*len(lat),[future_from]*len(lat),[future_to]*len(lat),[threshold_val]*len(lat),lat,lon))
#     print("search result - tool master test {} --------------------------  ".format(search_result))
#     output_format=[i[0] for i in search_result]
#     table_output=[i[1] for i in search_result]
#     print("output format - tool master test {} --------------------------  ".format(output_format))
#     print("table_output - tool master test {} --------------------------  ".format(table_output))
#     return output_format,table_output

# if __name__ == '__main__':
#     freeze_support()
#     user_region_='NAM'
#     user_var_='RX5DAY'
#     history_from=2001
#     history_to=2005
#     future_from=2036
#     future_to=2040
#     lat=[33.06,32.08]
#     lon=[-80.04,-80.05]
#     primary_key=8
#     threshold_value_less_than=0
#     threshold_value_more_than=0

#     output_format,table_output=run_tool_master(user_region_,user_var_,history_from,history_to,future_from,future_to,primary_key,threshold_value_less_than,threshold_value_more_than,lat,lon)

#     print(table_output)

