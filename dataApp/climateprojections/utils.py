# -*- coding: utf-8 -*-
import argparse
import os

# from tool_findregion import find_region


def parse_args(args):
    output_bucket = 'klima-datasets'
    target_folder = 'Cordex'
    output_file = 'benchmarktest.txt'

    parser = argparse.ArgumentParser()
       # TODO - use ccvari from below to derive lower path - this should just be download/smoketest
    parser.add_argument("-dd", "--dataRoot", help="Path to top level data directory",
                        default='download/benchmark')


    parser.add_argument("-b", "--bucket", help="S3 bucket", default=output_bucket)
    parser.add_argument("-f", "--target_folder", help="Folder within the bucket", default=target_folder)
    parser.add_argument("-i", "--indicator", help="Indicator for analysing data", choices=['RX1DAY', 'RX5DAY', 'TXX','THRESHOLD', 'CDD','THRESHOLD MORE THAN', "THRESHOLD LESS THAN","AVERAGE PRECIPITATION",'AVERAGE TEMPERATURE','AVERAGE MAXIMUM TEMPERATURE','AVERAGE MINIMUM TEMPERATURE'], default='CDD')
    parser.add_argument("-r", "--region", help="Geographic Region", choices=['NAM','EUR-22','AFR-22'], default='NAM')
    parser.add_argument("-hs", "--historicalstart", help="historical start year", default="2001")
    parser.add_argument("-he", "--historicalend", help="historical end year", default="2005")
    parser.add_argument("-fs", "--futurestart", help="future start year", default="2036")
    parser.add_argument("-fe", "--futureend", help="future end year", default="2040")
    parser.add_argument("-la", "--latitude", help="latitude", default="33.06")
    parser.add_argument("-lo", "--longitude", help="longitude", default="-80.04")
    parser.add_argument("-th", "--threshold", help="Threshold", default="32.2")

    return parser.parse_args(args)


def print_file_list(file_root):
    i = 0
    for root, subFolder, files in os.walk(file_root):
        for item in files:
            print(str(os.path.join(root, item)))
            i += 1
    print("read {0} files".format(i))


def getClimateVariable(ccvarinput):
    tasmax = 'tasmax'
    pr = 'pr'
    tas = 'tas'
    default="Variable not recognised"

    climateVariables ={
        'TXX': tasmax,
        'THRESHOLD': tasmax,
        'RX1DAY': pr,
        'RX5DAY': pr,
        'CDD': pr,
        "AVERAGE TEMPERATURE": tas,
        "AVERAGE MAXIMUM TEMPERATURE": tasmax,
        "AVERAGE PRECIPITATION": pr,
    }
    ccvari = climateVariables.get(ccvarinput, default)
    return ccvari


def getDownloadLocation(root, climate_variable):
    return os.path.join(root,climate_variable)

def getCORDEXLocation(root, climate_variable):
    return os.path.join(getDownloadLocation(root, climate_variable),"CORDEX")

def get_region_location(root, climate_variable,region):
    return os.path.join(getCORDEXLocation(root, climate_variable),region)


def getDependentVariables(ccvarinput):
    ccvari = ""
    climatevariable = ""
    thresholdMode = ""
    query_period = ""

    GreaterThan = 'GreaterThan'

    if ccvarinput == "TXX" or ccvarinput == "AVERAGE MAXIMUM TEMPERATURE":
        ccvari = 'tasmax'
        climatevariable = ccvarinput
        thresholdMode = 'GreaterThan'
        query_period = "year"
    elif ccvarinput == "RX1DAY" or ccvarinput == "RX5DAY" or ccvarinput == "CDD" or ccvarinput == "AVERAGE PRECIPITATION":
        ccvari = 'pr'
        climatevariable = ccvarinput
        thresholdMode = 'GreaterThan'
        query_period = "year"
    elif ccvarinput == "AVERAGE MINIMUM TEMPERATURE":
        ccvari = 'tasmin'
        climatevariable = ccvarinput
        thresholdMode = 'GreaterThan'
        query_period = "year"
    elif ccvarinput == "AVERAGE TEMPERATURE":
        ccvari = 'tas'
        climatevariable = ccvarinput
        thresholdMode = 'GreaterThan'
        query_period = "year"
    elif ccvarinput == "THRESHOLD LESS THAN":
        ccvari = 'tasmin'
        climatevariable = "THRESHOLD"
        thresholdMode = 'LessThan'
        query_period = "all"
    elif ccvarinput == "THRESHOLD MORE THAN":
        ccvari = 'tasmax'
        climatevariable = "THRESHOLD"
        thresholdMode = 'GreaterThan'
        query_period = "all"
    else:
        print("Variable not recognised")

    return ccvari, climatevariable, thresholdMode, query_period


# def buildArgs(staticArgs, latLonList, resolution):
#     args=[]

#     for lat, lon in latLonList:
#         theseArgsDict = staticArgs.copy()
#         lat, lon, region = getRegionsForLatLons(lat,lon,resolution)
#         print(f"{lat}, {lon}, {region}")
#         theseArgsDict["latitude"]=lat
#         theseArgsDict["longitude"]=lon
#         theseArgsDict["region"]=region
#         theseArgs = convertArgDictToArray(theseArgsDict)
#         args.append(theseArgs)

#     return args

# def buildParsedArgs(staticArgs, latLonList, resolution):
#     return list(map(parse_args,(buildArgs(staticArgs, latLonList, resolution))))

# def getRegionsForLatLons(lat, lon, resolution):
#     region, num = find_region(lat, lon)
#     regionResultion = f"{region}-{resolution}"
#     return (lat, lon, regionResultion)

# def convertArgDictToArray(argDict):
#     argArray = []
#     for k,v in argDict.items():
#         argArray.append(f"--{k}")
#         argArray.append(v)
#     return argArray

# def getLatLonTupleArray(latLons):
#     return list(map(lambda latLon: (latLon.split(",")[0].strip(), latLon.split(",")[1].strip()), latLons.split("_")))


# def splitLatLon(latLon):
#     return latLon.split(",")[0].strip(), latLon.split(",")[1].strip()