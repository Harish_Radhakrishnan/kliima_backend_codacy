# -*- coding: utf-8 -*-
from .utils import parse_args, print_file_list, getClimateVariable, getDependentVariables, getCORDEXLocation,get_region_location
from django.conf import settings
import os
#os.chdir("C:\\Users\\Guneet.Kohli\\GitLab\\climate-projections")
import json
import sys
import os
import numpy as np
import itertools
#import dill # pickling
from tqdm import tqdm # progresss bars
from copy import deepcopy # to deepcopy dictionaries
import argparse
# other imports
#import fileHandling # fileHandling functions developed for barclays, including a number of functions to process the input files 
#from ATR_utilities.infrastructure.file_path_holder import MultiSystemFilePath # allows the same file paths to be used on windows and the clu>

# projections package imports
from .climate_projections import io
from .climate_projections import data
from .climate_projections import calc_etccdi_indices as etccdi
from .climate_projections import other_rot_grid as rotgrid
from .climate_projections import utilities as util
from .climate_projections import general as gen

from multiprocessing import Process, Queue,freeze_support
import asyncio
import concurrent.futures
from argparse import Namespace
import pandas as pd

#custom functions

def csv_length_verify(dict_):
    keys = list(dict_.keys())
    len_ = [dict_[i].shape[0] for i in list(dict_.keys())]
    max_ = np.argmax(len_)
    max_count = len_[max_]
    keys.pop(max_)
    for key in keys: 
        diff = max_count - dict_[key].shape[0]
        dummy_array = np.array([None]*diff)
        dict_[key] = np.hstack((dict_[key],dummy_array))   
    return dict_

def raw_file_generation(csv_folder,lat,lon):
    raw_data_dict = {}
    for file in os.listdir(csv_folder):
        key = file.split(".")[0]
        path = csv_folder+'/'+file
        if key in ['hist','rcp45','rcp85']:
            raw_data_dict[key] = pd.read_csv(path)
            if 'Unnamed: 0' in list(raw_data_dict[key].columns):
                raw_data_dict[key].drop('Unnamed: 0',axis=1,inplace=True)
                raw_data_dict[key].columns = [key+"_"+column for column in raw_data_dict[key].columns]
    print("raw data in tool cloud ---------------->",
          list(raw_data_dict.keys()))
    list_df = [raw_data_dict[key] for key in list(raw_data_dict.keys())]
    data = pd.concat(list_df,axis=1)
    for file in os.listdir(csv_folder):
        key = file.split(".")[0]
        if key in ['hist','rcp45','rcp85']:
            path = csv_folder+'/'+file
            os.remove(path)
    print("raw data in tool cloud ----------------> completed")
    data.to_csv(csv_folder+'/raw-data_'+lat+"_"+lon+".csv")

def processMultipleLocations(argsArray, max_workers=10):
    loop = asyncio.get_event_loop()
    future = asyncio.ensure_future(processLocationsAsync(argsArray, max_workers))
    loop.run_until_complete(future)
    responses = future.result()
    return responses

async def processLocationsAsync(argsArray, max_workers):

    processed=0
    locations = len((argsArray))
    results=[]
    metadatas=[]
    with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
        futures = {executor.submit(processSingleLocation, args) for args in argsArray}
        for future in concurrent.futures.as_completed(futures):
            processed +=1
            result, metadata = future.result()
            print (f"processed location {processed}/{locations}")
            results.append(result)
            metadatas.append(metadata)
    return results,metadatas


def processSingleLocation(user_region_,user_var_,history_from,history_to,future_from,future_to,threshold,lat,lon):

    output_bucket = 'klima-datasets'
    target_folder = settings.CORDEX_FILE_PATH
    raw_csv_folder = settings.RAW_DATA_FILE_PATH
    per_model_csv_path = settings.PER_MODEL_DATA_FILE_PATH
    output_file = 'benchmarktest.txt'
    print(lat,type(lat))
    print(history_from,type(history_from))
    # parser = argparse.ArgumentParser()
    #    # TODO - use ccvari from below to derive lower path - this should just be download/smoketest
    # parser.add_argument("-dd", "--dataRoot", help="Path to top level data directory",
    #                     default='Cordex')


    # parser.add_argument("-b", "--bucket", help="S3 bucket", default=output_bucket)
    # parser.add_argument("-f", "--target_folder", help="Folder within the bucket", default=target_folder)
    # parser.add_argument("-i", "--indicator", help="Indicator for analysing data", choices=['RX1DAY', 'RX5DAY', 'TXX','THRESHOLD', 'CDD','THRESHOLD MORE THAN', "THRESHOLD LESS THAN","AVERAGE PRECIPITATION",'AVERAGE TEMPERATURE','AVERAGE MAXIMUM TEMPERATURE','AVERAGE MINIMUM TEMPERATURE'], default=user_var_)
    # parser.add_argument("-r", "--region", help="Geographic Region", choices=['NAM','EUR','AFR'], default=user_region_)
    # parser.add_argument("-hs", "--historicalstart", help="historical start year", default=str(history_from))
    # parser.add_argument("-he", "--historicalend", help="historical end year", default=str(history_to))
    # parser.add_argument("-fs", "--futurestart", help="future start year", default=str(future_from))
    # parser.add_argument("-fe", "--futureend", help="future end year", default=str(future_to))
    # parser.add_argument("-la", "--latitude", help="latitude", default=str(round(lat,4)))
    # parser.add_argument("-lo", "--longitude", help="longitude", default=str(round(lon,4)))
    # parser.add_argument("-th", "--threshold", help="Threshold", default=str(threshold))
    # args=parser.parse_args()
    # print_file_list(args.dataRoot)
    # print(args,type(args))
    # sys.exit()
    # args={"dataRoot":'Cordex',"bucket":output_bucket,"target_folder":target_folder,"indicator":user_var_,"region":user_region_,"historicalstart":str(history_from),"historicalend":str(history_to),
    # "futurestart":str(future_from),"futureend":str(future_to),"latitude":str(round(lat,4)),"longitude":str(round(lon,4)),"threshold":str(threshold)}
    # args=Namespace(args)
    args = Namespace()
    args.dataRoot = settings.CORDEX_DATAROOT_PATH
    args.bucket = output_bucket
    args.target_folder = target_folder
    args.indicator = user_var_
    args.region = user_region_
    args.historicalstart = str(history_from)
    args.historicalend = str(history_to)
    args.futurestart = str(future_from)
    args.futureend = str(future_to)
    args.latitude = str(round(lat,4))
    args.longitude = str(round(lon,4))
    args.threshold = str(threshold)
    print(args,type(args))
    # sys.exit()
    #input parameters. Make sure to check end year as some models only go to 2099 and need to be processed accordingly
    print_file_list(args.dataRoot)
    historicalexperiments=['historical']
    historicalrcp=['rcp85']
    print ("parsed argument_values: {0}".format(vars(args)))

    try:
        myMultiModelDataset_historical, numberyears = getHistoricalData(args, historicalexperiments, historicalrcp)
#        q45 = putFutureYearsScenarioOnQueue(args, historicalexperiments, myMultiModelDataset_historical, numberyears,
#                                            'rcp45')
#        q85= putFutureYearsScenarioOnQueue(args, historicalexperiments, myMultiModelDataset_historical, numberyears,
#                                            'rcp85')
#        model_resultsrcp45 = q45.get()
#        model_resultsrcp85 = q85.get()
        print("Before the model 45 and 85 function")
        model_resultsrcp45 = getFutureYears(args, historicalexperiments,myMultiModelDataset_historical,numberyears,'rcp45')
        print("Only 45 has ran")
        model_resultsrcp85 = getFutureYears(args, historicalexperiments,myMultiModelDataset_historical,numberyears,'rcp85')
       # resultsText = getResultsText(args, model_resultsrcp45, model_resultsrcp85)
        print("after the model 45 and model 85 function")
        resultsText = 'String format Output has been stopped'
        ccvari, climatevariable, thresholdMode, query_period = getDependentVariables(args.indicator)
        print("The arg has been passed")
        all_cordex_data = io.LocalFolderETCCDIDatabase(os.path.join(getCORDEXLocation(args.dataRoot, ccvari), args.region), 'CORDEX')
        print("this has only Cordex Location thing")
        searchResult = all_cordex_data.search()
        print(searchResult,"The search result ************")
#        table_output={"{}/{}".format(str(lat),str(lon)):{"rcp45":{"max": np.max(model_resultsrcp45), "min": np.min(model_resultsrcp45), "mean": np.mean(model_resultsrcp45),"median": np.median(model_resultsrcp45)},
#                    "rcp85":{"max": np.max(model_resultsrcp85), "min": np.min(model_resultsrcp85), "mean": np.mean(model_resultsrcp85),"median": np.median(model_resultsrcp85)}}}
        if model_resultsrcp45==[] and model_resultsrcp85!=[]:
            table_output={"{}/{}".format(str(lat),str(lon)):{"rcp45":{"max": 'No results for RCP45', "min": 'No results for RCP45', "mean": 'No results for RCP45',"median":'No results for RCP45'},
                        "rcp85":{"max": np.max(model_resultsrcp85), "min": np.min(model_resultsrcp85), "mean": np.mean(model_resultsrcp85),"median": np.median(model_resultsrcp85)}}}
        elif model_resultsrcp45!=[] and model_resultsrcp85==[]:
            table_output={"{}/{}".format(str(lat),str(lon)):{"rcp45":{"max": np.max(model_resultsrcp45), "min": np.min(model_resultsrcp45), "mean": np.mean(model_resultsrcp45),"median": np.median(model_resultsrcp45)},
            "rcp85":{"max": 'No results for RCP85', "min": 'No results for RCP85', "mean": 'No results for RCP85',"median":'No results for RCP85'}}}
        elif model_resultsrcp45==[] and model_resultsrcp85==[]:
            table_output={"{}/{}".format(str(lat),str(lon)):{"rcp45":{"max": 'No results for RCP45', "min": 'No results for RCP45', "mean": 'No results for RCP45',"median":'No results for RCP45'},
            "rcp85":{"max": 'No results for RCP85', "min": 'No results for RCP85', "mean": 'No results for RCP85' ,"median":'No results for RCP85'}}}
        elif model_resultsrcp45!=[] and model_resultsrcp85!=[]:
             table_output={"{}/{}".format(str(lat),str(lon)):{"rcp45":{"max": np.max(model_resultsrcp45), "min": np.min(model_resultsrcp45), "mean": np.mean(model_resultsrcp45),"median": np.median(model_resultsrcp45)},"rcp85":{"max": np.max(model_resultsrcp85), "min": np.min(model_resultsrcp85), "mean": np.mean(model_resultsrcp85),"median": np.median(model_resultsrcp85)}}}
        output_format={}
        test_string=str(searchResult).replace('\n','')
        test_string=test_string.replace(' ','')
        searchResult = json.loads(test_string)
        print(type(searchResult),"-------------------------------------------------------------searchResult---------------------------------------")
        output_format["{}/{}".format(str(lat),str(lon))]=[resultsText,searchResult]
        raw_file_generation(raw_csv_folder,args.latitude,args.longitude)
        raw_file_generation(per_model_csv_path,args.latitude,args.longitude)
    except  Exception as exception:
        error_check = "{}:{}".format(type(exception).__name__,exception)
        print(error_check)
        output_format,table_output=0,0

    return output_format,table_output

    # print ("path ----------------------------------------------------------  {}".format(os.path.join(args.dataRoot,user_var_)))
    # myMultiModelDataset_historical, numberyears = getHistoricalData(args, historicalexperiments, historicalrcp)

    # q45 = putFutureYearsScenarioOnQueue(args, historicalexperiments, myMultiModelDataset_historical, numberyears,
    #                                     'rcp45')

    # q85= putFutureYearsScenarioOnQueue(args, historicalexperiments, myMultiModelDataset_historical, numberyears,
    #                                     'rcp85')

    # model_resultsrcp45 = q45.get()
    # model_resultsrcp85 = q85.get()

    # resultsText = getResultsText(args, model_resultsrcp45, model_resultsrcp85)

    # ccvari, climatevariable, thresholdMode, query_period = getDependentVariables(args.indicator)
    # all_cordex_data = io.LocalFolderETCCDIDatabase(os.path.join(getCORDEXLocation(args.dataRoot, ccvari), args.region), 'CORDEX')

    # searchResult = all_cordex_data.search()
    
    # print ("search_result ---------------------------------------------- {}".format(searchResult))

    # table_output={"{}/{}".format(str(lat),str(lon)):{"rcp45":{"max": np.max(model_resultsrcp45), "min": np.min(model_resultsrcp45), "mean": np.mean(model_resultsrcp45)},
    #             "rcp85":{"max": np.max(model_resultsrcp85), "min": np.min(model_resultsrcp85), "mean": np.mean(model_resultsrcp85)}}}
    
    # test_string=str(searchResult).replace('\n','')
    # test_string=test_string.replace(' ','')
    # searchResult = json.loads(test_string)
    # output_format={}
    # output_format["{}/{}".format(str(lat),str(lon))]=[resultsText,searchResult]


    # return output_format,table_output


def putFutureYearsScenarioOnQueue(args, historicalexperiments, myMultiModelDataset_historical, numberyears, scenario):
    try:
        q45 = Queue()
        rcp45= Process(target=getFutureYearsAsync,
                        args=(args, historicalexperiments, myMultiModelDataset_historical, numberyears,
                            scenario, q45))
        rcp45.start()
    except:
        q45=0
    return q45
def getFutureYearsAsync(args, historicalexperiments,myMultiModelDataset_historical,numberyears,scenario_future,q ):
    try:
        model = getFutureYears(args, historicalexperiments,myMultiModelDataset_historical,numberyears,scenario_future)
        return q.put(model)
    except Exception as exception:
        message = "{}:{}".format(type(exception).__name__,exception)
        q.put(message)
        return q.put(model)

# def putFutureYearsScenarioOnQueue(args, historicalexperiments, myMultiModelDataset_historical, numberyears, scenario):
#     q45 = Queue()
#     rcp45 = Process(target=getFutureYearsAsync,
#                     args=(args, historicalexperiments, myMultiModelDataset_historical, numberyears,
#                           scenario, q45))
#     rcp45.start()
#     return q45


# def getFutureYearsAsync(args, historicalexperiments,myMultiModelDataset_historical,numberyears,scenario_future,q ):
#     model = getFutureYears(args, historicalexperiments,myMultiModelDataset_historical,numberyears,scenario_future)
#     q.put(model)

def getFutureYears(args, historicalexperiments,myMultiModelDataset_historical,numberyears,scenario_future):
    futurestartyear = int(args.futurestart)
    futureendyear = int(args.futureend)
    errorDict = []
    # Split the search period into historical and modelled
    historicalYears_future=list(itertools.chain(*[[futurestartyear if futurestartyear<2005 else []], [futureendyear if (futureendyear<2006 and futurestartyear<2005) else 2005 if(futurestartyear<2005) else []]]))
    futureYears_future=list(itertools.chain(*[[futurestartyear if (futurestartyear>2005 and futureendyear>2005) else 2006 if (futureendyear>2006 and futurestartyear<2006) else []],[futureendyear if futureendyear>2006 else []]]))
    mapper2d = util.RegularGridMapperPoints()  # Mapper for interpolation of locations

    model_results= []
    dbt_rcp45 = {}
    dbt_rcp85 = {}
    per_model_hist = {}
    per_model_rcp45 = {}
    per_model_rcp85 = {}
    raw_data_folder = settings.RAW_DATA_FILE_PATH
    per_model_csv_path = settings.PER_MODEL_DATA_FILE_PATH
    ccvari, climatevariable, thresholdMode, query_period = getDependentVariables(args.indicator)
    ccvar = [ccvari]
    region_future = args.region

    all_cordex_data_future = io.LocalFolderETCCDIDatabase(os.path.join(getCORDEXLocation(args.dataRoot, ccvari), args.region),'CORDEX')
    locationsByRegionDict = gen.buildInputDictFromArgs(args)
    inputDict = list(locationsByRegionDict.values())[0][0]

    searchResult = all_cordex_data_future.search(
        scenarios=scenario_future)  ## Note that should prob loop over future experiments

    model_names_future = list(searchResult.models)
    ## END CHANGE ##
    if isinstance(historicalYears_future[1], int) and isinstance(futureYears_future[1], int):
        experiments_future = [historicalexperiments[0], scenario_future]
        experimentname_future = historicalexperiments[0] + scenario_future
    elif not isinstance(historicalYears_future[1], int) and isinstance(futureYears_future[1], int):
        experiments_future = [scenario_future]
        experimentname_future = scenario_future
    ## CHANGE ##
    # Allow for purely historical data
    elif isinstance(historicalYears_future[0], int) and isinstance(historicalYears_future[1], int) and not isinstance(
            futureYears_future[1], int):
        experiments_future = [historicalexperiments[0]]
        experimentname_future = historicalexperiments[0]
    # data stored in dictionaries in prep for multiModelDataset
    dictOfModels_future = {}
    dictOfThresholds_future = {}
    dictOfSites_future = {}
    ## END CHANGE ##
    for my_model_future in tqdm(model_names_future, desc="Processing Models", leave=False):
        # try:
        # Check model has all variables - check is here so that it doesnt unecassary runs
        fileDoesNotExistFlag = False

        for var_future in ccvar:
            for experiment_future in experiments_future:
                try:
                    fileDoesNotExistFlag=False
                    # TODO - took out, doesn't seem to be used
                    #my_model_dataset_future = all_cordex_data_future.get_netcdf_dataset_object_multi(var_future, 'day',   my_model_future, experiment_future)
                except:
                    tqdm.write("Cant find file for:" + str(var_future) + "_day_" + str(my_model_future) + "_" + str(
                        experiment_future))
                    fileDoesNotExistFlag = True
                    break

        if fileDoesNotExistFlag:
            errorDictTemp_future = {}
            errorDictTemp_future["Model"] = my_model_future
            errorDictTemp_future["Scenario"] = scenario_future
            errorDictTemp_future["Region"] = region_future
            errorDict.append(errorDictTemp_future)
            continue

        # extract rotation poles
        my_model_dataset_future = all_cordex_data_future.get_netcdf_dataset_object_multi(ccvar[0], 'day',
                                                                                         my_model_future,
                                                                                         historicalexperiments[0])
        data_all_future = data.NetCDFDatasetRotated(my_model_dataset_future, ccvar[0], "all", "all", "all",
                                                    netcdf_longitudes_span_globe=True,
                                                    climate_variable_name_to_use=ccvar[0],
                                                    assume_equally_spaced_datetimes=True)

        latpole = data_all_future._rotated_grid_north_pole_latitude
        lonpole = data_all_future._rotated_grid_north_pole_longitude

        [rlonTemp2, rlatTemp2] = rotgrid.transform_to_rotated_grid(float(args.longitude), float(args.latitude), lonpole,
                                                                   latpole)
        siteNo_future, lon2, lat2, rlon2, rlat2, = [1], [args.longitude], [args.latitude], [rlonTemp2], [rlatTemp2]

        for numvar_future in range(len(ccvar)):
            # Combining historical and modelled data
            for counter, experiment_future in enumerate(tqdm(experiments_future, desc="Combining data", leave=False)):
                if experiment_future == 'historical':
                    yearArray_future = historicalYears_future
                else:
                    yearArray_future = futureYears_future

                # Get the handle for the dataset for the specific model, variable, time resolution and scenario - check with clare what this does and why its needed
                print ("**********#######################**********************************  - - - {}".format(ccvar[numvar_future]))
                my_model_dataset_future = all_cordex_data_future.get_netcdf_dataset_object_multi(ccvar[numvar_future],
                                                                                                 'day', my_model_future,
                                                                                                 experiment_future)

                data_all_future = data.NetCDFDatasetRotated(
                    my_model_dataset_future, ccvar[numvar_future], "all", "all", "all",
                    netcdf_longitudes_span_globe=True, climate_variable_name_to_use=ccvar[numvar_future],
                    assume_equally_spaced_datetimes=True
                )

                numberyears_future = yearArray_future[1] - yearArray_future[0] + 1

                # getting data for period - includes combining historical
                if counter > 0:
                    period_years_temp_future = etccdi.extract_years(ccvar[numvar_future], data_all_future,
                                                                    my_model_dataset_future, yearArray_future[0],
                                                                    numberyears_future)
                    for i in range(0, len(period_years_temp_future)):
                        period_years_future.append(period_years_temp_future[i])
                else:
                    period_years_future = etccdi.extract_years(ccvar[numvar_future], data_all_future,
                                                               my_model_dataset_future, yearArray_future[0],
                                                               numberyears_future)

            test_future = []
            dataSetDatetimes_future = []

            for yearr in tqdm(period_years_future, desc="extracting location timeseries", leave=False):
                ## CHANGE ##
                # extract_data_at_coordinates now returns a new numpy dataset for each coordinate. Changes here to allow for exisiting code to work

                dataSetDatetimes_future.append(yearr.numeric_datetimes[:])
                temp_future = yearr.extract_data_at_coordinates(latitudes=rlat2, longitudes=rlon2, mapper=mapper2d)
                test_future.append([i.values for i in temp_future])

            dbt_future = np.hstack(test_future)
            dbt_future_reshape = np.reshape(dbt_future,(dbt_future.shape[1],))

            if experiments_future[0]=="rcp45":
                
                dbt_rcp45[my_model_future] = dbt_future_reshape

            elif experiments_future[0]=="rcp85":

                dbt_rcp85[my_model_future] = dbt_future_reshape

            ## END CHANGE ##
            timearray_future = np.hstack(dataSetDatetimes_future)

        # create numpyDataset for each site, so that we can use the thresholding functions. This needs to be done for each site as NumpyDatasets as the data is no longer gridded
        # print("locationsByRegionDict[region_future]")
        # print(locationsByRegionDict[region_future])
        for count, site_future in enumerate(
                tqdm(locationsByRegionDict[region_future], desc="Processing Locations", leave=False)):
            # print("siteDataSet_future")
            # print("count: " + str(count))
            # print("site_future: ")
            # print(site_future)
            # thresholds=[site_future[ThresholdName] for ThresholdName in ['T']
            thresholds2 = [inputDict["T"]]
            indicators2 = ['DBT' + str(val) for val in thresholds2]
            indicatorOutputs2 = ['Threshold ' + i + " Exceeded" for i in ['DBT']]

            siteDataSet_future = data.NumpyDataset(dbt_future[count, :],
                                                   numeric_datetimes=timearray_future,
                                                   climate_variable_name=period_years_future[
                                                       0]._climate_variable_name_to_use,
                                                   numeric_datetime_calendar=period_years_future[
                                                       0]._numeric_datetime_calendar,
                                                   numeric_datetime_units=period_years_future[
                                                       0]._numeric_datetime_units,
                                                   latitudes=[float(args.latitude)],
                                                   longitudes=[float(args.longitude)]
                                                   )

            # For saving of the outputs, we want to be creating new dictionaries where needed, but using existing ones for the site where they already exists
            try:
                outputThresholdDict_future = dictOfSites_future[site_future['Site No']]
            except:
                outputThresholdDict_future = deepcopy(dictOfThresholds_future)

            for indNo, threshold in enumerate(tqdm(thresholds2, "Processing Thresholds", leave=False)):
                ## CHANGE ##
                # For saving of the outputs, we want to be creating new dictionaries where needed, but using existing ones for the thresholds where they already exists
                try:
                    outputModelDict_future = outputThresholdDict_future[threshold]
                except:
                    outputModelDict_future = deepcopy(dictOfModels_future)
                ## END CHANGE ##

                # Now indicators
                if climatevariable == "THRESHOLD":
                    indiciesDataset_future = etccdi.calc_ETCCDI(siteDataSet_future, period=query_period,
                                                                climate_index=climatevariable,
                                                                threshold=float(threshold) + 273.15,
                                                                thresholdMode=thresholdMode)
                else:
                    indiciesDataset_future = etccdi.calc_ETCCDI(siteDataSet_future, period=query_period,
                                                                climate_index=climatevariable)

                # data stored in dictionaries in prep for multiModelDataset
                outputModelDict_future[my_model_future] = indiciesDataset_future
                outputThresholdDict_future[threshold] = outputModelDict_future
                dictOfSites_future[site_future['Site No']] = outputThresholdDict_future
    # split point?
    # shared inputs
    # Now create and save the multiModelDatasets for later analyis
    if scenario_future == 'rcp45':
        dictAnomalyOutput45 = deepcopy(dictOfSites_future)
    else:
        dictAnomalyOutput85 = deepcopy(dictOfSites_future)
    for siteNo_future, dictOfThresholds_future in dictOfSites_future.items():
        for threshold, dictOfModels_future in dictOfThresholds_future.items():
            # Need to force the datetimes to the same value, as one of calanders is a 360 day calander which is not handelled well (see issue 9)
            # careful when doing this
            dictOfModels_future_List = list(dictOfModels_future.values())
            for model in dictOfModels_future.values():
                model._datetimes = dictOfModels_future_List[0].datetimes
                model._numeric_datetimes = dictOfModels_future_List[0]._numeric_datetimes
                model._numeric_datetime_units = dictOfModels_future_List[0]._numeric_datetime_units
                model._numeric_datetime_calendar = dictOfModels_future_List[0]._numeric_datetime_calendar

            if scenario_future == 'rcp45':
                myMultiModelDataset_future45 = data.MultiModelDataset(dictOfModels_future)

                for i in model_names_future:
                    # print i
                    historicaldata = myMultiModelDataset_historical._original_datasets[i].values
                    # print historicaldata
                    futuredata = myMultiModelDataset_future45._original_datasets[i].values
                    # print futuredata
                    if ccvari == 'pr':
                        result = np.mean(futuredata) / np.mean(historicaldata)
                        dictAnomalyOutput45[siteNo_future][threshold][i] = result
                    else:
                        result = np.mean(futuredata) - np.mean(historicaldata)
                        dictAnomalyOutput45[siteNo_future][threshold][i] = result
                    per_model_rcp45[i] = [np.mean(futuredata)]
                    model_results.append(result)

            else:
                myMultiModelDataset_future85 = data.MultiModelDataset(dictOfModels_future)

                for i in model_names_future:
                    # print i
                    historicaldata = myMultiModelDataset_historical._original_datasets[i].values
                    # print historicaldata
                    futuredata = myMultiModelDataset_future85._original_datasets[i].values
                    # print futuredata
                    if ccvari == 'pr':
                        result = np.mean(futuredata) / np.mean(historicaldata)
                        dictAnomalyOutput85[siteNo_future][threshold][i] = result

                    else:
                        result = np.mean(futuredata) - np.mean(historicaldata)
                        dictAnomalyOutput85[siteNo_future][threshold][i] = result
                    per_model_hist[i] = [np.mean(historicaldata)]
                    per_model_rcp85[i] = [np.mean(futuredata)]
                    model_results.append(result)

    if dbt_rcp45 != {}:

        dbt_rcp45 = csv_length_verify(dbt_rcp45)
        raw_data = pd.DataFrame(dbt_rcp45)
        raw_data.to_csv(raw_data_folder+"rcp45.csv")
        per_model_rcp45 = pd.DataFrame(per_model_rcp45)
        per_model_rcp45.to_csv(per_model_csv_path+"rcp45.csv")

    elif dbt_rcp85 !={}:

        dbt_rcp85 = csv_length_verify(dbt_rcp85)
        raw_data = pd.DataFrame(dbt_rcp85)
        raw_data.to_csv(raw_data_folder+"rcp85.csv")
        per_model_hist = pd.DataFrame(per_model_hist)
        per_model_hist.to_csv(per_model_csv_path+"hist.csv")
        per_model_rcp85 = pd.DataFrame(per_model_rcp85)
        per_model_rcp85.to_csv(per_model_csv_path+"rcp85.csv")

    if climatevariable=='THRESHOLD':
        return np.divide(model_results,numberyears)
    else:
        return model_results


def getHistoricalData(args, historicalexperiments, historicalrcp):

    historicalYears = list(itertools.chain(*[[int(args.historicalstart) if int(args.historicalstart) < 2005 else []], [
        int(args.historicalend) if (int(args.historicalend) < 2006 and int(args.historicalstart) < 2005) else 2005 if (
                int(args.historicalstart) < 2005) else []]]))
    futureYears = list(itertools.chain(*[[int(args.historicalstart) if (
            int(args.historicalstart) > 2005 and int(args.historicalend) > 2005) else 2006 if (
            int(args.historicalend) > 2006 and int(args.historicalstart) < 2006) else []], [
                                             int(args.historicalend) if int(args.historicalend) > 2006 else []]]))
    ccvari, climatevariable, thresholdMode, query_period = getDependentVariables(args.indicator)
    ccvar = [ccvari]
    all_cordex_data = io.LocalFolderETCCDIDatabase(os.path.join(getCORDEXLocation(args.dataRoot, ccvari), args.region), 'CORDEX')
    mapper2d = util.RegularGridMapperPoints()  # Mapper for interpolation of locations
    locationsByRegionDict = gen.buildInputDictFromArgs(args)
    inputDict = list(locationsByRegionDict.values())[0][0]
    errorDict = []
    dbt_hist = {}
    raw_data_folder = settings.RAW_DATA_FILE_PATH
    for scenario in tqdm(historicalrcp, desc="Processing Scenarios", leave=False):
        # searches models and gets model_names temp so the models which only contain historical and future
        # searchResult = all_cordex_data.search(scenarios=scenario)  ## Note that should prob loop over future experiments
        searchResult_historical = all_cordex_data.search(scenarios='historical')

        ## CHANGE ##
        # This change was just due to availability of data on the cluster
        model_names = list(searchResult_historical.models)
        ## END CHANGE ##

        if isinstance(historicalYears[1], int) and isinstance(futureYears[1], int):
            experiments = [historicalexperiments[0], scenario]
            experimentname = historicalexperiments[0] + scenario
        elif not isinstance(historicalYears[1], int) and isinstance(futureYears[1], int):
            experiments = [scenario]
            experimentname = scenario

        # Allow for purely historical data
        elif isinstance(historicalYears[0], int) and isinstance(historicalYears[1], int) and not isinstance(
                futureYears[1], int):
            experiments = [historicalexperiments[0]]
            experimentname = historicalexperiments[0]

        # data stored in dictionaries in prep for multiModelDataset
        dictOfModels = {}
        dictOfThresholds = {}
        dictOfSites = {}
        for my_model in tqdm(model_names, desc="Processing Models", leave=False):
            # try:
            # Check model has all variables - check is here so that it doesnt unecassary runs
            fileDoesNotExistFlag = False

            for var in ccvar:
                for experiment in experiments:
                    try:
                        # TODO -took out this statement,as it doesn't seem to be used
                        fileDoesNotExistFlag=False
                        # my_model_dataset = all_cordex_data.get_netcdf_dataset_object_multi(var, 'day', my_model,  experiment)
                    except:
                        tqdm.write("Cant find file for:" + str(var) + "_day_" + str(my_model) + "_" + str(experiment))
                        fileDoesNotExistFlag = True
                        break

            if fileDoesNotExistFlag:
                errorDictTemp = {}
                errorDictTemp["Model"] = my_model
                errorDictTemp["Scenario"] = scenario
                errorDictTemp["Region"] = args.region
                errorDict.append(errorDictTemp)
                continue

            # extract rotation poles
            my_model_dataset = all_cordex_data.get_netcdf_dataset_object_multi(ccvar[0], 'day', my_model,
                                                                               historicalexperiments[0])
            data_all = data.NetCDFDatasetRotated(my_model_dataset, ccvar[0], "all", "all", "all",
                                                 netcdf_longitudes_span_globe=True,
                                                 climate_variable_name_to_use=ccvar[0],
                                                 assume_equally_spaced_datetimes=True)

            latpole = data_all._rotated_grid_north_pole_latitude
            lonpole = data_all._rotated_grid_north_pole_longitude

            [rlonTemp, rlatTemp] = rotgrid.transform_to_rotated_grid(float(args.longitude), float(args.latitude),
                                                                     lonpole, latpole)
            siteNo, lon, lat, rlon, rlat, = [1], [args.longitude], [args.latitude], [rlonTemp], [rlatTemp]

            for numvar in range(len(ccvar)):

                # Combining historical and modelled data
                for counter, experiment in enumerate(tqdm(experiments, desc="Combining data", leave=False)):
                    if experiment == 'historical':
                        yearArray = historicalYears
                    else:
                        yearArray = futureYears

                    # Get the handle for the dataset for the specific model, variable, time resolution and scenario - check with clare what this does and why its needed
                    my_model_dataset = all_cordex_data.get_netcdf_dataset_object_multi(ccvar[numvar], 'day', my_model,
                                                                                       experiment)
                    # print my_model_dataset._files

                    # Get a the dataset
                    data_all = data.NetCDFDatasetRotated(
                        my_model_dataset, ccvar[numvar], "all", "all", "all",
                        netcdf_longitudes_span_globe=True, climate_variable_name_to_use=ccvar[numvar],
                        assume_equally_spaced_datetimes=True
                    )

                    numberyears = yearArray[1] - yearArray[0] + 1

                    # getting data for period - includes combining historical
                    if counter > 0:
                        period_years_temp = etccdi.extract_years(ccvar[numvar], data_all, my_model_dataset,
                                                                 yearArray[0], numberyears)
                        for i in range(0, len(period_years_temp)):
                            period_years.append(period_years_temp[i])
                    else:
                        period_years = etccdi.extract_years(ccvar[numvar], data_all, my_model_dataset, yearArray[0],
                                                            numberyears)

                test = []
                dataSetDatetimes = []

                for yearr in tqdm(period_years, desc="extracting location timeseries", leave=False):
                    ## CHANGE ##
                    # extract_data_at_coordinates now returns a new numpy dataset for each coordinate. Changes here to allow for exisiting code to work

                    dataSetDatetimes.append(yearr.numeric_datetimes[:])
                    temp = yearr.extract_data_at_coordinates(latitudes=rlat, longitudes=rlon, mapper=mapper2d)
                    test.append([i.values for i in temp])

                dbt = np.hstack(test)
                dbt_reshape = np.reshape(dbt,(dbt.shape[1],))
                dbt_hist[my_model] = dbt_reshape
                ## END CHANGE ##
                timearray = np.hstack(dataSetDatetimes)

            # create numpyDataset for each site, so that we can use the thresholding functions. This needs to be done for each site as NumpyDatasets as the data is no longer gridded

            for count, site in enumerate(
                    tqdm(locationsByRegionDict[args.region], desc="Processing Locations", leave=False)):

                # thresholds=[site[ThresholdName] for ThresholdName in ['T']
                thresholds = [inputDict["T"]]
                indicators = ['DBT' + str(val) for val in thresholds]
                indicatorOutputs = ['Threshold ' + i + " Exceeded" for i in ['DBT']]

                siteDataSet = data.NumpyDataset(dbt[count, :],
                                                numeric_datetimes=timearray,
                                                climate_variable_name=period_years[0]._climate_variable_name_to_use,
                                                numeric_datetime_calendar=period_years[0]._numeric_datetime_calendar,
                                                numeric_datetime_units=period_years[0]._numeric_datetime_units,
                                                latitudes=[float(args.latitude)],
                                                longitudes=[float(args.longitude)]
                                                )

                ## CHANGE ##
                # For saving of the outputs, we want to be creating new dictionaries where needed, but using existing ones for the site where they already exists
                try:
                    outputThresholdDict = dictOfSites[site['Site No']]
                except:
                    outputThresholdDict = deepcopy(dictOfThresholds)
                ## END CHANGE ##

                for indNo, threshold in enumerate(tqdm(thresholds, "Processing Thresholds", leave=False)):
                    ## CHANGE ##
                    # For saving of the outputs, we want to be creating new dictionaries where needed, but using existing ones for the thresholds where they already exists
                    try:
                        outputModelDict = outputThresholdDict[threshold]
                    except:
                        outputModelDict = deepcopy(dictOfModels)
                    ## END CHANGE ##

                    indicator = indicators[indNo]

                    # Now indicators
                    if climatevariable == "THRESHOLD":
                        indiciesDataset = etccdi.calc_ETCCDI(siteDataSet, query_period, climate_index=climatevariable,
                                                             threshold=float(threshold) + 273.15,
                                                             thresholdMode=thresholdMode)
                    else:
                        indiciesDataset = etccdi.calc_ETCCDI(siteDataSet, period=query_period,
                                                             climate_index=climatevariable)

                    # data stored in dictionaries in prep for multiModelDataset
                    outputModelDict[my_model] = indiciesDataset
                    outputThresholdDict[threshold] = outputModelDict
                    dictOfSites[site['Site No']] = outputThresholdDict

        # Now create and save the multiModelDatasets for later analyis
        for siteNo, dictOfThresholds in dictOfSites.items():
            for threshold, dictOfModels in dictOfThresholds.items():
                # Need to force the datetimes to the same value, as one of calanders is a 360 day calander which is not handelled well (see issue 9)
                # careful when doing this
                dictOfModelsList = list(dictOfModels.values())
                for model in dictOfModelsList:
                    model._datetimes = dictOfModelsList[0].datetimes
                    model._numeric_datetimes = dictOfModelsList[0]._numeric_datetimes
                    model._numeric_datetime_units = dictOfModelsList[0]._numeric_datetime_units
                    model._numeric_datetime_calendar = dictOfModelsList[0]._numeric_datetime_calendar

                myMultiModelDataset_historical = data.MultiModelDataset(dictOfModels)

    dbt_hist = csv_length_verify(dbt_hist)
    raw_data = pd.DataFrame(dbt_hist)
    raw_data.to_csv(raw_data_folder+"hist.csv")

    return myMultiModelDataset_historical, numberyears


def getTrend(args, model_resultsrcp85):
    ccvari, climatevariable, thresholdMode, query_period = getDependentVariables(args.indicator)
    trend = []
    if ccvari == 'pr':
        if np.mean(model_resultsrcp85) > 1:
            trend = "increase"
        else:
            trend = "decrease"
    else:
        if np.mean(model_resultsrcp85) > 0:
            trend = "increase"
        else:
            trend = "decrease"
    return trend


def getResultsText(args,  model_resultsrcp45, model_resultsrcp85):
    resultsText = ""
    ccvarinput = args.indicator
    trend = getTrend(args, model_resultsrcp85)
    if ccvarinput == 'TXX':
        resultsText = "TXX (Yearly maximum value of daily maximum temperature) is projected to " + str(
            round(float(args.latitude), 2)) + "," + str(round(float(args.longitude), 2)) + " is projected to " + trend + " by " + str((
       round(
           np.mean(
               model_resultsrcp85),
           2))) + "°C  for " + args.futurestart + "-" + args.futureend + " and emission scenario rcp8.5, in comparison to the baseline time period " + args.historicalstart + "-" + args.historicalend + ". The uncertainty range is " + str(
            (round(np.min(model_resultsrcp85), 2))) + "°C to " + str(
            (round(np.max(model_resultsrcp85), 2))) + "°C. The projection for rcp4.5 is " + str(
            (round(np.mean(model_resultsrcp45), 2))) + "°C with an uncertainty range " + str(
            (round(np.min(model_resultsrcp45), 2))) + "°C to " + str((round(np.max(model_resultsrcp45), 2))) + "°C."
    elif ccvarinput == 'RX1DAY':
        # sys.stdout = open("test_rx1day.txt", "w")
        resultsText = "RX1DAY (Yearly maximum value of 1-day precipitation)is projected to " + str(
            round(float(args.latitude), 2)) + "," + str(
            round(float(args.longitude), 2)) + " is projected to " + trend + " by a factor of " + str((round(
            np.mean(model_resultsrcp85),
            2))) + " for " + args.futurestart + "-" + args.futureend + " and emission scenario rcp8.5, in comparison to the baseline time period " + args.historicalstart + "-" + args.historicalend + ". The uncertainty range is " + str(
            (round(np.min(model_resultsrcp85), 2))) + " to " + str(
            (round(np.max(model_resultsrcp85), 2))) + ". The projection for rcp4.5 is " + str(
            (round(np.mean(model_resultsrcp45), 2))) + " with an uncertainty range " + str(
            (round(np.min(model_resultsrcp45), 2))) + " to " + str((round(np.max(model_resultsrcp45), 2))) + "."
    elif ccvarinput == 'RX5DAY':
        # sys.stdout = open("test_rx5day.txt", "w")
        resultsText = "RX5DAY (Yearly maximum value of 5-day precipitation)is projected to " + str(
            round(float(args.latitude), 2)) + "," + str(
            round(float(args.longitude), 2)) + " is projected to " + trend + " by a factor of " + str((round(
            np.mean(model_resultsrcp85),
            2))) + " for " + args.futurestart + "-" + args.futureend + " and emission scenario rcp8.5, in comparison to the baseline time period " + args.historicalstart + "-" + args.historicalend + ". The uncertainty range is " + str(
            (round(np.min(model_resultsrcp85), 2))) + " to " + str(
            (round(np.max(model_resultsrcp85), 2))) + ". The projection for rcp4.5 is " + str(
            (round(np.mean(model_resultsrcp45), 2))) + " with an uncertainty range " + str(
            (round(np.min(model_resultsrcp45), 2))) + " to " + str((round(np.max(model_resultsrcp45), 2))) + "."
    elif ccvarinput == 'AVERAGE MAXIMUM TEMPERATURE':
        # sys.stdout = open("test_averagetasmax.txt", "w")
        resultsText = "Average maximum temperature is projected to " \
                      + str(round(float(args.latitude), 2)) + "," + str(round(float(args.longitude), 2)) + " is projected to " \
                      + trend + " by " + str((round(np.mean(model_resultsrcp85), 2))) + "°C for " \
                      + args.futurestart + "-" + args.futureend + " and emission scenario rcp8.5, in comparison to the baseline time period " \
                      + args.historicalstart + "-" + args.historicalend + ". The uncertainty range is " + str(
            (round(np.min(model_resultsrcp85), 2))) \
                      + "°C to " + str((round(np.max(model_resultsrcp85), 2))) + "°C. The projection for rcp4.5 is " \
                      + str((round(np.mean(model_resultsrcp45), 2))) + "°C with an uncertainty range " \
                      + str((round(np.min(model_resultsrcp45), 2))) + "°C to " + str(
            (round(np.max(model_resultsrcp45), 2))) + "°C."
    elif ccvarinput == 'AVERAGE MINIMUM TEMPERATURE':
        # sys.stdout = open("test_averagetasmin.txt", "w")
        resultsText = "Average daily minimum temperature is projected to " \
                      + str(round(float(args.latitude), 2)) + "," + str(round(float(args.longitude), 2)) + " is projected to " \
                      + trend + " by " + str((round(np.mean(model_resultsrcp85), 2))) + "°C  for " \
                      + args.futurestart + "-" + args.futureend + " and emission scenario rcp8.5, in comparison to the baseline time period " \
                      + args.historicalstart + "-" + args.historicalend + ". The uncertainty range is " + str(
            (round(np.min(model_resultsrcp85), 2))) \
                      + "°C to " + str((round(np.max(model_resultsrcp85), 2))) + "°C. The projection for rcp4.5 is " \
                      + str((round(np.mean(model_resultsrcp45), 2))) + "°C with an uncertainty range " \
                      + str((round(np.min(model_resultsrcp45), 2))) + "°C to " + str(
            (round(np.max(model_resultsrcp45), 2))) + "°C."
    elif ccvarinput == 'AVERAGE TEMPERATURE':
        # sys.stdout = open("test_averagetas.txt", "w")
        resultsText = "Average daily temperature is projected to " \
                      + str(round(float(args.latitude), 2)) + "," + str(round(float(args.longitude), 2)) + " is projected to " \
                      + trend + " by " + str((round(np.mean(model_resultsrcp85), 2))) + "°C  for " \
                      + args.futurestart + "-" + args.futureend + " and emission scenario rcp8.5, in comparison to the baseline time period " \
                      + args.historicalstart + "-" + args.historicalend + ". The uncertainty range is " + str(
            (round(np.min(model_resultsrcp85), 2))) \
                      + "°C to " + str((round(np.max(model_resultsrcp85), 2))) + "°C. The projection for rcp4.5 is " \
                      + str((round(np.mean(model_resultsrcp45), 2))) + "°C with an uncertainty range " \
                      + str((round(np.min(model_resultsrcp45), 2))) + "°C to " + str(
            (round(np.max(model_resultsrcp45), 2))) + "°C."
    elif ccvarinput == 'CDD':
        # sys.stdout = open("test_cdd.txt", "w")
        resultsText = "CDD (Cumulative Dry Days) for location at " + str(round(float(args.latitude), 2)) + "," + str(
            round(float(args.longitude), 2)) + " is projected to " + trend + " by a factor of " + str((round(
            np.mean(model_resultsrcp85),
            2))) + " for " + args.futurestart + "-" + args.futureend + " and emission scenario rcp8.5, in comparison to the baseline time period " + args.historicalstart + "-" + args.historicalend + ". The uncertainty range is " + str(
            (round(np.min(model_resultsrcp85), 2))) + " to " + str(
            (round(np.max(model_resultsrcp85), 2))) + ". The projection for rcp4.5 is " + str(
            (round(np.mean(model_resultsrcp45), 2))) + " with an uncertainty range " + str(
            (round(np.min(model_resultsrcp45), 2))) + " to " + str((round(np.max(model_resultsrcp45), 2))) + "."
    elif ccvarinput == 'AVERAGE PRECIPITATION':
        # sys.stdout = open("test_averagepr.txt", "w")
        resultsText = "Average precipitation is projected to " + str(round(float(args.latitude), 2)) + "," + str(
            round(float(args.longitude), 2)) + " is projected to " + trend + " by a factor of " + str((round(
            np.mean(model_resultsrcp85),
            2))) + " for " + args.futurestart + "-" + args.futureend + " and emission scenario rcp8.5, in comparison to the baseline time period " + args.historicalstart + "-" + args.historicalend + ". The uncertainty range is " + str(
            (round(np.min(model_resultsrcp85), 2))) + " to " + str(
            (round(np.max(model_resultsrcp85), 2))) + ". The projection for rcp4.5 is " + str(
            (round(np.mean(model_resultsrcp45), 2))) + " with an uncertainty range " + str(
            (round(np.min(model_resultsrcp45), 2))) + " to " + str((round(np.max(model_resultsrcp45), 2))) + "."
    elif ccvarinput == 'THRESHOLD LESS THAN':
        # sys.stdout = open("test_thresholdlessthan.txt", "w")
        resultsText = "Days less than the threshold of " + (args.threshold) + "°C for location at " + str(
            round(float(args.latitude), 2)) + "," + str(round(float(args.longitude), 2)) + " is projected to " + trend + " by " + str(
            abs(int(np.mean(
                model_resultsrcp85)))) + " days per year for " + args.futurestart + "-" + args.futureend + " and emission scenario rcp8.5, in comparison to the baseline time period " + args.historicalstart + "-" + args.historicalend + ". The uncertainty range is " + str(
            int(np.min(model_resultsrcp85))) + " to " + str(
            int(np.max(model_resultsrcp85))) + " days per year. The projection for rcp4.5 is " + str(
            int(np.mean(model_resultsrcp45))) + " with an uncertainty range " + str(
            int(np.min(model_resultsrcp45))) + " to " + str(int(np.max(model_resultsrcp45))) + " days per year."
    elif ccvarinput == 'THRESHOLD MORE THAN':
        # sys.stdout = open("test_thresholdmorethan.txt", "w")
        resultsText = "Days more than the threshold of " + (args.threshold) + "°C for location at " + str(
            round(float(args.latitude), 2)) + "," + str(round(float(args.longitude), 2)) + " is projected to " + trend + " by " + str(
            abs(int(np.mean(
                model_resultsrcp85)))) + " days per year for " + args.futurestart + "-" + args.futureend + " and emission scenario rcp8.5, in comparison to the baseline time period " + args.historicalstart + "-" + args.historicalend + ". The uncertainty range is " + str(
            int(np.min(model_resultsrcp85))) + " to " + str(
            int(np.max(model_resultsrcp85))) + " days per year. The projection for rcp4.5 is " + str(
            int(np.mean(model_resultsrcp45))) + " with an uncertainty range " + str(
            int(np.min(model_resultsrcp45))) + " to " + str(int(np.max(model_resultsrcp45))) + " days per year."
    else:
        resultsText = "Variable not recognised"
    return resultsText


# if __name__ == '__main__':
    
#     # parsed_args = parse_args(sys.argv[1:])
#     #print(parsed_args)
#     user_region_='NAM'
#     user_var_='RX5DAY'
#     history_from=2001
#     history_to=2005
#     future_from=2036
#     future_to=2040
#     lat=33.06
#     lon=-80.04
#     threshold=0
#     results, metadata = processSingleLocation(user_region_,user_var_,history_from,history_to,future_from,future_to,threshold,lat,lon)
#     freeze_support()
#     print ("Results: "  , results)
#     print ("Results: "  , metadata)






