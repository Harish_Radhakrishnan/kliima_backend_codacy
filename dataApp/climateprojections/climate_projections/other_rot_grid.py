# A long winded bit of code that allows you to specify and pole and go to and from rotated and true latitude and longitude components
# It could be sped up by storing the values of math.pi /180. and that inverted, also by storing some cos(...) etc. values or using trig identities.

import math

def transform_to_rotated_grid(lon, lat, NP_lon, NP_lat, inverse=False):
        """
        Performs transformations to/from rotated grid.

        Inputs:

          lon, lat: longitude (degrees) and latitude (degrees)
                    of a point X, as seen in the non-rotated grid

          NP_lon, NP_lat: longitude (degrees) and latitude (degrees)
                    of the North pole, as seen in the non-rotated grid

          inverse: optional input -- set to a true value for inverse transform
                          (coords on rotated grid to coords on nonrotated)

        Latitudes in range [-90, 90]
        Longitudes in range [-180, 180]

        Returns:

            The coordinates of the point X (in degrees) as seen in the rotated
            grid (or the non-rotated grid in case of inverse transform), as a
            2-element tuple: (longitude, latitude)

        """

        lon = (lon * math.pi) / 180; # Convert degrees to radians
        lat = (lat * math.pi) / 180;

        SP_lat = -NP_lat; #Convert to the South Pole, as that's what our algorithm actually uses.
        if NP_lon > 0:
            SP_lon = NP_lon - 180;
        else:
            SP_lon = NP_lon + 180;

        theta = 90 + SP_lat; # Rotation around y - axis
        theta = (theta * math.pi) / 180;

        phi = SP_lon; # Rotation around z - axis
        phi = (phi * math.pi) / 180; # Convert degrees to radians

        x = math.cos(lon)*math.cos(lat); # Convert from spherical to cartesian coordinates
        y = math.sin(lon)*math.cos(lat);
        z = math.sin(lat);

        if not inverse:
# Regular -> Rotated
            x_new = math.cos(theta)* math.cos(phi)* x + math.cos(theta)* math.sin(phi)* y + math.sin(theta)* z;
            y_new = -math.sin(phi)* x + math.cos(phi)* y;
            z_new = -math.sin(theta)* math.cos(phi)* x - math.sin(theta)* math.sin(phi)* y + math.cos(theta)* z;

        else:
#  Rotated -> Regular
            phi = -phi;
            theta = -theta;

            x_new = math.cos(theta)* math.cos(phi)* x + math.sin(phi)* y + math.sin(theta)* math.cos(phi)* z;
            y_new = -math.cos(theta)* math.sin(phi)* x + math.cos(phi)* y - math.sin(theta)* math.sin(phi)* z;
            z_new = -math.sin(theta)* x + math.cos(theta)* z;

        lon_new = math.atan2(y_new, x_new); # Convert cartesian back to spherical coordinates
        lat_new = math.asin(z_new);
        lon_new = (lon_new * 180) / math.pi; # Convert radians back to degrees
        lat_new = (lat_new * 180) / math.pi;

        return (lon_new, lat_new)



 #--------------------------
# main program - to test

if __name__ == "__main__":
#NAM
    lonpole = 83
    latpole = 42.5

#top left (rlon = -34, rlat = -30.71)
    lon = -126.3
    lat = 10.4

#EUR
#If you use 198 get the wrong lon at -8
#    lonpole = 198#-162
#    latpole = 39.25

#top left rlon = -28.375, rlat = -23.375
#    lon = -10.1
#    lat = 22

    (lonrot, latrot) = transform_to_rotated_grid(lon, lat, lonpole, latpole)

    (lon, lat) = transform_to_rotated_grid(lonrot, latrot, lonpole, latpole, True)

    print ('Rotated grid: ')
    print ()
    print (' Location of pole of rotated grid as seen in non-rotated grid:')
    print ('   Lon=%s, Lat=%s' % (lonpole, latpole))
    print ()
    print ('Location of chosen point in non-rotated grid:')
    print ('  Lon=%s, Lat=%s' % (lon, lat))
    print ()
    print ('Location of chosen point as seen in rotated grid:')
    print ('  Lon=%s, Lat=%s' % (lonrot, latrot))
    print ()
