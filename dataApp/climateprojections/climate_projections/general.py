import csv
import fnmatch
import os


def createFolder(directory):
	try:
		if not os.path.exists(directory):
			os.makedirs(directory)
	except OSError:
		print ('Error: Creating directory. ' +  directory)


def get_filedata(filename):
    filedata = []

    with open(filename, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            filedata.append(row)

    return filedata


def getFilePaths(dir,expression):
    matches = []
    for root, dirnames, filenames in os.walk(dir):
        for filename in fnmatch.filter(filenames, expression):
            matches.append(os.path.join(root, filename))

    return matches


def loadAndCompressDictionaries(filePaths):
    dictAll=[]
    for filePath in filePaths:
        siteData=get_filedata(filePath)
        for site in siteData:
            dictAll.append(site)

    return dictAll


def getAndCombineThresholds(FileLoc,Region,ThresholdNames):
	# returns the unique thresholds for the region specified, from the input file which specifies a number of locations and thresholds
    import itertools

    filedata=get_filedata(FileLoc)
    ThresholdCollection=[]
    for counter,Threshold in enumerate(ThresholdNames):
        ThresholdCollection.append([i[Threshold] for i in filedata if i['Region']==Region ])

    ThresholdCollection=list(itertools.chain(*ThresholdCollection))

    return sorted(set(ThresholdCollection))


def getThresholdPerRegion(FileLoc,ThresholdNames):
	# returns the unique thresholds for all regions in a dictionary
    regions=getRegions(FileLoc)
    ThresholdDict={}
    for region in regions:
        Thresholds=getAndCombineThresholds(FileLoc,region,ThresholdNames)
        ThresholdDict[region]=Thresholds

    return ThresholdDict


def findModels(climate_database, climate_variable, time_scale, scenario):
    #gets models with both a historical and climate component - doesnt really fit in this file
    searchResult = climate_database.search(scenarios = scenario)
    searchResult_historical = climate_database.search(scenarios = 'historical')
    model_names=list(searchResult.models & searchResult_historical.models)

    return model_names


def getLocationsPerRegion(FileLoc):
    regions=getRegions(FileLoc)
    output={}
    filedata=get_filedata(FileLoc)
    for region in regions:
        for row in filedata:
            if row["Region"]==region:
                try:
                    output[region].append(row)
                except:
                    output[region]=[]
                    output[region].append(row)
    return output


def getRegions(FileLoc):
    regions=[]
    filedata=get_filedata(FileLoc)
    for row in filedata:
        regions.append(row["Region"])

    return list(set(regions))

def buildInputDictFromArgs(args):
    return buildInputDict(args.region,
                                   args.indicator, args.latitude, args.longitude,
                                   args.historicalstart, args.historicalend,
                                    args.futurestart, args.futureend, args.threshold)

def buildInputDict(region, indicator,latitude,longitude,historicalstart,historicalend,futurestart,futureend,threshold):
    return  {region: [
      {'Site No': '1',
       'Var': indicator,
      'latitude': latitude,
       'longitude': longitude,
      'Region': region,
       'Historical Start': historicalstart,
      'Historical End': historicalend,
       'Future Start': futurestart,
      'Future End': futureend,
       'T': threshold}
     ]}