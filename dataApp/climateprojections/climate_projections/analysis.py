import datetime

from . import utilities
import climate_projections
# import climate_projections.data as data


class ETCCDIAnalyser(utilities.CanLog):
    def __init__(self, database):
        """

        Parameters
        ----------
        database : :climate_change_projections.data.Database
        """
        utilities.CanLog.__init__(self)
        self._HISTORIC_SCENARIO_NAME = "historical"
        self._database = database
        return

    def calculate_extreme_dataset(self, climate_index, time_resolution, scenario,
                                  models, relative_operation, reference_future_year,
                                  latitude_range_specification='all',
                                  longitude_range_specification='all',
                                  reference_model=None):
        # This function does not allow the user to specify the realisation (e.g. r1i1p1) but uses the earliest available.
        # Realisation is not specified in "get_dataset" call, but that function uses the earliest.
        # TODO: allow only future scenarios

        reference_historic_year = 2005
        number_of_years_in_average = 20
        historic_datetime_range_specification = {
            "range": [
                datetime.datetime(reference_historic_year - number_of_years_in_average,
                                  1, 1, 0, 0, 0, 0),
                datetime.datetime(reference_historic_year, 1, 1, 0, 0, 0, 0)
            ],
            "type": "including"
        }
        future_datetime_range_specification = {
            "range": [
                datetime.datetime(reference_future_year - number_of_years_in_average,
                                  1, 1, 0, 0, 0, 0),
                datetime.datetime(reference_future_year, 1, 1, 0, 0, 0, 0)
            ],
            "type": "including"
        }

        historic_datasets = {}
        future_datasets = {}
        relative_future_datasets = {}
        for model in models:
            # Get full datasets
            future_netcdf = self._database.get_dataset(
                climate_index, time_resolution, model, scenario
            )
            future_dataset = climate_projections.data.NetCDFDataset(
                future_netcdf, '{0}ETCCDI'.format(climate_index),
                future_datetime_range_specification, latitude_range_specification,
                longitude_range_specification,
                netcdf_longitudes_span_globe=True,
                climate_variable_name_to_use=climate_index,
                assume_equally_spaced_datetimes=True
            )
            future_datasets[model] = future_dataset

            historic_netcdf = self._database.get_dataset(
                climate_index, time_resolution, model, self._HISTORIC_SCENARIO_NAME
            )
            historic_dataset = climate_projections.data.NetCDFDataset(
                historic_netcdf, '{0}ETCCDI'.format(climate_index),
                historic_datetime_range_specification, latitude_range_specification,
                longitude_range_specification,
                netcdf_longitudes_span_globe=True,
                climate_variable_name_to_use=climate_index,
                assume_equally_spaced_datetimes=True
            )
            historic_datasets[model] = historic_dataset

            # Calculate averages over time
            averaged_future_dataset = future_dataset.mean_over_time('all')
            averaged_historic_dataset = historic_dataset.mean_over_time('all')
            if relative_operation == "difference":
                relative_future_dataset = averaged_future_dataset.subtract(
                    averaged_historic_dataset, map_to_this=True
                )
            elif relative_operation == "fraction":
                relative_future_dataset = averaged_future_dataset.divide(
                    averaged_historic_dataset, map_to_this=True
                )
            elif callable(relative_operation):
                raise NotImplementedError(
                    "Cannot currently use custom functions for relative_operation.")
            else:
                raise ValueError("The relative_operation '{0}' is not supported.".format(
                    relative_operation
                ))
            relative_future_datasets[model] = relative_future_dataset

        if len(relative_future_datasets) > 1:
            multi_model_dataset = climate_projections.data.MultiModelDataset(
                relative_future_datasets, reference_model_name=reference_model)
            multi_model_mean_dataset = multi_model_dataset.mean_over_models()
        else:
            multi_model_mean_dataset = relative_future_datasets.values()[0]

        return multi_model_mean_dataset

    def calculate_historic_model_mean(self, climate_index, time_resolution, models,
                                      latitude_range_specification='all',
                                      longitude_range_specification='all'):
        # TODO: allow only future scenarios

        reference_historic_year = 2005
        # reference_future_year = 2080
        historic_datetime_range_specification = {
            "range": [
                datetime.datetime(reference_historic_year - 20, 1, 1, 0, 0, 0, 0),
                datetime.datetime(reference_historic_year, 1, 1, 0, 0, 0, 0)
            ],
            "type": "including"
        }
        # future_datetime_range_specification = {
        #     "range": [
        #         datetime.datetime(reference_future_year - 20, 1, 1, 0, 0, 0, 0),
        #         datetime.datetime(reference_future_year, 1, 1, 0, 0, 0, 0)
        #     ],
        #     "type": "including"
        # }

        historic_datasets = {}
        averaged_historic_datasets = {}
        # future_datasets = {}
        # relative_future_datasets = {}
        for model in models:
            # Get full datasets
            # future_netcdf = self._database.get_dataset(
            #     climate_index, time_resolution, model, scenario
            # )
            # future_dataset = climate_projections.data.NetCDFDataset(
            #     future_netcdf, '{0}ETCCDI'.format(climate_index),
            #     future_datetime_range_specification, latitude_range_specification,
            #     longitude_range_specification,
            #     netcdf_longitudes_span_globe=True,
            #     climate_variable_name_to_use=climate_index,
            #     assume_equally_spaced_datetimes=True
            # )
            # future_datasets[model] = future_dataset

            historic_netcdf = self._database.get_dataset(
                climate_index, time_resolution, model, self._HISTORIC_SCENARIO_NAME
            )
            historic_dataset = climate_projections.data.NetCDFDataset(
                historic_netcdf, '{0}ETCCDI'.format(climate_index),
                historic_datetime_range_specification, latitude_range_specification,
                longitude_range_specification,
                netcdf_longitudes_span_globe=True,
                climate_variable_name_to_use=climate_index,
                assume_equally_spaced_datetimes=True
            )
            historic_datasets[model] = historic_dataset

            # Calculate averages over time
            # averaged_future_dataset = future_dataset.mean_over_time('all')
            averaged_historic_dataset = historic_dataset.mean_over_time('all')
            averaged_historic_datasets[model] = averaged_historic_dataset
            # if relative_operation == "difference":
            #     relative_future_dataset = averaged_future_dataset.subtract(
            #         averaged_historic_dataset, map_to_this=True
            #     )
            # elif relative_operation == "fraction":
            #     relative_future_dataset = averaged_future_dataset.divide(
            #         averaged_historic_dataset, map_to_this=True
            #     )
            # elif callable(relative_operation):
            #     raise NotImplementedError(
            #         "Cannot currently use custom functions for relative_operation.")
            # else:
            #     raise ValueError("The relative_operation '{0}' is not supported.".format(
            #         relative_operation
            #     ))
            # relative_future_datasets[model] = relative_future_dataset

        reference_model_name = "CMCC-CM"
        averaged_mapped_historic_datasets = {}
        for model_name, averaged_historic_dataset in \
                averaged_historic_datasets.iteritems():

            mapped_average_historic_dataset = averaged_historic_dataset.map_to_reference(
                averaged_historic_datasets[reference_model_name]
            )
            averaged_mapped_historic_datasets[model_name] = \
                mapped_average_historic_dataset

        historic_multi_model_dataset = climate_projections.data.MultiModelDataset(
            averaged_mapped_historic_datasets)

        return historic_multi_model_dataset.mean_over_models()


