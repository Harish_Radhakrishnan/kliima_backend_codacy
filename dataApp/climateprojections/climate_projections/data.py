import abc
import bisect

import netCDF4
import numpy as np
# import cartopy.crs as ccrs
# import matplotlib.pyplot as plt
# import matplotlib.dates as mpl_dates

from . import utilities
from . import io
import scipy.stats as sst
from . import other_rot_grid as rotgrid
from copy import deepcopy # to deepcopy dictionaries 

#TODO: Ideally put in a check that times are monotonic as can get wrong results that look right from time subset stuff


def _calculate_longitude_range_to_resolve_to(longitudes):

    first_longitude = longitudes[0]
    last_longitude = longitudes[-1]
    if first_longitude > last_longitude:
        # Then we choose the range such that when we resolve any number in the array of
        # longitudes into this range, it will be >= the first_longitude. This is the
        # key to getting the algorithms that search for the nearest longitude in a
        # NetCDF variable array to work when the underlying array of longitudes is not
        # monotonically increasing.
        boundary_offset = (first_longitude - last_longitude) / 2.0
        longitude_range_to_resolve_to = [
            first_longitude - boundary_offset,
            first_longitude - boundary_offset + 360.0
        ]
    else:
        # Handling the seam between the end and beginning of the NetCDF's longitude range:
        # We need to decide how to resolve longitudes that fall between the last and
        # first longitudes in the NetCDF's list of longitudes => If we choose the
        # 'seam' to be halfway between the two, this will ensure that the
        # ElementIndexFinder.find_index_of_nearest() function works properly at the
        # boundaries of the longitude array when it is treated as being cyclic.
        # Note that this function is also relevant for NumpyDataset
        longitude_range = last_longitude - first_longitude
        boundary_offset = (360.0 - longitude_range) / 2.0
        longitude_range_to_resolve_to = [
            first_longitude - boundary_offset,
            last_longitude + boundary_offset
        ]
    return longitude_range_to_resolve_to


def _resolve_longitude_into_range(longitude, longitude_range_to_resolve_to):
    """
    Resolves the input longitude to the equivalent longitude that lies inside
    longitude_range_to_resolve_to.

    Longitude is cyclic over 360 degrees. In order to enable things like
    specifying ranges that span over the cyclic boundary, specifying
    positions in negative degrees or beyond 360 degrees, we use this function to
    resolve any input longitude into the range that is used in the underlying
    NetCDF file. Note that any number at the end of the cyclic range will be resolved
    as the start of the range e.g. if longitude_range_to_resolve_to = [0, 360],
    then 360, 720, 1080 etc. will all be resolved as 0.

    Parameters
    ----------
    longitude : float
        The longitude that will be resolved.
    longitude_range_to_resolve_to : list
        e.g. [0, 360]. The range should cover a full 360 degrees. The output "resolved"
        longitude will lie in this range.

    Returns
    -------
    resolved_longitude : float
    """

    if longitude_range_to_resolve_to[0] <= longitude < \
            longitude_range_to_resolve_to[1]:
        resolved_longitude = longitude
    elif longitude < longitude_range_to_resolve_to[0]:
        # Remove any multiples of 360 that separate the input longitude from the
        # target range
        resolved_longitude = (
                longitude +
                ((longitude_range_to_resolve_to[1] - longitude) // 360.0) * 360.0
        )
    else: # longitude >= longitude_range_to_resolve_to[1]
        # Remove any multiples of 360 that separate the input longitude from the
        # target range
        resolved_longitude = (
                longitude -
                ((longitude - longitude_range_to_resolve_to[0]) // 360.0) * 360.0
        )

    return resolved_longitude


class ElementIndexFinder(utilities.CanLog):
    def __init__(self, array, spacing=None, spacing_assumption="variable",
                 value_range=None, neighbourhood_size=None, treat_as_cyclic=False):
        """Assumes the input is a numpy array, and that the values are sorted in
        canonical order.

        Primarily provides the find_index_of_nearest() member function, which finds
        the index of the element with value nearest to the input value. It has been
        designed to minimise the number of elements in the input array that it needs to
        access before it finds the nearest; this is primarily of benefit when working
        with netcdf files, as it can reduce the amount of data that needs to be read
        from the netcdf file. The search algorithm first makes an educated guess as to
        where the nearest value lies, using the inputs for spacing and
        spacing_assumption. It will then search the local neighbourhood of this initial
        guess (with size = neighbourhood size) using a bisecting algorithm. If the
        nearest value isn't found in this neighbourhood, then it will search a new,
        neighbouring neighbourhood; this will be repeated until the value is found.

        Parameters
        ----------
        array : array_like
            The array that will be searched when this relevant member functions of
            this class are used.
        spacing : float, optional
            The spacing between consecutive values in the array. If not supplied,
            it will be calculated.
        spacing_assumption : {None, "constant", "variable"}, optional
            If spacing_assumption = None, then find_index_of_nearest() will use
            the full neighbourhood searching algorithm to find the correct index.
            If spacing_assumption = "variable", then find_index_of_nearest() will do
            the same as for spacing_assumption = None, except it will use the "spacing"
            variable (supplied or estimated) to perform an initial guess on the
            location of the index.
            If spacing_assumption = "constant", then find_index_of_nearest() will
            calculate the correct index based on the spacing between one pair of
            consecutive values (or the input to the "spacing" variable, if one was
            supplied), rather than searching for it.
        neighbourhood_size : int
            Will always use an odd number. If you enter an even number, it will add one.
        value_range : list, optional
            e.g. [2.0, 8.0]. If input, will assume the items in this list as the start
            and end values of the input array. This is for extra efficiency; in case
            these values are already known, we don't need to access the array to get them.
        treat_as_cyclic : bool
            This allows the range indices output by the find_range_indices function to
            cycle around the end/beginning of the array. Note that this class assumes
            that any values input into its member functions have already been
            pre-processed so as to lie within the cyclic range (which monotonically
            increases from its start to its end) e.g. on a 360degree cyclic range going
            from 0 to 360 degrees, it is assumed that -1 will already have been
            converted to 359 before being input as one of the values in
            find_range_indices.
        """
        utilities.CanLog.__init__(self)

        _VALID_SPACING_ASSUMPTIONS = {None, "constant", "variable"}

        self._NEIGHBOURHOOD_FRACTION = 0.02
        self._MAX_NEIGHBOURHOOD_SIZE = 101
        self._MIN_NEIGHBOURHOOD_SIZE = 3

        self._treat_as_cyclic = treat_as_cyclic
        self._array = array

        if neighbourhood_size is None:
            # Automatically choose an appropriate neighbourhood size
            self._neighbourhood_size = int(min(
                max(
                    len(self._array) // (1.0 / self._NEIGHBOURHOOD_FRACTION),
                    self._MIN_NEIGHBOURHOOD_SIZE
                ),
                self._MAX_NEIGHBOURHOOD_SIZE
            ))
        else:
            self._neighbourhood_size = neighbourhood_size
        # Add one if it is an even number
        if self._neighbourhood_size % 2 == 0:
            self._neighbourhood_size += 1

        self._spacing_assumption = spacing_assumption
        if self._spacing_assumption not in _VALID_SPACING_ASSUMPTIONS:
            raise ValueError(("'{0}' is not a valid option "
                              "for spacing_assumption").format(self._spacing_assumption))

        estimate_spacing = False
        if spacing is None and \
                (self._spacing_assumption == "constant" or
                 self._spacing_assumption == "variable"):
            estimate_spacing = True

        if value_range is None:
            self._value_range = [self._array[0], self._array[-1]]
        else:
            self._value_range = value_range

        if estimate_spacing:
            self._spacing = ((self._value_range[1] - self._value_range[0]) /
                             (len(self._array) - 1))
        else:
            self._spacing = spacing

        return

    def find_index_of_nearest(self, value):
        """Finds the index in the array that corresponds to the element nearest to value.

        This has been designed to capitalise on the 'chunking' of arrays in netcdf
        files. It tries to look at as few of the array's values as possible, which will
        minimise the amount of the array that needs to be read from disk.
        """
        # Way of finding index that will read all values in the array
        # index = (np.abs(self._array - value)).argmin()

        if value <= self._value_range[0]:
            index = 0
        elif value >= self._value_range[1]:
            index = len(self._array) - 1
        elif self._spacing_assumption == "constant":
            index = int(round((value - self._value_range[0]) / self._spacing))
        elif self._spacing_assumption == "variable":
            initial_index_guess = int(
                round((value - self._value_range[0]) / self._spacing)
            )
            # Check the initial guess to see whether we were lucky enough to find it
            # first try.
            found_nearest, index_of_nearest = self._check_initial_index_guess(
                initial_index_guess, value
            )
            if found_nearest:
                index = index_of_nearest
            else:
                # We need to conduct a full search
                search_neighbourhood_start_index = max(
                    0,
                    initial_index_guess -
                    (self._neighbourhood_size - 1) // 2
                )
                index = self._find_index_of_nearest_by_neighbourhoods(
                    value, search_neighbourhood_start_index, self._neighbourhood_size
                )
        elif self._spacing_assumption is None:
            search_neighbourhood_start_index = 0
            neighbourhood_size = len(self._array)
            index = self._find_index_of_nearest_by_neighbourhoods(
                value, search_neighbourhood_start_index, neighbourhood_size
            )
        else:
            raise ValueError("Invalid spacing_assumption '{0}'.".format(
                self._spacing_assumption
            ))

        return index

    def _check_initial_index_guess(self, initial_index_guess, value):
        # Make the search neighbourhood size very small (= 3) and centre it on the
        # initial guess
        index_of_nearest_in_neighbourhood, relation_to_neighbourhood = \
            self._find_index_of_nearest_in_neighbourhood(
                value,
                max(0, initial_index_guess - 1),
                min(len(self._array), initial_index_guess + 2)
            )
        if relation_to_neighbourhood == "in":
            found_nearest = True
            index_of_nearest = index_of_nearest_in_neighbourhood
        else:
            found_nearest = False
            index_of_nearest = None

        return found_nearest, index_of_nearest

    def _get_index_of_nearest_value(self, value, index_of_adjacent_lower_value):
        value_to_left = self._array[index_of_adjacent_lower_value]
        value_to_right = self._array[index_of_adjacent_lower_value + 1]
        if value - value_to_left < value_to_right - value:
            return index_of_adjacent_lower_value
        else:
            return index_of_adjacent_lower_value + 1

    def _find_index_of_nearest_by_neighbourhoods(self, value,
                                                 neighbourhood_start_index_in,
                                                 neighbourhood_size):
        """
        Recursive function to find the index by checking neighbourhoods
        This function assumes that a check has already been carried out to verify
        that the nearest value is within the array, and not equal to or outside of
        either of its boundary values. As a result of this, the neighbourhoods are
        restricted to exclude the values at either end of the array. A special check is
        carried out to see whether the either of the ends is the nearest value.
        """
        neighbourhood_start_index = max(1, neighbourhood_start_index_in)
        neighbourhood_end_index = min(
            neighbourhood_start_index + neighbourhood_size,
            len(self._array) - 1
        )

        index_of_nearest_in_neighbourhood, relation_to_neighbourhood = \
            self._find_index_of_nearest_in_neighbourhood(
                value, neighbourhood_start_index, neighbourhood_end_index
            )

        if relation_to_neighbourhood == "in":
            index = index_of_nearest_in_neighbourhood
        elif relation_to_neighbourhood == "left":
            if index_of_nearest_in_neighbourhood == 1:
                # Since we've assumed that the nearest value is within the array and
                # not equal to or outside of either of its boundary values,
                # the nearest value must either be at index 0 or index 1
                index = self._get_index_of_nearest_value(value, 0)
            else:
                # Search a new neighbourhood to the left of this one
                new_neighbourhood_start_index = max(
                    1,
                    neighbourhood_start_index - neighbourhood_size
                )
                index = self._find_index_of_nearest_by_neighbourhoods(
                    value, new_neighbourhood_start_index, neighbourhood_size
                )
        elif relation_to_neighbourhood == "right":
            if index_of_nearest_in_neighbourhood == len(self._array) - 2:
                # Since we've assumed that the nearest value is within the array and
                # not equal to or outside of either of its boundary values,
                # the nearest value must either be at index N-1 or index N-2, where
                # N is the length of the array.
                index = self._get_index_of_nearest_value(
                    value, index_of_nearest_in_neighbourhood
                )
            else:
                new_neighbourhood_start_index = max(1, neighbourhood_end_index)
                index = self._find_index_of_nearest_by_neighbourhoods(
                    value, new_neighbourhood_start_index, neighbourhood_size
                )
        else:
            raise ValueError("Invalid relation_to_neighbourhood type '{0}'.".format(
                relation_to_neighbourhood))

        return index

    def _find_index_of_nearest_in_neighbourhood(self, value, neighbourhood_start_index,
                                                neighbourhood_end_index):
        """
        Note that the neighbourhood_start_index is a range-index i.e. it points to a
        position between values in the array, not to any value itself.
        """
        # Note that the value returned by bisect_left is a range-index,
        # like neighbourhood_start_value
        bisect_left_index = bisect.bisect_left(self._array, value,
                                               lo=neighbourhood_start_index,
                                               hi=neighbourhood_end_index)

        if bisect_left_index == neighbourhood_end_index:
            relation_to_neighbourhood = "right"
            # Note that the neighbourhood_end_index is a range-index, like the
            # neighbourhood_start_index, so we need to subtract 1 in order to get the
            # index that points to the value at the end of the neighbourhood.
            index = neighbourhood_end_index - 1
        elif bisect_left_index == neighbourhood_start_index:
            relation_to_neighbourhood = "left"
            index = neighbourhood_start_index
        else:
            relation_to_neighbourhood = "in"
            index = self._get_index_of_nearest_value(value, bisect_left_index - 1)
        return index, relation_to_neighbourhood

    def find_item_index(self, value, matching_specification):
        if matching_specification == "exact":
            index, index_before_adjustment, adjustment_direction  = \
                self._find_index_using_match_type(value, "exact", "item")
        elif matching_specification == "nearest":
            index, index_before_adjustment, adjustment_direction = \
                self._find_index_using_match_type(value, "nearest", "item")
        else:
            raise ValueError(("An invalid matching_specification '{0}' was used. See "
                              "the documentation for this function for valid "
                              "choices.").format(matching_specification))
        return index

    def find_range_indices(self, value_range, matching_specification):
        """Find the index range of the array that corresponds to the value_range.

        Parameters
        ----------
        value_range : list
            e.g. [3.2, 5.4]. The output indices will correspond to the start and end of
            this range in the array.
        matching_specification : basestring or iterable
            Examples:
                "nearest"
                ("exact", "nearest")
                ("within", "including")
            Used to specify the how the values at the start and end of the output
            range relate to the input range specified.
            Can be specified as a string, or a tuple containing two strings. If a tuple
            is used, then each element specifies how the corresponding boundary of the
            range is picked. The following options can be used in the tuple form:
            {
                "exact": If the exact value specified isn't found in the array, then an
                error is raised.
                "nearest": Finds the nearest matching value
                "within": Finds the nearest matching value that will result in the
                output range excluding the specified value
                "including": Finds the nearest matching value that will result in the
                output range including the specified value
            }
            The following options can be used if a single string is specified:
            {
                "exact": If either of the exact values specified aren't found,
                then an error is raised.
                "nearest": Finds the nearest matching values at each end of the
                range.
                "including": Finds the smallest range that includes the the input values
                specified.
                "within": Finds the largest range that is within, but does not include
                the input values specified.
            }
        """

        if not isinstance(matching_specification, str):
            # Assume tuple form has been specified
            if len(matching_specification) != 2:
                raise ValueError(("Invalid range_specification '{0}'. "
                                  "matching_specification must be either a string, or a "
                                  "list containing two strings.").format(
                    matching_specification))
            (start_index, start_index_before_adjustment,
             start_index_adjustment_direction) = \
                self._find_index_using_match_type(
                    value_range[0], matching_specification[0], "start")
            (end_index, end_index_before_adjustment,
             end_index_adjustment_direction) = \
                self._find_index_using_match_type(
                    value_range[1], matching_specification[1], "end")
        else:
            # Assume a single string has been specified.
            if matching_specification == "exact":
                (start_index, start_index_before_adjustment,
                 start_index_adjustment_direction) = \
                    self._find_index_using_match_type(value_range[0], "exact", "start")
                (end_index, end_index_before_adjustment,
                 end_index_adjustment_direction) = \
                    self._find_index_using_match_type(value_range[1], "exact", "end")
            elif matching_specification == "nearest":
                (start_index, start_index_before_adjustment,
                 start_index_adjustment_direction) = \
                    self._find_index_using_match_type(value_range[0], "nearest", "start")
                (end_index, end_index_before_adjustment,
                 end_index_adjustment_direction) = \
                    self._find_index_using_match_type(value_range[1], "nearest", "end")
            elif matching_specification == "including":
                (start_index, start_index_before_adjustment,
                 start_index_adjustment_direction) = \
                    self._find_index_using_match_type(value_range[0], "including",
                                                      "start")
                (end_index, end_index_before_adjustment,
                 end_index_adjustment_direction) = \
                    self._find_index_using_match_type(value_range[1], "including", "end")
            elif matching_specification == "within":
                (start_index, start_index_before_adjustment,
                 start_index_adjustment_direction) = \
                    self._find_index_using_match_type(value_range[0], "within", "start")
                (end_index, end_index_before_adjustment,
                 end_index_adjustment_direction) = \
                    self._find_index_using_match_type(value_range[1], "within", "end")
            else:
                raise ValueError(("An invalid matching_specification '{0}' was used. See "
                                  "the documentation for this function for valid "
                                  "choices.").format(matching_specification))

        # Because the "including" match type adjusts the output range index relative to
        # the position of the input value in the array, there is a possible scenario in
        # which the start and end range indices erroneously jump over each other. If
        # the array isn't being treated as cyclic, we won't do anything about this,
        # because subsequent error checking by other functions should catch that the
        # start_index <= end_index, giving a zero/invalid size array, but if the array
        # is being treated as cyclic, we need to do some special handling to make sure
        # the output range is either treated as covering the whole array, or being an
        # empty range, as appropriate.
        indices_jumped_over_each_other = self._check_if_indices_jumped_over_each_other(
            start_index, start_index_before_adjustment, start_index_adjustment_direction,
            end_index, end_index_before_adjustment, end_index_adjustment_direction,
            value_range[0] <= value_range[1]
        )
        if self._treat_as_cyclic and (indices_jumped_over_each_other or
                                      start_index == end_index):
            # Put both ends of the output range at the same position
            range_indices = [start_index, start_index]
            # Determine whether the output range should be treated as covering the
            # whole array or being an empty range
            if value_range[0] <= value_range[1]:
                flag = "empty"
            else:
                flag = "all"
        else:
            # We can use the range indices we found normally
            range_indices = [start_index, end_index]
            flag = None

        return range_indices, flag

    def _check_if_indices_jumped_over_each_other(
            self, start_index, start_index_before_adjustment,
            start_index_adjustment_direction, end_index, end_index_before_adjustment,
            end_index_adjustment_direction,
            is_input_start_value_less_than_or_equal_to_end):
        # First check if start index jumped over non-adjusted end index
        jumps = 0
        if is_input_start_value_less_than_or_equal_to_end:
            relative_position_of_start_index_before_adjustment = "left"
        else:
            relative_position_of_start_index_before_adjustment = "right"
        if self._check_if_index_jumped_over_another(
                start_index, start_index_before_adjustment,
                start_index_adjustment_direction, end_index_before_adjustment,
                relative_position_of_start_index_before_adjustment):
            jumps += 1
            relative_position_of_end_before_adjustment = \
                relative_position_of_start_index_before_adjustment
        else:
            if relative_position_of_start_index_before_adjustment == "left":
                relative_position_of_end_before_adjustment = "right"
            else:
                relative_position_of_end_before_adjustment = "left"
        # Now check if end index jumped over adjusted start index
        if self._check_if_index_jumped_over_another(
                end_index, end_index_before_adjustment, end_index_adjustment_direction,
                start_index, relative_position_of_end_before_adjustment):
            jumps += 1

        if jumps == 0 or jumps == 2:
            indices_jumped_over_each_other = False
        else:
            indices_jumped_over_each_other = True
        return indices_jumped_over_each_other

    def _check_if_index_jumped_over_another(self, new_index, index_before_adjustment,
                                            adjustment_direction, reference_index,
                                            relative_position_before_adjustment):
        # Check whether the adjusted index jumped over the edge of the array to the
        # other side
        if (adjustment_direction < 0 and new_index > index_before_adjustment) or \
                (adjustment_direction > 0 and new_index < index_before_adjustment):
            index_jumped_over_edge_of_array = True
        else:
            index_jumped_over_edge_of_array = False

        # Note that the result of this function is irrelevant if new_index ==
        # reference_index, so it doesn't really matter that we use <= below
        if new_index <= reference_index:
            relative_position_after_adjustment = "left"
        else:
            relative_position_after_adjustment = "right"

        if (not index_jumped_over_edge_of_array and
            relative_position_after_adjustment == relative_position_before_adjustment) \
                or \
            (index_jumped_over_edge_of_array and
             relative_position_after_adjustment != relative_position_before_adjustment):
            index_jumped_over_reference = False
        else:
            index_jumped_over_reference = True
        return index_jumped_over_reference

    def _find_index_using_match_type(self, value, match_type, index_type):
        """ This function returns range indices"""

        if index_type == "start" or index_type == "item":
            offset_for_index_type = 0
        elif index_type == "end":
            offset_for_index_type = 1
        else:
            raise ValueError(("Invalid index type '{0}'. Valid types are 'start', "
                              "'end' or 'item'.").format(index_type))
        # Note that this returns an index of type 'item', rather than range,
        # so be aware that it is later converted appropriately to a range index (
        # according to index_type).
        index_of_nearest = self.find_index_of_nearest(value)

        # Adjustment is only relevant when using the "including" or "within" match
        # types. It refers to the shifting of the range index either one to the left or
        # one to the right relative to the position of the input value in order to make
        # sure the input value is included ("including") or excluded ("within") in the
        # output range as appropriate. We need to track this when the array is being
        # treated as cyclic.
        index_before_adjustment = None
        adjustment_direction = None

        if match_type == "exact":
            if self._array[index_of_nearest] == value:
                index = index_of_nearest + offset_for_index_type
            else:
                raise ValueError(
                    ("The value '{0}' is not in the array. The nearest "
                     "value is '{1}'").format(value, self._array[index_of_nearest])
                )
        elif match_type == "nearest":
            index = index_of_nearest + offset_for_index_type
        elif match_type == "including" and index_type == "end":
            if value <= self._array[index_of_nearest]:
                index = index_of_nearest + 1
            else:
                # The nearest value in the array is smaller-than/before the input value,
                # so choose the value one after, if available, to make sure the range
                # includes the input.
                index = index_of_nearest + 2
            index_before_adjustment = index - 1
            adjustment_direction = +1
        elif match_type == "within" and index_type == "end":
            if value > self._array[index_of_nearest]:
                index = index_of_nearest + 1
            else:
                # The nearest value in the array is larger-than/after the input value,
                # so choose the value one before, if available.
                index = index_of_nearest
            # Note that no "adjustment" is done in this case
        elif match_type == "including" and index_type == "start":
            if value >= self._array[index_of_nearest]:
                index = index_of_nearest
            else:
                # The nearest value in the array is larger-than/after the input value,
                # so choose the value one before, if available, to make sure the range
                # includes the input.
                index = index_of_nearest - 1
            index_before_adjustment = index + 1
            adjustment_direction = -1
        elif match_type == "within" and index_type == "start":
            if value < self._array[index_of_nearest]:
                index = index_of_nearest
            else:
                # The nearest value in the array is smaller-than/before the input value,
                # so choose the value one after, if available.
                index = index_of_nearest + 1
            # Note that no "adjustment" is done in this case
        else:
            raise ValueError(("An invalid matching_specification '{0}' for index with "
                              "type {1} was used. See the documentation for this "
                              "function for valid choices.").format(match_type,
                                                                    index_type))

        if index_type == "item":
            maximum_allowable_index = len(self._array) - 1
        else:
            maximum_allowable_index = len(self._array)

        # Handle appropriate bounding of indices depending on whether we're treating
        # the array as cyclic or not.
        if self._treat_as_cyclic:
            # Resolve into to non-negative indices
            # Negative indices are fine, as they will be resolved correctly for both
            # "item" and range index types, but we do need to correct indices that go
            # beyond the length of the array
            if index > maximum_allowable_index:
                index -= len(self._array)
            elif index < 0:
                index += len(self._array)
        else:
            # Bound the index between 0 and the maximum allowable index (which is
            # dependent on whether it is a "item" or range index.
            index = min(
                maximum_allowable_index,
                max(
                    0,
                    index
                )
            )

        return index, index_before_adjustment, adjustment_direction


class Dataset(utilities.CanLog):
    """Abstract class for a climate change projection dataset.

    A dataset can have up to effectively 3 dimensions - it can vary across date-time,
    latitude and longitude, or any subset thereof.
    This class provides useful member functions for doing useful things with the data,
    including plotting, aggregrating and mapping to different grids.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        utilities.CanLog.__init__(self)

        # self._DEFAULT_MAPPER = analysis.DecisionTreeRegressorMapper2D()
        self._DEFAULT_MAPPER = utilities.DecisionTreeRegressorMapper2D()
        self._DEFAULT_MAPPER_POINTS = utilities.RegularGridMapperPoints()
        self._EXPORTER_FACTORY = io.ExporterFactory()

        self._assume_equally_spaced_datetimes = None
        self._climate_variable_name_to_use = None
        self._values = None
        self._numeric_datetimes = None
        self._numeric_datetime_range = None
        self._numeric_datetime_calendar = None
        self._numeric_datetime_units = None
        self._datetimes = None
        self._latitudes = None
        self._latitude_range = None
        self._longitudes = None
        self._longitude_range = None
        self._longitudes_span_globe = None
        return

    @property
    def datetimes(self):
        if self._numeric_datetimes is None:
            return None
        else:
            if self._datetimes is None:
                self._datetimes = self._convert_to_datetime_format(
                    self._numeric_datetimes)
            return self._datetimes

    @property
    def climate_variable_name(self):
        return self._climate_variable_name_to_use

    @property
    def values(self):
        return self._values

    @property
    def numeric_datetimes(self):
        return self._numeric_datetimes

    @property
    def numeric_datetime_range(self):
        return self._numeric_datetime_range

    @property
    def numeric_datetime_calendar(self):
        return self._numeric_datetime_calendar

    @property
    def numeric_datetime_units(self):
        return self._numeric_datetime_units

    @property
    def assume_equally_spaced_datetimes(self):
        return self._assume_equally_spaced_datetimes

    @property
    def latitudes(self):
        return self._latitudes

    @property
    def latitude_range(self):
        return self._latitude_range

    @property
    def longitudes(self):
        return self._longitudes

    @property
    def longitude_range(self):
        return self._longitude_range

    @property
    def longitudes_span_globe(self):
        return self._longitudes_span_globe

    @abc.abstractmethod
    def subset(self, datetime_subset_specification=None,
               latitude_subset_specification=None,
               longitude_subset_specification=None):
        return

    def __sub__(self, dataset):
        # TODO: Implement other simple maths operators
        return self.subtract(dataset)

    def __div__(self, dataset):
        return self.divide(dataset)

    def map_to_reference(self, reference_dataset, mapper=None):
        """Maps this dataset onto the same grid as a reference_dataset

        Currently only supports mapping when both this and the reference_dataset only
        vary with latitude and longitude.

        Parameters
        ----------
        reference_dataset : :obj:`climate_projections.data.Dataset`
            The target grid to map to will be taken from this dataset.
        mapper : :obj:`climate_projections.utilities.Mapper2D`, optional
            Optionally specify an object that will perform the mapping. If not
            provided, a default mapper will be used.

        Returns
        -------
        mapped_dataset : :obj:`climate_projections.data.Dataset`
            Note that this object remains unchanged.
        """
        mapped_dataset = self.map_to(
            datetimes=reference_dataset.datetimes, latitudes=reference_dataset.latitudes,
            longitudes=reference_dataset.longitudes, mapper=mapper
        )
        return mapped_dataset

    def map_to(self, datetimes=None, latitudes=None, longitudes=None, mapper=None):
        """Maps this dataset onto the specified grid

        Currently only supports mapping when both this and the target grid only
        vary with latitude and longitude.

        Parameters
        ----------
        datetimes : list of
            The target grid to map to will be taken from this dataset.
        mapper : :obj:`climate_projections.utilities.Mapper2D`, optional
            Optionally specify an object that will perform the mapping. If not
            provided, a default mapper will be used.

        Returns
        -------
        mapped_dataset : :obj:`climate_projections.data.Dataset`
            Note that this object remains unchanged.
        """
        if self._numeric_datetimes is None and datetimes is not None:
            raise ValueError(
                ("Cannot map to specified date-times because this dataset does not vary "
                 "with time")
            )
        if self._latitudes is None and latitudes is not None:
            raise ValueError(
                ("Cannot map to specified latitudes because this dataset does not vary "
                 "with latitude")
            )
        if self._longitudes is None and longitudes is not None:
            raise ValueError(
                ("Cannot map to specified longitudes because this dataset does not vary "
                 "with longitude")
            )

        if mapper is None:
            mapper_to_use = self._DEFAULT_MAPPER
        else:
            mapper_to_use = mapper


        ## The following try/except statements are there to try and get the datetimes into formats which can be compared with each other
        try:
            target_datetimes=np.asarray([i._to_real_datetime() for i in datetimes])
        except:
            target_datetimes=datetimes

        try:
            self_datetimes=np.asarray([i._to_real_datetime() for i in self.datetimes])
        except:
            self_datetimes=self.datetimes

        if not (target_datetimes is None and self_datetimes is None):
            if not (target_datetimes==self_datetimes).all(): # Need to deal with different date time formats here gah
                raise NotImplementedError("Both the dataset to map, and the reference dataset, need to have have matching datetime arrays")


        if latitudes is None:
            target_latitudes = self._latitudes
        else:
            target_latitudes = latitudes
        if longitudes is None:
            target_longitudes = self._longitudes
        else:
            target_longitudes = longitudes
        # TODO: Different potential mapping methodologies could be significantly
        # more complex than the current default. I may need to return to this
        # and put more of the latitude and longitude handling into the mapping
        # class and re-write this appropriately.

        if datetimes is None: # No variation in time
            mapped_values = mapper_to_use.map(
                self._values, self._latitudes, self._longitudes,
                target_latitudes, target_longitudes
            )
        else:
            mapped_values=[]
            for i in range(np.shape(self._values)[0]):
                mapped_values.append(mapper_to_use.map(self._values[i], self._latitudes, self._longitudes,target_latitudes, target_longitudes))


        mapped_dataset = NumpyDataset(
            mapped_values, numeric_datetimes=self._numeric_datetimes, latitudes=target_latitudes,
            longitudes=target_longitudes,
            climate_variable_name=self._climate_variable_name_to_use,
            assume_equally_spaced_datetimes=self._assume_equally_spaced_datetimes,
            numeric_datetime_calendar=self._numeric_datetime_calendar,
            numeric_datetime_units=self._numeric_datetime_units
        )

        return mapped_dataset

    def extract_data_at_coordinates(self, datetimes=None, latitudes=None, longitudes=None, mapper=None):
        """
        Interpolate and extracts data at specified coordinates particular coordinates
        This is a bit of a hack at the moment, but the functionality is needed.
        
        Parameters
        ----------
        datetimes : list of datetimes - does nothing
        latitudes : list of latitudes to map to. Each latitude corresponds to a longitude
        longitude : list of longitudes to map to
                      cordinates will be specified as [latitude[i], longitude[i]] where i is the coordinate number
        mapper : :obj:`climate_projections.utilities.Mapper2D`, optional
            Optionally specify an object that will perform the mapping. If not
            provided, a default mapper will be used.

        Returns
        -------
        List of numpy datasets, each containing values for a single longitude latitude pair
        """

        if self._latitudes is None and latitudes is not None:
            raise ValueError(
                ("Cannot map to specified latitudes because this dataset does not vary "
                 "with latitude")
            )
        if self._longitudes is None and longitudes is not None:
            raise ValueError(
                ("Cannot map to specified longitudes because this dataset does not vary "
                 "with longitude")
            )

        if mapper is None:
            mapper_to_use = self._DEFAULT_MAPPER_POINTS
        else:
            mapper_to_use = mapper


        if latitudes is None:
            target_latitudes = self._latitudes
        else:
            target_latitudes = latitudes
        if longitudes is None:
            target_longitudes = self._longitudes
        else:
            target_longitudes = longitudes
        
        data_at_coords=[]
        

        if not self.numeric_datetimes is None:       # (not self._datetimes is None):   <-- This doesnt work for some reason!! why? if i break on this line, it works, but if i dont, it enters the wrong statement?
            for loop in range(np.shape(self.values)[0]):
                mapped_values = mapper_to_use.map(
                self._values[loop,:,:], self._latitudes, self._longitudes,
                target_latitudes, target_longitudes)
                data_at_coords.append(mapped_values)
                
            data_at_coords=np.asarray(data_at_coords)

            listOfExtractedCoordDatasets=[]
            for i in range(np.shape(data_at_coords)[1]):
                extractedCoordDataSet=NumpyDataset(np.expand_dims(np.expand_dims(data_at_coords[:,i],axis=1),axis=2),
                                    numeric_datetimes=self.numeric_datetimes,
                                    climate_variable_name=self._climate_variable_name_to_use,
                                    numeric_datetime_calendar=self._numeric_datetime_calendar,
                                    numeric_datetime_units=self._numeric_datetime_units,
                                    latitudes=[target_latitudes[i]],
                                    longitudes=[target_longitudes[i]]
                                    )
                listOfExtractedCoordDatasets.append(extractedCoordDataSet)

            return listOfExtractedCoordDatasets
        
        else:

            mapped_values = mapper_to_use.map(
            self._values[:,:], self._latitudes, self._longitudes,
            target_latitudes, target_longitudes)

            data_at_coords.append(mapped_values)

            extractedCoordDataSet=NumpyDataset(data_at_coords,
                                numeric_datetimes=self.numeric_datetimes,
                                climate_variable_name=self._climate_variable_name_to_use,
                                numeric_datetime_calendar=self._numeric_datetime_calendar,
                                numeric_datetime_units=self._numeric_datetime_units,
                                latitudes=target_latitudes,
                                longitudes=target_longitudes
                                )
                                
            return extractedCoordDataSet

    def aggregate_over_time(self, datetime_range_specification, aggregration_function):
        # TODO: Use the numpy.apply_along_axis function to implement this.
        pass

    def aggregate_over_time_with_numpy_function_over_axis(self,
                                                          datetime_range_specification,
                                                          numpy_function_over_axis):
        """ """

        if self._numeric_datetimes is None:
            raise AttributeError("This dataset doesn't vary with time.")
            # TODO: Implement this. The time array is a set of grid-centre points and the
            # 'time_bnds' array holds the actual grid cell sizes. If we don't assume the
            # date-times are equally spaced, then we need to use the time_bnds array to
            # get the time bounds.
        else:
            if not self._assume_equally_spaced_datetimes:
                raise NotImplementedError(
                    "Currently only works when datetimes are equally spaced")
                # Can be implemented using numpy.average, which accepts weights
                # https://docs.scipy.org/doc/numpy/reference/generated/numpy.average.html
            else:
                if self._latitudes is not None:
                    latitude_subset_specification = 'all'
                else:
                    latitude_subset_specification = None
                if self._longitudes is not None:
                    longitude_subset_specification = 'all'
                else:
                    longitude_subset_specification = None
                dataset_for_specified_datetimes = self.subset(
                    datetime_subset_specification=datetime_range_specification,
                    latitude_subset_specification=latitude_subset_specification,
                    longitude_subset_specification=longitude_subset_specification
                )
                climate_variable_as_numpy_array = np.asarray(
                    dataset_for_specified_datetimes.values[:])
                # Note that datetime will always be along the first axis
                result = numpy_function_over_axis(climate_variable_as_numpy_array, 0)
                # Create NumpyDataset
                resulting_dataset = NumpyDataset(
                    result, numeric_datetimes=None,
                    latitudes=self._latitudes, longitudes=self._longitudes,
                    climate_variable_name=self._climate_variable_name_to_use,
                    assume_equally_spaced_datetimes=self._assume_equally_spaced_datetimes,
                    numeric_datetime_calendar=None, numeric_datetime_units=None
                )

        return resulting_dataset

    def mean_over_time(self, datetime_range_specification):
        return self.aggregate_over_time_with_numpy_function_over_axis(
            datetime_range_specification, np.mean)

    def aggregate_over_space(self, aggregation_function):
        pass

    def binary_operate(self, dataset, operation, map_to_this=False):
        """

        If the datetimes, latitudes and longitudes of the two datasets do not match,
        set interpolate=True to interpolate the input dataset to be consistent with
        this dataset. Note that, if interpolate=False, no check is carried out to ensure
        they are consistent with each other.

        Parameters
        ----------
        dataset
        operation : {'difference', 'fraction', callable function}
        map_to_this : bool, optional
            Whether or not to interpolate the input dataset to match this dataset first.

        Returns
        -------

        """
        pass

    def binary_operate_with_numpy_ufunc(self, dataset, numpy_ufunc, map_to_this=False,
                                        mapper=None):
        """Applies a specified binary operator (a numpy "ufunc") to this and another
        dataset.

        Typical binary operators are addition, subtraction, multiplication and
        division. Any arbitrary binary operator can be used, but the numpy_ufunc input
        must be an object that operates similarly to a numpy ufunc object.
        The dataset in this object is the primary operand, the input dataset to the
        function is the secondary operand.

        Parameters
        ----------
        map_to_this : bool, optional
            If the datetimes, latitudes and longitudes of the two datasets do not match,
            set map_to_this=True to map the input dataset to the same grid as the
            dataset stored in this object, before performing the binary operation. Note
            that, if map_to_this=False, no check is carried out to ensure they are
            consistent with each other.
        numpy_ufunc : numpy ufunc or similar
            e.g. numpy.divide, numpy.subtract
            This is a callable object (e.g. a function) that is or acts like a numpy
            "universal function" aka a "ufunc". It can do a binary operation,
            like addition or multiplication, with two multi-dimensional lists.
            For a list of useful ufuncs, see https://docs.scipy.org/doc/numpy-1.15.1/reference/ufuncs.html#math-operations
            For an in-depth explanation of ufuncs, see https://docs.scipy.org/doc/numpy-1.15.1/reference/ufuncs.html
        mapper : :obj:`climate_projections.utilities.Mapper2D`, optional
            Optionally specify an object that will perform the mapping. If not
            provided, a default mapper will be used.

        Returns
        -------
        result_dataset : :obj:`climate_projections.data.Dataset`
            Note that this object remains unchanged.
        """


        # TODO: subsetting
        # subset_future_dataset = self.subset(
        #     datetime_range_specification=future_date_range_specification,
        #     latitude_range_specification=latitude_range_specification,
        #     longitude_range_specification=longiutde_range_specification
        # )
        # subset_historic_dataset = historic_dataset.subset(
        #     datetime_range_specification=historic_date_range_specification,
        #     latitude_range_specification=latitude_range_specification,
        #     longitude_range_specification=longiutde_range_specification
        # )
        # subset_future_dataset = self
        # subset_historic_dataset = dataset
        #
        # averaged_future_dataset = subset_future_dataset.mean_over_time('all')
        # averaged_historic_dataset = subset_historic_dataset.mean_over_time('all')

        # if relative_change_operation == 'difference':
        #     averaged_future_dataset_array = np.asarray(
        #         averaged_future_dataset.climate_variable)
        #     averaged_historic_dataset_array = np.asarray(
        #         averaged_historic_dataset.values)
        #     relative_change = averaged_future_dataset - averaged_historic_dataset_array
        # elif relative_change_operation == 'fraction':
        #     averaged_future_dataset_array = np.asarray(
        #         averaged_future_dataset.climate_variable)
        #     averaged_historic_dataset_array = np.asarray(
        #         averaged_historic_dataset.values)
        #     relative_change = averaged_future_dataset / averaged_historic_dataset_array
        # elif callable(relative_change_operation):
        # # TODO: Implement user entering their own custom function
        #     raise NotImplementedError("Custom operations are not currently supported.")
        # else:
        #     raise ValueError("Unsupported relative_change_operation '{0}'.".format(
        #         relative_change_operation
        #     ))

        operand1 = np.asarray(self._values)
        if map_to_this:
            mapped_dataset = dataset.map_to(self.datetimes, self._latitudes,
                                            self._longitudes, mapper=mapper)
            operand2 = np.asarray(mapped_dataset.values)
        else:
            operand2 = np.asarray(dataset.values)

        result_values = numpy_ufunc(operand1, operand2)

        result_dataset = NumpyDataset(
            result_values, numeric_datetimes=self._numeric_datetimes,
            latitudes=self._latitudes, longitudes=self._longitudes,
            climate_variable_name=self._climate_variable_name_to_use,
            assume_equally_spaced_datetimes=self._assume_equally_spaced_datetimes,
            numeric_datetime_calendar=self._numeric_datetime_calendar,
            numeric_datetime_units=self._numeric_datetime_units
        )
        return result_dataset

    def subtract(self, dataset, map_to_this=False, mapper=None):
        """Subtract the input dataset from the dataset stored in this object.

        See the binary_operate_with_numpy_ufunc() function for information on the
        optional arguments.
        """
        # TODO: Implement other simple maths operators
        return self.binary_operate_with_numpy_ufunc(dataset, np.subtract,
                                                    map_to_this=map_to_this,
                                                    mapper=mapper)

    def divide(self, dataset, map_to_this=False, mapper=None):
        """Divide the dataset stored in this object by the input dataset.

        See the binary_operate_with_numpy_ufunc() function for information on the
        optional arguments.
        """
        return self.binary_operate_with_numpy_ufunc(dataset, np.divide,
                                                    map_to_this=map_to_this,
                                                    mapper=mapper)

    def plot(self, datetime_specification=None, latitude_specification=None,
             longitude_specification=None, figure=None, subplot_position=(1, 1, 1),
             **plotting_options):
        """
        Plots the data over a map with the specified projection

        Parameters
        ----------

        datetime_specification :
            Required if the dataset varies with time.

        new_figure : bool, optional
            If True, will create a new figure for the plot. If you want to add the
            plot, for example, as a subplot inside an existing figure, you can set this
            to False and use the output figure_axes instead.

        Returns
        -------
        figure_axes : matlibplot figure_axes object

        """
        self._check_specifications_against_dimensions(datetime_specification,
                                                      latitude_specification,
                                                      longitude_specification,
                                                      False)
        if datetime_specification is None and self._numeric_datetimes is not None:
            datetime_specification_to_use = 'all'
        else:
            datetime_specification_to_use = datetime_specification
        if latitude_specification is None and self._latitudes is not None:
            latitude_specification_to_use = 'all'
        else:
            latitude_specification_to_use = latitude_specification
        if longitude_specification is None and self._longitudes is not None:
            longitude_specification_to_use = 'all'
        else:
            longitude_specification_to_use = longitude_specification

        dataset_to_plot = self.subset(
            datetime_subset_specification=datetime_specification_to_use,
            latitude_subset_specification=latitude_specification_to_use,
            longitude_subset_specification=longitude_specification_to_use
        )
        return dataset_to_plot.plot_whole_dataset(figure=figure,
                                                  subplot_position=subplot_position,
                                                  **plotting_options)

    def plot_whole_dataset(self, figure=None, subplot_position=(1, 1, 1),
                           **plotting_options):
        # Determine the number of dimensions that the output data has so that we know
        # what plotting method to use
        num_plotting_dimensions = self._values.ndim

        if num_plotting_dimensions >= 3:
            raise ValueError(("The resulting dataset has too many dimensions ({0}) to "
                              "plot.").format(num_plotting_dimensions))
        elif num_plotting_dimensions == 2:
            if self._numeric_datetimes is None and self._latitudes is not None and \
                    self._longitudes is not None:
                plotter = Plotter2DMap()
                plotter_arguments = (self._latitudes, self._longitudes, self._values)
                x_label = 'Latitude'
                y_label = 'Longitude'
            else:
                raise NotImplementedError(("Can currently only plot when the dataset's "
                                           "axes are latitude and longitude."))
        elif num_plotting_dimensions == 1:
            if self._numeric_datetimes is not None and self._latitudes is None and \
                    self._longitudes is None:
                plotter = PlotterDateTime()
                plotter_arguments = (self.datetimes[:], self._values[:])
                x_label = 'Date-time'
                y_label = self._climate_variable_name_to_use
        else:
            raise NotImplementedError(("The resulting dataset has {0} dimensions. "
                                       "Cannot currently plot this many.").format(
                num_plotting_dimensions))

        plotting_options_to_use = plotting_options.copy()
        # It seems x and y labels don't work in the standard way for cartopy plots
        if 'x_label' not in plotting_options_to_use:
            plotting_options_to_use['x_label'] = x_label
        if 'y_label' not in plotting_options_to_use:
            plotting_options_to_use['y_label'] = y_label
        if 'title' not in plotting_options_to_use:
            plotting_options_to_use['title'] = self._climate_variable_name_to_use

        figure_to_use, output_figure_axes = plotter.plot(
            *plotter_arguments, figure=figure,
            subplot_position=subplot_position, **plotting_options_to_use
        )

        return figure_to_use, output_figure_axes

    def export_netcdf(self, file_path):

        return

    def export(self, file_path, exporter_type=None, **exporter_options):
        if exporter_type is None:
            # Try determining the type from the file name
            exporter = self._EXPORTER_FACTORY.get_exporter_from_file_name(
                file_path, **exporter_options)
        else:
            exporter = self._EXPORTER_FACTORY.get_exporter(exporter_type,
                                                           **exporter_options)

        return exporter.export(self, file_path)


    def data_at_location(self, datetime_specification=None, latitude_specification=None, longitude_specification=None):
    # Look at this function - is it depreciated
    # Specify the (nearest) time, lat and long to extract the value from the dataset at
    # Returns the value, and the time, lat and long it corresponds to
    # currently only works for Numpy Dataset, not for NetCDF

        datetime = None
        latitude = None
        longitude = None

        self._check_specifications_against_dimensions(datetime_specification,
                                                      latitude_specification,
                                                      longitude_specification,
                                                      True)

        # Set-up structure to serach for specified datetime, latitude and longitude
        numeric_datetimes_subset, datetime_getitem_index_specification = \
            self._setup_subset_for_dimension(
                datetime_specification, self._numeric_datetimes, "date-time",
                self._prepare_numeric_datetime_array_and_search_values_for_index_search
            )
        latitudes_subset, latitude_getitem_index_specification = \
            self._setup_subset_for_dimension(
                latitude_specification, self._latitudes, "latitude",
                self._prepare_latitude_array_and_search_values_for_index_search
            )
        longitudes_subset, longitude_getitem_index_specification = \
            self._setup_subset_for_dimension(
                longitude_specification, self._longitudes, "longitude",
                self._prepare_longitude_array_and_search_values_for_index_search
            )

        values_getitem_index_specification = []

        if datetime_getitem_index_specification is not None:
            values_getitem_index_specification.append(datetime_getitem_index_specification)
            datetime = self._datetimes[datetime_getitem_index_specification]

        if latitude_getitem_index_specification is not None:
            values_getitem_index_specification.append(latitude_getitem_index_specification)
            latitude = self._latitudes[latitude_getitem_index_specification]

        if longitude_getitem_index_specification is not None:
            values_getitem_index_specification.append(longitude_getitem_index_specification)
            longitude = self._longitudes[longitude_getitem_index_specification]

        values_getitem_index_specification = tuple(values_getitem_index_specification)
        value = self._values[values_getitem_index_specification]

        return {"value": value, "datetime": datetime, "latitude": latitude, "longitude": longitude}


    def _subset_dimension(self, subset_specification, underlying_array,
                          dimension_is_cyclic, dimension_name,
                          function_to_prepare_array_and_values_for_index_search):
        if subset_specification is None:
            subset_index_specification_in_underlying_array = None
        else:
            if ('range' in subset_specification and 'value' in
                    subset_specification) or \
                    ('value' in subset_specification and 'index' in
                     subset_specification) or \
                    ('range' in subset_specification and 'index' in
                     subset_specification):
                raise ValueError(
                    ("Invalid {0} subset specification. More than one of the keys "
                     "'value', 'range' or 'index' have been specified. Only one can be "
                     "specified. Input subset specification: {1}.").format(
                        dimension_name, subset_specification
                    )
                )

            if subset_specification == 'all':
                subset_index_specification_in_underlying_array = [
                    0, len(underlying_array)]
            elif 'range' in subset_specification:
                if subset_specification['range'] == 'all':
                    subset_index_specification_in_underlying_array = [
                        0, len(underlying_array)]
                elif subset_specification['type'] == 'indices':
                    subset_index_specification_in_underlying_array = \
                        subset_specification['range']
                else:
                    resolved_underlying_array, resolved_value_range = \
                            function_to_prepare_array_and_values_for_index_search(
                                underlying_array, subset_specification['range'])

                    index_finder = ElementIndexFinder(
                        resolved_underlying_array,
                        spacing=subset_specification.get('spacing', None),
                        spacing_assumption=subset_specification.get(
                            'spacing_assumption', None),
                        treat_as_cyclic=dimension_is_cyclic
                    )
                    subset_index_specification_in_underlying_array, index_range_flag = \
                        index_finder.find_range_indices(
                            resolved_value_range,
                            subset_specification['type']
                        )
                    # If the longitudes are treated as cyclic, in some scenarios it's
                    # possible for the start index in longitude_range_indices to be >= the
                    # end index (which indicates we want the range to cycle over the seam)
                    # even though we actually wanted a small range (i.e. not spanning over
                    # the seam; in this scenario, we should actually return a zero-length
                    # range). The reverse scenario can also occur: the start index is <=
                    # the end index and we actually wanted the large range that spans over
                    # the seam (in this scenario, we should actually return the whole
                    # range). In these scenarios we modify the longitude_range_indices
                    # to resolve this problem.
                    if dimension_is_cyclic and index_range_flag == "empty":
                        raise ValueError(
                            ("The specified {0} range "
                             "contains no elements: '{1}'").format(
                                dimension_name, subset_specification)
                        )
                    elif dimension_is_cyclic and index_range_flag == "all":
                        self._logger.warning(
                            ("The specified {0} subset range '{1}' contains all elements "
                             "in the underlying array. The full underlying array will "
                             "be used, which means the first element will not "
                             "necessarily correspond to the start of the specified "
                             "range.").format(dimension_name, subset_specification)
                        )
                        subset_index_specification_in_underlying_array = [
                            0, len(underlying_array)]

                if not dimension_is_cyclic and \
                        (subset_index_specification_in_underlying_array[1] <=
                         subset_index_specification_in_underlying_array[0]):
                    raise ValueError(
                        "The specified {0} range '{1}' contains no elements".format(
                            dimension_name, subset_specification)
                    )
            elif 'list' in subset_specification:
                if subset_specification['type'] == 'indices':
                    subset_index_specification_in_underlying_array = \
                    subset_specification['list']
                else:
                   # for x in subset_specification['list']:

                    raise ValueError(
                        ("Clare has not supported this option yet ")
                    )
            elif 'value' in subset_specification:
                resolved_underlying_array, resolved_value = \
                    function_to_prepare_array_and_values_for_index_search(
                        underlying_array, subset_specification['value'])

                index_finder = ElementIndexFinder(
                    resolved_underlying_array,
                    spacing=subset_specification.get('spacing', None),
                    spacing_assumption=subset_specification.get(
                        'spacing_assumption', None),
                    treat_as_cyclic=dimension_is_cyclic
                )
                subset_index_specification_in_underlying_array = \
                    index_finder.find_item_index(
                        resolved_value,
                        subset_specification['type']
                    )
            elif 'index' in subset_specification:
                subset_index_specification_in_underlying_array = \
                    subset_specification['index']
            else:
                raise ValueError(
                    ("Invalid {0} subset specification. See documentation for "
                     "valid specifications. Input subset specification: "
                     "{1}.").format(dimension_name, subset_specification)
                )

        return subset_index_specification_in_underlying_array

    def _prepare_numeric_datetime_array_and_search_values_for_index_search(
            self, numeric_datetimes, datetime_search_specification):
        numeric_datetime_search_specification = self._convert_to_numeric_datetime_format(
            datetime_search_specification)
        return numeric_datetimes, numeric_datetime_search_specification

    def _prepare_latitude_array_and_search_values_for_index_search(
            self, latitudes, latitude_search_specification):
        return latitudes, latitude_search_specification

    def _prepare_longitude_array_and_search_values_for_index_search(
            self, longitudes, longitude_search_specification):
        # Determine what longitude range to resolve to
        longitude_range_to_resolve_to = _calculate_longitude_range_to_resolve_to(
            longitudes)

        if longitudes[-1] < longitudes[0]:
            # Then the array of longitudes is not monotonically increasing,
            # which is a requirement for the ElementIndexFinder to work. To fix
            # this, we use the NetCDFMonotonicLongitudes class, which will
            # resolve the longitudes into a range such that the array will be
            # monotonically increasing, but only resolves the longitudes as
            # they are requested (using e.g. longitudes[2:5]). This avoids
            # unnecessarily processing the whole array ahead of time i.e. this
            # is lazy evaluation.
            monotonic_longitudes = MonotonicLongitudes(
                longitudes,
                longitude_range_to_resolve_to=longitude_range_to_resolve_to
            )
        else:
            monotonic_longitudes = longitudes

        try:
            iterator = iter(longitude_search_specification)
        except TypeError:
            is_iterable = False
        else:
            is_iterable = True

        if is_iterable:
            resolved_longitude_search_specification = [
                _resolve_longitude_into_range(longitude, longitude_range_to_resolve_to)
                for longitude in longitude_search_specification
            ]
        else:
            resolved_longitude_search_specification = _resolve_longitude_into_range(
                longitude_search_specification, longitude_range_to_resolve_to)

        return monotonic_longitudes, resolved_longitude_search_specification

    def _check_specifications_against_dimensions(
            self, datetime_subset_specification, latitude_subset_specification,
            longitude_subset_specification, require_specifications_for_active_dimensions):
        self._check_specification_against_dimension(
            datetime_subset_specification, self._numeric_datetimes, "time",
            require_specifications_for_active_dimensions
        )
        self._check_specification_against_dimension(
            latitude_subset_specification, self._latitudes, "latitude",
            require_specifications_for_active_dimensions
        )
        self._check_specification_against_dimension(
            longitude_subset_specification, self._longitudes, "longitude",
            require_specifications_for_active_dimensions
        )
        return

    def _check_specification_against_dimension(
            self, subset_specification, target_array, dimension_name,
            require_specification_if_dimension_is_active):
        if target_array is None:
            if subset_specification is not None:
                raise ValueError(
                    ("This dataset does not vary with {0}, so it cannot be "
                     "subset with '{1}'.").format(dimension_name, subset_specification)
                )
        else:
            if require_specification_if_dimension_is_active and \
                    subset_specification is None:
                raise ValueError(
                    ("This dataset varies with {0}, so a subset_specification must be "
                     "specified for this dimension.").format(dimension_name)
                )
        return

    def _convert_to_numeric_datetime_format(self, datetimes):
        return netCDF4.date2num(datetimes, self._numeric_datetime_units,
                                self._numeric_datetime_calendar)

    def _convert_to_datetime_format(self, numeric_datetimes):
        return netCDF4.num2date(numeric_datetimes, self._numeric_datetime_units,
                                self._numeric_datetime_calendar)


# Create a NetCDFDataset(Dataset) that capitalises on the lazy evaluation of
# the NetCDF arrays, which could be particularly useful if the data is stored online
class NetCDFDataset(Dataset):
    def __init__(self, netcdf_dataset_object, netcdf_climate_variable_name,
                 datetime_subset_specification, latitude_subset_specification,
                 longitude_subset_specification,
                 netcdf_longitudes_span_globe,
                 climate_variable_name_to_use=None,
                 assume_equally_spaced_datetimes=False):
        """

        Parameters
        ----------
        netcdf_dataset_object
        datetime_subset_specification
        latitude_subset_specification
        longitude_subset_specification
        netcdf_longitudes_span_globe : bool
            If True, then the longitude variable in the underlying netcdf file will be
            treated as cyclic, meaning that the user can specify a range that crosses
            over the longitude = 0.0 point (or wherever the 'seam' is).
        longitude_range_to_resolve_to : {None, :list:}, optional
            Optionally specify the longitude range that all longitudes will be resolved
            to (e.g. if [0, 360] is specified, then -1 and 367 will be resolved to 359
            and 7 respectively). This range should correspond to the range used in the
            underlying NetCDF file. If None is specified, then it is calculated from
            the longitudes stored in the underlying NetCDF file.
        """
        Dataset.__init__(self)
        # TODO: Need to track any dimensions that have been effectively removed (by
        # specifying a single item at that dimension), because this information is
        # needed when further subsetting of the same NetCDF dataset is done.

        # Just need to initialise this
        self._datetimes = None

        self._netcdf_dataset_object = netcdf_dataset_object
        if isinstance(self._netcdf_dataset_object, netCDF4.MFDataset):
            self._netcdf_numeric_datetimes = netCDF4.MFTime(
                self._netcdf_dataset_object.variables[io.DEFAULT_NETCDF_DIMENSION_NAMES[io.NUMERIC_DATE_TIMES_ID]]
            )
        else:
            self._netcdf_numeric_datetimes = self._netcdf_dataset_object.variables[
                io.DEFAULT_NETCDF_DIMENSION_NAMES[io.NUMERIC_DATE_TIMES_ID]
            ]
        self._netcdf_latitudes = self._netcdf_dataset_object.variables[
            io.DEFAULT_NETCDF_DIMENSION_NAMES[io.LATITUDES_ID]
        ]
        self._netcdf_longitudes = self._netcdf_dataset_object.variables[
            io.DEFAULT_NETCDF_DIMENSION_NAMES[io.LONGITUDES_ID]
        ]
        self._netcdf_longitudes_span_globe = netcdf_longitudes_span_globe
        self._netcdf_climate_variable_name = netcdf_climate_variable_name
        if climate_variable_name_to_use is not None:
            self._climate_variable_name_to_use = climate_variable_name_to_use
        else:
            self._climate_variable_name_to_use = self._netcdf_climate_variable_name
        self._assume_equally_spaced_datetimes = assume_equally_spaced_datetimes
        # TODO: Make the range specification arguments optional. There should be a
        # range specification provided for each relevant dimension that exists in the
        # netcdf file. Below, implement a check to ensure the NetCDF file contains the
        # exact same set of dimensions (no more or less).

        self._numeric_datetime_units = self._netcdf_numeric_datetimes.units
        self._numeric_datetime_calendar = self._netcdf_numeric_datetimes.calendar

        # Do subsetting
        # Note that the output datetimes_are_cyclic is not used, as it's irrelevant for
        # datetimes.
        (netcdf_datetime_subset_index_specification, self._numeric_datetimes,
         self._numeric_datetime_range, datetimes_are_cyclic) = \
            self._setup_subset_for_dimension(
                datetime_subset_specification, self._netcdf_numeric_datetimes,
                False, "date-time",
                self._prepare_numeric_datetime_array_and_search_values_for_index_search
            )
        # Note that the output latitudes_are_cyclic is not used, as it's irrelevant for
        # latitudes.
        (netcdf_latitude_subset_index_specification, self._latitudes,
         self._latitude_range, latitudes_are_cyclic) = \
            self._setup_subset_for_dimension(
                latitude_subset_specification, self._netcdf_latitudes,
                False, "latitude",
                self._prepare_latitude_array_and_search_values_for_index_search
            )
        (netcdf_longitude_subset_index_specification, self._longitudes,
         self._longitude_range, self._longitudes_span_globe) = \
            self._setup_subset_for_dimension(
                longitude_subset_specification, self._netcdf_longitudes,
                self._netcdf_longitudes_span_globe, "longitude",
                self._prepare_longitude_array_and_search_values_for_index_search
            )

        self._subset_index_specification_for_each_dimension = []
        values_subset_index_specifications = []
        cyclic_netcdf_variable_dimension_indices = []
        if netcdf_datetime_subset_index_specification is not None:
            values_subset_index_specifications.append(
                netcdf_datetime_subset_index_specification)
            self._subset_index_specification_for_each_dimension.append(
                netcdf_datetime_subset_index_specification)
        else:
            self._subset_index_specification_for_each_dimension.append(None)
        if netcdf_latitude_subset_index_specification is not None:
            values_subset_index_specifications.append(
                netcdf_latitude_subset_index_specification)
            self._subset_index_specification_for_each_dimension.append(
                netcdf_latitude_subset_index_specification)
        else:
            self._subset_index_specification_for_each_dimension.append(None)
        if netcdf_longitude_subset_index_specification is not None:
            values_subset_index_specifications.append(
                netcdf_longitude_subset_index_specification)
            self._subset_index_specification_for_each_dimension.append(
                netcdf_longitude_subset_index_specification)
            if self._netcdf_longitudes_span_globe:
                cyclic_netcdf_variable_dimension_indices.append(
                    len(values_subset_index_specifications) - 1
                )
        else:
            self._subset_index_specification_for_each_dimension.append(None)

        self._values = NetCDFVariable(
            self._netcdf_dataset_object.variables[self._netcdf_climate_variable_name],
            values_subset_index_specifications,
            cyclic_netcdf_variable_dimension_indices
            =cyclic_netcdf_variable_dimension_indices
        )
        return

    @property
    def netcdf_climate_variable_name(self):
        return self._netcdf_climate_variable_name

    def _setup_subset_for_dimension(
            self, subset_specification, underlying_array, underlying_array_is_cyclic,
            dimension_name, function_to_prepare_array_and_values_for_index_search):

        # Find and setup the specified subset of longitudes
        subset_index_specification_in_underlying_array = self._subset_dimension(
            subset_specification, underlying_array, underlying_array_is_cyclic,
            dimension_name, function_to_prepare_array_and_values_for_index_search
        )

        if subset_index_specification_in_underlying_array is None:
            # This dimension doesn't exist in the underlying netcdf
            array_subset = None
            array_subset_range = None
            subset_is_cyclic = None
        elif isinstance(subset_index_specification_in_underlying_array, int):
            # A single value was specified in the input subset specification,
            # so the corresponding item at this dimension will be used, and this
            # dimension will effectively be removed from the resulting subset.
            array_subset = None
            array_subset_range = None
            subset_is_cyclic = None
        else:
            # Assume a range was specified in the input subset specification
            if underlying_array_is_cyclic:
                cyclic_variable_dimension_indices = [0]
            else:
                cyclic_variable_dimension_indices = []
            # Only if the underlying netcdf values for this dimension are cyclic (e.g.
            # if the array is longitudes and they span a complete loop over the globe)
            # and the specified value range uses all the underlying values will the
            # subset also be cyclic, and therefore support cyclic value specification
            # (which is handled by the NetCDFVariable class)
            if underlying_array_is_cyclic and \
                    subset_index_specification_in_underlying_array == \
                    [0, len(underlying_array)]:
                subset_is_cyclic = True
            else:
                subset_is_cyclic = False
            array_subset = NetCDFVariable(
                underlying_array, [subset_index_specification_in_underlying_array],
                cyclic_netcdf_variable_dimension_indices=cyclic_variable_dimension_indices
            )
            array_subset_range = underlying_array[
                [subset_index_specification_in_underlying_array[0],
                 subset_index_specification_in_underlying_array[1] - 1]
            ]

        return (subset_index_specification_in_underlying_array, array_subset,
                array_subset_range, subset_is_cyclic)

    # def _setup_subset_for_dimension(self, underlying_array, subsetting_function,
    #                                 subsetting_function_arguments,
    #                                 underlying_array_is_cyclic):
    #
    #     # Find and setup the specified subset of longitudes
    #     subset_index_specification_in_underlying_array = subsetting_function(
    #         *subsetting_function_arguments)
    #
    #     if subset_index_specification_in_underlying_array is None:
    #         # This dimension doesn't exist in the underlying netcdf
    #         array_subset = None
    #         array_subset_range = None
    #         subset_is_cyclic = None
    #     elif isinstance(subset_index_specification_in_underlying_array, int):
    #         # A single value was specified in the input
    #         # subset specification (inside the subsetting_function_arguments tuple),
    #         # so the corresponding item at this dimension will be used, and this
    #         # dimension will effectively be removed from the resulting subset.
    #         array_subset = None
    #         array_subset_range = None
    #         subset_is_cyclic = None
    #     else:
    #         if underlying_array_is_cyclic:
    #             cyclic_variable_dimension_indices = [0]
    #         else:
    #             cyclic_variable_dimension_indices = []
    #         # Only if the underlying netcdf values for this dimension are cyclic (e.g.
    #         # if the array is longitudes and they span a complete loop over the globe)
    #         # and the specified value range uses all the underlying values will the
    #         # subset be cyclic, and therefore support cyclic value specification (which
    #         # is handled by the NetCDFVariable class)
    #         if underlying_array_is_cyclic and \
    #                 subset_index_specification_in_underlying_array == \
    #                 [0, len(underlying_array)]:
    #             subset_is_cyclic = True
    #         else:
    #             subset_is_cyclic = False
    #         array_subset = NetCDFVariable(
    #             underlying_array, [subset_index_specification_in_underlying_array],
    #             cyclic_netcdf_variable_dimension_indices=cyclic_variable_dimension_indices
    #         )
    #         array_subset_range = underlying_array[
    #             [subset_index_specification_in_underlying_array[0],
    #              subset_index_specification_in_underlying_array[1] - 1]
    #         ]
    #
    #     return (subset_index_specification_in_underlying_array, array_subset,
    #             array_subset_range, subset_is_cyclic)

    # def _setup_longitudes_subset(self, longitude_range_specification,
    #                              underlying_longitude_array,
    #                              underlying_longitudes_span_globe):
    #     # Find and setup the specified subset of longitudes
    #     subset_range_indices_in_underlying_array = self._subset_longitudes(
    #         longitude_range_specification, underlying_longitude_array,
    #         underlying_longitudes_span_globe
    #     )
    #     if subset_range_indices_in_underlying_array is not None:
    #         if underlying_longitudes_span_globe:
    #             cyclic_variable_dimension_indices = [0]
    #         else:
    #             cyclic_variable_dimension_indices = []
    #         # Only if the underlying netcdf longitudes span the globe and the specified
    #         # longitude range uses all the netcdf longitudes will the subset longitudes
    #         # also span the globe, and therefore support cyclic longitude specification.
    #         if underlying_longitudes_span_globe and \
    #                 subset_range_indices_in_underlying_array == \
    #                 [0, len(underlying_longitude_array)]:
    #             subset_longitudes_span_globe = True
    #         else:
    #             subset_longitudes_span_globe = False
    #         longitudes_subset = NetCDFVariable(
    #             underlying_longitude_array, [subset_range_indices_in_underlying_array],
    #             cyclic_netcdf_variable_dimension_indices=cyclic_variable_dimension_indices
    #         )
    #         longitudes_subset_range = underlying_longitude_array[
    #             [subset_range_indices_in_underlying_array[0],
    #              subset_range_indices_in_underlying_array[1] - 1]
    #         ]
    #     else:
    #         longitudes_subset = None
    #         longitudes_subset_range = None
    #         subset_longitudes_span_globe = None
    #
    #     # return subset_range_indices_in_underlying_array, subset_longitudes_span_globe
    #     return (subset_range_indices_in_underlying_array, longitudes_subset,
    #             longitudes_subset_range, subset_longitudes_span_globe)

    # def _setup_datetimes_subset(self, datetime_subset_specification,
    #                             underlying_datetime_array):
    #
    #     subset_index_specification_in_underlying_array = self._subset_datetimes(
    #         datetime_subset_specification, underlying_datetime_array)
    #     if subset_index_specification_in_underlying_array is None:
    #         # There are no datetimes in the underlying netcdf
    #         numeric_datetimes_subset = None
    #         numeric_datetimes_subset_range = None
    #     elif isinstance(subset_index_specification_in_underlying_array, int):
    #         # A single datetime was specified in the input
    #         # datetime_subset_specification, so the corresponding item at this
    #         # dimension will be used, and this dimension will effectively be removed
    #         # from the resulting subset.
    #         numeric_datetimes_subset = None
    #         numeric_datetimes_subset_range = None
    #     else:
    #         # The subset_index_specification_in_underlying_array is a list with two
    #         # items, corresponding to range indices.
    #         numeric_datetimes_subset = NetCDFVariable(
    #             underlying_datetime_array,
    #             [subset_index_specification_in_underlying_array]
    #         )
    #         numeric_datetimes_subset_range = underlying_datetime_array[
    #             [subset_index_specification_in_underlying_array[0],
    #              subset_index_specification_in_underlying_array[1] - 1]
    #         ]
    #
    #     return (subset_index_specification_in_underlying_array, numeric_datetimes_subset,
    #             numeric_datetimes_subset_range)

    # def _setup_latitudes_subset(self, latitude_subset_specification,
    #                             underlying_latitude_array):
    #
    #     subset_range_indices_in_underlying_array = self._subset_latitudes(
    #         latitude_subset_specification, underlying_latitude_array)
    #
    #     if subset_range_indices_in_underlying_array is None:
    #         # There are no latitudes in the underlying netcdf
    #         latitudes_subset = None
    #         latitudes_subset_range = None
    #     elif isinstance(subset_range_indices_in_underlying_array, int):
    #         # A single latitude was specified in the input
    #         # datetime_subset_specification, so the corresponding item at this
    #         # dimension will be used, and this dimension will effectively be removed
    #         # from the resulting subset.
    #         latitudes_subset = None
    #         latitudes_subset_range = None
    #     else:
    #         # The subset_index_specification_in_underlying_array is a list with two
    #         # items, corresponding to range indices.
    #         latitudes_subset = NetCDFVariable(
    #             underlying_latitude_array, [subset_range_indices_in_underlying_array])
    #         latitudes_subset_range = underlying_latitude_array[
    #             [subset_range_indices_in_underlying_array[0],
    #              subset_range_indices_in_underlying_array[1] - 1]
    #         ]
    #
    #
    #     return (subset_range_indices_in_underlying_array, latitudes_subset,
    #             latitudes_subset_range)

    def _get_new_subset_index_specification(
            self, subset_specification, underlying_array, underlying_array_is_cyclic,
            original_subset_index_specification, dimension_name,
            function_to_prepare_array_and_values_for_index_search):

        if original_subset_index_specification is None:
            # This dimension does not exist in the underlying netcdf dataset.
            new_subset_index_specification = None
        elif isinstance(original_subset_index_specification, int):
            # The original subset represented by this object already used a single item
            # from this dimension, which means that this dimension was effectively
            # inactive. Therefore, in the new subset we need to make sure we continue
            # to use the same single item in this dimension of the underlying netcdf
            # dataset.
            new_subset_index_specification = {
                    'index': original_subset_index_specification
                }
        else:
            subset_index_specification_in_underlying_array = self._subset_dimension(
                subset_specification, underlying_array, underlying_array_is_cyclic,
                dimension_name, function_to_prepare_array_and_values_for_index_search
            )

            if subset_index_specification_in_underlying_array is None:
                new_subset_index_specification = None
            elif isinstance(subset_index_specification_in_underlying_array, list):
                netcdf_array_index_slice_or_list = \
                    underlying_array.resolve_to_netcdf_index_specification(
                        slice(subset_index_specification_in_underlying_array[0],
                              subset_index_specification_in_underlying_array[1])
                    )
                # If the array is longitudes, the result may be a slice, as usual,
                # or if the underlying longitudes span the globe and the requested subset
                # spans over the edge of the underlying array, then the result will be a
                # list of item indices.
                if isinstance(netcdf_array_index_slice_or_list, slice):
                    netcdf_array_index_range = [
                        netcdf_array_index_slice_or_list.start,
                        netcdf_array_index_slice_or_list.stop
                    ]
                else:
                    netcdf_array_index_range = [
                        netcdf_array_index_slice_or_list[0],
                        netcdf_array_index_slice_or_list[-1] + 1
                    ]
                new_subset_index_specification = {
                    'range': netcdf_array_index_range,
                    'type': 'indices'
                }
            elif isinstance(subset_index_specification_in_underlying_array, int):
                netcdf_array_index = \
                    underlying_array.resolve_to_netcdf_index_specification(
                        subset_index_specification_in_underlying_array
                    )
                new_subset_index_specification = {
                    'index': netcdf_array_index
                }
            else:
                raise ValueError("Invalid subset index specification {0}.".format(
                    subset_index_specification_in_underlying_array
                ))

        return new_subset_index_specification

    def subset(self, datetime_subset_specification=None,
               latitude_subset_specification=None, longitude_subset_specification=None):
        """
        Will output a new NetCDFDataset that spans the input ranges. Any data
        outside the specified ranges will be not be in the new NetCDFDataset. This needs
        to be quite clever by making the new NetCDFDataset point to the same
        netcdf4.Dataset as this one, but only making available the data for the new
        ranges.
        """
        # TODO: Rework to allow not just ranges, but also individual items to be
        # specified. This will mean the corresponding dimension will be removed from
        # the output array, so need to handle this appropriately.

        # Check that specifications have been provided for all dimensions that exist in
        # this dataset, and that None has been provided where that dimension doesn't
        # exist.
        self._check_specifications_against_dimensions(datetime_subset_specification,
                                                      latitude_subset_specification,
                                                      longitude_subset_specification,
                                                      True)
        # Do subsetting
        new_subset_datetime_range_specification = \
            self._get_new_subset_index_specification(
                datetime_subset_specification, self._numeric_datetimes, False,
                self._subset_index_specification_for_each_dimension[0], "date-time",
                self._prepare_numeric_datetime_array_and_search_values_for_index_search
            )
        new_subset_latitude_range_specification = \
            self._get_new_subset_index_specification(
                latitude_subset_specification, self._latitudes, False,
                self._subset_index_specification_for_each_dimension[1], "latitude",
                self._prepare_latitude_array_and_search_values_for_index_search
            )
        new_subset_longitude_range_specification = \
            self._get_new_subset_index_specification(
                longitude_subset_specification, self._longitudes,
                self._longitudes_span_globe,
                self._subset_index_specification_for_each_dimension[2], "longitude",
                self._prepare_longitude_array_and_search_values_for_index_search
            )

        subset_dataset = NetCDFDataset(
            self._netcdf_dataset_object, self._netcdf_climate_variable_name,
            new_subset_datetime_range_specification,
            new_subset_latitude_range_specification,
            new_subset_longitude_range_specification, self._netcdf_longitudes_span_globe,
            climate_variable_name_to_use=self._climate_variable_name_to_use,
            assume_equally_spaced_datetimes=self._assume_equally_spaced_datetimes
        )
        return subset_dataset


class NetCDFDatasetRotated(NetCDFDataset):
    def __init__(self, netcdf_dataset_object, netcdf_climate_variable_name,
                 datetime_subset_specification, latitude_subset_specification,
                 longitude_subset_specification,
                 netcdf_longitudes_span_globe,
                 climate_variable_name_to_use=None,
                 assume_equally_spaced_datetimes=False):
        """

        Parameters
        ----------
        netcdf_dataset_object
        datetime_subset_specification
        latitude_subset_specification
        longitude_subset_specification
        netcdf_longitudes_span_globe : bool
            If True, then the longitude variable in the underlying netcdf file will be
            treated as cyclic, meaning that the user can specify a range that crosses
            over the longitude = 0.0 point (or wherever the 'seam' is).
        longitude_range_to_resolve_to : {None, :list:}, optional
            Optionally specify the longitude range that all longitudes will be resolved
            to (e.g. if [0, 360] is specified, then -1 and 367 will be resolved to 359
            and 7 respectively). This range should correspond to the range used in the
            underlying NetCDF file. If None is specified, then it is calculated from
            the longitudes stored in the underlying NetCDF file.
        """
        Dataset.__init__(self)
        # TODO: Need to track any dimensions that have been effectively removed (by
        # specifying a single item at that dimension), because this information is
        # needed when further subsetting of the same NetCDF dataset is done.

        # Just need to initialise this
        self._datetimes = None

        self._netcdf_dataset_object = netcdf_dataset_object
        if isinstance(self._netcdf_dataset_object, netCDF4.MFDataset):
            self._netcdf_numeric_datetimes = netCDF4.MFTime(
                self._netcdf_dataset_object.variables[io.DEFAULT_NETCDF_DIMENSION_NAMES[io.NUMERIC_DATE_TIMES_ID]]
            )
        else:
            self._netcdf_numeric_datetimes = self._netcdf_dataset_object.variables[
                io.DEFAULT_NETCDF_DIMENSION_NAMES[io.NUMERIC_DATE_TIMES_ID]
            ]

#CP! the following 6 lines are the only difference with the NetCDFDataset class
        self._netcdf_latitudes = self._netcdf_dataset_object.variables['rlat']
        self._netcdf_longitudes = self._netcdf_dataset_object.variables['rlon']
        try:
            self._netcdf_exact_latitudes  = self._netcdf_dataset_object.variables['lat']
            self._netcdf_exact_longitudes = self._netcdf_dataset_object.variables['lon']
        except:
            self._netcdf_exact_latitudes  = self._netcdf_dataset_object.variables['latitude']
            self._netcdf_exact_longitudes = self._netcdf_dataset_object.variables['longitude']
        
        try:
            self._rotated_grid_north_pole_latitude  = self._netcdf_dataset_object.variables['rotated_latitude_longitude'].grid_north_pole_latitude
            self._rotated_grid_north_pole_longitude = self._netcdf_dataset_object.variables['rotated_latitude_longitude'].grid_north_pole_longitude
        except: 
            self._rotated_grid_north_pole_latitude  = self._netcdf_dataset_object.variables['rotated_pole'].grid_north_pole_latitude
            self._rotated_grid_north_pole_longitude = self._netcdf_dataset_object.variables['rotated_pole'].grid_north_pole_longitude
        
        
        self._netcdf_longitudes_span_globe = netcdf_longitudes_span_globe
        self._netcdf_climate_variable_name = netcdf_climate_variable_name
        if climate_variable_name_to_use is not None:
            self._climate_variable_name_to_use = climate_variable_name_to_use
        else:
            self._climate_variable_name_to_use = self._netcdf_climate_variable_name
        self._assume_equally_spaced_datetimes = assume_equally_spaced_datetimes
        # TODO: Make the range specification arguments optional. There should be a
        # range specification provided for each relevant dimension that exists in the
        # netcdf file. Below, implement a check to ensure the NetCDF file contains the
        # exact same set of dimensions (no more or less).

        self._numeric_datetime_units = self._netcdf_numeric_datetimes.units
        self._numeric_datetime_calendar = self._netcdf_numeric_datetimes.calendar

        # Do subsetting
        # Note that the output datetimes_are_cyclic is not used, as it's irrelevant for
        # datetimes.
        (netcdf_datetime_subset_index_specification, self._numeric_datetimes,
         self._numeric_datetime_range, datetimes_are_cyclic) = \
            self._setup_subset_for_dimension(
                datetime_subset_specification, self._netcdf_numeric_datetimes,
                False, "date-time",
                self._prepare_numeric_datetime_array_and_search_values_for_index_search
            )
        # Note that the output latitudes_are_cyclic is not used, as it's irrelevant for
        # latitudes.
        (netcdf_latitude_subset_index_specification, self._latitudes,
         self._latitude_range, latitudes_are_cyclic) = \
            self._setup_subset_for_dimension(
                latitude_subset_specification, self._netcdf_latitudes,
                False, "latitude",
                self._prepare_latitude_array_and_search_values_for_index_search
            )
        (netcdf_longitude_subset_index_specification, self._longitudes,
         self._longitude_range, self._longitudes_span_globe) = \
            self._setup_subset_for_dimension(
                longitude_subset_specification, self._netcdf_longitudes,
                self._netcdf_longitudes_span_globe, "longitude",
                self._prepare_longitude_array_and_search_values_for_index_search
            )

        self._subset_index_specification_for_each_dimension = []
        values_subset_index_specifications = []
        cyclic_netcdf_variable_dimension_indices = []
        if netcdf_datetime_subset_index_specification is not None:
            values_subset_index_specifications.append(
                netcdf_datetime_subset_index_specification)
            self._subset_index_specification_for_each_dimension.append(
                netcdf_datetime_subset_index_specification)
        else:
            self._subset_index_specification_for_each_dimension.append(None)
        if netcdf_latitude_subset_index_specification is not None:
            values_subset_index_specifications.append(
                netcdf_latitude_subset_index_specification)
            self._subset_index_specification_for_each_dimension.append(
                netcdf_latitude_subset_index_specification)
        else:
            self._subset_index_specification_for_each_dimension.append(None)
        if netcdf_longitude_subset_index_specification is not None:
            values_subset_index_specifications.append(
                netcdf_longitude_subset_index_specification)
            self._subset_index_specification_for_each_dimension.append(
                netcdf_longitude_subset_index_specification)
            if self._netcdf_longitudes_span_globe:
                cyclic_netcdf_variable_dimension_indices.append(
                    len(values_subset_index_specifications) - 1
                )
        else:
            self._subset_index_specification_for_each_dimension.append(None)

        self._values = NetCDFVariable(
            self._netcdf_dataset_object.variables[self._netcdf_climate_variable_name],
            values_subset_index_specifications,
            cyclic_netcdf_variable_dimension_indices
            =cyclic_netcdf_variable_dimension_indices
        )
        return


#CP! Not tried using these two functions yet
    def _convert_to_latitude_from_rotated_grid(self):
        """
        To plot the data, move it from the rotated lat-lon grid to the true one.
        :param rlon:
        :return:
        """
        for rlat in self._netcdf_latitudes:
            for rlon in self._netcdf_longitudes:
              true_latitudes = self._netcdf_dataset_object.variables['lat'][rlat][rlon]
        return true_latitudes

    def _convert_to_longitude_from_rotated_grid(self):
        """
        To plot the data, move it from the rotated lat-lon grid to the true one.
        :param rlon:
        :return:
        """
        for rlat in self._netcdf_latitudes:
            for rlon in self._netcdf_longitudes:
              true_longitudes = self._netcdf_dataset_object.variables['lat'][rlat][rlon]
        return true_longitudes





    def subset(self, datetime_subset_specification=None,
               latitude_subset_specification=None, longitude_subset_specification=None):
        """
        Will output a new NetCDFDataset that spans the input ranges. Any data
        outside the specified ranges will be not be in the new NetCDFDataset. This needs
        to be quite clever by making the new NetCDFDataset point to the same
        netcdf4.Dataset as this one, but only making available the data for the new
        ranges.
        """
        # TODO: Rework to allow not just ranges, but also individual items to be
        # specified. This will mean the corresponding dimension will be removed from
        # the output array, so need to handle this appropriately.

        # Check that specifications have been provided for all dimensions that exist in
        # this dataset, and that None has been provided where that dimension doesn't
        # exist.
        self._check_specifications_against_dimensions(datetime_subset_specification,
                                                      latitude_subset_specification,
                                                      longitude_subset_specification,
                                                      True)
        # Do subsetting
        new_subset_datetime_range_specification = \
            self._get_new_subset_index_specification(
                datetime_subset_specification, self._numeric_datetimes, False,
                self._subset_index_specification_for_each_dimension[0], "date-time",
                self._prepare_numeric_datetime_array_and_search_values_for_index_search
            )
        new_subset_latitude_range_specification = \
            self._get_new_subset_index_specification(
                latitude_subset_specification, self._latitudes, False,
                self._subset_index_specification_for_each_dimension[1], "latitude",
                self._prepare_latitude_array_and_search_values_for_index_search
            )
        new_subset_longitude_range_specification = \
            self._get_new_subset_index_specification(
                longitude_subset_specification, self._longitudes,
                self._longitudes_span_globe,
                self._subset_index_specification_for_each_dimension[2], "longitude",
                self._prepare_longitude_array_and_search_values_for_index_search
            )

        subset_dataset = NetCDFDatasetRotated(
            self._netcdf_dataset_object, self._netcdf_climate_variable_name,
            new_subset_datetime_range_specification,
            new_subset_latitude_range_specification,
            new_subset_longitude_range_specification, self._netcdf_longitudes_span_globe,
            climate_variable_name_to_use=self._climate_variable_name_to_use,
            assume_equally_spaced_datetimes=self._assume_equally_spaced_datetimes
        )
        return subset_dataset

    def data_in_bounding_box(self, min_longitude, min_latitude, max_longitude, max_latitude):
        """
        Returns a subset of the NetCDFDatasetRotated
        Cuts out the section of rotated lat-long data that lies in the bounding box for a give true latitude and longitude.
        Internally computes the rotated coordinates and uses the subset call.
#FRA	France	-5.4534286, 41.2632185	9.8678344, 51.268318
#Greater London 0.489, 51.28, 0.236, 51.686

        :param min_longitude: between -90 and 90  (left side)
        :param min_latitude: between -180 and 180 (bottom side)
        :param max_longitude: between -90 and 90  (right side)
        :param max_latitude: between -180 and 180 (top side)
        :return: a subset of the NetCDFDatasetRotated
        """

        if(min_longitude > max_longitude or min_latitude > max_latitude):
            raise ValueError(
                "Check the order of the latitudes and longitudes, minimums exceed maximums."
            )

        lonpole = self._rotated_grid_north_pole_longitude
        latpole = self._rotated_grid_north_pole_latitude

        # 4-3
        # 1-2
        # corner = lon, lat
        corner1 = rotgrid.transform_to_rotated_grid(min_longitude, min_latitude, lonpole, latpole)
        corner2 = rotgrid.transform_to_rotated_grid(max_longitude, min_latitude, lonpole, latpole)
        corner3 = rotgrid.transform_to_rotated_grid(max_longitude, max_latitude, lonpole, latpole)
        corner4 = rotgrid.transform_to_rotated_grid(min_longitude, max_latitude, lonpole, latpole)

        min_rot_longitude = min(corner1[0], corner4[0])
        min_rot_latitude  = min(corner1[1], corner2[1])
        max_rot_longitude = min(corner2[0], corner3[0])
        max_rot_latitude  = min(corner3[1], corner4[1])

        my_bounded_dataset = self.subset(
            datetime_subset_specification = 'all',
            latitude_subset_specification = {'range':[min_rot_latitude, max_rot_latitude], 'type': 'including'},
            longitude_subset_specification = {'range':[min_rot_longitude, max_rot_longitude], 'type': 'including'}
        )


        return my_bounded_dataset

#CP! latest
    def export(self, file_path, exporter_type=None, **exporter_options):
        if exporter_type is None:
            # Try determining the type from the file name
            exporter = self._EXPORTER_FACTORY.get_exporter_from_file_name(
                file_path, **exporter_options)
        else:
            exporter = self._EXPORTER_FACTORY.get_exporter(exporter_type,
                                                           **exporter_options)
#
        exporter.export(self, file_path)



class NetCDFVariable(utilities.CanLog):
    def __init__(self, netcdf_variable_object, subset_index_specification,
                 cyclic_netcdf_variable_dimension_indices=None):
        """
        Uses standard python indexing.

        Designed to be used with netcdf4.Variables, but will equally work with numpy
        arrays. One wouldn't normally bother using this class for numpy arrays,
        because the numpy package creates "views" of data rather than copying it by
        default, which is effectively the purpose of this class. One might want to use
        class for its ability to support cyclic arrays however, which numpy arrays
        (and equivalently, views) do not support, as far as I know.
        """
        utilities.CanLog.__init__(self)

        self._variable_object = netcdf_variable_object
        self._subset_index_specification = subset_index_specification
        if cyclic_netcdf_variable_dimension_indices is None:
            self._cyclic_variable_dimension_indices = []
        else:
            self._cyclic_variable_dimension_indices = \
                cyclic_netcdf_variable_dimension_indices

        # Check there is one index specification for each dimension of the dataset
        if self._variable_object.ndim != len(self._subset_index_specification):
            raise ValueError(("The number of subset index ranges provided ({0}) does not "
                              "match the number of dimensions of the variable "
                              "({1}).").format(len(self._subset_index_specification),
                                               self._variable_object.ndim))

        # Check that the index specification for at least one dimension is a range,
        # otherwise the result would not be an array, but a single element instead,
        # which is not supported by this class.
        found_range_index_specification = False
        for dimension_index_specification in self._subset_index_specification:
            if isinstance(dimension_index_specification, list):
                found_range_index_specification = True
                break
        if not found_range_index_specification:
            raise ValueError(
                ("The subset_index_specification '{0}' contains no range indices. At "
                 "least one dimension must be a range.").format(
                    subset_index_specification)
            )

        # Check that cyclic_variable_dimension_indices has been specified correctly
        for cyclic_variable_dimension_index in self._cyclic_variable_dimension_indices:
            if not 0 <= cyclic_variable_dimension_index < self._variable_object.ndim:
                raise ValueError(("The dimension with index {0} has been specified as "
                                  "cyclic, but this dimension does not exist.").format(
                    cyclic_variable_dimension_index
                ))

        # Resolve any negative indices and "end" to their positive integer index
        # equivalents. We also determine the number of 'active' dimensions. The subset
        # array may have a different number of dimensions to the underlying netcdf array:
        # if a single item index (rather than a range) has been specified for a given
        # dimension in the underlying netcdf array, this dimension is effectively
        # removed from the subset array.
        resolved_subset_index_specification = []
        num_active_dimensions = 0
        for dimension_index, dimension_index_specification in \
                enumerate(self._subset_index_specification):

            array_length = self._variable_object.shape[dimension_index]

            if isinstance(dimension_index_specification, list):
                # A range has been specified at this dimension
                num_active_dimensions += 1

                start_index = self._resolve_to_positive_integer_index(
                    dimension_index_specification[0], True, array_length,
                    dimension_index, False
                )
                end_index = self._resolve_to_positive_integer_index(
                    dimension_index_specification[1], True, array_length,
                    dimension_index, True
                )
                if end_index == start_index:
                    raise IndexError(
                        ("The start and end indices in the index range {0} for "
                         "dimension no. {1} must be different.").format(
                            str(dimension_index_specification), dimension_index + 1)
                    )
                # If the variable at this dimension isn't cyclic, check that the end
                # index is greater than the start index.
                variable_dimension_is_cyclic = \
                    dimension_index in self._cyclic_variable_dimension_indices
                if (not variable_dimension_is_cyclic) and end_index < start_index:
                    raise IndexError(
                        ("The index range {0} for dimension no. {1} is invalid. The end "
                         "index must be greater than the start index.").format(
                            str(dimension_index_specification), dimension_index + 1
                        )
                    )
                resolved_dimension_index_specification = [start_index, end_index]
            elif isinstance(dimension_index_specification, int):
                # A single item has been specified at this dimension
                resolved_dimension_index_specification = \
                    self._resolve_to_positive_integer_index(
                        dimension_index_specification, False, array_length,
                        dimension_index, False
                    )
            else:
                raise ValueError(
                    ("The dimension at index {0} has an invalid index specification "
                     "{1}. Only a 2-item list or a single integer is allowed.").format(
                        dimension_index, dimension_index_specification
                    )
                )

            resolved_subset_index_specification.append(
                resolved_dimension_index_specification)

        self._subset_index_specification = resolved_subset_index_specification
        self._ndim = num_active_dimensions
        self._shape, self._shape_including_inactive_dimensions = self._calculate_shape()

        return

    def __len__(self):
        return self.shape[0]

    def __getitem__(self, index_specification):
        """ Uses standard python indexing. """

        netcdf_index_specification = self.resolve_to_netcdf_index_specification(
            index_specification)

        return self._variable_object[netcdf_index_specification]

    def resolve_to_netcdf_index_specification(self, index_specification):
        """ Uses standard python indexing. """

        # If the index specification has a comma in it, and therefore specifies an
        # operation over more than one dimension
        # - e.g. variable[1, 2:5]; variable[4:, :, 2] etc.
        # Otherwise it will be a single item (either an integer, slice or list); in this
        # case, we wrap it in a tuple to make subsequent processing simpler:
        if isinstance(index_specification, tuple):
            index_specification_to_use = index_specification
        else:
            index_specification_to_use = tuple([index_specification])

        # The most robust way to do what we want is to generate a netcdf index
        # specification that has an explicit index specification for each dimension of
        # the underlying netcdf array.
        netcdf_index_specification = []
        # This tracks how many dimension index specifications we've used from the input
        # index_specification (where each entry between a comma in the
        # index_specification is a 'dimension index specification').
        current_input_index_specification_position = 0
        # Let's also track our position through the number of active dimensions
        current_active_dimension_position = 0
        for netcdf_dimension_index, underlying_subset_dimension_index_specification in \
                enumerate(self._subset_index_specification):
            if isinstance(underlying_subset_dimension_index_specification, int):
                # This dimension is effectively inactive and we just use the item index
                # for this dimension
                netcdf_dimension_index_specification = \
                    underlying_subset_dimension_index_specification
            else:
                # This dimension is active, so we need to resolve the corresponding
                # part of the input index_specification to the corresponding netcdf
                # index specification, for this dimension.
                if current_input_index_specification_position < len(
                        index_specification_to_use):
                    # We use the relevant part from the input index_specification
                    dimension_index_specification = index_specification_to_use[
                        current_input_index_specification_position]
                    netcdf_dimension_index_specification = \
                        self._resolve_to_netcdf_index_specification_for_a_dimension(
                            dimension_index_specification, netcdf_dimension_index)

                    current_input_index_specification_position += 1
                else:
                    # We've used all the parts of the input index_specification. If
                    # there wasn't an explicit index specification for each dimension,
                    # it is implied that all elements are taken in each remaining
                    # dimension. We need to resolve the appropriate underlying netcdf
                    # index specifications that represent taking all elements for the
                    # remaining dimensions.
                    length_of_dimension = self.shape[current_active_dimension_position]
                    index_specification_for_all_values = slice(length_of_dimension)
                    netcdf_dimension_index_specification = \
                        self._resolve_to_netcdf_index_specification_for_a_dimension(
                            index_specification_for_all_values, netcdf_dimension_index
                        )

                current_active_dimension_position += 1

            netcdf_index_specification.append(netcdf_dimension_index_specification)

        if len(netcdf_index_specification) > 1:
            netcdf_index_specification = tuple(netcdf_index_specification)
        else:
            netcdf_index_specification = netcdf_index_specification[0]

        return netcdf_index_specification

    def _resolve_to_netcdf_index_specification_for_a_dimension(self, index_specification,
                                                               variable_dimension_index):
        if isinstance(index_specification, slice):
            start_index = self._resolve_to_netcdf_single_index(
                index_specification.start, "start", variable_dimension_index)
            end_index = self._resolve_to_netcdf_single_index(
                index_specification.stop, "end", variable_dimension_index)
            # If the variable at this dimension is cyclic and the end
            # index is less than the start index (meaning the output array needs to
            # cycle over the end to the beginning of the underlying netcdf array at
            # this dimension)
            variable_dimension_is_cyclic = variable_dimension_index in \
                                           self._cyclic_variable_dimension_indices
            if variable_dimension_is_cyclic and start_index > end_index:
                # We can simplify things if the start_index is the end of the dimension,
                # or if the end_index is the beginning of the dimension
                netcdf_dimension_length = self._variable_object.shape[
                    variable_dimension_index]
                # If the dimension is being treated as cyclic, the end of the underlying
                # netcdf dimension is equivalent to the start, and vice versa.
                range_wraps_over_end_of_dimension = True
                if start_index == netcdf_dimension_length:
                    start_index = 0
                    range_wraps_over_end_of_dimension = False
                elif end_index == 0:
                    end_index = netcdf_dimension_length
                    range_wraps_over_end_of_dimension = False

                if not range_wraps_over_end_of_dimension:
                    netcdf_index_specification = slice(start_index, end_index,
                                                       index_specification.step)
                else:
                    # The easiest way to form the range specification when it wraps
                    # over the end of the dimension is as a list of all the individual
                    # elements. This allows us to stitch the two segments together
                    # directly.
                    # If a step other than 1 was specified in the input
                    # index_specification, then we need to handle the start of the
                    # second segment appropriately to ensure the stepping is carried
                    # across the segments correctly.
                    if index_specification.step is None:
                        step = 1
                    else:
                        step = index_specification.step
                    remainder = (netcdf_dimension_length - start_index) % step
                    if remainder == 0:
                        offset = 0
                    else:
                        offset = step - remainder
                    netcdf_index_specification = (
                        range(start_index, netcdf_dimension_length, step) +
                        range(0 + offset, end_index, step)
                    )
            else:
                # The specified range doesn't wrap over the end of the dimension
                netcdf_index_specification = slice(start_index, end_index,
                                                   index_specification.step)
        else:
            # Assume the index specified is an integer or a list of integers
            # Use this try-except clause to determine whether the input index
            # specification is an iterable object
            try:
                iterator = iter(index_specification)
            except TypeError:
                is_iterable = False
            else:
                is_iterable = True

            if is_iterable:
                # Assume the index specified is a list of integers
                netcdf_index_specification = [
                    self._resolve_to_netcdf_single_index(index, "item",
                                                         variable_dimension_index)
                    for index in index_specification
                ]
            else:
                # Assume the index specified is an integer
                netcdf_index_specification = self._resolve_to_netcdf_single_index(
                    index_specification, "item", variable_dimension_index)

        return netcdf_index_specification

    def _resolve_to_netcdf_single_index(self, index_in_subset, index_type,
                                        variable_dimension_index):
        INDEX_TYPES = {"start", "end", "item"}
        if index_type not in INDEX_TYPES:
            raise TypeError(
                "Invalid index type '{0}'. Valid types are {1}.".format(
                    index_type, str(INDEX_TYPES))
                )

        subset_index_range = self._subset_index_specification[variable_dimension_index]
        variable_dimension_is_cyclic = variable_dimension_index in \
                                       self._cyclic_variable_dimension_indices

        if index_in_subset is None and index_type == "start":
            # This applies when a range is specified without an explicit start index,
            # implying that it should start from the beginning of the array, e.g.
            #   my_array[:4]
            resolved_netcdf_index = subset_index_range[0]
        elif index_in_subset is None and index_type == "end":
            # This applies when a range is specified without an explicit end index,
            # implying that it should go to the end of the array, e.g.
            #   my_array[2:]
            resolved_netcdf_index = subset_index_range[1]
        else:
            # Indexing works differently for ranges than it does for individual items,
            # so we need to make a note of which type has been specified.
            if index_type == "start" or index_type == "end":
                index_is_for_range = True
            else:
                index_is_for_range = False

            # Determine the length of the subset at this dimension
            # If the variable at this dimension is cyclic and the subset wraps over
            # the end of this dimension...
            netcdf_dimension_length = self._variable_object.shape[
                variable_dimension_index]

            subset_dimension_length = self._shape_including_inactive_dimensions[
                variable_dimension_index]
            # Note that this function also checks that the specified index lies within
            # the allowable range (worked out from subset_array_length).
            positive_index_in_subset = self._resolve_to_positive_integer_index(
                index_in_subset, index_is_for_range, subset_dimension_length,
                variable_dimension_index, False
            )
            resolved_netcdf_index = subset_index_range[0] + positive_index_in_subset

            # If the variable at this dimension is cyclic and the subset wraps over
            # the end of this dimension, then its possible the "resolved_netcdf_index"
            # that we've calculated will be a number greater than the length of the
            # underlying netcdf dimension. In this case, the index actually lies in the
            # part of the subset that has wrapped over the end to the beginning of the
            # underlying netcdf dimension, so we determine the actual netcdf index below.
            if variable_dimension_is_cyclic and \
                subset_index_range[0] > subset_index_range[1]:
                # Indexing works differently for single items and ranges, so determine
                # the appropriate maximum index.
                if index_is_for_range:
                    largest_allowable_netcdf_index = netcdf_dimension_length
                else:
                    largest_allowable_netcdf_index = netcdf_dimension_length - 1

                if resolved_netcdf_index > largest_allowable_netcdf_index:
                    # The index is in the part of the subset that has wrapped over the end
                    # to the beginning of the underlying netcdf dimension.
                    resolved_netcdf_index = (resolved_netcdf_index -
                                             netcdf_dimension_length)

        return resolved_netcdf_index

    def _resolve_to_positive_integer_index(self, index, index_is_for_range, array_length,
                                           variable_dimension_index,
                                           allow_special_end_specification):
        # resolved_index = None
        if not isinstance(index, int):
            # End indices can be specified as "end". Any other non-integer input will
            # result in an error.
            if allow_special_end_specification and index == "end":
                if index_is_for_range:
                    return array_length
                else:
                    # Index is for a single item, so the last item in the array is
                    # given by the array length - 1
                    return array_length - 1
            else:
                raise TypeError(
                    ("The index '{0}' for dimension no. {1} is invalid. The "
                     "index must be an integer less than the length of the "
                     "dimension, or it can be 'end' for the end index.").format(
                        index, variable_dimension_index + 1)
                )
        else: # The index provided is an integer
            if index < 0:
                resolved_index = array_length + index
                if resolved_index < 0:
                    raise IndexError(
                        ("The index '{0}'for dimension no. {1} is invalid, "
                         "because it points to an item before the start of "
                         "the array.").format(index, variable_dimension_index + 1)
                    )
                return resolved_index
            else: # The index provided is a positive integer
                if index_is_for_range:
                    largest_allowable_index = array_length
                else:
                    largest_allowable_index = array_length - 1

                if index > largest_allowable_index:
                    raise IndexError(
                        ("The index '{0}' for dimension no. {1} is larger than the "
                         "array length.").format(index, variable_dimension_index + 1)
                    )
                return index

        # Should never reach this part of the code, but putting this in just in case.
        # raise IndexError(
        #     ("Could not resolve the index '{0}' for dimension no. {1} to a positive "
        #      "integer.").format(index, variable_dimension_index + 1)
        #
        # # return resolved_index
        # return

    def _calculate_shape(self):
        shape = []
        shape_with_inactive_dimensions = []
        for variable_dimension_index, dimension_index_specification in enumerate(
                self._subset_index_specification):
            if isinstance(dimension_index_specification, list):
                # This dimension is active. If the index specification at this
                # dimension were an integer, then this dimension would be inactive.
                subset_index_range = dimension_index_specification
                variable_dimension_is_cyclic = \
                    variable_dimension_index in self._cyclic_variable_dimension_indices
                # Determine the length of the subset at this dimension
                # If the variable at this dimension is cyclic and the subset wraps over
                # the end of this dimension...
                netcdf_dimension_length = self._variable_object.shape[
                    variable_dimension_index]
                if variable_dimension_is_cyclic and \
                        subset_index_range[0] > subset_index_range[1]:
                    subset_dimension_length = (
                        # Length of the dimension that goes from the beginning of the
                        # subset to the end of the dimension.
                        (netcdf_dimension_length - subset_index_range[0]) +
                        # Length of the dimension that goes from the beginning of the
                        # dimension to the end of the subset.
                        subset_index_range[1]
                    )
                else:
                    subset_dimension_length = (subset_index_range[1] -
                                               subset_index_range[0])
                shape.append(subset_dimension_length)
                shape_with_inactive_dimensions.append(subset_dimension_length)
            else:
                shape_with_inactive_dimensions.append(0)

        return tuple(shape), tuple(shape_with_inactive_dimensions)

    @property
    def variable_object(self):
        return self._variable_object

    @property
    def shape(self):
        return self._shape

    @property
    def ndim(self):
        return self._ndim


class MonotonicLongitudes(utilities.CanLog):
    def __init__(self, longitudes_array_object, longitude_range_to_resolve_to=None):
        """ This class is used to automatically resolve any longitudes obtained from
        the underlying array into a range that is 360 degrees long, and ensures the
        effective resulting array of longitudes is monotonically increasing.
        It acts as a transparent wrapper over the input longitudes_array_object, so
        the original object's attributes are all accessible as normal, and only the
        __getitem__ method is overridden.
        """
        utilities.CanLog.__init__(self)
        self._longitudes_array_object = longitudes_array_object
        if longitude_range_to_resolve_to is not None:
            self._longitude_range_to_resolve_to = longitude_range_to_resolve_to
        else:
            self._longitude_range_to_resolve_to = \
                _calculate_longitude_range_to_resolve_to(self._longitudes_array_object)
        return

    @property
    def longitude_range_to_resolve_to(self):
        return self._longitude_range_to_resolve_to

    def __len__(self):
        return len(self._longitudes_array_object)

    def __getitem__(self, index_specification):
        longitude_item_or_array = self._longitudes_array_object[index_specification]
        try:
            iterator = iter(longitude_item_or_array)
        except TypeError:
            is_iterable = False
        else:
            is_iterable = True
        if is_iterable:
            resolved_longitudes = []
            for longitude in np.nditer(longitude_item_or_array):
                resolved_longitude = _resolve_longitude_into_range(
                    longitude, self._longitude_range_to_resolve_to)
                resolved_longitudes.append(resolved_longitude)
            return resolved_longitudes
        else:
            resolved_longitude = _resolve_longitude_into_range(
                longitude_item_or_array, self._longitude_range_to_resolve_to)
            return resolved_longitude

    # def __getattr__(self, attribute):
    #     return getattr(self._longitudes_array_object, attribute)


class NumpyDataset(Dataset):
    def __init__(self, values, numeric_datetimes=None, latitudes=None, longitudes=None,
                 climate_variable_name=None, assume_equally_spaced_datetimes=False,
                 numeric_datetime_calendar=None, numeric_datetime_units=None):
        """
        Note that (at least currently) this class does not support cyclic
        longitudes, because it is assumed that those kinds of datasets will only ever
        be converted from NetCDF to Numpy datasets after some subset of longitudes has
        been taken.
        """
        Dataset.__init__(self)

        if numeric_datetimes is not None:
            self._numeric_datetimes = np.asarray(numeric_datetimes)

            if len(self._numeric_datetimes)>1:
                self._numeric_datetime_range = [self._numeric_datetimes[0],
                                                self._numeric_datetimes[1]]
            else:
                self._numeric_datetime_range = [self._numeric_datetimes[0],
                                self._numeric_datetimes[0]]

            self._numeric_datetime_calendar = numeric_datetime_calendar
            self._numeric_datetime_units = numeric_datetime_units
            self._datetimes = None
        else:
            self._numeric_datetimes = None
            self._numeric_datetime_range = None
            self._numeric_datetime_calendar = None
            self._numeric_datetime_units = None
            self._datetimes = None
        if latitudes is not None:
            self._latitudes = np.asarray(latitudes)
            if len(latitudes)>1:
                self._latitude_range = [self._latitudes[0], self._latitudes[1]]
            else:
                self._latitude_range = [self._latitudes[0], self._latitudes[0]]
        else:
            self._latitudes = None
            self._latitude_range = None
        if longitudes is not None:
            self._longitudes = np.asarray(longitudes)
            if len(longitudes)>1:
                self._longitude_range = [self._longitudes[0], self._longitudes[1]]
            else:
                self._longitude_range = [self._longitudes[0], self._longitudes[0]]
        else:
            self._longitudes = None
            self._longitude_range = None
        self._values = np.asarray(values)
        self._assume_equally_spaced_datetimes = assume_equally_spaced_datetimes
        # Perform some data consistency checks
        num_dimensions_expected = 0
        for array in [numeric_datetimes, latitudes, longitudes]:
            if array is not None:
                num_dimensions_expected += 1
        if num_dimensions_expected != self._values.ndim:
            raise ValueError(
                ("The value array has {0} dimensions, rather than the expected {1} "
                 "dimensions.").format(self._values.ndim, num_dimensions_expected)
            )
        current_dimension_index = -1
        if numeric_datetimes is not None:
            current_dimension_index += 1
            dimension_length = self._values.shape[current_dimension_index]
            times_array_length = len(numeric_datetimes)
            if dimension_length != times_array_length:
                raise ValueError(
                    ("The length of the times array ({0}) does not match the length of "
                     "the corresponding dimension in the values array ({1}).").format(
                        times_array_length, dimension_length
                    )
                )
        if latitudes is not None:
            current_dimension_index += 1
            dimension_length = self._values.shape[current_dimension_index]
            latitudes_array_length = len(latitudes)
            if dimension_length != latitudes_array_length:
                raise ValueError(
                    ("The length of the latitudes array ({0}) does not match the length "
                     "of the corresponding dimension in the values array ({1}).").format(
                        latitudes_array_length, dimension_length
                    )
                )
        if longitudes is not None:
            current_dimension_index += 1
            dimension_length = self._values.shape[current_dimension_index]
            longitudes_array_length = len(longitudes)
            if dimension_length != longitudes_array_length:
                raise ValueError(
                    ("The length of the longitudes array ({0}) does not match the length "
                     "of the corresponding dimension in the values array ({1}).").format(
                        longitudes_array_length, dimension_length
                    )
                )

        self._climate_variable_name_to_use = climate_variable_name

        return

    def _setup_subset_for_dimension(
            self, subset_specification, array, dimension_name,
            function_to_prepare_array_and_values_for_index_search):

        index_specification = self._subset_dimension(
            subset_specification, array, False, dimension_name,
            function_to_prepare_array_and_values_for_index_search
        )
        # Convert to an index specification that can be used with __getitem__
        if index_specification is None:
            subset_array = None
            getitem_index_specification = None
        elif isinstance(index_specification, list):
            # A range was specified
            getitem_index_specification = slice(
                index_specification[0],
                index_specification[1]
            )
            subset_array = array[getitem_index_specification]
        else:
            # Assume a single item was specified
            getitem_index_specification = index_specification
            subset_array = array[getitem_index_specification]

        return subset_array, getitem_index_specification

    def subset(self, datetime_subset_specification=None,
               latitude_subset_specification=None,
               longitude_subset_specification=None):
        """ Will output a new NumpyDataset that spans the input ranges. Any data
        outside the specified ranges will be dropped from the new NumpyDataset"""
        # Check that specifications have been provided for all dimensions that exist in
        # this dataset, and that None has been provided where that dimension doesn't
        # exist.
        self._check_specifications_against_dimensions(datetime_subset_specification,
                                                      latitude_subset_specification,
                                                      longitude_subset_specification,
                                                      True)

        # Find and setup the specified subset of longitudes
        numeric_datetimes_subset, datetime_getitem_index_specification = \
            self._setup_subset_for_dimension(
                datetime_subset_specification, self._numeric_datetimes, "date-time",
                self._prepare_numeric_datetime_array_and_search_values_for_index_search
            )
        latitudes_subset, latitude_getitem_index_specification = \
            self._setup_subset_for_dimension(
                latitude_subset_specification, self._latitudes, "latitude",
                self._prepare_latitude_array_and_search_values_for_index_search
            )
        longitudes_subset, longitude_getitem_index_specification = \
            self._setup_subset_for_dimension(
                longitude_subset_specification, self._longitudes, "longitude",
                self._prepare_longitude_array_and_search_values_for_index_search
            )

        values_getitem_index_specification = []
        for getitem_index_specification in [datetime_getitem_index_specification,
                                            latitude_getitem_index_specification,
                                            longitude_getitem_index_specification]:
            if getitem_index_specification is not None:
                values_getitem_index_specification.append(getitem_index_specification)
        values_getitem_index_specification = tuple(values_getitem_index_specification)
        values_subset = self._values[values_getitem_index_specification]

        numpy_dataset_subset = NumpyDataset(
            values_subset, numeric_datetimes=numeric_datetimes_subset,
            latitudes=latitudes_subset, longitudes=longitudes_subset,
            climate_variable_name=self._climate_variable_name_to_use,
            assume_equally_spaced_datetimes=self._assume_equally_spaced_datetimes,
            numeric_datetime_calendar=self._numeric_datetime_calendar,
            numeric_datetime_units=self._numeric_datetime_units
        )
        return numpy_dataset_subset


class MultiModelDataset(utilities.CanLog):
    def __init__(self, datasets=None, reference_model_name=None):
        """
        This class handles any climate data processing that involves the use of
        datasets from two or more separate climate models

        Things to implement:
        Append datasets - Done
        Remove datasets - Done
        Save/dump? (Need to test saving and loading from linux and back)
        Return stats Summary (at location?) - Done (at location) - havent tested on dataset with more than one value yet
        Ability to subtract (and add,multiply,divide) mutltimodeldatasets - Done
        Plots?


        Parameters
        ----------
        datasets : dict
        """
        utilities.CanLog.__init__(self)

        if datasets is not None: 
            self._original_datasets = datasets
            if reference_model_name is not None:
                self._reference_model_name_to_use = reference_model_name
            else:
               self._reference_model_name_to_use = list(self._original_datasets.keys())[0]

            self._reference_dataset = self._original_datasets[self._reference_model_name_to_use]
            self._mapped_datasets = {}
            for model_name, dataset in self._original_datasets.items():
                if model_name == self._reference_model_name_to_use:
                    mapped_dataset = dataset
                else:
                    mapped_dataset = dataset.map_to_reference(self._reference_dataset)
                self._mapped_datasets[model_name] = mapped_dataset
        else:
            self._reference_model_name_to_use = None
            self._reference_dataset = None
            self._mapped_datasets = {}
            self._original_datasets={}


    def remove_model(self,model_name):
        """
        Removes specified model from the MultiModelDataset
        """
        del self._mapped_datasets[model_name]
        del self._original_datasets[model_name]

    def append_models(self,datasets):
        """
        Add models to the class

        
        Parameters
        ----------
        datasets : dict
        """

        # If dataset was previously empty, create a rerefence model
        if len(self._mapped_datasets)==0:
            self._reference_model_name_to_use = datasets.keys()[0]
            self._reference_dataset = datasets[self._reference_model_name_to_use]
            
        # Add to exisiting dictionaries
        for model_name, dataset in datasets.items():
            self._original_datasets[model_name]=dataset

            if model_name == self._reference_model_name_to_use:
                mapped_dataset = dataset
            else:
                mapped_dataset = dataset.map_to_reference(self._reference_dataset)
            
            self._mapped_datasets[model_name] = mapped_dataset


    @property
    def model_names(self):
        """
        Returns a list of models in the MultiModelDataset
        """
        return self._mapped_datasets.keys()

    @property
    def original_values(self):
        """
        Returns a list of values in
        """
        return [self._original_datasets[i].values for i in self.model_names]

    @property
    def mapped_values(self):
        """
        Returns a list of values in
        """
        return [self._mapped_datasets[i].values for i in self.model_names]



    def mean_over_models(self,zscore_limit=3):
        """
        Calculates the mean over the models

        Parameters
        ----------
        zscore_limit = Exclude data greater than zscore standard deviations from the mean. zscore_limit = None will disable this and cause all data to be included

        Returns
        ----------
        NumpyDataset containing the mean values
        """
        self._return_error_on_empty_class()

        numpy_array_of_all_value_datasets=self._remove_data_above_zscore_limit(zscore_limit)

        mean_value_dataset = np.nanmean(numpy_array_of_all_value_datasets, axis=0)
        #mean_value_dataset = numpy_array_of_all_value_datasets.mean(axis=0)
        reference_dataset = self._mapped_datasets.values()[0]
        mean_dataset = NumpyDataset(
            mean_value_dataset,
            numeric_datetimes=reference_dataset.numeric_datetimes,
            latitudes=reference_dataset.latitudes,
            longitudes=reference_dataset.longitudes,
            climate_variable_name=reference_dataset.climate_variable_name,
            assume_equally_spaced_datetimes
            =reference_dataset.assume_equally_spaced_datetimes,
            numeric_datetime_calendar=reference_dataset.numeric_datetime_calendar,
            numeric_datetime_units=reference_dataset.numeric_datetime_units
        )

        return mean_dataset

    def median_over_models(self,zscore_limit=3):
        """
        Calculates the median over the models

        Parameters
        ----------
        zscore_limit = Exclude data greater than zscore standard deviations from the mean. zscore_limit = None will disable this and cause all data to be included
        
        Returns
        ----------
        NumpyDataset containing the median values
        """
        self._return_error_on_empty_class()

        numpy_array_of_all_value_datasets=self._remove_data_above_zscore_limit(zscore_limit)
            

        median_value_dataset = np.nanmedian(numpy_array_of_all_value_datasets, axis=0)
        #mean_value_dataset = numpy_array_of_all_value_datasets.mean(axis=0)
        reference_dataset = self._mapped_datasets.values()[0]
        median_dataset = NumpyDataset(
            median_value_dataset,
            numeric_datetimes=reference_dataset.numeric_datetimes,
            latitudes=reference_dataset.latitudes,
            longitudes=reference_dataset.longitudes,
            climate_variable_name=reference_dataset.climate_variable_name,
            assume_equally_spaced_datetimes
            =reference_dataset.assume_equally_spaced_datetimes,
            numeric_datetime_calendar=reference_dataset.numeric_datetime_calendar,
            numeric_datetime_units=reference_dataset.numeric_datetime_units
        )

        return median_dataset

    def std_deviation_over_models(self,zscore_limit=3):
        """
        Calculates the standard deviation over the models

        Parameters
        ----------
        zscore_limit = Exclude data greater than zscore standard deviations from the mean. zscore_limit = None will disable this and cause all data to be included
        
        Returns
        ----------
        NumpyDataset containing the standard deviation values
        """
        self._return_error_on_empty_class()

        array_of_all_value_datasets = []

        numpy_array_of_all_value_datasets=self._remove_data_above_zscore_limit(zscore_limit)

        standard_deviation_value_dataset = np.nanstd(numpy_array_of_all_value_datasets, axis=0)
        #mean_value_dataset = numpy_array_of_all_value_datasets.mean(axis=0)
        reference_dataset = self._mapped_datasets.values()[0]

        standard_deviation_dataset = NumpyDataset(
            standard_deviation_value_dataset,
            numeric_datetimes=reference_dataset.numeric_datetimes,
            latitudes=reference_dataset.latitudes,
            longitudes=reference_dataset.longitudes,
            climate_variable_name=reference_dataset.climate_variable_name,
            assume_equally_spaced_datetimes
            =reference_dataset.assume_equally_spaced_datetimes,
            numeric_datetime_calendar=reference_dataset.numeric_datetime_calendar,
            numeric_datetime_units=reference_dataset.numeric_datetime_units
        )

        return standard_deviation_dataset

    def min_max_over_models(self,zscore_limit=3):
        """
        Calculates the minimum and maximum values over the models

        Parameters
        ----------
        zscore_limit = Exclude data greater than zscore standard deviations from the mean. zscore_limit = None will disable this and cause all data to be included
        
        Returns
        ----------
        Two NumpyDatasets containing the minimum and maximum values
        """
        self._return_error_on_empty_class()

        numpy_array_of_all_value_datasets=self._remove_data_above_zscore_limit(zscore_limit)

        max_value = np.nanmax(numpy_array_of_all_value_datasets, axis=0)
        min_value = np.nanmin(numpy_array_of_all_value_datasets, axis=0)
        #mean_value_dataset = numpy_array_of_all_value_datasets.mean(axis=0)
        reference_dataset = self._mapped_datasets.values()[0]

        min_dataset = NumpyDataset(
            min_value,
            numeric_datetimes=reference_dataset.numeric_datetimes,
            latitudes=reference_dataset.latitudes,
            longitudes=reference_dataset.longitudes,
            climate_variable_name=reference_dataset.climate_variable_name,
            assume_equally_spaced_datetimes
            =reference_dataset.assume_equally_spaced_datetimes,
            numeric_datetime_calendar=reference_dataset.numeric_datetime_calendar,
            numeric_datetime_units=reference_dataset.numeric_datetime_units
        )

        max_dataset = NumpyDataset(
            max_value,
            numeric_datetimes=reference_dataset.numeric_datetimes,
            latitudes=reference_dataset.latitudes,
            longitudes=reference_dataset.longitudes,
            climate_variable_name=reference_dataset.climate_variable_name,
            assume_equally_spaced_datetimes
            =reference_dataset.assume_equally_spaced_datetimes,
            numeric_datetime_calendar=reference_dataset.numeric_datetime_calendar,
            numeric_datetime_units=reference_dataset.numeric_datetime_units
        )

        return min_dataset, max_dataset

    def confidence_intervals_over_models(self,zscore_limit=3,confidence_interval=0.95):
        """
        Calculates the upper and lower confidence of the mean value. Uses the student t distribution

        Parameters
        ----------
        zscore_limit = Exclude data greater than zscore standard deviations from the mean. zscore_limit = None will disable this and cause all data to be included
        confidence_interval = confidence interval, between 0 and 1

        Returns
        ----------
        Two NumpyDatasets containing the upper and lower confidence intervals
        """
        self._return_error_on_empty_class()

        numpy_array_of_all_value_datasets=self._remove_data_above_zscore_limit(zscore_limit)
        
        confidenceIntervalArray=sst.t.interval(confidence_interval, np.shape(numpy_array_of_all_value_datasets)[0]-1, loc=np.nanmean(numpy_array_of_all_value_datasets, axis=0), scale=sst.sem(numpy_array_of_all_value_datasets,axis=0,nan_policy='omit'))

        confidence_interval_upper_dataset_values = confidenceIntervalArray[1]
        confidence_interval_lower_dataset_values = confidenceIntervalArray[0]
        #mean_value_dataset = numpy_array_of_all_value_datasets.mean(axis=0)
        reference_dataset = self._mapped_datasets.values()[0]

        confidence_interval_upper_dataset = NumpyDataset(
            confidence_interval_upper_dataset_values,
            numeric_datetimes=reference_dataset.numeric_datetimes,
            latitudes=reference_dataset.latitudes,
            longitudes=reference_dataset.longitudes,
            climate_variable_name=reference_dataset.climate_variable_name,
            assume_equally_spaced_datetimes
            =reference_dataset.assume_equally_spaced_datetimes,
            numeric_datetime_calendar=reference_dataset.numeric_datetime_calendar,
            numeric_datetime_units=reference_dataset.numeric_datetime_units
        )

        confidence_interval_lower_dataset = NumpyDataset(
            confidence_interval_lower_dataset_values,
            numeric_datetimes=reference_dataset.numeric_datetimes,
            latitudes=reference_dataset.latitudes,
            longitudes=reference_dataset.longitudes,
            climate_variable_name=reference_dataset.climate_variable_name,
            assume_equally_spaced_datetimes
            =reference_dataset.assume_equally_spaced_datetimes,
            numeric_datetime_calendar=reference_dataset.numeric_datetime_calendar,
            numeric_datetime_units=reference_dataset.numeric_datetime_units
        )

        return confidence_interval_lower_dataset, confidence_interval_upper_dataset

    def percentiles_over_models(self,percentiles,zscore_limit=3):
        """
        Calculates the specified percentiles

        Parameters
        ----------
        zscore_limit = Exclude data greater than zscore standard deviations from the mean. zscore_limit = None will disable this and cause all data to be included
        percentiles = list of percentiles to compute, between 0 and 100 inclusive
        
        Returns
        ----------
        A list of NumpyDatasets containing the percentile values 
        """
        self._return_error_on_empty_class()

        numpy_array_of_all_value_datasets=self._remove_data_above_zscore_limit(zscore_limit)
        
        percentilesArray=np.percentile(numpy_array_of_all_value_datasets,percentiles,axis=0)
        reference_dataset = self._mapped_datasets.values()[0]

        percentilesDataset=[]
        for i in range(np.shape(percentilesArray)[0]):
            onePercentileDataset = NumpyDataset(
                percentilesArray[i],
                numeric_datetimes=reference_dataset.numeric_datetimes,
                latitudes=reference_dataset.latitudes,
                longitudes=reference_dataset.longitudes,
                climate_variable_name=reference_dataset.climate_variable_name,
                assume_equally_spaced_datetimes
                =reference_dataset.assume_equally_spaced_datetimes,
                numeric_datetime_calendar=reference_dataset.numeric_datetime_calendar,
                numeric_datetime_units=reference_dataset.numeric_datetime_units
            )
            percentilesDataset.append(onePercentileDataset)

        return percentilesDataset

    def zscore_of_models(self):
        """
        Calculates the zscore of each model. Can be used to identify outliers
        
        Returns
        ----------
        A dict where the keys are the model, and the items are NumpyDatasets containing the zscores
        """
        self._return_error_on_empty_class()

        array_of_all_value_datasets = []

        for dataset in self._mapped_datasets.values():
            array_of_all_value_datasets.append(dataset.values)

        array_of_all_value_datasets = tuple(array_of_all_value_datasets)
        numpy_array_of_all_value_datasets = np.stack(array_of_all_value_datasets)
        numpy_array_of_all_value_datasets_z_scores = sst.zscore(numpy_array_of_all_value_datasets, axis = 0)
            
        reference_dataset = self._mapped_datasets.values()[0]

        zscoreDict={}
        for count,keys in enumerate(self._mapped_datasets.keys()):
            zscore_values=numpy_array_of_all_value_datasets_z_scores[count]
            zscoreDict[keys]= NumpyDataset(
                zscore_values,
                numeric_datetimes=reference_dataset.numeric_datetimes,
                latitudes=reference_dataset.latitudes,
                longitudes=reference_dataset.longitudes,
                climate_variable_name=reference_dataset.climate_variable_name,
                assume_equally_spaced_datetimes
                =reference_dataset.assume_equally_spaced_datetimes,
                numeric_datetime_calendar=reference_dataset.numeric_datetime_calendar,
                numeric_datetime_units=reference_dataset.numeric_datetime_units
            )




        return zscoreDict

    def stats_summary_at_location(self,latitude=None, longitude=None, mapper=None):
        """
        Returns the stats summary for a single longitude and latitude
        """
    
        if latitude==None or longitude==None:
            if len(self.mean_over_models().latitudes)==1 and len(self.mean_over_models().longitudes):
                stats_dict_list=[]
                stats_dict_orig={}
                #extract stats
                for datetime_value in self.mean_over_models().datetimes:
                    stats_dict=deepcopy(stats_dict_orig)
                    stats_dict["mean"]=float(self.mean_over_models().values)
                    stats_dict["med"]=float(self.median_over_models().values)
                    stats_dict["whislo"]=float(self.min_max_over_models()[0].values)
                    stats_dict["whishi"]=float(self.min_max_over_models()[1].values)
                    stats_dict["q1"]=float(self.percentiles_over_models([25,75])[0].values)
                    stats_dict["q3"]=float(self.percentiles_over_models([25,75])[1].values)
                    stats_dict["cilo"]=float(self.confidence_intervals_over_models()[0].values) # Note, these are about the mean
                    stats_dict["cihi"]=float(self.confidence_intervals_over_models()[1].values) # Note, these are about the mean
                    
                    stats_dict_list.append(stats_dict)

                return stats_dict_list



            else:
                self._logger.exception("If the datasets contain more than one longitude and/or latitude value, you must provide values of longitude and latitude to the function")
                raise Exception("If the datasets contain more than one longitude and/or latitude value, you must provide values of longitude and latitude to the function")
        else:
            #
            if not (isinstance(latitude,float) or isinstance(longitude,float)):
                self._logger.exception("Values of longitude and latitude must be a float")
                raise Exception("Values of longitude and latitude must be a float")
            else:
                #extract location data
                dict_for_single_location_multimodeldataset={}
                for model_name, dataset in self._original_datasets.items():
                    single_location_dataset=extract_data_at_coordinates(datetimes=None, latitudes=latitude, longitudes=longitude, mapper=mapper)
                    dict_for_single_location_multimodeldataset[model_name]
                
                single_location_multimodeldataset=MultiModelDataset(dict_for_single_location_multimodeldataset,reference_model_name=self._reference_model_name_to_use)
                stats_dict_list=single_location_multimodeldataset.stats_summary_at_location(latitude=latitude, longitude=longitude, mapper=mapper)

                return stats_dict_list
        
        


        

    

    def aggregate_over_models(self):

        return

    def plot(self, model, datetime_specification=None, figure_axes=None,
             **plotting_options):
        pass

    def _return_error_on_empty_class(self):
        if len(self._mapped_datasets)==0:
            self._logger.exception("Exception: MultiModelDataset needs at least 1 model for this method")
            raise Exception("Exception: MultiModelDataset needs at least 1 model for this method")
        else:
            pass

    def _remove_data_above_zscore_limit(self,zscore_limit):
        
        array_of_all_value_datasets = []

        for dataset in self._mapped_datasets.values():
            array_of_all_value_datasets.append(dataset.values)
        array_of_all_value_datasets = tuple(array_of_all_value_datasets)
        numpy_array_of_all_value_datasets = np.stack(array_of_all_value_datasets)
        
        # Zscores are a way of excluding outlying data
        if zscore_limit is not None:
            numpy_array_of_all_value_datasets_z_scores = sst.zscore(numpy_array_of_all_value_datasets, axis = 0)
            numpy_array_of_all_value_datasets[np.abs(numpy_array_of_all_value_datasets_z_scores) > zscore_limit] = np.nan
        else:
            pass

        return numpy_array_of_all_value_datasets

    def _check_dimensions(self,other):
        """
        Check whether the dimensions of two muliModelDatasets are the same. This is a requirement for arithmetic operations
        """
        # Check Type
        if not isinstance(other,MultiModelDataset):
            self._logger.exception("Operation can only be carried out for instances of MultiModelDataset")
            raise Exception("Operation can only be carried out for instances of MultiModelDataset")

        # Check whether the keys are the same
        if not set(self._original_datasets.keys()) == set(other._original_datasets.keys()):
            self._logger.exception("Both datasets must have the same set of keys by which to reference the different models")
            raise Exception("Both datasets must have the same set of keys by which to reference the different models")

        # Now check dimensions match
        for model_name, dataset in self._original_datasets.items():
            if not np.shape(self._original_datasets[model_name].values) == np.shape(other._original_datasets[model_name].values):
                self._logger.exception("Dimension mismatch between MultiModelDatasets for key " + model_name)
                raise Exception("Dimension mismatch between MultiModelDatasets for key " + model_name)
                


        
        

    def __add__(self, other):
        # Magic method for addition. Note, that the addition is carried out on the values, other attributes remain the same as the datasets in self.
        # Returns a MultiModelDataset, so statistics can be carried out on the results of the arithmetic
        self._check_dimensions(other)
        processed_dict={}
        for model_name, dataset in self._original_datasets.items():
            values=dataset.values+other._original_datasets[model_name].values
            new_dataset = NumpyDataset(
                values,
                numeric_datetimes=dataset.numeric_datetimes,
                latitudes=dataset.latitudes,
                longitudes=dataset.longitudes,
                climate_variable_name=dataset.climate_variable_name,
                assume_equally_spaced_datetimes=dataset.assume_equally_spaced_datetimes,
                numeric_datetime_calendar=dataset.numeric_datetime_calendar,
                numeric_datetime_units=dataset.numeric_datetime_units)

            processed_dict[model_name]=new_dataset
        
        return MultiModelDataset(processed_dict,reference_model_name=self._reference_model_name_to_use)






    def __sub__(self, other):
        # Magic method for subtraction. Note, that the subtraction is carried out on the values, other attributes remain the same as the datasets in self.
        # Returns a MultiModelDataset, so statistics can be carried out on the results of the arithmetic
        self._check_dimensions(other)

        processed_dict={}
        for model_name, dataset in self._original_datasets.items():
            values=dataset.values-other._original_datasets[model_name].values
            new_dataset = NumpyDataset(
                values,
                numeric_datetimes=dataset.numeric_datetimes,
                latitudes=dataset.latitudes,
                longitudes=dataset.longitudes,
                climate_variable_name=dataset.climate_variable_name,
                assume_equally_spaced_datetimes=dataset.assume_equally_spaced_datetimes,
                numeric_datetime_calendar=dataset.numeric_datetime_calendar,
                numeric_datetime_units=dataset.numeric_datetime_units)

            processed_dict[model_name]=new_dataset
        
        return MultiModelDataset(processed_dict,reference_model_name=self._reference_model_name_to_use)

    def __mul__(self, other):
        # Magic method for multiplication. Note, that the multiplication is carried out on the values, other attributes remain the same as the datasets in self.
        # Returns a MultiModelDataset, so statistics can be carried out on the results of the arithmetic
        self._check_dimensions(other)

        processed_dict={}
        for model_name, dataset in self._original_datasets.items():
            values=np.multiply(dataset.values,other._original_datasets[model_name].values)
            new_dataset = NumpyDataset(
                values,
                numeric_datetimes=dataset.numeric_datetimes,
                latitudes=dataset.latitudes,
                longitudes=dataset.longitudes,
                climate_variable_name=dataset.climate_variable_name,
                assume_equally_spaced_datetimes=dataset.assume_equally_spaced_datetimes,
                numeric_datetime_calendar=dataset.numeric_datetime_calendar,
                numeric_datetime_units=dataset.numeric_datetime_units)

            processed_dict[model_name]=new_dataset
        
        return MultiModelDataset(processed_dict,reference_model_name=self._reference_model_name_to_use)

    def __div__(self, other):
        # Magic method for division. Note, that the division is carried out on the values, other attributes remain the same as the datasets in self.
        # Returns a MultiModelDataset, so statistics can be carried out on the results of the arithmetic
        self._check_dimensions(other)

        processed_dict={}
        for model_name, dataset in self._original_datasets.items():
            values=np.divide(dataset.values,other._original_datasets[model_name].values)
            new_dataset = NumpyDataset(
                values,
                numeric_datetimes=dataset.numeric_datetimes,
                latitudes=dataset.latitudes,
                longitudes=dataset.longitudes,
                climate_variable_name=dataset.climate_variable_name,
                assume_equally_spaced_datetimes=dataset.assume_equally_spaced_datetimes,
                numeric_datetime_calendar=dataset.numeric_datetime_calendar,
                numeric_datetime_units=dataset.numeric_datetime_units)

            processed_dict[model_name]=new_dataset
        
        return MultiModelDataset(processed_dict,reference_model_name=self._reference_model_name_to_use)


class MultiTimeDataset(utilities.CanLog):
    def __init__(self, datasets):
        """
        This class handles any climate data processing that involves the use of
        datasets that are not contiguous in time all from the same model.
        Currently assumes they are in the correct order.

        Parameters
        ----------
        datasets : dict, indexed by time
        """
        utilities.CanLog.__init__(self)
        self._original_datasets = datasets

        return

    def mean_over_datasets(self):
        array_of_all_value_datasets = []
        #datetime_range = [self._original_datasets.keys()[0],  self._original_datasets.keys()[1]]
        for dataset in self._original_datasets.values():
            array_of_all_value_datasets.append(dataset.values)
        array_of_all_value_datasets = tuple(array_of_all_value_datasets)
        numpy_array_of_all_value_datasets = np.stack(array_of_all_value_datasets)

        mean_value_dataset = numpy_array_of_all_value_datasets.mean(axis=0)
        reference_dataset = self._original_datasets.values()[0]

        mean_dataset = NumpyDataset(
            mean_value_dataset,
            numeric_datetimes=None,
            #numeric_datetimes=reference_dataset.numeric_datetimes,
            #numeric_datetimes=datetime_range,
            latitudes=reference_dataset.latitudes,
            longitudes=reference_dataset.longitudes,
            climate_variable_name=reference_dataset.climate_variable_name,
            assume_equally_spaced_datetimes
            =reference_dataset.assume_equally_spaced_datetimes,
            numeric_datetime_calendar=reference_dataset.numeric_datetime_calendar,
            numeric_datetime_units=reference_dataset.numeric_datetime_units
        )

        return mean_dataset



class PlotterDateTime:
    def __init__(self):
        return

    def plot(self, datetimes, values, figure=None, subplot_position=(1, 1, 1), title=None,
             x_label=None, y_label=None, grid=True):
        # Convert date-times to Matplotlib format - which is number of days since 0001-1-1, indexed from 0.
        # matplotlib_numeric_datetimes = mpl_dates.date2num(datetimes)
        # Use in-built netCDF conversion as that can handle non-datetime objects (e.g. cftime objects) which arise if the calenders are not "standard" or "gregorian".
        netcdf_numeric_datetimes = netCDF4.date2num(datetimes,"days since 0001-1-1", "standard") - 1
        if figure is None:
            figure_to_use = plt.figure()
        else:
            figure_to_use = figure
        if isinstance(subplot_position, tuple):
            output_figure_axes = figure_to_use.add_subplot(*subplot_position)
        else:
            output_figure_axes = figure_to_use.add_subplot(subplot_position)

        output_figure_axes.plot_date(netcdf_numeric_datetimes, values)
        if title is not None:
            output_figure_axes.set_title(title)
        if x_label is not None:
            output_figure_axes.set_xlabel(x_label)
        if y_label is not None:
            output_figure_axes.set_ylabel(y_label)
        if grid:
            output_figure_axes.grid()

        return figure_to_use, output_figure_axes


class Plotter2DMap:
    def __init__(self):
        return

    def plot(self, latitudes, longitudes, values, figure=None, subplot_position=(1, 1, 1),
             projection=None, coastlines=True, show_full_globe=False, show_legend=True,
             title=None, x_label=None, y_label=None):
        if projection is None:
            # Default projection
            projection_to_use = ccrs.PlateCarree()
        else:
            projection_to_use = projection

        if figure is None:
            figure_to_use = plt.figure()
        else:
            figure_to_use = figure
        # figure.add_subplot() accepts the subplot position either as three
        # successive integer arguments, or as a single integer argument,
        # so we support the same for our subplot_position keyword argument.
        if isinstance(subplot_position, tuple):
            output_figure_axes = figure_to_use.add_subplot(
                *subplot_position, projection=projection_to_use)
        else:
            output_figure_axes = figure_to_use.add_subplot(
                subplot_position, projection=projection_to_use)

        if coastlines:
            output_figure_axes.coastlines()
        if show_full_globe:
            output_figure_axes.set_global()

        # plt.contourf requires the longitudes to be monotonically increasing.
        # The following achieves this:
        # Somehow, the contourf function manages to bypass the
        # MonotonicLongitudes implementation of __getitem__ and ends up using
        # the underlying longitude array if we don't make the explicit
        # conversion to a monotonic array using the [:] operator at the end of
        # the class.
        monotonic_longitudes = MonotonicLongitudes(longitudes)[:]
        # print monotonic_longitudes[:]
        contour_set = output_figure_axes.contourf(monotonic_longitudes, latitudes, values,
                                                  transform=projection_to_use)
        #Consider using imshow for precipitation plots as this gives continual colouring, not discrete
        #contour_set = output_figure_axes.imshow(values)

        if show_legend:
            figure_to_use.colorbar(contour_set)
        if title is not None:
            output_figure_axes.set_title(title)
        # It seems x and y labels don't work in the standard way for cartopy plots.
        # if x_label is not None:
        #     output_figure_axes.set_xlabel(x_label)
        # if y_label is not None:
        #     output_figure_axes.set_ylabel(y_label)

        return figure_to_use, output_figure_axes
