from . import data
from . import utilities as utilites
import numpy as np
import datetime
import time
from . import io
import logging
import warnings
import netCDF4
import copy
'''
General TODO list:
# Structure of this module?
# Think carefully about inputs/outputs of functions 
#   Most of the indicies currently take in a data.netCDFDatasetRotated and return a numpyArray
#   This wont work for sparse time series, as these cannot be stored in a data.netCDFDatasetRotated
# Maybe have fuctions return a data.numpyDataset return a data.netCDFDatasetRotated
# Done - Ability to change periods?

'''

# Create a logger for this module, which will have a name
# equal to this module, and is also linked into
# the master application's logging hierarchy.
_logger = logging.getLogger(str(__name__))
# If no handlers have been added to this logger (i.e. it's just been newly
# created), then add a NullHandler to avoid a warning message about no handlers
# being present being logged.
if len(_logger.handlers) == 0:
	_logger.addHandler(logging.NullHandler())

def split_dataset_by_period(dataset,period,**kwargs):
    '''
    Generalised function which splits the provided dataset into specified period.
    
    If 360 day calender, changes time iterator upper minus one so runs (bit hacky)

    dataset: Input dataset

    period 
            "all" - whole dataset
            "year" - self explanatory
            "month" - self explanatory
            "monthRange" - Range of months, must also provide monthRange argument
                monthRange=[<integer month>]
                monthRange=[<Integer FirstMonth>,<Integer LastMonth>]
                    Note that, for month ranges overlapping the year, integers over 12 are accepted
                    i.e. monthRange=[12, 14] refers to range from December to the following Feburary
            "week" - self explanatory
            "day" - self explanatory
    '''
    startTime=dataset.datetimes[0]
    endTime=dataset.datetimes[-1]

    timeIterator=dataset.datetimes[0]
    timeIteratorUpper=dataset.datetimes[0]

    dataset_split_into_periods=[]

    while timeIteratorUpper<endTime:
        if period=="all":
            timeIteratorUpper=endTime
        elif period == "year":
            timeIteratorUpper=timeIterator+utilites.cfTimeRelativeDelta(years=1)
        elif period == "month":
            timeIteratorUpper=timeIterator+utilites.cfTimeRelativeDelta(months=1)
        elif period == "monthRange":
            try:
                monthRange=kwargs["monthRange"]
                if len(monthRange)==1:
                    timeIterator=timeIterator.replace(month=monthRange[0])
                    timeIteratorUpper=timeIterator+utilites.cfTimeRelativeDelta(months=1)
                elif len(monthRange)==2:
                    timeIterator=timeIterator.replace(month=monthRange[0])
                    monthDiff=monthRange[1]-monthRange[0]
                    timeIteratorUpper=timeIterator+utilites.cfTimeRelativeDelta(months=monthDiff+1)
                else:
                    raise ValueError ("monthRange should be a list with length 1 or 2")

            except KeyError:
                _logger.exception("For period=\"monthRange\", you must provide a keyword argument monthRange. Format monthRange=[<Integer Month>] or monthRange=[<Integer FirstMonth>,<Integer LastMonth>]")
                raise KeyError ("For period=\"monthRange\", you must provide a keyword argument monthRange. Format monthRange=[<Integer Month>] or monthRange=[<Integer FirstMonth>,<Integer LastMonth>]")
        elif period == "week":
            timeIteratorUpper=timeIterator+utilites.cfTimeRelativeDelta(weeks=1)
        elif period == "day":
            timeIteratorUpper=timeIterator+utilites.cfTimeRelativeDelta(days=1)
        else:
            raise Exception ("Not a valid period. Valid periods are \"all\", \"year\", \"month\",\"monthRange\", \"week\" or \"day\"")



        timeIteratorUpperMinus1=timeIteratorUpper-datetime.timedelta(days=1)
        _logger.debug("Period: " + str(timeIterator) + " - " + str(timeIteratorUpperMinus1))

        if timeIteratorUpperMinus1>endTime:            
            _logger.warning("Period: " + str(timeIterator) +" - " + str(timeIteratorUpper) + " extends beyond end of dataset: " + str(endTime) + ". Ending ETCCDI calculation. return dataset will have N-1 time values.")
            warnings.warn("Period: " + str(timeIterator) +" - " + str(timeIteratorUpper) + " extends beyond end of dataset: " + str(endTime) + ". Ending ETCCDI calculation. return dataset will have N-1 time values.") 
            break

        # Extract the data
        dateTimesInPeriod = {
            "range": [timeIterator,timeIteratorUpperMinus1],
            "type": "including"
        }

        thisPeriodData = dataset.subset(dateTimesInPeriod,'all','all')
        dataset_split_into_periods.append(thisPeriodData)
        #allDataPerPeriod.append(thisPeriodData)

        # If we are looking at specific months we now move a whole year ahead, otherwise just move to next period
        if period=="monthRange":
            timeIterator=timeIterator+utilites.cfTimeRelativeDelta(years=1)
        else:
            timeIterator=timeIteratorUpper

    return dataset_split_into_periods
    


def calc_ETCCDI(dataset,period="month",climate_index="CDD",**kwargs):
    '''
    Generalised function which splits the provided dataset into specified period, 
    and then applies the climate index across the specified period, before returning
    a dataset with the resulting climate index.
    
    If 360 day calender, changes time iterator upper minus one so runs (bit hacky)

    dataset: Input dataset
    period 
            "all" - whole dataset
            "year" - self explanatory
            "month" - self explanatory
            "monthRange" - Range of months, must also provide monthRange argument
                monthRange=[<integer month>]
                monthRange=[<Integer FirstMonth>,<Integer LastMonth>]
                    Note that, for month ranges overlapping the year, integers over 12 are accepted
                    i.e. monthRange=[12, 14] refers to range from December to the following Feburary
            "week" - self explanatory
            "day" - self explanatory
    climate_index
        "CDD" - cumulative dry days
                optional argument threshold=<value> (default is 1.0/86400)
        "RX1DAY"
        "RX5DAY"
        "TXX"
        "THRESHOLD" 
            mandatory argument threshold=<value>
            optional argument thresholdMode="GreaterThan" or "LessThan" (default is "GreaterThan")
    '''

    
    dataset_split_into_periods=split_dataset_by_period(dataset,period=period,**kwargs)
    resultDataPerPeriod=[]
    newTimeArray=[]
    
    for thisPeriodData in dataset_split_into_periods:
        # Save new time array for writing later
        # Currently saves the first date of each period
        # TODO - save period somewhere
        newTimeArray.append(thisPeriodData.datetimes[0])

    
        # Do calcs - and warn user if the variable in the dataset is not what is expected for the index
        if climate_index=="CDD":
            if dataset.climate_variable_name != 'pr': # Check whether climate variable is precipitation, and if not, warn the user
                warnings.warn('CDD index warning: Climate variable in dataset is not precipitation')
            try:
                threshold=kwargs["threshold"]
                resultDataPerPeriod.append(CDD_generic(np.asarray(thisPeriodData.values),threshold=threshold))
            except KeyError:
                resultDataPerPeriod.append(CDD_generic(np.asarray(thisPeriodData.values)))

        elif climate_index=="RX5DAY":
            if dataset.climate_variable_name != 'pr': # Check whether climate variable is precipitation, and if not, warn the user
                warnings.warn('RX5day index warning: Climate variable in dataset is not precipitation')
            
            resultDataPerPeriod.append(RX5DAY_generic(np.asarray(thisPeriodData.values)))

        elif climate_index=="RX1DAY":
            if dataset.climate_variable_name != 'pr': # Check whether climate variable is precipitation, and if not, warn the user
                warnings.warn('RX5day index warning: Climate variable in dataset is not precipitation')
            
            resultDataPerPeriod.append(RX1DAY_generic(np.asarray(thisPeriodData.values)))

        elif climate_index=="TXX":
            if dataset.climate_variable_name != 'tasmax': # Check whether climate variable is max temp, and if not, warn the user
                warnings.warn('TXX index warning: Climate variable in dataset is not max temperature')
            
            resultDataPerPeriod.append(TXX_generic(np.asarray(thisPeriodData.values)))
            
        elif climate_index=="AVERAGE TEMPERATURE":
            if dataset.climate_variable_name != 'tas': # Check whether climate variable is max temp, and if not, warn the user
                warnings.warn('AVERAGE index warning: Climate variable in dataset is not average temperature')
            
            resultDataPerPeriod.append(AVERAGE_generic(np.asarray(thisPeriodData.values)))

        elif climate_index=="AVERAGE MAXIMUM TEMPERATURE":
            if dataset.climate_variable_name != 'tasmax': # Check whether climate variable is max temp, and if not, warn the user
                warnings.warn('AVERAGE index warning: Climate variable in dataset is not maximum temperature')

            resultDataPerPeriod.append(AVERAGE_generic(np.asarray(thisPeriodData.values)))
            
        elif climate_index=="AVERAGE MINIMUM TEMPERATURE":
            if dataset.climate_variable_name != 'tasmin': # Check whether climate variable is max temp, and if not, warn the user
                warnings.warn('AVERAGE index warning: Climate variable in dataset is not maximum temperature')

            resultDataPerPeriod.append(AVERAGE_generic(np.asarray(thisPeriodData.values)))
                
        elif climate_index=="AVERAGE PRECIPITATION":
            if dataset.climate_variable_name != 'pr': # Check whether climate variable is max temp, and if not, warn the user
                warnings.warn('AVERAGE index warning: Climate variable in dataset is not precipitation')
            
            resultDataPerPeriod.append(AVERAGE_generic(np.asarray(thisPeriodData.values)))
            

        elif climate_index=="THRESHOLD":
            try:
                threshold=kwargs["threshold"]
            except KeyError:
                raise KeyError ("Argument threshold=<value> must be passed to this function when using climateIndex=\"THRESHOLD\" ")
                
            try:
                thresholdMode=kwargs["thresholdMode"]
                resultDataPerPeriod.append(Thresholding_generic(np.asarray(thisPeriodData.values), threshold=threshold,Mode=thresholdMode))
            except KeyError:
                resultDataPerPeriod.append(Thresholding_generic(np.asarray(thisPeriodData.values), threshold=threshold))
        else:
            raise NotImplementedError ("Climate Index " + climate_index + " not found")


    # create a Numpy dataset to hold the results
    # These should sit alongside the NetCDFDatasetRotated as it doesn't know about true lat and true lon.
    # Can we create a copy of the NetCDFDatasetRotated instead?
    # Don't think the datetimes are numeric here

    numericNewTimeArray=netCDF4.date2num(newTimeArray, dataset._numeric_datetime_units,
                                dataset._numeric_datetime_calendar)

    resulting_dataset = data.NumpyDataset(
        resultDataPerPeriod, numeric_datetimes=numericNewTimeArray,
        latitudes=dataset.latitudes, longitudes=dataset.longitudes,
        climate_variable_name=climate_index,
        assume_equally_spaced_datetimes=dataset._assume_equally_spaced_datetimes,
        numeric_datetime_calendar=dataset._numeric_datetime_calendar, numeric_datetime_units=dataset._numeric_datetime_units)

    return resulting_dataset # testing
    
def CDD_generic(array, threshold=1.0/86400): #gk this one
    '''
    Compute CCD on a generic Numpy array  and return a Numpy array.
    '''
    vals = array

    vals_less_than_threshold = vals < threshold
    returnData = np.apply_along_axis(count_number_of_consec_trues, 0, vals_less_than_threshold)

    return returnData

def RX5DAY_generic(array): #gk this one
    '''
    Compute the maximum 5 day precipation in the provided array. 
    e.g. If array is a year, it will give maximium 5 day precipitation in year
         If array is a month, it will give maximum 5 day precipiation in a month 

    '''

    vals5day = array
    count=[]
    all5day=[]
    x=len(vals5day)-4
    for j in range (x):
        #print i
        count=vals5day[j]+vals5day[j+1]+vals5day[j+2]+vals5day[j+3]+vals5day[j+4]
        all5day.append(count)

    prmax5day=np.max(all5day,axis=0)*86400 #units to mm

    return prmax5day


def RX1DAY_generic(array): #gk this one
    '''
    Compute the maximum 1 day precipation in the provided array. 
    e.g. If array is a year, it will give maximium 1 day precipitation in year
         If array is a month, it will give maximum 1 day precipiation in a month 

    '''

    vals = array
    prmax=vals.max(axis=0)*86400 #units to mm

    return prmax

def TXX_generic(array): #gk this one
    '''
    Compute the maximum daily temperature in provided array
    e.g. If array is a year, it will give maximium temperature in year
         If array is a month, it will give maximum temperature in month 

    '''


    vals = array
    txxmax=vals.max(axis=0)

    return txxmax

def AVERAGE_generic(array): #gk this one
    '''
    Compute the average daily temperature in provided array
 

    '''


    vals = array
    aver=vals.mean(axis=0)

    return aver

def Thresholding_generic(generic_numpy_array, threshold=None,Mode='GreaterThan'):
    '''
    Compute number of datapoints with values 'GreaterThan' or 'LessThan' the provided threshold value.
    works for a generic numpy array, and returns a generic numpy array
    Assumes axis 0 is the time dimension (will accept time series,as long as axis 0 is time)
    '''
   # wall_clock_start = time.time()

    if threshold==None:
        raise Exception ("Argument threshold=<value> must be passed to this function")

    tempArray = np.copy(generic_numpy_array)

    if Mode=='GreaterThan':

        super_threshold_indices1 = tempArray < float(threshold)
        super_threshold_indices2 = tempArray >= float(threshold)

    elif Mode=='LessThan':
        super_threshold_indices1 = tempArray >= float(threshold)
        super_threshold_indices2 = tempArray < float(threshold)

    tempArray[super_threshold_indices1]=0
    tempArray[super_threshold_indices2]=1
    total=np.sum(tempArray,0)

    return total




def count_number_of_consec_trues(my_boolean_list):
# a generalised way of counting up for consecutive trues
# for example used for my_boolean_list time series of numpy data showing if pr < threshold for CCD calc
    num = 0
    curr_num = 0

    for value in my_boolean_list:
        #if this_day == fill_val continue
        if value:
            curr_num = curr_num + 1
        else:
            num = max(num,curr_num)
            curr_num = 0

    return max(num, curr_num)



#this function gets the years you want in a box you want


def extract_years_bounding_box(ccvar,dataset,my_model_dataset, start_year, no_years,min_longitude, min_latitude, max_longitude, max_latitude):
    '''
    :param ccvar: pr or tasmax - no need to put in as predefined in script
    :param dataset: a climate dataset
    :param my_model_dataset: a handle for climate dataset #is this needed check with clare
    :param start_year: datetime
    :param no_years: how many years to go forwards
    :min_longitude: of region of interest
    :min_latitude: of region of interest
    :max_longitude:of region of interest
    :max_latitude: of region of interest
    :return: of region of interest

    Compute CCD climate index per month from first for a number of months.
    For all lat-lons in the dataset.
    '''''


    start_datetime = copy.deepcopy(dataset.datetimes[0])
    uk=[]
    years= range(0,no_years+1)

    for i in years:
        start_datetime=start_datetime.replace(year=start_year+i)
        end_datetime=start_datetime+utilites.cfTimeRelativeDelta(years=1,days=-1)
        

        looped_years = {"range": [start_datetime,end_datetime], 'type': 'including'}

        _logger.info(looped_years)    
        data_this_year = data.NetCDFDatasetRotated(
     my_model_dataset, ccvar, looped_years, "all", "all",
     netcdf_longitudes_span_globe=True, climate_variable_name_to_use=ccvar,
     assume_equally_spaced_datetimes=True
 )
        data_this_year_all_uk = data_this_year.data_in_bounding_box(min_longitude, min_latitude, max_longitude, max_latitude) #function includes rotation, needs more than 1 lat and 1 long - need to adapt for for loop       
        uk.append(data_this_year_all_uk)   
        
        
    _logger.info(years)
    _logger.info(uk)

    return uk

def extract_years(ccvar,dataset,my_model_dataset, start_year, no_years):
    '''
    :param ccvar: pr or tasmax - no need to put in as predefined in script
    :param dataset: a climate dataset
    :param my_model_dataset: a handle for climate dataset #is this needed check with clare
    :param start_year: datetime
    :param no_years: how many years to go forwards
    :return: of region of interest

    Compute CCD climate index per month from first for a number of months.
    For all lat-lons in the dataset.
    '''''

    start_datetime = copy.deepcopy(dataset.datetimes[0])


    try:
        region=[]
        years= range(0,no_years)

        for i in years:

            start_datetime=start_datetime.replace(year=start_year+i)
            end_datetime=start_datetime+utilites.cfTimeRelativeDelta(years=1,days=-1)
            
            looped_years = {
                "range": [start_datetime,end_datetime], 'type': 'including'}

            _logger.info(looped_years)    
            data_this_year = data.NetCDFDatasetRotated(
            my_model_dataset, ccvar, looped_years, "all", "all",
            netcdf_longitudes_span_globe=True, climate_variable_name_to_use=ccvar,
            assume_equally_spaced_datetimes=True)
            
            region.append(data_this_year)
            
            
        _logger.info(years)
        _logger.info(region)

        return region

    except:
        region=[]
        years= range(0,no_years)

        for i in years:
            

            start_datetime=start_datetime.replace(year=start_year+i)
            end_datetime=start_datetime+utilites.cfTimeRelativeDelta(years=1,days=-1)
            
            looped_years = {
                "range": [start_datetime,end_datetime], 'type': 'including'}

            _logger.info(looped_years)    
            data_this_year = data.NetCDFDataset(
            my_model_dataset, ccvar, looped_years, "all", "all",
            netcdf_longitudes_span_globe=True, climate_variable_name_to_use=ccvar,
            assume_equally_spaced_datetimes=True)
            
            region.append(data_this_year)
            
            
        _logger.info(years)
        _logger.info(region)

        return region
     
def createnetcdf_bounding_box(templatename,min_longitude, min_latitude, max_longitude, max_latitude):
    '''
    :param ccvar: name of template netcdf
    :min_longitude: of region of interest
    :min_latitude: of region of interest
    :max_longitude:of region of interest
    :max_latitude: of region of interest
    :return: template netcdf for results
    '''''
    netcdffile=templatename
    
    one_time ={'range': [datetime.datetime(2014,1,1,12), datetime.datetime(2014,1,1,11)], 'type': 'including'}
    data_one_time = data.NetCDFDatasetRotated(
            my_model_dataset, ccvar,one_time, "all", "all",
            netcdf_longitudes_span_globe=True, climate_variable_name_to_use=ccvar,
            assume_equally_spaced_datetimes=True
            )
    one_time_all_uk = data_one_time.data_in_bounding_box(min_longitude, min_latitude, max_longitude, max_latitude) #function includes rotation, needs more than 1 lat and 1 long
    one_time_file = io.RotatedNetCDFExporter()
    one_time_file.export(one_time_all_uk, netcdffile)
    return netcdffile[0]

def createnetcdf(model,variable,templatename,start_year):
    '''
    :param ccvar: name of template netcdf

    :return: template netcdf for results
    '''''
    netcdffile=templatename
    
    one_time ={'range': [datetime.datetime(start_year,1,1,12), datetime.datetime(start_year,1,1,11)], 'type': 'including'}
    data_one_time = data.NetCDFDatasetRotated(
            model, variable,one_time, "all", "all",
            netcdf_longitudes_span_globe=True, climate_variable_name_to_use=variable,
            assume_equally_spaced_datetimes=True
            )
   
    one_time_file = io.RotatedNetCDFExporter()
    one_time_file.export(data_one_time, netcdffile)
    return netcdffile[0]

# At some point if you want to compute the CDD data per month, or similar, you might need to increment per month
# def addmonths(date,number_months):
#     targetmonth=date.month+number_months
#     try:
#         date.replace(year=date.year+int(targetmonth/12),month=(targetmonth%12))
#     except:
#         # There is an exception if the day of the month we're in does not exist in the target month
#         # Go to the FIRST of the month AFTER, then go back one day.
#         date.replace(year=date.year+int((targetmonth+1)/12),month=((targetmonth+1)%12),day=1)
#         date+=datetime.timedelta(days=-1)
    
