import logging
import abc
from datetime import timedelta
import numpy as np
from sklearn.tree import DecisionTreeRegressor
from scipy.interpolate import RegularGridInterpolator as rgi
import scipy.optimize as sopt
import math
import cftime
import itertools
import copy
import datetime
import dateutil.relativedelta as dateUtil

class CanLog(object):
    """Class with an automatically initialised, personal logger. Designed to be inherited.
    """
    def __init__(self):
        # Create a logger for this class, which will have a name
        # specific to the class that creates it, and is also linked into
        # the master application's logging hierarchy.
        logger = logging.getLogger(str(self.__module__) + "." + self.__class__.__name__)
        # If no handlers have been added to this logger (i.e. it's just been newly
        # created), then add a NullHandler to avoid a warning message about no handlers
        # being present being logged.
        if len(logger.handlers) == 0:
            logger.addHandler(logging.NullHandler())
        self._logger = logger
        return


class Mapper2D(CanLog):
    """
    Abstract class for a way to map from one grid to another.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        CanLog.__init__(self)
        return

    @abc.abstractmethod
    def map(self, values, from_axis_1_array, from_axis_2_array, to_axis_1_array,
            to_axis_2_array):
        """ Map from the input to the target grid. """
        return

class MapperPoints(CanLog):
    """
    Abstract class for a way to map from one grid to a set of points.

    Parameters:
    from_axis_1_array, from_axis_2_array - array which defines the grid to map from
    to_points_axis_1,to_points_axis_2 - an array which defines the location of the points along each axis. 
                                        Each index in to_points_axis_1 corresponds with an index in to_points_axis_2,
                                        and together these define the location of a point
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        CanLog.__init__(self)
        return

    @abc.abstractmethod
    def map(self, values, from_axis_1_array, from_axis_2_array, to_points_axis_1,
            to_points_axis_2):
        """ Map from the input grid to the target points."""
        return


class DecisionTreeRegressorMapper2D(Mapper2D):
    def __init__(self):
        Mapper2D.__init__(self)
        return

    def map(self, values, from_axis_1_array, from_axis_2_array, to_axis_1_array,
            to_axis_2_array):
        """
        Fits data a decision tree regressor on old grid.
        Uses the trained model to regress values onto new grid
        """
        regressor = DecisionTreeRegressor()

        X = np.array(np.meshgrid(from_axis_1_array, from_axis_2_array)).T.reshape(-1, 2)
        numpy_values_array = np.asarray(values)
        Y = numpy_values_array.reshape(X.shape[0], )

        regressor.fit(X, Y)

        new_X = np.array(
            np.meshgrid(to_axis_1_array, to_axis_2_array)
        ).T.reshape(-1, 2)

        mapped_values = regressor.predict(new_X)
        mapped_values = mapped_values.reshape(len(to_axis_1_array), len(to_axis_2_array))
        return mapped_values


class RegularGridMapperPoints(MapperPoints):
    def __init__(self):
        MapperPoints.__init__(self)
        return

    def map(self, values, from_axis_1_array, from_axis_2_array, to_points_axis_1,
            to_points_axis_2):
        """
        Use Bilinear interpolation to map between the grid and the specified points
        """

        INTERPOLATOR = rgi((from_axis_1_array[:], from_axis_2_array[:]), values, bounds_error=False, fill_value=np.nan)
        m = np.asarray(to_points_axis_1).shape
        INPUT_DATA = np.vstack((np.asarray(to_points_axis_1).ravel(), np.asarray(to_points_axis_2).ravel())).T
        mapped_values=INTERPOLATOR(INPUT_DATA).reshape(m)

        return mapped_values

class NearestNeighbourMapper2D(MapperPoints):
    def __init__(self):
        Mapper2D.__init__(self)
        return

    def map(self, values, from_axis_1_array, from_axis_2_array, to_axis_1_array,
            to_axis_2_array):
        """
        Use nearest neighbour interpolation to map between the grid and the specified points
        """

        INTERPOLATOR = rgi((from_axis_1_array[:], from_axis_2_array[:]), values, bounds_error=False, fill_value=np.nan, method="nearest")
        m = np.asarray(to_axis_1_array).shape
        INPUT_DATA = np.vstack((np.asarray(to_axis_1_array).ravel(), np.asarray(to_axis_2_array).ravel())).T
        mapped_values=INTERPOLATOR(INPUT_DATA).reshape(m)

        return mapped_values

class WetBulbUtilities(CanLog):
    """
    Class with a variety utilities for calculating the wet bulb temperature using an interpolator. 
    Assumes standard atmospheric pressure

    WetBulbUtilities(dbt,sh)
        Where dbt and sh are numpy arrays defining the range of (d)ry (b)ulb (t)emperatures  and (s)pecific (h)umidities
        for which the interpolator is created
        Default values are normally sufficient

        WetBulbUtilities.INTERPOLATOR 
            On first call calculates and returns the interpolator
            On subsequent calls returns the interpolator
            (can be saved to speed up future calculations if needed)

        WetBulbUtilities.WBTCalculator(sh,dbt,INTERPOLATOR_OBJECT=None)
            sh and dbt are numpy arrrays containing the values of dbt and sh for which to calculate the wbt
            If an interpolator object is not provided (as is default), it will request one from WetBulbInterpolator.INTERPOLATOR 
            returns wet bulb tempetature
    """
    def __init__(self,dbt=np.linspace(-30, 50, 100) + 273.15,sh=np.linspace(1e-15, 0.03, 100)):
        CanLog.__init__(self)
        self.__INTERPOLATOR=None
        self._dbt=dbt
        self._sh=sh


    @property
    def INTERPOLATOR(self):
        if self.__INTERPOLATOR==None:
            SH, DBT = np.meshgrid(self._sh, self._dbt)
            data = np.zeros(SH.size)
            for i, (sh_p, dbt_p) in enumerate(zip(SH.ravel(), DBT.ravel())):

                data[i] =self.WBTCalculator_training(sh_p, dbt_p)

            self.__INTERPOLATOR = rgi((self._dbt, self._sh), data.reshape(DBT.shape), bounds_error=False, fill_value=-99999)

        return self.__INTERPOLATOR

    def WBTCalculator_training(self,sh,dbt):
        sol = sopt.fsolve(self.WBTeq,dbt,(sh,dbt),xtol=0.0001)[0]
        if (sol > dbt):
            sol=dbt
        #
        return sol

    def WBTeq(self,x,g,Tdry):
        c2K = 273.15
        C1 = 30.59051
        C2 = -8.2
        C3 = 2.4804e-3
        C4 = -3142.31
        Pa = 101.325
        if (x-c2K) >= 0 :
            B=7.99e-4 #this depends on WBT >=0
        else:
            B=7.20e-4

        fs=1 #enhancement factor
        LHS = (Pa*g/(fs*(0.62197+g))) + Pa*B*Tdry
        y= 10**(C1 + C2*math.log(x,10) + C3*x + C4/x) + Pa*B*x - LHS

        return y

    def WBTCalculator(self,sh,dbt,INTERPOLATOR_OBJECT=None):
        if INTERPOLATOR_OBJECT==None:
            INTERPOLATOR_OBJECT=self.INTERPOLATOR

        m = sh.shape
        INPUT_DATA = np.vstack((dbt.ravel(),sh.ravel())).T
        return INTERPOLATOR_OBJECT(INPUT_DATA).reshape(m)

# Original function from Kasia, for reference
#
# class Raster(object):
#
#     def __init__(self, lat, lon, data):
#         self.lat = lat  # netcdf.variables['lat'][:].data
#         self.lon = lon  # netcdf.variables['lon'][:].data
#         self.data = data  # netcdf.variables['suETCCDI'][time slice].data
#
#     def regress_resolution(self, new_lat, new_lon):
#         '''
#         Fits data a decision tree regressor on old grid.
#         Uses the trained model to regress values onto new grid
#         '''
#         regressor = DecisionTreeRegressor()
#
#         X = np.array(np.meshgrid(self.lat, self.lon)).T.reshape(-1, 2)
#         Y = self.data.reshape(X.shape[0], )
#
#         regressor.fit(X, Y)
#
#         new_X = np.array(np.meshgrid(new_lat, new_lon)).T.reshape(-1, 2)
#
#         result = regressor.predict(new_X)
#         result = result.reshape(int(new_lat.shape[0]), int(new_lon.shape[0]))
#
#         return Raster(new_lat, new_lon, result)
#
#     def plot(self):
#         '''
#         Plots matrix as a sort of heatmap for non zero entries
#         '''
#         x, y = self.data.nonzero()
#         plt.scatter(y, x, s=2.5, c=self.data[x, y])
#
#     def locate_common_index(self, regressed_raster, decimals=1):
#         '''
#         Common-ish index between two rasters. Rounds up the lat+lon to `decimals`
#         decimal points (default 1) then intersects the cross product of old_lat x
#         old_lon and new_lat x new_lon to find the idecies that will be used to
#         grab the data values for comparison
#         '''
#         r1_lat = self.lat
#         r2_lat = regressed_raster.lat
#         r1_lon = self.lon
#         r2_lon = regressed_raster.lon
#
#         lats_lons_dict = {'r1_lat': r1_lat, 'r2_lat': r2_lat, 'r1_lon': r1_lon,
#                           'r2_lon': r2_lon}
#
#         lats_lons_dec_dict = {}
#         for key, value in lats_lons_dict.items():
#             lats_lons_dec_dict['{}_dec'.format(key)] = np.around(value, decimals=decimals)
#
#         r1_cross_prod = np.array(np.meshgrid(lats_lons_dec_dict['r1_lat_dec'],
#                                              lats_lons_dec_dict['r1_lon_dec'])).T.reshape(
#             -1, 2)
#         r2_cross_prod = np.array(np.meshgrid(lats_lons_dec_dict['r2_lat_dec'],
#                                              lats_lons_dec_dict['r2_lon_dec'])).T.reshape(
#             -1, 2)
#
#         nrows, ncols = r1_cross_prod.shape
#         dtype = {'names': ['f{}'.format(i) for i in range(ncols)],
#                  'formats': ncols * [r1_cross_prod.dtype]}
#
#         intersection = np.intersect1d(r1_cross_prod.view(dtype),
#                                       r2_cross_prod.view(dtype))
#
#         intersection = intersection.view(r1_cross_prod.dtype).reshape(-1, ncols)
#
#         # alternative to above but maybe slower
#         # intersection = np.array([x for x in set(tuple(x) for x in r1_cross_prod) & set(tuple(x) for x in r2_cross_prod)])
#
#         bools_1 = np.isin(r1_cross_prod, intersection)
#         r1_ix = np.logical_and(bools_1[:, 0], bools_1[:, 1])
#         bools_2 = np.isin(r2_cross_prod, intersection)
#         r2_ix = np.logical_and(bools_2[:, 0], bools_2[:, 1])
#
#         return (r1_ix, r2_ix)
#
#     def compare_data(self, regressed_raster, decimal_accuracy=1):
#         old_ix, new_ix = self.locate_common_index(regressed_raster,
#                                                   decimals=decimal_accuracy)
#         old_data = self.data.reshape(old_ix.shape[0], )[old_ix]
#         new_data = regressed_raster.data.reshape(new_ix.shape[0], )[new_ix]
#
#         difference = np.subtract(old_data, new_data)
#
#         return stats.describe(difference)

class cfTimeRelativeDelta(CanLog):

    _SPECIAL_CALENDAR_INFO={"360_day":{"year":360,
                                        "month":[30,30,30,30,30,30,30,30,30,30,30,30],
                                        "week":7},
                            "noleap":{"year":365,
                                        "month":[31,28,31,30,31,30,31,31,30,31,30,31],
                                        "week":7},
                            "all_leap":{"year":366,
                                        "month":[31,29,31,30,31,30,31,31,30,31,30,31],
                                        "week":7}
                                        }



    def __init__(self,years=0, months=0, days=0, leapdays=0, weeks=0,
                 hours=0, minutes=0, seconds=0, microseconds=0):
        """
        Class which handles relative time periods cfTime with non standard calendars. 
        Implements addition and subtraction

        See for inspiration. https://dateutil.readthedocs.io/en/stable/_modules/dateutil/relativedelta.html#relativedelta
        
        Supported Calendars:
        'standard', 'gregorian', 'proleptic_gregorian' 'noleap', '365_day', '360_day', 'all_leap', '366_day'

        Notes:
        Does not work with dates pre 1583 to avoid problems that would come with the switch between Julian and Gregorian calendar
        """
        self.years=years
        self.months=months
        self.days=days
        self.leapdays=leapdays
        self.weeks=weeks
        self.hours=hours
        self.minutes=minutes
        self.seconds=seconds
        self.microseconds=microseconds


    def __add__(self,other):
        """
        Calendar aware addition function
        """

        if other.year<1583:
            raise Exception("Invalid year %s. cfTimeRelativeDelta does not support years before 1583" % other.year)

        if isinstance(other,cftime.Datetime360Day) or isinstance(other,cftime.DatetimeNoLeap)or isinstance(other,cftime.DatetimeAllLeap)\
        or (isinstance(other,cftime.datetime) and (other.calendar=="360_day" or other.calendar=="noleap" or other.calendar=="all_leap")):
            # Special calendar addition
            days_to_add=self.years*self._SPECIAL_CALENDAR_INFO[other.calendar]["year"]+\
                        self._days_to_add(other.month,other.day,other.calendar)+\
                        self.weeks*self._SPECIAL_CALENDAR_INFO[other.calendar]["week"]+\
                        self.days

            newdatetime=other+timedelta(days=days_to_add,seconds=self.seconds,microseconds=self.microseconds,
                            minutes=self.minutes, hours=self.hours)

        elif isinstance(other,datetime.datetime):
            # datetime.datetime so use standard relativedelta
            newdatetime=other+dateUtil.relativedelta(years=self.years,
                                                    months=self.months,
                                                    weeks=self.weeks,
                                                    days=self.days,
                                                    leapdays=self.leapdays,
                                                    hours=self.hours,
                                                    minutes=self.minutes,
                                                    seconds=self.seconds,
                                                    microseconds=self.microseconds)

        elif isinstance(other,cftime.DatetimeGregorian) or isinstance(other,cftime.DatetimeProlepticGregorian)\
            or (isinstance(other,cftime.datetime) and (other.calendar=="gregorian" or other.calendar=="proleptic_gregorian")):

            # This is essentially a normal calendar (as we dont support dates pre 1583) so use the relativedelta from dateutil.
            #  cftime.DatetimeGregorian -> realdatetime +relativedelta -> cftime.DatetimeGregorian
            realdateime=other._to_real_datetime()
            summed_time=realdateime+dateUtil.relativedelta(years=self.years,
                                                    months=self.months,
                                                    weeks=self.weeks,
                                                    days=self.days,
                                                    leapdays=self.leapdays,
                                                    hours=self.hours,
                                                    minutes=self.minutes,
                                                    seconds=self.seconds,
                                                    microseconds=self.microseconds)

            newdatetime=cftime.datetime(summed_time.year,
                                                 summed_time.month,
                                                 summed_time.day,
                                                 hour=summed_time.hour,
                                                 minute=summed_time.minute,
                                                 second=summed_time.second,
                                                 microsecond=summed_time.microsecond,
                                                 calendar=other.calendar)

        else:
            raise Exception("Object of type %s is not a supported datetime object" % str(type(other)))
            

        if newdatetime.year<1583:
            raise Exception("Invalid year %s. cfTimeRelativeDelta does not support years before 1583" % newdatetime.year)

        return newdatetime

    def __radd__(self,other):
        return self.__add__(other)

    def __rsub__(self, other):
        return self.__sub__(other)


    def __sub__(self, other):
        reversed_time_delta=cfTimeRelativeDelta(years=-self.years, 
                            months=-self.months, 
                            days=-self.days, 
                            leapdays=-self.leapdays,
                            weeks=-self.weeks,
                            hours=-self.hours, 
                            minutes=-self.minutes,
                            seconds=-self.seconds,
                            microseconds=-self.microseconds)

        return reversed_time_delta.__add__(other)
        

    def _days_to_add(self,start_month,start_day,calendar):
        """ 
        Days to add per month for special calendars
        """
        
        if start_month<1 or start_month>12:
            raise Exception("Start month %s is invalid. Must be a value between 1 and 12" % start_month)

        # Extract info about calendars
        days_in_months=copy.deepcopy(self._SPECIAL_CALENDAR_INFO[calendar]["month"])

        if self.months<0:
            # If we are subtracting we wish to go in the opposite direction
            days_in_months.reverse() #
            days_in_months=[-i for i in days_in_months]
            cyclic_day_iterator=itertools.cycle(days_in_months)
            start_month=12-start_month+2
            start_day=-start_day
        else:
            cyclic_day_iterator=itertools.cycle(days_in_months)

        list_of_days_to_sum=[]
        
        for count,no_of_days in enumerate(cyclic_day_iterator,1):
            if count == (start_month+abs(self.months)):
                # break out of the loop once we've looped over our months
                break

            if count >= start_month:
                list_of_days_to_sum.append(no_of_days)

        # Now we must correct for the case where the start day 
        # of the starting month is beyond the last day of the final month
        if self.months<0:
            correction=min(0,start_day-list_of_days_to_sum[-1])
        else:
            correction=-max(0,start_day-no_of_days)

        list_of_days_to_sum.append(correction)
        


        return sum(list_of_days_to_sum)




