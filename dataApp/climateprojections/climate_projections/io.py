import abc
import os
import json
import re
import operator
from collections import OrderedDict

import netCDF4
import csv

from . import utilities


NUMERIC_DATE_TIMES_ID = "numeric date-times"
LATITUDES_ID = "latitudes"
LONGITUDES_ID = "longitudes"
VALUES_ID = "values"
DEFAULT_NETCDF_DIMENSION_NAMES = {
    NUMERIC_DATE_TIMES_ID: "time",
    LATITUDES_ID: "lat",
    LONGITUDES_ID: "lon"
}

CLIMATE_INDEX_KEY_NAME = "climate index"
MODEL_KEY_NAME = "model"
REALISATION_KEY_NAME = "realisation"
VERSION_KEY_NAME = "version"
TIME_RESOLUTION_KEY_NAME = "time resolution"
SCENARIO_KEY_NAME = "scenario"
PATH_KEY_NAME = "path"

_ATTRIBUTE_IDS_IN_CANONICAL_ORDER = [
    CLIMATE_INDEX_KEY_NAME,
    SCENARIO_KEY_NAME,
    MODEL_KEY_NAME,
    TIME_RESOLUTION_KEY_NAME,
    REALISATION_KEY_NAME,
    VERSION_KEY_NAME
]


class Sort:
    ASCENDING = "ascending"
    DESCENDING = "descending"


class Database(utilities.CanLog):
    """
    Abstract class for a database that can access climate change projection data,
    wherever it is stored, and returns the data in a useful format.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        utilities.CanLog.__init__(self)
        return

    @abc.abstractmethod
    def get_dataset(self, climate_index, time_resolution, model_name, scenario,
                    realisation=None, version_id=None):
        """Should return a numpy array: dataset[timeIndex, latitudeIndex,
        longitudeIndex]"""
        return

    # @abc.abstractmethod
    # def get_latitudes(self, climate_index, model_name, scenario, realisation=None,
    #                   version_id=None):
    #     """Should return an array of latitudes for the relevant dataset"""
    #     return
    #
    # @abc.abstractmethod
    # def get_longitudes(self, climate_index, model_name, scenario, realisation=None,
    #                   version_id=None):
    #     """Should return an array of longitudes for the relevant dataset"""
    #     return
    #
    # @abc.abstractmethod
    # def get_times(self, climate_index, model_name, scenario, realisation=None,
    #                    version_id=None):
    #     """Should return an array of times for the relevant dataset"""
    #     return

    # def get_available_models(self, scenario):


class LocalFolderETCCDIDatabase(Database):

    #TODO: you now call thiw with arguemnt "CORDEX". The function nae is misleadig.  Either change, and update in all project, or understand how to use wrappers
    def __init__(self, root_folder_path=None, climate_database=None):
        Database.__init__(self)
        print("root_folder_path: " + root_folder_path)
        self._SCENARIO_FOLDER_NAMES_DICT = {
            "historical": "historical",
            "historicalExt": "historicalExt",
            "historicalGHG": "historicalGHG",
            "historicalMisc": "historicalMisc",
            "historicalNat": "historicalNat",
            "rcp26": "rcp26",
            "rcp2.6": "rcp26",
            "2.6": "rcp26",
            "rcp45": "rcp45",
            "rcp4.5": "rcp45",
            "4.5": "rcp45",
            "rcp60": "rcp60",
            "rcp6.0": "rcp60",
            "rcp6": "rcp60",
            "6": "rcp60",
            "6.0": "rcp60",
            "rcp85": "rcp85",
            "rcp8.5": "rcp85",
            "8.5": "rcp85",           
            "historicalrcp85": "historicalrcp85",
            "historicalrcp45": "historicalrcp45"
        }
        self._DEFAULT_ROOT_FOLDER_PATH = \
            'W:\\OPENFOAM\\scratch\\klende\\ftp.cccma.ec.gc.ca\\data\\climdex\\CMIP5'
        self._TIME_RESOLUTION_ID_TABLE = {
            'y': 'yr',
            'yr': 'yr',
            'year': 'yr',
            'yearly': 'yr',
            'm': 'mon',
            'mon': 'mon',
            'month': 'mon',
            'monthly': 'mon',
            'd': 'day',
            'day': 'day',
            'daily': 'day'
        }
        self._DEFAULT_CLIMATE_DATABASE = "CLIM5"

        if root_folder_path is None:
            self._root_folder_path = self._DEFAULT_ROOT_FOLDER_PATH
        else:
            self._root_folder_path = root_folder_path

        if not os.path.isdir(self._root_folder_path):
            raise IOError('The following path is not a folder: "{0}"'.format(
                self._root_folder_path))

        if climate_database is None:
            self._climate_database = self._DEFAULT_CLIMATE_DATABASE
        else:
            self._climate_database = climate_database

        if self._climate_database not in ["CLIM5", "CORDEX"]:
            raise IOError('Climate database "{0}" is not recognised, please use CLIM5 or CORDEX if appropriate'.format(
                self._climate_database))

        self._all_available_datasets_search_result = None
        self.refresh()

        return

    @property
    def root_folder(self):
        return self._root_folder_path

    def refresh(self):
        all_available_datasets_search_result_items = self._find_all_available_datasets()
        self._all_available_datasets_search_result = \
            LocalFolderETCCDIDatabaseSearchResult(
                all_available_datasets_search_result_items)
        return

    def _find_all_available_datasets(self):
        search_result_items = []
        for (this_folder_path, child_folder_names, child_file_names) in os.walk(
                self._root_folder_path):
            for child_file_name in child_file_names:
                if child_file_name.endswith(".nc") and \
                        self._whether_to_include_in_database(this_folder_path,
                                                             child_file_name):
                    relative_folder_path = this_folder_path[len(self._root_folder_path):]
                    if relative_folder_path.startswith("\\") or \
                            relative_folder_path.startswith("/"):
                        relative_folder_path = relative_folder_path[1:]
                    relative_path_to_file = os.path.join(relative_folder_path,
                                                         child_file_name)
                    search_result_item = self._convert_to_search_result_item(
                        relative_path_to_file)
                    search_result_items.append(search_result_item)

        return search_result_items

    def _whether_to_include_in_database(self, path_fo_file, file_name):
        # Ignore Omon (monthly data for Oceans) and
        # "Imon" (monthly data for sea ice); so using this rule
        # to exclude it from the database.
        if "mon" in file_name:
            if "Omon" in file_name:
                return 0
            if "Imon" in file_name:
                return 0
        # Found a special "thresholds" file in
        # historical\CMCC-CM\r1i1p1\v20120514\base_1961-1990; so using this rule
        # to exclude it from the database.
        return not file_name.startswith("thresholds")

    def _convert_to_search_result_item(self, relative_path_to_netcdf_file):
        path_as_list = self._split_all(relative_path_to_netcdf_file)
        if len(path_as_list) == 6:
            climate_index, time_resolution = \
                self._climate_index_and_time_resolution_from_file_name(path_as_list[-1])
            search_result_item = {
                SCENARIO_KEY_NAME: path_as_list[0],
                MODEL_KEY_NAME: path_as_list[1],
                REALISATION_KEY_NAME: path_as_list[2],
                VERSION_KEY_NAME: path_as_list[3],
                TIME_RESOLUTION_KEY_NAME: time_resolution,
                CLIMATE_INDEX_KEY_NAME: climate_index,
                PATH_KEY_NAME: relative_path_to_netcdf_file
            }

        elif len(path_as_list) == 1:
            climate_index, time_resolution, model, scenario, realisation = \
                self._all_information_from_file_name(relative_path_to_netcdf_file)
            search_result_item = {
                SCENARIO_KEY_NAME: scenario,
                MODEL_KEY_NAME: model,
                REALISATION_KEY_NAME: realisation,
                VERSION_KEY_NAME: '',
                TIME_RESOLUTION_KEY_NAME: time_resolution,
                CLIMATE_INDEX_KEY_NAME: climate_index,
                PATH_KEY_NAME: relative_path_to_netcdf_file
            }

        else:
            raise ValueError(("Invalid path to netcdf file '{0}'. Expected 5 levels of "
                              "folders, found {1}").format(relative_path_to_netcdf_file,
                                                           len(path_as_list)))

        # Use the canonical order for the key-value pairs in the search_result_item
        ordered_search_result_item = OrderedDict([])
        for attribute_id in _ATTRIBUTE_IDS_IN_CANONICAL_ORDER:
            ordered_search_result_item[attribute_id] = search_result_item[attribute_id]
        # Remember to also add in the path key
        ordered_search_result_item[PATH_KEY_NAME] = search_result_item[PATH_KEY_NAME]
        return ordered_search_result_item

    def _climate_index_and_time_resolution_from_file_name(self, file_name):
        file_name_parts = file_name.split("_")
        # Remove ETCDDI from the first part of the file name to get the index name
        climate_index = file_name_parts[0][:-6]
        time_resolution = file_name_parts[1]
        return climate_index, time_resolution

    def _all_information_from_file_name(self, file_name):
        """Internal function to get meta-data from the netcdf filename
        ETCCDI filename: cddETCCDI_yr_ACCESS1-3_rcp45_r1i1p1_2006-2100.nc
        CLIM5 filename: pr_day_ACCESS1-0_rcp85_r1i1p1_20810101-21001231.nc
        CORDEX filename: pr_EUR-11_CNRM-CERFACS-CNRM-CM5_rcp45_r1i1p1_CNRM-ALADIN53_v1_day_20760101-20801231.nc
        """
        if(self._climate_database == "CLIM5"):
            return self._all_information_from_CLIM5_file_name(file_name)
        elif(self._climate_database == "CORDEX"):
            return self._all_information_from_CORDEX_file_name(file_name)
        else:
            raise ValueError(
                'When processing file, internal error must have occured in setting climate_database property.'
            )

    def _all_information_from_CLIM5_file_name(self, file_name):

        # filename in this format: pr_day_ACCESS1-0_rcp85_r1i1p1_20810101-21001231.nc
        file_name_parts = file_name.split("_")
        # Remove ETCDDI from the first part of the file name to get the index name
        if file_name_parts[0].endswith('ETCCDI'):
            climate_index = file_name_parts[0][:-6]
        else:
            climate_index = file_name_parts[0]

        # Parse Amon - rejected Omon and IOmon
        if file_name_parts[1] == 'Amon':
            time_resolution = 'mon'
        else:
            time_resolution = file_name_parts[1]

        if(time_resolution) not in self._TIME_RESOLUTION_ID_TABLE:
            raise ValueError(
                '"{0}" has not got time resolution as second component of filename. Expect it is not CLIM5 data.'.format(file_name)
            )

        model = file_name_parts[2]
        scenario = file_name_parts[3]
        realisation = file_name_parts[4]
        return climate_index, time_resolution, model, scenario, realisation

    def _all_information_from_CORDEX_file_name(self, file_name):
        """Internal function to get meta-data from the netcdf filename
        CORDEX filename: pr_EUR-11_CNRM-CERFACS-CNRM-CM5_rcp45_r1i1p1_CNRM-ALADIN53_v1_day_20760101-20801231.nc
        """

        file_name_parts = file_name.split("_")

        climate_index = file_name_parts[0]

        if (file_name_parts[1]) in self._TIME_RESOLUTION_ID_TABLE:
            raise ValueError(
                '"{0}" has got time resolution as second component of filename. Expect it is CLIM5 data not CORDEX.'.format(
                    file_name)
            )

        region = file_name_parts[1]
        driver_model = file_name_parts[2]
        scenario = file_name_parts[3]
        realisation = file_name_parts[4]
        RCM_model = file_name_parts[5]
        version = file_name_parts[6]
        time_resolution = file_name_parts[7]

        model = driver_model + '-driving-' + RCM_model

        return climate_index, time_resolution, model, scenario, realisation

    def _split_all(self, path):
        all_parts = []
        while 1:
            parts = os.path.split(path)
            if parts[0] == path:  # sentinel for absolute paths
                all_parts.insert(0, parts[0])
                break
            elif parts[1] == path:  # sentinel for relative paths
                all_parts.insert(0, parts[1])
                break
            else:
                path = parts[0]
                all_parts.insert(0, parts[1])
        return all_parts

    def search(self, climate_indices=None, time_resolutions=None, model_names=None,
               scenarios=None, realisations=None, version_ids=None):
        """
        Returns a list of what is available in the database.
        Call without any arguments to get a list of everything in the database.
        Call with the following optional parameters.

        For example: climate_database.search(climate_indices=['pr', 'su'])

        :param climate_indices:
        :param time_resolutions:
        :param model_names:
        :param scenarios:
        :param realisations:
        :param version_ids:
        :return:
        """
        print("Searching: ")
        search_result = self._all_available_datasets_search_result.filtered(
            climate_indices=climate_indices, time_resolutions=time_resolutions,
            model_names=model_names, scenarios=scenarios, realisations=realisations,
            version_ids=version_ids
        )
        return search_result

    def _resolve_time_resolution_id(self, time_resolution):
        try:
            time_resolution_id = self._TIME_RESOLUTION_ID_TABLE[time_resolution.lower()]
        except KeyError:
            raise ValueError(
                ('"{0}" is an invalid time resolution specification. Use one of the '
                 'following: "{1}"').format(time_resolution,
                                            set(self._TIME_RESOLUTION_ID_TABLE.values()))
            )
        return time_resolution_id

    def _pick_first_realisation(self, available_realisations, extra_error_message=""):
        # We assume all realisation ids match the standard form. If we find an id
        # that doesn't match, we will raise an error.

        # Each realisation id string will be parsed into a list of integers
        parsed_realisation_ids = []
        alternative_id_found = False
        for realisation in available_realisations:
            # Use regex to match the expected form of: 'r[number]i[number]p[number]'
            # Copy the regex expression into https://regexr.com/ to get a breakdown
            # of how it works.
            if not re.match(r"\br\d+i\d+p\d+\b", realisation):
                alternative_id_found = True
                alternative_id = realisation
                break
            else:
                parsed_realisation_id = re.split("[ip]", realisation[1:])
                # Convert strings to ints
                parsed_realisation_id = [int(value) for value in parsed_realisation_id]
                parsed_realisation_ids.append(parsed_realisation_id)

        if alternative_id_found:
            raise ValueError(
                ("An alternative realisation id format was found ('{0}'), so cannot "
                 "automatically pick the 'first' realisation. The standard, expected "
                 "format is 'r[number]i[number]p[number]. {1}").format(
                    alternative_id, extra_error_message)
            )

        # Sort and then pick the first realisation
        parsed_realisation_ids.sort(key=operator.itemgetter(0, 1, 2))
        picked_parsed_realisation_id = parsed_realisation_ids[0]
        picked_realisation_id = "r{0}i{1}p{2}".format(
            picked_parsed_realisation_id[0], picked_parsed_realisation_id[1],
            picked_parsed_realisation_id[2]
        )

        return picked_realisation_id

    def get_netcdf_dataset_ESGF(self, climate_index, time_resolution, time_range,
                                model_name, scenario, realisation=None, version_id=None):
        """
        Relevant for the ESGF datasets (not the ETCCDI ones) - development to eventually
        replace get_netcdf_dataset(). The difference is that it
        includes arguments to specify the extent of the data. Since we will be accessing
        the "aggregate" .nc files via OPeNDAP, we need to know ahead of time what time
        range we want (each aggregate .nc file only contains a subset of the full time
        range). The full set of spatial data is in each .nc file, so we don't need to
        specify latitutdes and longitudes ahead of time, but we may as well provide the
        option.
        """
        pass



    def get_netcdf_dataset_object(self, climate_index, time_resolution, model_name,
                                  scenario,
                                  realisation=None, version_id=None):
        if realisation is None:
            realisations = None
        else:
            realisations = [realisation]

        if version_id is None:
            version_ids = None
        else:
            version_ids = [version_id]

        search_result = self.search(
            climate_indices=[climate_index], time_resolutions=[time_resolution],
            model_names=[model_name], scenarios=[scenario], realisations=realisations,
            version_ids=version_ids
        )
        number_of_matches = len(search_result)
        if number_of_matches == 0:
            raise ValueError(
                    ("Couldn't find any matching netcdfs in the "
                     "database. climate_index = {0}, time_resolution = {1}, "
                     "model_name = {2}, scenario = {3}, realisation = {4}, "
                     "version_id = {5}").format(climate_index, time_resolution, 
                                   model_name, scenario, realisation, 
                                   version_id)
                )
        elif number_of_matches > 1:
            # We want to pick the first realisation and version
            sorted_search_result = search_result
            if realisation is not None:
                sorted_search_result = sorted_search_result.sorted(
                    realisation=Sort.ASCENDING)
            if version_id is not None:
                sorted_search_result = sorted_search_result.sorted(
                    version_id=Sort.ASCENDING)
            search_result_item = sorted_search_result[0]
        else:
            # Found only one match
            search_result_item = search_result[0]
        netcdf_relative_file_path = search_result_item["path"]
        netcdf_file_path = os.path.join(self._root_folder_path, netcdf_relative_file_path)

        netcdf_dataset_object = netCDF4.Dataset(netcdf_file_path)
        print(netcdf_file_path,"---------------")
        # To open with parallel I/O enabled:
        # netcdf_data_set = netCDF4.Dataset(netcdf_file_path, 'r', parallel=True)
        # See http://unidata.github.io/netcdf4-python/ for details.

        return netcdf_dataset_object

    # def get_netcdf_dataset_object(self, climate_index, time_resolution, model_name,
    #                               scenario,
    #                               realisation=None, version_id=None):
    #     """
    #     Get a numpy dataset (varying in time, latitude and longitude) for the specified
    #     variable.
    #
    #     Parameters
    #     ----------
    #     variable_name : str
    #         Blah.
    #     model_name : str
    #         Blah.
    #     scenario: str
    #         Blah.
    #     realisation : None or str, optional
    #         Specify the realisation of the climate model. If None is specified,
    #         then the first realisation will be used.
    #
    #     Returns
    #     -------
    #     numpy array
    #
    #     Examples
    #     --------
    #
    #     """
    #
    #     # Build a dictionary of the input options. We just use this to provide
    #     # all relevant information if an error is raised.
    #     inputs_dictionary = {
    #         "climate index": climate_index,
    #         "time resolution": time_resolution,
    #         "model name": model_name,
    #         "scenario": scenario,
    #         "realisation": realisation,
    #         "version id": version_id
    #     }
    #
    #     if scenario.lower() not in self._SCENARIO_FOLDER_NAMES_DICT:
    #         raise ValueError(
    #             ('"{0}" is not a valid scenario. Valid scenarios are: {1}.'
    #              'Requested input was {2}').format(
    #                 scenario, set(self._SCENARIO_FOLDER_NAMES_DICT.values()),
    #                 inputs_dictionary
    #             )
    #         )
    #
    #     scenario_folder_path = os.path.join(
    #         self._root_folder_path, self._SCENARIO_FOLDER_NAMES_DICT[scenario.lower()]
    #     )
    #
    #     # Get a list of all available models from the folder
    #     available_model_names = []
    #     for file_or_folder in os.listdir(scenario_folder_path):
    #         file_or_folder_path = os.path.join(scenario_folder_path, file_or_folder)
    #         if os.path.isdir(file_or_folder_path):
    #             available_model_names.append(file_or_folder)
    #
    #     if model_name not in available_model_names:
    #         raise ValueError(
    #             ('The model "{0}" is not available. Available models are: {1}.'
    #              'Requested input was {2}').format(
    #                 model_name, available_model_names, inputs_dictionary
    #             )
    #         )
    #
    #     model_folder_path = os.path.join(scenario_folder_path, model_name)
    #
    #     # Get a list of all available realisations from the folder
    #     available_realisations = []
    #     for file_or_folder in os.listdir(model_folder_path):
    #         file_or_folder_path = os.path.join(model_folder_path, file_or_folder)
    #         if os.path.isdir(file_or_folder_path):
    #             available_realisations.append(file_or_folder)
    #
    #     if realisation is None:
    #         realisation_folder_name = self._pick_first_realisation(
    #             available_realisations,
    #             'Requested input was {0}'.format(inputs_dictionary)
    #         )
    #     else:
    #         if realisation not in available_realisations:
    #             raise ValueError(
    #                 ('The realisation "{0}" is not available. Available realisations '
    #                  'are: {1}. Requested input was {2}').format(
    #                     realisation, available_realisations, inputs_dictionary
    #                 )
    #             )
    #         realisation_folder_name = realisation
    #
    #     realisation_folder_path = os.path.join(model_folder_path, realisation_folder_name)
    #
    #     # Get a list of all available versions from the folder
    #     available_versions = []
    #     for file_or_folder in os.listdir(realisation_folder_path):
    #         file_or_folder_path = os.path.join(realisation_folder_path, file_or_folder)
    #         if os.path.isdir(file_or_folder_path):
    #             available_versions.append(file_or_folder)
    #
    #     if version_id is None:
    #         version_folder_name = sorted(available_versions)[0]
    #     else:
    #         if version_id not in available_versions:
    #             raise ValueError(
    #                 ('The version "{0}" is not available. Available versions '
    #                  'are: {1}. Requested input was {2}').format(
    #                     version_id, available_versions, inputs_dictionary
    #                 )
    #             )
    #         version_folder_name = version_id
    #
    #     version_folder_path = os.path.join(realisation_folder_path, version_folder_name)
    #
    #     # We now assume there is one folder at this level, which contains the netcdf
    #     # (.nc) files. There is one file for each index.
    #     netcdf_folder_name = os.listdir(version_folder_path)[0]
    #     netcdf_folder_path = os.path.join(version_folder_path, netcdf_folder_name)
    #
    #     # Get a list of all available indices and resolutions from the folder
    #     available_indices = []
    #     index_time_resolutions = []
    #     netcdf_file_names = []
    #     for file_or_folder in os.listdir(netcdf_folder_path):
    #         file_or_folder_path = os.path.join(netcdf_folder_path, file_or_folder)
    #         if os.path.isfile(file_or_folder_path) and file_or_folder.endswith(".nc"):
    #             netcdf_file_names.append(file_or_folder)
    #             # Each netcdf file name has the following format:
    #             # [index]ETCCDI_[time resolution]_[model]_[scenario]_[realisation]_[year range]
    #             # For example:
    #             # tn10pETCCDI_mon_FGOALS-g2_rcp45_r1i1p1_200601-220312.nc
    #             file_name_parts = file_or_folder.split("_")
    #             # Remove ETCDDI from the first part of the file name to get the index name
    #             available_indices.append(file_name_parts[0][:-6])
    #             index_time_resolutions.append(file_name_parts[1])
    #
    #     if climate_index not in available_indices:
    #         raise ValueError(
    #             ('The index "{0}" is not available. Available indices are: {1}. '
    #              'Requested input was {2}').format(
    #                 climate_index, set(available_indices), inputs_dictionary
    #             )
    #         )
    #
    #     file_indices = []
    #     for i, available_climate_index in enumerate(available_indices):
    #         if climate_index.lower() == available_climate_index.lower():
    #             file_indices.append(i)
    #
    #     found_time_resolution = False
    #     netcdf_file_index = None
    #     for i in file_indices:
    #         available_time_resolution = index_time_resolutions[i]
    #         if self._resolve_time_resolution_id(time_resolution) == \
    #                 available_time_resolution:
    #             netcdf_file_index = i
    #             found_time_resolution = True
    #             break
    #
    #     if not found_time_resolution:
    #         raise ValueError(
    #             ("The time resolution '{0}' isn't available for the climate index "
    #              "'{1}'. Available time resolutions are: '{2}'. Requested input "
    #              "was {3}").format(
    #                 time_resolution, climate_index,
    #                 [index_time_resolutions[i] for i in file_indices],
    #                 inputs_dictionary
    #             )
    #         )
    #
    #     netcdf_file_name = netcdf_file_names[netcdf_file_index]
    #     netcdf_file_path = os.path.join(netcdf_folder_path, netcdf_file_name)
    #
    #     netcdf_dataset_object = netCDF4.Dataset(netcdf_file_path)
    #     # To open with parallel I/O enabled:
    #     # netcdf_data_set = netCDF4.Dataset(netcdf_file_path, 'r', parallel=True)
    #     # See http://unidata.github.io/netcdf4-python/ for details.
    #
    #     return netcdf_dataset_object

    # CP Oct 2018:
    # Below is very similar to above but uses MFD call to get mutliple files, not just return the first file
    # Should they ideally be combined into the same routine or is it clearer what you get if we keep them seperate?
    def get_netcdf_dataset_object_multi(self, climate_index, time_resolution, model_name,
                                  scenario,
                                  realisation=None, version_id=None):
        if realisation is None:
            realisations = None
        else:
            realisations = [realisation]

        if version_id is None:
            version_ids = None
        else:
            version_ids = [version_id]

        search_result = self.search(
            climate_indices=[climate_index], time_resolutions=[time_resolution],
            model_names=[model_name], scenarios=[scenario], realisations=realisations,
            version_ids=version_ids
        )
        print ("----------------------################## -------------io search result {}".format(search_result))
        number_of_matches = len(search_result)
        if number_of_matches == 0:
            raise ValueError("Couldn't find any matching netcdfs in the database.")
        elif number_of_matches > 1:
            # Instead we grab them all
            # TO DO: Check if logic below works to ensure all files have the same realisation and version id
            # TO DO: Any further checks on which files are in case they should not be combined?
            sorted_search_result = search_result

            if realisation is None:
                sorted_search_result = sorted_search_result.sorted(
                    realisation=Sort.ASCENDING)
                if(sorted_search_result[0]['realisation'] != sorted_search_result[number_of_matches-1]['realisation']):
                    raise ValueError("Mixture of realisations - please specify the realisation.")
            if version_id is None:
                sorted_search_result = sorted_search_result.sorted(
                    version_id=Sort.ASCENDING)
                if(sorted_search_result[0]['version'] != sorted_search_result[number_of_matches-1]['version']):
                    raise ValueError("Mixture of version_ids - please specify the version_id.")

            path_list = []

            for i in range(number_of_matches):
                netcdf_relative_file_path = sorted_search_result[i]["path"]
                path_list.append(os.path.join(self._root_folder_path, netcdf_relative_file_path))

            #To read the files in the correct numerical order I think it suffices to sort the file names to be in ascending order
            path_list = sorted(path_list)
            print(path_list,"path list ----------------------------------------------------")
            netcdf_dataset_object = netCDF4.MFDataset(path_list)
        else:
            # Found only one match
            search_result_item = search_result[0]
            netcdf_relative_file_path = search_result_item["path"]
            netcdf_file_path = os.path.join(self._root_folder_path, netcdf_relative_file_path)
            netcdf_dataset_object = netCDF4.Dataset(netcdf_file_path)
            print(netcdf_file_path,"---------------")

        return netcdf_dataset_object

    def get_dataset(self, climate_index, time_resolution, model_name,
                    scenario, time_range=None, latitude_range=None,
                    longitude_range=None, realisation=None, version_id=None):
        """
        Get a numpy dataset (varying in time, latitude and longitude) for the specified
        variable.

        Parameters
        ----------

        Returns
        -------
        numpy array

        Examples
        --------
        >>> monitor_points_ccl = generate_cartesian_monitor_points_ccl(
        ...     {'Point A': [1.0, 3.0, 5.0], 'Point B': [4.0, 2.0, 2.0]},
        ...     ['Pressure', 'Velocity']
        ... )
        >>> print monitor_points_ccl.to_string(allow_partial_specification=True)
        Blah

        """
        netcdf_dataset_object = self.get_netcdf_dataset_object(
            climate_index, time_resolution, model_name, scenario,
            realisation=realisation, version_id=version_id
        )

        return netcdf_dataset_object


class LocalFolderETCCDIDatabaseSearchResult(utilities.CanLog):
    def __init__(self, search_result_items, sort=True):
        utilities.CanLog.__init__(self)
        self._SCENARIO_FOLDER_NAMES_DICT = {
            "historical": "historical",
            "historicalExt": "historicalExt",
            "historicalGHG": "historicalGHG",
            "historicalMisc": "historicalMisc",
            "historicalNat": "historicalNat",
            "rcp26": "rcp26",
            "rcp2.6": "rcp26",
            "2.6": "rcp26",
            "rcp45": "rcp45",
            "rcp4.5": "rcp45",
            "4.5": "rcp45",
            "rcp60": "rcp60",
            "rcp6.0": "rcp60",
            "rcp6": "rcp60",
            "6": "rcp60",
            "6.0": "rpc60",
            "rcp85": "rcp85",
            "rcp8.5": "rcp85",
            "8.5": "rcp85",
            "historicalrcp85": "historicalrcp85",
            "historicalrcp45": "historicalrcp45",
            "obs": "obs"
        }
        self._TIME_RESOLUTION_ID_TABLE = {
            'y': 'yr',
            'yr': 'yr',
            'year': 'yr',
            'yearly': 'yr',
            'm': 'mon',
            'mon': 'mon',
            'month': 'mon',
            'monthly': 'mon',
            'd': 'day',
            'day': 'day',
            'daily': 'day'
        }
        self._TIME_RESOLUTIONS_CANONICAL_ORDER = ['day', 'mon', 'yr']
        # self._ATTRIBUTE_INDEX_TO_ATTRIBUTE_ID = {
        #     0: CLIMATE_INDEX_KEY_NAME,
        #     1: SCENARIO_KEY_NAME,
        #     2: MODEL_KEY_NAME,
        #     3: TIME_RESOLUTION_KEY_NAME,
        #     4: REALISATION_KEY_NAME,
        #     5: VERSION_KEY_NAME
        # }
        self._SORTING_FUNCTIONS_DICTIONARY_FOR_SEARCH_RESULT_ITEM = {
            CLIMATE_INDEX_KEY_NAME: operator.itemgetter(CLIMATE_INDEX_KEY_NAME),
            TIME_RESOLUTION_KEY_NAME:
                self._time_resolution_sorting_function_for_search_result_item,
            MODEL_KEY_NAME: operator.itemgetter(MODEL_KEY_NAME),
            SCENARIO_KEY_NAME: operator.itemgetter(SCENARIO_KEY_NAME),
            REALISATION_KEY_NAME:
                self._realisation_sorting_function_for_search_result_item,
            VERSION_KEY_NAME: operator.itemgetter(VERSION_KEY_NAME)
        }
        self._SORTING_FUNCTIONS_DICTIONARY_FOR_HITS_DICTIONARY = {
            CLIMATE_INDEX_KEY_NAME: operator.itemgetter(0),
            TIME_RESOLUTION_KEY_NAME:
                self._time_resolution_sorting_function_for_hits_dictionary,
            MODEL_KEY_NAME: operator.itemgetter(0),
            SCENARIO_KEY_NAME: operator.itemgetter(0),
            REALISATION_KEY_NAME: self._realisation_sorting_function_for_hits_dictionary,
            VERSION_KEY_NAME: operator.itemgetter(0)
        }
        if sort:
            items_to_use = self._sort_items(
                search_result_items,
                climate_index=(
                    Sort.ASCENDING,
                    _ATTRIBUTE_IDS_IN_CANONICAL_ORDER.index(CLIMATE_INDEX_KEY_NAME)
                ),
                time_resolution=(
                    Sort.ASCENDING,
                    _ATTRIBUTE_IDS_IN_CANONICAL_ORDER.index(TIME_RESOLUTION_KEY_NAME)
                ),
                model_name=(
                    Sort.ASCENDING,
                    _ATTRIBUTE_IDS_IN_CANONICAL_ORDER.index(MODEL_KEY_NAME)
                ),
                scenario=(
                    Sort.ASCENDING,
                    _ATTRIBUTE_IDS_IN_CANONICAL_ORDER.index(SCENARIO_KEY_NAME)
                ),
                realisation=(
                    Sort.ASCENDING,
                    _ATTRIBUTE_IDS_IN_CANONICAL_ORDER.index(REALISATION_KEY_NAME)
                ),
                version_id=(
                    Sort.ASCENDING,
                    _ATTRIBUTE_IDS_IN_CANONICAL_ORDER.index(VERSION_KEY_NAME)
                )
            )
        else:
            items_to_use = search_result_items
        self._items = items_to_use

        # Construct useful properties
        self._hits_dictionary = OrderedDict([])
        for attribute_id in _ATTRIBUTE_IDS_IN_CANONICAL_ORDER:
            self._hits_dictionary[attribute_id] = OrderedDict([])
        self._climate_indices = set([])
        self._time_resolutions = set([])
        self._models = set([])
        self._scenarios = set([])
        self._realisations = set([])
        self._versions = set([])
        sets_dictionary = {
            CLIMATE_INDEX_KEY_NAME: self._climate_indices,
            TIME_RESOLUTION_KEY_NAME: self._time_resolutions,
            MODEL_KEY_NAME: self._models,
            SCENARIO_KEY_NAME: self._scenarios,
            REALISATION_KEY_NAME: self._realisations,
            VERSION_KEY_NAME: self._versions
        }
        for item in self._items:
            for attribute_id in self._hits_dictionary.keys():
                self._build_attribute_sets_and_hits_dictionary(
                    attribute_id, item, sets_dictionary[attribute_id],
                    self._hits_dictionary
                )
        # In the hits dictionary, sort the results for each attribute
        sorted_hits_dictionary = OrderedDict([])
        for attribute_id, sub_hits_dictionary in self._hits_dictionary.items():
            sorting_function = \
                self._get_sorting_function_for_attribute_for_hits_dictionary(
                    attribute_id)
            sorted_sub_hits_dictionary = OrderedDict(
                sorted(sub_hits_dictionary.items(), key=sorting_function)
            )
            sorted_hits_dictionary[attribute_id] = sorted_sub_hits_dictionary
        self._hits_dictionary = sorted_hits_dictionary
        # What is displayed to screen when displaying this object
        self._representation = "{"+'"Hits"'+ ": {}".format(str( json.dumps(self._hits_dictionary, indent=0)))+"}"
        #self._representation = "No. items: {0}.\nHits: {1}".format(
            #len(self._items), json.dumps(self._hits_dictionary, indent=4))
        #self._representation = {"No. items": len(self._items),"Hits":json.dumps(self._hits_dictionary, indent=4)}
        self._as_string = self._representation
        return

    def __getitem__(self, item):
        return self._items[item]

    def __repr__(self):
        return self._representation

    def __str__(self):
        return self._as_string

    @property
    def items(self):
        return self._items

    @property
    def hits_dictionary(self):
        return self._hits_dictionary

    def _build_attribute_sets_and_hits_dictionary(self, attribute_name, item,
                                                  set_for_attribute, hits_dictionary):
        attribute_value = item[attribute_name]
        if attribute_value not in set_for_attribute:
            hits_dictionary[attribute_name][attribute_value] = 1
            set_for_attribute.add(attribute_value)
        else:
            hits_dictionary[attribute_name][attribute_value] += 1
        return

    def __len__(self):
        return len(self._items)

    @property
    def climate_indices(self):
        return self._climate_indices

    @property
    def time_resolutions(self):
        return self._time_resolutions

    @property
    def models(self):
        return self._models

    @property
    def scenarios(self):
        return self._scenarios

    @property
    def realisations(self):
        return self._realisations

    @property
    def versions(self):
        return self._versions

    def _get_key_by_value(self, value, dictionary):
        return dictionary.keys()[dictionary.values().index(value)]

    def _resolve_scenario_name(self, scenario):
        if scenario.lower() not in self._SCENARIO_FOLDER_NAMES_DICT:
            raise ValueError(
                '"{0}" is not a valid scenario. Valid scenarios are: {1}.'.format(
                    scenario, set(self._SCENARIO_FOLDER_NAMES_DICT.values())
                )
            )
        else:
            return self._SCENARIO_FOLDER_NAMES_DICT[scenario.lower()]

    def _resolve_time_resolution(self, time_resolution):
        try:
            time_resolution_id = self._TIME_RESOLUTION_ID_TABLE[time_resolution.lower()]
        except KeyError:
            raise ValueError(
                ('"{0}" is an invalid time resolution specification. Use one of the '
                 'following: "{1}"').format(time_resolution,
                                            set(self._TIME_RESOLUTION_ID_TABLE.values()))
            )
        return time_resolution_id

    def filtered(self, climate_indices=None, time_resolutions=None, model_names=None,
                 scenarios=None, realisations=None, version_ids=None):
        # Allow the user to input a single string rather than a list
        if isinstance(climate_indices, str):
            climate_indices_to_use = [climate_indices]
        else:
            climate_indices_to_use = climate_indices
        if isinstance(time_resolutions, str):
            time_resolutions_to_use = [time_resolutions]
        else:
            time_resolutions_to_use = time_resolutions
        if isinstance(model_names, str):
            model_names_to_use = [model_names]
        else:
            model_names_to_use = model_names
        if isinstance(scenarios, str):
            scenarios_to_use = [scenarios]
        else:
            scenarios_to_use = scenarios
        if isinstance(realisations, str):
            realisations_to_use = [realisations]
        else:
            realisations_to_use = realisations
        if isinstance(version_ids, str):
            version_ids_to_use = [version_ids]
        else:
            version_ids_to_use = version_ids

        # Convert input filters to the "resolved" name, where relevant.
        if scenarios is None:
            resolved_scenario_names = None
        else:
            resolved_scenario_names = set([])
            for scenario in scenarios_to_use:
                resolved_scenario_name = self._resolve_scenario_name(scenario)
                resolved_scenario_names.add(resolved_scenario_name)
            resolved_scenario_names = list(resolved_scenario_names)
        if time_resolutions_to_use is None:
            resolved_time_resolutions = None
        else:
            resolved_time_resolutions = set([])
            for time_resolution in time_resolutions_to_use:
                resolved_time_resolution = self._resolve_time_resolution(time_resolution)
                resolved_time_resolutions.add(resolved_time_resolution)
            resolved_time_resolutions = list(resolved_time_resolutions)

        # Filter the list
        filtered_items = []
        for item in self._items:
            if (climate_indices_to_use is None or item[CLIMATE_INDEX_KEY_NAME] in
                climate_indices_to_use) and \
                    (resolved_time_resolutions is None or item[TIME_RESOLUTION_KEY_NAME]
                    in resolved_time_resolutions) and \
                    (model_names_to_use is None or item[MODEL_KEY_NAME] in
                     model_names_to_use) and \
                    (resolved_scenario_names is None or item[SCENARIO_KEY_NAME] in
                     resolved_scenario_names) and \
                    (realisations_to_use is None or item[REALISATION_KEY_NAME] in
                     realisations_to_use) and \
                    (version_ids_to_use is None or item[VERSION_KEY_NAME] in
                     version_ids_to_use):
                filtered_items.append(item)

        filtered_search_result = LocalFolderETCCDIDatabaseSearchResult(filtered_items,
                                                                       sort=False)
        return filtered_search_result

    def _sort_items(self, items, climate_index=None, time_resolution=None,
                    model_name=None, scenario=None, realisation=None, version_id=None):
        """
                Can sort by any of the attributes by specifying "ascending" or "descending" for
                that attribute. If sorting by more than one, you need to enter a tuple or list,
                where the first item is "ascending" or "descending" and the second is a number
                determining its place in the sorting order. E.g.
                    climate_indices=("ascending", 2), version_ids=("descending", 1)
                will sort by version id in descending order first, then by climate index in
                ascending order.
                Parameters
                ----------
                climate_indices
                time_resolutions
                model_names
                scenarios
                realisations
                version_ids

                Returns
                -------

                """
        # Build a list that specifies how to sort the search result
        # First, count how many are not None
        sorting_specifications = {
            CLIMATE_INDEX_KEY_NAME: climate_index,
            SCENARIO_KEY_NAME: scenario,
            MODEL_KEY_NAME: model_name,
            TIME_RESOLUTION_KEY_NAME: time_resolution,
            REALISATION_KEY_NAME: realisation,
            VERSION_KEY_NAME: version_id
        }
        active_sort_attribute_ids = []
        for attribute_id, attribute_sorting_specification in \
                sorting_specifications.items():
            if attribute_sorting_specification is not None:
                active_sort_attribute_ids.append(attribute_id)

        if len(active_sort_attribute_ids) == 0:
            sorted_items = items
        elif len(active_sort_attribute_ids) == 1:
            attribute_id = active_sort_attribute_ids[0]
            # Put in a dummy sorting order position so that we can use the same function
            # as for when there are multiple attributes to sort by
            attribute_sorting_specification = sorting_specifications[attribute_id]
            if not (attribute_sorting_specification == Sort.ASCENDING or
                    attribute_sorting_specification == Sort.DESCENDING):
                raise ValueError(("Unrecognised sort direction '{0}' specified for "
                                  "{1}.").format(attribute_sorting_specification,
                                                 attribute_id))
            new_attribute_sorting_specification = tuple(
                [attribute_sorting_specification, 0])
            sorting_specifications[attribute_id] = new_attribute_sorting_specification
            sorted_items = self._sort_using_specifications(
                sorting_specifications, active_sort_attribute_ids, items)
        else:
            # Sorting by more than one attribute
            sorted_items = self._sort_using_specifications(
                sorting_specifications, active_sort_attribute_ids, items)
        return sorted_items

    def _sort_using_specifications(self, sorting_specifications,
                                   active_sort_attribute_ids, items):
        # First, get a list containing just the active attributes from the sorting
        # specification
        active_attribute_sorting_specifications = []
        sorting_order_positions = set([])
        for sort_attribute_id in active_sort_attribute_ids:
            sorting_attribute_specification = sorting_specifications[
                sort_attribute_id]
            sort_direction = sorting_attribute_specification[0]
            sorting_order_position = sorting_attribute_specification[1]
            # Check that this is the specification is in the correct form
            if not (len(sorting_attribute_specification) == 2 and
                    (sort_direction == Sort.ASCENDING or
                     sort_direction == Sort.DESCENDING) and
                    isinstance(sorting_order_position, int)):
                raise ValueError(
                    ("When sorting by more than one attribute, each sorting "
                     "specification must be of the form (`sort direction`, "
                     "`sorting order position`). The specification '{0}' for {1} "
                     "does not fit this form. See the documentation of this "
                     "function for more information.").format(
                        sorting_attribute_specification, sort_attribute_id
                    )
                )
            # Check that no sorting order position has been specified more than once
            if sorting_order_position in sorting_order_positions:
                raise ValueError(
                    ("The sorting order position {0} specified for {1} has been "
                     "specified for another attribute. Each sorting order position "
                     "must be unique.".format(sorting_order_position,
                                              sort_attribute_id))
                )
            sorting_order_positions.add(sorting_order_position)
            # Add the attribute_id to the specification
            extended_sorting_attribute_specification = (
                sort_attribute_id, sort_direction, sorting_order_position
            )
            active_attribute_sorting_specifications.append(
                extended_sorting_attribute_specification)
        # Now, sort this list by sorting order position, in reverse order
        reverse_sorted_active_attribute_sorting_specifications = sorted(
            active_attribute_sorting_specifications, key=operator.itemgetter(2),
            reverse=True
        )
        # We achieve the desired multi-attribute sort by sorting from the lowest
        # attribute to the highest in the sorting order.
        sorted_items = items
        for extended_sorting_attribute_specification in \
                reverse_sorted_active_attribute_sorting_specifications:
            attribute_id = extended_sorting_attribute_specification[0]
            sort_direction = extended_sorting_attribute_specification[1]
            if sort_direction == Sort.ASCENDING:
                reverse = False
            elif sort_direction == Sort.DESCENDING:
                reverse = True
            else:
                raise ValueError(
                    "Unrecognised sort direction '{0}' specified for {1}.".format(
                        extended_sorting_attribute_specification, attribute_id
                    )
                )
            sorting_function_for_attribute = \
                self._get_sorting_function_for_attribute_for_search_result_item(
                    attribute_id)
            sorted_items = sorted(sorted_items, key=sorting_function_for_attribute,
                                  reverse=reverse)
        return sorted_items

    def sorted(self, climate_index=None, time_resolution=None, model_name=None,
               scenario=None, realisation=None, version_id=None):
        """
        Can sort by any of the attributes by specifying "ascending" or "descending" for
        that attribute. If sorting by more than one, you need to enter a tuple or list,
        where the first item is "ascending" or "descending" and the second is a number
        determining its place in the sorting order. E.g.
            climate_indices=("ascending", 2), version_ids=("descending", 1)
        will sort by version id in descending order first, then by climate index in
        ascending order.
        Parameters
        ----------
        climate_indices
        time_resolutions
        model_names
        scenarios
        realisations
        version_ids

        Returns
        -------

        """
        sorted_items = self._sort_items(self._items, climate_index=climate_index,
                                        time_resolution=time_resolution,
                                        model_name=model_name, scenario=scenario,
                                        realisation=realisation, version_id=version_id)
        sorted_search_result = LocalFolderETCCDIDatabaseSearchResult(sorted_items,
                                                                     sort=False)
        return sorted_search_result

    def _get_sorting_function_for_attribute_for_search_result_item(self, attribute_id):
        return self._SORTING_FUNCTIONS_DICTIONARY_FOR_SEARCH_RESULT_ITEM[attribute_id]

    def _get_sorting_function_for_attribute_for_hits_dictionary(self, attribute_id):
        return self._SORTING_FUNCTIONS_DICTIONARY_FOR_HITS_DICTIONARY[attribute_id]

    def _time_resolution_sorting_function_for_search_result_item(self,
                                                                 search_result_item):
        time_resolution = search_result_item[TIME_RESOLUTION_KEY_NAME]
        position_in_canonical_order = self._time_resolution_sorting_function(
            time_resolution)
        return position_in_canonical_order

    def _time_resolution_sorting_function(self, time_resolution):
        position_in_canonical_order = self._TIME_RESOLUTIONS_CANONICAL_ORDER.index(
            time_resolution)
        return position_in_canonical_order

    def _time_resolution_sorting_function_for_hits_dictionary(self,
                                                              dictionary_entry_tuple):
        time_resolution = dictionary_entry_tuple[0]
        position_in_canonical_order = self._time_resolution_sorting_function(
            time_resolution)
        return position_in_canonical_order

    def _realisation_sorting_function_for_search_result_item(self, search_result_item):
        # We assume all realisation ids match the standard form. If we find the id
        # doesn't match, we will raise an error.
        realisation_id = search_result_item[REALISATION_KEY_NAME]
        parsed_realisation_id = self._realisation_sorting_function(realisation_id)
        return parsed_realisation_id

    def _realisation_sorting_function_for_hits_dictionary(self, dictionary_entry_tuple):
        # We assume all realisation ids match the standard form. If we find the id
        # doesn't match, we will raise an error.
        realisation_id = dictionary_entry_tuple[0]
        parsed_realisation_id = self._realisation_sorting_function(realisation_id)
        return parsed_realisation_id

    def _realisation_sorting_function(self, realisation_id):
        # We assume all realisation ids match the standard form. If we find the id
        # doesn't match, we will raise an error.
        # Parse the realisation id into a list of integers.
        # Use regex to match the expected form of: 'r[number]i[number]p[number]'
        # Copy the regex expression into https://regexr.com/ to get a breakdown
        # of how it works.
        if not re.match(r"\br\d+i\d+p\d+\b", realisation_id):
            raise ValueError(
                ("An alternative realisation id format was found ('{0}'), so cannot "
                 "sort it. The standard, expected format is "
                 "'r[number]i[number]p[number]'.").format(realisation_id)
            )
        # Split by both 'i' and 'p'
        parsed_realisation_id = re.split("[ip]", realisation_id[1:])
        # Convert strings to ints and make it a tuple
        parsed_realisation_id = tuple(int(value) for value in parsed_realisation_id)
        # Tuples will be sorted correctly by the sorted() function i.e. by the first
        # item, then the second, then the third etc.
        return parsed_realisation_id


class ExporterFactory:
    def __init__(self):
        self._netcdf_type_name = "netcdf"
        self._csv_type_name = "csv"

        self._exporter_dictionary = {
            self._netcdf_type_name: NetCDFExporter,
            self._csv_type_name: CSVExporter
        }
        self._exporter_file_extensions = {
            self._netcdf_type_name: {"nc"},
            self._csv_type_name: {"csv"}
        }
        return

    def get_exporter(self, exporter_type, **kwargs):
        """

        Parameters
        ----------
        exporter_type: basestring
             The type of the exporter

        Returns
        -------

        """
        return self._exporter_dictionary[exporter_type.lower()](**kwargs)

    def get_exporter_from_file_name(self, file_path, **kwargs):
        extension = os.path.splitext(file_path)[-1]
        # Remove the leading "." and make lowercase
        extension = extension[1:].lower()
        exporter_type = None
        for file_type, valid_extensions in self._exporter_file_extensions.iteritems():
            if extension in valid_extensions:
                exporter_type = file_type
                break
        if exporter_type is None:
            raise ValueError(("No matching exporter for this file type with extension "
                              "{0}.".format(extension)))
        else:
            return self.get_exporter(exporter_type, **kwargs)


class Exporter(utilities.CanLog):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        utilities.CanLog.__init__(self)
        return

    @abc.abstractmethod
    def export(self, climate_dataset, file_path):
        return


class NetCDFExporter(Exporter):
    def __init__(self, numeric_date_time_formatting=None, latitude_formatting=None,
                 longitude_formatting=None, value_formatting=None):
        Exporter.__init__(self)
        self._NUMERIC_DATE_TIMES_ID = NUMERIC_DATE_TIMES_ID
        self._LATITUDES_ID = LATITUDES_ID
        self._LONGITUDES_ID = LONGITUDES_ID
        self._VALUES_ID = VALUES_ID
        self._DEFAULT_NETCDF_EXPORT_OPTIONS = {
            self._NUMERIC_DATE_TIMES_ID: {"name": "time", "data-type": "f8",
                                          "units": "hours since 0001-01-01 00:00:00.0",
                                          "calendar": "gregorian"},
            self._LATITUDES_ID: {"name": "lat", "data-type": "f8"},
            self._LONGITUDES_ID: {"name": "lon", "data-type": "f8"},
            self._VALUES_ID: {"data-type": "f8"}
        }
        self._netcdf_export_options = self._DEFAULT_NETCDF_EXPORT_OPTIONS.copy()
        if numeric_date_time_formatting is not None:
            for option, value in numeric_date_time_formatting.iteritems():
                self._netcdf_export_options[self._NUMERIC_DATE_TIMES_ID][option] = value
        if latitude_formatting is not None:
            for option, value in latitude_formatting.iteritems():
                self._netcdf_export_options[self._LATITUDES_ID][option] = value
        if longitude_formatting is not None:
            for option, value in longitude_formatting.iteritems():
                self._netcdf_export_options[self._LONGITUDES_ID][option] = value
        if value_formatting is not None:
            for option, value in value_formatting.iteritems():
                self._netcdf_export_options[self._VALUES_ID][option] = value
        return

    def export(self, climate_dataset, file_path):
        netcdf_dataset = netCDF4.Dataset(file_path, "w")
        netcdf_value_dimensions = []
        netcdf_value_array_index_specification = []

        if climate_dataset.numeric_datetimes is not None:
            netcdf_dataset.createDimension(
                self._netcdf_export_options[self._NUMERIC_DATE_TIMES_ID]["name"], None
            )
            netcdf_numeric_datetimes = netcdf_dataset.createVariable(
                self._netcdf_export_options[self._NUMERIC_DATE_TIMES_ID]["name"],
                self._netcdf_export_options[self._NUMERIC_DATE_TIMES_ID]["data-type"],
                (self._netcdf_export_options[self._NUMERIC_DATE_TIMES_ID]["name"],)
            )
            netcdf_numeric_datetimes.units = self._netcdf_export_options[
                self._NUMERIC_DATE_TIMES_ID]["units"]
            netcdf_numeric_datetimes.calendar = self._netcdf_export_options[
                self._NUMERIC_DATE_TIMES_ID]["calendar"]
            netcdf_numeric_datetimes[:] = netCDF4.date2num(
                climate_dataset.datetimes[:], netcdf_numeric_datetimes.units,
                netcdf_numeric_datetimes.calendar
            )

            netcdf_value_dimensions.append(
                self._netcdf_export_options[self._NUMERIC_DATE_TIMES_ID]["name"])
            netcdf_value_array_index_specification.append(
                slice(len(climate_dataset.numeric_datetimes)))

        if climate_dataset.latitudes is not None:
            netcdf_dataset.createDimension(
                self._netcdf_export_options[self._LATITUDES_ID]["name"],
                len(climate_dataset.latitudes)
            )
            netcdf_latitudes = netcdf_dataset.createVariable(
                self._netcdf_export_options[self._LATITUDES_ID]["name"],
                self._netcdf_export_options[self._LATITUDES_ID]["data-type"],
                (self._netcdf_export_options[self._LATITUDES_ID]["name"],)
            )
            netcdf_latitudes[:] = climate_dataset.latitudes[:]

            netcdf_value_dimensions.append(
                self._netcdf_export_options[self._LATITUDES_ID]["name"])
            netcdf_value_array_index_specification.append(
                slice(len(climate_dataset.latitudes)))

        if climate_dataset.longitudes is not None:
            netcdf_dataset.createDimension(
                self._netcdf_export_options[self._LONGITUDES_ID]["name"],
                len(climate_dataset.longitudes)
            )
            netcdf_longitudes = netcdf_dataset.createVariable(
                self._netcdf_export_options[self._LONGITUDES_ID]["name"],
                self._netcdf_export_options[self._LONGITUDES_ID]["data-type"],
                (self._netcdf_export_options[self._LONGITUDES_ID]["name"],)
            )
            netcdf_longitudes[:] = climate_dataset.longitudes[:]

            netcdf_value_dimensions.append(
                self._netcdf_export_options[self._LONGITUDES_ID]["name"])
            netcdf_value_array_index_specification.append(
                slice(len(climate_dataset.longitudes)))

        netcdf_values = netcdf_dataset.createVariable(
            climate_dataset.climate_variable_name,
            self._netcdf_export_options[self._VALUES_ID]["data-type"],
            tuple(netcdf_value_dimensions)
        )

        netcdf_values[netcdf_value_array_index_specification] = climate_dataset.values[:]

        netcdf_dataset.close()

        return file_path


class RotatedNetCDFExporter(NetCDFExporter):
    def __init__(self, numeric_date_time_formatting=None, latitude_formatting=None,
                 longitude_formatting=None, value_formatting=None):
        NetCDFExporter.__init__(self, numeric_date_time_formatting=None, latitude_formatting={"name": "rlat"},
                 longitude_formatting={"name": "rlon"}, value_formatting=None)

        return

    def export(self, climate_dataset, file_path):
        

        netcdf_dataset = netCDF4.Dataset(file_path, "w")
        print(file_path,"---------------")

        netcdf_value_dimensions = []
        netcdf_value_array_index_specification = []

        if climate_dataset.numeric_datetimes is not None:
            netcdf_dataset.createDimension(
                self._netcdf_export_options[self._NUMERIC_DATE_TIMES_ID]["name"], None
            )
            netcdf_numeric_datetimes = netcdf_dataset.createVariable(
                self._netcdf_export_options[self._NUMERIC_DATE_TIMES_ID]["name"],
                self._netcdf_export_options[self._NUMERIC_DATE_TIMES_ID]["data-type"],
                (self._netcdf_export_options[self._NUMERIC_DATE_TIMES_ID]["name"],)
            )
            netcdf_numeric_datetimes.units = self._netcdf_export_options[
                self._NUMERIC_DATE_TIMES_ID]["units"]
            netcdf_numeric_datetimes.calendar = self._netcdf_export_options[
                self._NUMERIC_DATE_TIMES_ID]["calendar"]
            netcdf_numeric_datetimes[:] = netCDF4.date2num(
                climate_dataset.datetimes[:], netcdf_numeric_datetimes.units,
                netcdf_numeric_datetimes.calendar
            )

            netcdf_value_dimensions.append(
                self._netcdf_export_options[self._NUMERIC_DATE_TIMES_ID]["name"])
            netcdf_value_array_index_specification.append(
                slice(len(climate_dataset.numeric_datetimes)))

        if climate_dataset.latitudes is not None:
            netcdf_dataset.createDimension(
                self._netcdf_export_options[self._LATITUDES_ID]["name"],
                len(climate_dataset.latitudes)
            )
            netcdf_latitudes = netcdf_dataset.createVariable(
                self._netcdf_export_options[self._LATITUDES_ID]["name"],
                self._netcdf_export_options[self._LATITUDES_ID]["data-type"],
                (self._netcdf_export_options[self._LATITUDES_ID]["name"],)
            )
            netcdf_latitudes[:] = climate_dataset.latitudes[:]

            netcdf_value_dimensions.append(
                self._netcdf_export_options[self._LATITUDES_ID]["name"])
            netcdf_value_array_index_specification.append(
                slice(len(climate_dataset.latitudes)))

        if climate_dataset.longitudes is not None:
            netcdf_dataset.createDimension(
                self._netcdf_export_options[self._LONGITUDES_ID]["name"],
                len(climate_dataset.longitudes)
            )
            netcdf_longitudes = netcdf_dataset.createVariable(
                self._netcdf_export_options[self._LONGITUDES_ID]["name"],
                self._netcdf_export_options[self._LONGITUDES_ID]["data-type"],
                (self._netcdf_export_options[self._LONGITUDES_ID]["name"],)
            )
            netcdf_longitudes[:] = climate_dataset.longitudes[:]

            netcdf_value_dimensions.append(
                self._netcdf_export_options[self._LONGITUDES_ID]["name"])
            netcdf_value_array_index_specification.append(
                slice(len(climate_dataset.longitudes)))

        netcdf_values = netcdf_dataset.createVariable(
            climate_dataset.climate_variable_name,
            self._netcdf_export_options[self._VALUES_ID]["data-type"],
            tuple(netcdf_value_dimensions)
        )

        netcdf_values[netcdf_value_array_index_specification] = climate_dataset.values[:]


#CP! next try to add in all the NetCDF lon and lat, even if it's for a bigger grid
        if climate_dataset._netcdf_exact_latitudes is not None and \
            climate_dataset._netcdf_exact_longitudes is not None:

            rlon_range = climate_dataset.longitudes._subset_index_specification[0]
            rlat_range = climate_dataset.latitudes._subset_index_specification[0]

           # netcdf_all_rlongitudes[:] = climate_dataset._netcdf_longitudes[:]

            netcdf_exact_latitudes = netcdf_dataset.createVariable(
                'lat','f8', ("rlat", "rlon")
            )
            netcdf_exact_latitudes[:] = climate_dataset._netcdf_exact_latitudes[rlat_range[0]:rlat_range[1], rlon_range[0]:rlon_range[1]]

            netcdf_exact_longitudes = netcdf_dataset.createVariable(
                'lon','f8', ("rlat", "rlon")
            )
            netcdf_exact_longitudes[:] = climate_dataset._netcdf_exact_longitudes[rlat_range[0]:rlat_range[1], rlon_range[0]:rlon_range[1]]

            netcdf_rotated_pole = netcdf_dataset.createVariable(
                'rotated_latitude_longitude','int'
            )
            netcdf_rotated_pole.grid_north_pole_longitude = climate_dataset._rotated_grid_north_pole_longitude
            netcdf_rotated_pole.grid_north_pole_latitude  = climate_dataset._rotated_grid_north_pole_latitude

            #this is the bit that allows panalopy to know about lat and lon when plotting
            netcdf_values.coordinates = "lat lon"

        netcdf_dataset.close()

        return file_path


class CSVExporter(Exporter):
    ''' Simple CSV export Lat, Long, Data for one time.
    Could extend to output timeseries for a Lat, Long location.'''
    def __init__(self):
        Exporter.__init__(self)
        return

    def export(self, climate_dataset, file_path):

#add check that has 1 data-time, and more than one lat and longitude
     #   if climate_dataset._netcdf_exact_latitudes is not None and \
     #       climate_dataset._netcdf_exact_longitudes is not None:
        #if isinstance(climate_dataset, climate_projections.data.NetCDFDatasetRotated):
     #       output_latitudes = climate_dataset._netcdf_exact_latitudes
     #       output_longitudes = climate_dataset._netcdf_exact_longitudes
     #   else:
      #      output_latitudes = climate_dataset.latitudes
      #      output_longitudes = climate_dataset.longitudes

        output_latitudes = climate_dataset.latitudes
        output_longitudes = climate_dataset.longitudes

        with open(file_path, "w") as csv_dataset:
            csv_dataset_writer = csv.writer(csv_dataset)
            csv_dataset_writer.writerow(
                ['latitude', 'longitude', climate_dataset.climate_variable_name + '_anomoly'])

            for ilat, lat in enumerate(output_latitudes):
                for ilon, lon in enumerate(output_longitudes):
                    csv_dataset_writer.writerow([lat, lon, climate_dataset.values[ilat, ilon]])

        return file_path

class RotatedCSVExporter(Exporter):
    ''' Simple CSV export Lat, Long, Data for one time.
    Could extend to output timeseries for a Lat, Long location.'''
    #TODO: Make a common CSV exporter that is intelligent and can identify if it's rotated or not
    #      e.g. use if isinstance(test, data.NetCDFDatasetRotated):...
    #      Issue was I couldn't see what the class should be when in "io".

    def __init__(self):
        Exporter.__init__(self)
        return

    def export(self, climate_dataset, file_path):

# TODO: add check that has 1 data-time, and more than one lat and longitude
     #   if climate_dataset._netcdf_exact_latitudes is not None and \
     #       climate_dataset._netcdf_exact_longitudes is not None:

        rlat_range = climate_dataset.latitudes._subset_index_specification[0]
        rlat_length = rlat_range[1] - rlat_range[0]
        rlon_range = climate_dataset.longitudes._subset_index_specification[0]
        rlon_length = rlon_range[1] - rlon_range[0]

        exact_latitudes = climate_dataset._netcdf_exact_latitudes
        exact_longitudes = climate_dataset._netcdf_exact_longitudes

        with open(file_path, "w") as csv_dataset:
            csv_dataset_writer = csv.writer(csv_dataset)
            csv_dataset_writer.writerow(
                ['latitude', 'longitude', climate_dataset.climate_variable_name + '_anomoly'])

            for ilat in range(rlat_length):
                for ilon in range(rlon_length):
                    csv_dataset_writer.writerow(
                        [exact_latitudes[rlat_range[0] + ilat, rlon_range[0] + ilon],
                         exact_longitudes[rlat_range[0] + ilat, rlon_range[0] + ilon],
                         climate_dataset.values[ilat, ilon]])

        return file_path
