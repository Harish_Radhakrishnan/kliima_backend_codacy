import re
import boto3
from joblib import Parallel, delayed
import s3fs
import xarray as xr
from io import StringIO 
import requests
import numpy as np
import pandas as pd 
import string
import random
from collections import Counter
import os
import copy
import shutil
from django.conf import settings
from .climateprojections.tool_master_cloud_test import run_tool_master
from dataApp import logger
from arup.utils import LogUtils

def file_process(string,key):
    """ 
    process the file names
    """
    string = string.split("/")
    string = string[-1].split("_")
    string.remove(key)
    string.pop()
    string = "_".join(string)
    return string

def files_eliminating(files):
    """ 
    eliminating the models that doesn't
    contains historical, rcp45, rcp85
    """
    file_keys = ["historical","rcp45","rcp85"]
    keys = []
    length = np.array([len(files['historical']),
                       len(files['rcp45']),len(files['rcp85'])])
    for i in range(0,3):
        max_ = np.argmax(length)
        keys.append(file_keys[max_])
        length = np.delete(length,max_)
        file_keys.remove(file_keys[max_])
    new_files = {"historical":[],'rcp45':[],'rcp85':[]}
    for first_file in files[keys[0]]:
        present = False
        first_check = file_process(first_file,keys[0])
        if len(files[keys[1]]) < len(files[keys[2]]):
            diff = len(files[keys[2]]) - len(files[keys[1]])
            dummy = ["a/b/a_b_{}_c_2010".format(keys[1])]*diff
            files[keys[1]].extend(dummy)
        for second_file,third_file in zip(files[keys[1]],files[keys[2]]):
            second_check = file_process(second_file,keys[1])
            third_check = file_process(third_file,keys[2])
            if first_check == second_check:
                new_files[keys[1]].append(second_file)
                present = True
            if first_check == third_check:
                new_files[keys[2]].append(third_file)
                present =True
            else:
                pass
        if present == True:
            new_files[keys[0]].append(first_file)
        else:
            pass
    return new_files

def getting_all_model_files(temp_file_data22, temp_file_data):
    """ 
    utils for filtering the files based on 
    models
    """
    all_model_file=[]
    for model_check_split in temp_file_data22:
        model_list_split=model_check_split.split("_")
        temp_file_data1=[]
        for required_region in temp_file_data:
            if model_list_split[0]  in required_region and model_list_split[1]  in required_region and model_list_split[2]  in required_region and model_list_split[3]  in required_region:
                temp_file_data1.append(required_region)
                all_model_file.extend(temp_file_data1)
            else:
                pass
    return all_model_file

def file_filter(user_region_,user_var_,history_from,
                history_to,future_from,future_to,check_value,
                model,institute,rcm):
    """ 
    filter the required files
    """
    dicts={"Africa":"AFR","Antarctica":"ANT",
            "Arctic":"ARC","Australasia":"AUS",
            "Central America":"CAM","Central Asia":"CAS",
            "East Asia":"EAS","Europe":"EUR",
            "Mediterranean":"MED","Middle East North Africa":"MNA",
            "North America":"NAM","South America":"SAM",
            "South Asia":"WAS","South East Asia":"SEA"}
    user_region=dicts.get(user_region_)
    temp_user_var=user_var_[0]
    var_dicts={"Maximum temperature":"tasmax",
                "Temperature threshold more than":"tasmax",
                "Temperature threshold less than":"tasmin",
                "Average daily temperature":"tas",
                "Average daily maximum temperature":"tasmax",
                "Average daily minimum temperature":"tasmin",
                "Daily max gust":"wsgsmax",
                "Gust threshold more than":"wsgsmax",
                "Wind threshold more than":"sfcWindmax",
                "Daily average wind speed":"sfcWindmax",
                "Specfic humidity":"huss","Relative humidity":"hurs",
                "Snow depth":"snd","Sea level Rise":"snd",
                "Snow threshold more than":"snd",
                "Average daily precipitation":"pr",
                "Precipitation threshold more than":"pr",
                "Precipitation threshold less than":"pr",
                "Maximum 1 day rainfall":"pr","Maximum 5 day rainfall":"pr",
                "Cumulative dry days":"pr"}
    user_var=var_dicts.get(user_var_[0]) 
    file_path=user_region+"/"+user_var
    s3 = boto3.resource('s3')
    my_bucket = s3.Bucket('klima-datasets')
    filter_file=[]
    for object_summary in my_bucket.objects.filter(Prefix="Cordex"+"/"+ file_path+"/"):
        #Cordex/NAM/tasmax
        list_objects=object_summary.key
        filter_file.append(list_objects)
    temp_file_data=[]
    for required_region in filter_file:
        if (check_value in required_region) and ("r1i1p1" in required_region):
            temp_file_data.append(required_region)
        else:
            pass
    if model != None and institute != None and rcm != None:
        all_model_file = []
        for file_name in temp_file_data:
            file_name = re.findall(r"(?=.*{})(?=.*{})(?=.*{}).*".format(model,institute,rcm),file_name)
            if file_name != []:
                all_model_file.append(file_name[0])
    else: 
        if user_region == "NAM":
            temp_file_data22=[
                                "NAM-44_CCCma-CanESM2_CCCma_CanRCM4",
                                "NAM-44_CCCma-CanESM2_SMHI_RCA4",
                                "NAM-44_CCCma-CanESM2_UQAM_CRCM5",
                                "NAM-44_MPI-M-MPI-ESM-LR_UQAM_CRCM5",
                                "NAM-44_MPI-M-MPI-ESM-MR_UQAM_CRCM5",
                                "NAM-22_CCCma_CCCma-CanESM2_CanRCM4",
                                "NAM-22_OURANOS_MPI-M-MPI-ESM-LR_CRCM5",
                                "NAM-22_OURANOS_CNRM-CERFACS-CNRM-CM5_CRCM5",
                                "NAM-22_OURANOS_CCCma-CanESM2_CRCM5",
                                "NAM-22_OURANOS_NOAA-GFDL-GFDL-ESM2M_CRCM5",
                                "NAM-22_UQAM_CCCma-CanESM2_CRCM5",
                                "NAM-22_UQAM_MPI-M-MPI-ESM-LR_CRCM5",
                                "NAM-22_MPI-M-MPI-ESM-MR_UQAM_CRCM5",
                                "NAM-22_MOHC-HadGEM2-ES_GERICS_REMO2015",
                                "NAM-22_MPI-M-MPI-ESM-LR_GERICS_REMO2015",
                                "NAM-22_NCC-NorESM1-M_GERICS_REMO2015"
                            ]

            all_model_file = getting_all_model_files(temp_file_data22, temp_file_data)
            print("all model ******************************** *************************",all_model_file)
        elif user_region == "AFR":
            temp_file_data22=[
                            "AFR-22_CCCma-CanESM2_CCCma_CanRCM4",
                            "AFR-22_MOHC-HadGEM2-ES_GERICS_REMO2015",
                            "AFR-22_MPI-M-MPI-ESM-LR_GERICS_REMO2015",
                            "AFR-22_NCC-NorESM1-M_GERICS_REMO2015",
                            "AFR-44_CCCma-CanESM2_CCCma_CanRCM4",
                            "AFR-44_CNRM-CERFACS-CNRM-CM5_CLMcom_CCLM4-8-17",
                            "AFR-44_MOHC-HadGEM2-ES_CLMcom_CCLM4-8-17",
                            "AFR-44_MPI-M-MPI-ESM-LR_CLMcom_CCLM4-8-17",
                            "AFR-44_IPSL-IPSL-CM5A-LR_GERICS_REMO2009",
                            "AFR-44_MIROC-MIROC5_GERICS_REMO2009",
                            "AFR-44_MOHC-HadGEM2-ES_GERICS_REMO2009",
                            "AFR-44_MPI-M-MPI-ESM-LR_MPI-CSC_REMO2009",
                            "AFR-44_CCCma-CanESM2_SMHI_RCA4",
                            "AFR-44_CNRM-CERFACS-CNRM-CM5_SMHI_RCA4",
                            "AFR-44_CSIRO-QCCCE-CSIRO-Mk3-6-0_SMHI_RCA4",
                            "AFR-44_ICHEC-EC-EARTH_SMHI_RCA4",
                            "AFR-44_IPSL-IPSL-CM5A-MR_SMHI_RCA4",
                            "AFR-44_MIROC-MIROC5_SMHI_RCA4",
                            "AFR-44_MOHC-HadGEM2-ES_SMHI_RCA4",
                            "AFR-44_MPI-M-MPI-ESM-LR_SMHI_RCA4",
                            "AFR-44_NCC-NorESM1-M_SMHI_RCA4",
                            "AFR-44_NOAA-GFDL-GFDL-ESM2M_SMHI_RCA4"
                            ]
            # Iterative approch
            all_model_file = getting_all_model_files(temp_file_data22, temp_file_data)
        elif user_region == "EAS":
            temp_file_data22=[
                        "EAS-44_CLMcom_CNRM-CERFACS-CNRM-CM5_CCLM5-0-2",
                        "EAS-44_CLMcom_MOHC-HadGEM2-ES_CCLM5-0-2",
                        "EAS-44_CLMcom_MPI-M-MPI-ESM-LR_CCLM5-0-2",
                        "EAS-22_MOHC-HadGEM2-ES_GERICS_REMO2015",
                        "EAS-22_MPI-M-MPI-ESM-LR_GERICS_REMO2015",
                        "EAS-22_NCC-NorESM1-M_GERICS_REMO2015"
                    ]
            all_model_file = getting_all_model_files(temp_file_data22, temp_file_data)
        elif user_region == "AUS":
            temp_file_data22=[
                        "AUS-22_MOHC-HadGEM2-ES_CLMcom-HZG_CCLM5-0-15",
                        "AUS-22_MPI-M-MPI-ESM-LR_CLMcom-HZG_CCLM5-0-15",
                        "AUS-22_NCC-NorESM1-M_CLMcom-HZG_CCLM5-0-15",
                        "AUS-22_MOHC-HadGEM2-ES_GERICS_REMO2015",
                        "AUS-22_MPI-M-MPI-ESM-LR_GERICS_REMO2015",
                        "AUS-22_NCC-NorESM1-M_GERICS_REMO2015",
                        "AUS-44_MPI-M-MPI-ESM-LR_CLMcom_CCLM4-8-17-CLM3-5"]
            all_model_file = getting_all_model_files(temp_file_data22, temp_file_data)
        elif user_region == "SAM":
            temp_file_data22=[
                            "SAM-22_MOHC-HadGEM2-ES_GERICS_REMO2015",
                            "SAM-22_MPI-M-MPI-ESM-LR_GERICS_REMO2015",
                            "SAM-22_NCC-NorESM1-M_GERICS_REMO2015" ,
                            "SAM-44_MPI-M-MPI-ESM-LR_MPI-CSC_REMO2009",
                            "SAM-44_CCCma-CanESM2_SMHI_RCA4",
                            "SAM-44_CNRM-CERFACS-CNRM-CM5_SMHI_RCA4",
                            "SAM-44_CSIRO-QCCCE-CSIRO-Mk3-6-0_SMHI_RCA4",
                            "SAM-44_IPSL-IPSL-CM5A-MR_SMHI_RCA4",
                            "SAM-44_MIROC-MIROC5_SMHI_RCA4",
                            "SAM-44_MOHC-HadGEM2-ES_SMHI_RCA4",
                            "SAM-44_MPI-M-MPI-ESM-LR_SMHI_RCA4",
                            "SAM-44_NCC-NorESM1-M_SMHI_RCA4",
                            "SAM-44_NOAA-GFDL-GFDL-ESM2M_SMHI_RCA4"
                        ]
            all_model_file = getting_all_model_files(temp_file_data22, temp_file_data)
        elif user_region == "SEA":
            temp_file_data22=[
                        "SEA-22_MOHC-HadGEM2-ES_GERICS_REMO2015",
                        "SEA-22_MPI-M-MPI-ESM-LR_GERICS_REMO2015",
                        "SEA-22_NCC-NorESM1-M_GERICS_REMO2015",
                        "SEA-22_MOHC-HadGEM2-ES_SMHI_RCA4"
                    ]
            all_model_file = getting_all_model_files(temp_file_data22, temp_file_data)
        else:
            all_model_file=temp_file_data
####################
# Zero Byte File length checking
    # file_data=[]
    # for files_s3 in all_model_file:
    #     s3 = s3fs.S3FileSystem(anon=False)
    #     val=boto3.resource('s3').Bucket('klima-datasets').Object(files_s3).content_length
    #     if val !=0:
    #         s3path = 's3://klima-datasets/'+files_s3
    #         file_data.append(files_s3)
    #     else:
    #         pass
    file_data= all_model_file
    print(all_model_file , 'All model file details')
    file_format_split={}
    all_experiment=["historical", "rcp45", "rcp85"]
    for exp_class in all_experiment:
        temp_files=[]
        for file_classify in file_data:
            if exp_class in file_classify:
                file_drops=file_classify.replace(".nc","")
                year_seperate=file_drops[-17:]
                year_list=year_seperate.split("-")
                temp=[]
                for divide_year in year_list:
                    temp_value=divide_year[:-4]
                    temp.append(temp_value)
                for i in range(0, len(temp)):
                    temp[i] = int(temp[i])
                temp = list(range(temp[0],temp[1]+1))  
                if exp_class=="historical":
                    range_val= list(range(history_from,history_to+1))
                else:
                    range_val= list(range(future_from,future_to+1))
                  #  print(range_val,"rcp value")
               # print(temp,"temp")
                difference=[]
                for year_value in temp:
                    if year_value in range_val:
                        difference.append(year_value)
                    else:
                        pass
                if len(difference) !=0:
                    temp_files.append(file_classify)
                else:
                    pass
        file_format_split[exp_class]=temp_files
    return file_format_split

def download_files_from_s3(user_region_,user_var_,history_from,
                           history_to,future_from,future_to,check_value,
                           model,institute,rcm, request):
    """ 
    download the filtered files from s3
    """
    dicts={"Africa":"AFR","Antarctica":"ANT",
            "Arctic":"ARC","Australasia":"AUS",
            "Central America":"CAM","Central Asia":"CAS",
            "East Asia":"EAS","Europe":"EUR",
            "Mediterranean":"MED","Middle East North Africa":"MNA",
            "North America":"NAM","South America":"SAM",
            "South Asia":"WAS","South East Asia":"SEA"}
    user_region=dicts.get(user_region_)
    temp_user_var=user_var_[0]
    var_dicts={"Maximum temperature":"tasmax",
            "Temperature threshold more than":"tasmax",
            "Temperature threshold less than":"tasmin",
            "Average daily temperature":"tas",
            "Average daily maximum temperature":"tasmax",
            "Average daily minimum temperature":"tasmin",
            "Daily max gust":"wsgsmax",
            "Gust threshold more than":"wsgsmax",
            "Wind threshold more than":"sfcWindmax",
            "Daily average wind speed":"sfcWindmax",
            "Specfic humidity":"huss","Relative humidity":"hurs",
            "Snow depth":"snd","Sea level Rise":"snd",
            "Snow threshold more than":"snd",
            "Average daily precipitation":"pr",
            "Precipitation threshold more than":"pr",
            "Precipitation threshold less than":"pr",
            "Maximum 1 day rainfall":"pr","Maximum 5 day rainfall":"pr",
            "Cumulative dry days":"pr"}
    user_var=var_dicts.get(user_var_[0]) 
    file_path=user_var+"/"+"CORDEX"+"/"+user_region+"/"
    path= settings.CORDEX_FILE_PATH
    try:
        #os.rmdir(path)
        shutil.rmtree(path)
        os.makedirs(path+file_path)
    except:
        os.makedirs(path+file_path)
    #file_format_split={key: value for key, value in \
        # file_format_split.items() if len(value) !=0}
    file_format_split=file_filter(user_region_,user_var_,
                                  history_from,history_to,future_from,
                                  future_to,check_value,model,institute,rcm)
    #file_format_split = files_eliminating(file_format_split)
    file_format_split=dict( [(k,v) for k,v in file_format_split.items() \
                            if len(v)>0])
    file_check=file_format_split.keys()
    dummy_file_check=file_format_split.keys()
    status={}
    if "historical" in dummy_file_check:
        if len(dummy_file_check)>=1:
            for temp_file_check in file_check:
                list_of_files= file_format_split.get(temp_file_check) 
                for file_name in list_of_files:
                    s3 = boto3.client('s3')
                    temp_s3=file_name.split("/")
                    s3.download_file('klima-datasets', file_name, 
                                     path+file_path+temp_s3[-1])
                print(temp_file_check,"download done")
                logger.info(LogUtils.\
                    load_message(**request.log_utils, 
                                 message=f"{temp_file_check} download done"))
            status["info"]="all historical ,rcp45,rcp85 is present and downloaded"
            status["data"]=True
        else:
            status["info"]="only historical is present ,no rcp45 and rcp85"
            status["data"]=False
    else:
        status["info"]="No historical is present"
        status["data"]=False
    return status

def run_master(user_region,user_var,history_from,
               history_to,future_from,future_to,primary_key,
               threshold_value_less_than,threshold_value_more_than,
               lat,lon,model,institute,rcm,region_short,request):  
    if region_short == None:
        regions_dict={"Africa":["AFR-44"],
                    "Antarctica":["ANT-44"],
                    "Artic":["ARC-22","ARC-44"],
                    "Australasia":["AUS-22","AUS-44"],
                    "Central America":["CAM-22","CAM-44"],
                    "Central Asia":["CAS-22","CAS-44"],
                    "East Asia":["EAS-22","EAS-44"],
                    "Europe":["EUR-11","EUR-22","EUR-44"],
                    "Mediterranean":["MED-11"],
                    "Middle East North Africa":["MNA-22","MNA-44"],
                    "North America":["NAM-22","NAM-44"],
                    "South America":["SAM-20","SAM-22","SAM-44"],
                    "South Asia":["WAS-22","WAS-44"],
                    "South East Asia":["SEA-22"]}
    else:
        regions_dict={user_region:[region_short]}
    region_fill= regions_dict.get(user_region) 
    region_details=[]
    for check_value in region_fill:
        file_format_split=file_filter(user_region,user_var,history_from,history_to,future_from,future_to,check_value,model,institute,rcm)
        file_format_split=dict( [(k,v) for k,v in file_format_split.items() if len(v)>0])
        file_check=file_format_split.keys()
        if len(file_check)>=2:
            region_details.append(check_value)
    # Example : region_details = ["EUR-11","EUR-22"]
    status={"data":True}
    index = 0
    history_data = []
    display_data = []
    errors = []
    while index != len(region_details):
        output = []
        table_outputs = []
        check_value = region_details[index]
        status = download_files_from_s3(user_region,user_var,history_from,
                                        history_to,future_from,future_to,
                                        check_value,model,institute,rcm,
                                        request)
        if status["data"] == True:
            var_dicts={"Maximum temperature":"TXX",
            "Temperature threshold more than":"THRESHOLD MORE THAN",
            "Temperature threshold less than":"THRESHOLD LESS THAN",
            "Average daily temperature":"AVERAGE TEMPERATURE",
            "Average daily maximum temperature":"AVERAGE MAXIMUM TEMPERATURE",
            "Average daily minimum temperature":"AVERAGE MINIMUM TEMPERATURE",
            "Average daily precipitation":"AVERAGE PRECIPITATION",
            "Precipitation threshold more than":"AVERAGE PRECIPITATION",
            "Precipitation threshold less than":"AVERAGE PRECIPITATION",
            "Maximum 1 day rainfall":"RX1DAY",
            "Maximum 5 day rainfall":"RX5DAY",
            "Cumulative dry days":"CDD"}
            user_var_=var_dicts.get(user_var[0])
            user_region_= check_value.split("-")[0]
            output_format,table_output,error = run_tool_master(user_region_,
                                            user_var_,history_from,history_to,
                                            future_from,future_to,primary_key,
                                            threshold_value_less_than,
                                            threshold_value_more_than,lat,lon,
                                            check_value, request)
            for out,tab in zip(output_format,table_output):
                if out[list(out.keys())[0]][0] != "Variable not recognised":
                    output.append(out)
                    table_outputs.append(tab)
                else:
                    coord = list(out.keys())[0].split("/")
                    output.append({"{}/{}".\
                    format(float(coord[0]),
                    float(coord[1])):"This lat/lon is not present in Cordex"})
                    table_outputs.append(
                    {"{}/{}".format(float(coord[0]),float(coord[1])):\
                    {'rcp45': {'max': "N/A", 'min': "N/A", 
                               'mean': "N/A",'median':"N/A"}, 
                    'rcp85': {'max': "N/A", 'min': "N/A", 
                              'mean': "N/A",'median':"N/A"}}})
        history_data.append({"region":check_value,
                             "data":output})
        display_data.append({"region":check_value,
                             "data":table_outputs})
        errors.append({"region":check_value,
                       "error":error})
        index +=1    
    return history_data,display_data,errors
