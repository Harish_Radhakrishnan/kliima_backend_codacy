import s3fs
import xarray as xr
import glob
import sys
import numpy as np
import pandas as pd 
import string
import random
import boto3
from collections import Counter

class CordexNcSearchs():
    
    def __init__(self,list_of_files,user_lat,user_lon,user_var):
        self.list_of_files = list_of_files
        self.user_lat = user_lat
        self.user_lon = user_lon
        self.user_var = user_var
        
    def run(self):
        list_of_files = self.list_of_files
        size_of_list=len(list_of_files)
        dict_files={}
        while size_of_list !=0:
            initial_index=list_of_files[0]
            initial_index=initial_index[:-16]
            temp_list=[]
            temp_list.clear()
            for file_name in list_of_files:
                if initial_index in file_name:
                    temp_list.append(file_name)
            for delete_files in temp_list:
                list_of_files.remove(delete_files)
            res = ''.join(random.choices(string.ascii_uppercase +\
                            string.digits, k = 7))
            dict_files[res]= temp_list
            size_of_list=len(list_of_files)
        file_keys=dict_files.keys()
        all_dataframe=[]
        for values in file_keys:
            values_of_file=dict_files.get(values)
            file_data=[]
            for files_split in values_of_file:
                file_path=files_split.replace('_','/')
                file_path=file_path.replace('.nc','')
                s3 = s3fs.S3FileSystem(anon=False)
                val=boto3.resource('s3').\
                Bucket('arup-climate-global-656165081472-aws-config').\
                Object(file_path+'/'+files_split).content_length 
                if val !=0:
                    s3path = 's3://arup-climate-global-656165081472-aws-config/'+file_path+'/'+files_split
                    file_data.append(s3path)
                else:
                    pass
            try:
                if len(file_data) !=0:
                    fileset = [s3.open(file) for file in file_data]
                    data = xr.open_mfdataset(fileset)
                    latvar=data.lat.values
                    lonvar= data.lon.values
                    lat0=self.user_lat
                    lon0=self.user_lon
                    latvals = latvar[:]
                    lonvals = lonvar[:]
                    dist_sq = (latvals-lat0)**2 + (lonvals-lon0)**2
                    # 1D index of min element
                    minindex_flattened = dist_sq.argmin()
                    iy_min,ix_min = np.unravel_index(minindex_flattened,
                                                     latvals.shape)
                    temp = data.variables[self.user_var]
                    # Creating an empty pandas dataframe
                    try:
                        data['time'] = data.indexes['time'].normalize()
                    except:
                        data['time'] = data.indexes['time'].to_datetimeindex()
                    index_time = data["time"].to_series()
                    res_val = ''.join(random.choices(string.ascii_uppercase, 
                                                     k = 7))
                    df = pd.DataFrame(0, columns = [res_val], 
                                      index = index_time)
                    dt = np.arange(0, data.variables['time'].size)
                    for time_index in dt:
                        df.iloc[time_index] = temp[time_index,iy_min ,ix_min]
                    df.index = pd.to_datetime(df.index).strftime('%Y-%m')
                    random_df_name = ''.join(random.choices(string.ascii_uppercase+string.digits, k = 7))
                    random_df_name=df
                    all_dataframe.append(random_df_name)
                else:
                    pass
            except:
                pass
        if len(all_dataframe)!=0:
            combing_data = pd.concat(all_dataframe,axis=1)
            combing_data[self.user_var]=combing_data.iloc[:,:].mean(axis=1)
            combing_data=combing_data.filter([self.user_var])
            combing_data['Time'] = combing_data.index
            combing_data.reset_index(drop=True, inplace=True)
            combing_data['Lat'] =lat0
            combing_data['Lon'] =lon0
        return combing_data

class CordexNcSearch():

    def __init__(self,list_of_files,user_lat,user_lon,user_var):
        self.list_of_files = list_of_files
        self.user_lat = user_lat
        self.user_lon = user_lon
        self.user_var = user_var

    def run(self):
        dummy=[]
        for lat_one,lon_one in zip(self.user_lat,self.user_lon):
            k_both=[lat_one,lon_one]
            dummy.append(k_both)
        result = Counter()
        for lat_dup, lon_dup in dummy:
            result.update({lat_dup:lon_dup})
        co_ordinats_latand_lon=map(list, result.items())
        list_of_files = self.list_of_files
        list_of_files = list(dict.fromkeys(list_of_files))
        print(list_of_files)
        size_of_list=len(list_of_files)
        dict_files={}
        while size_of_list !=0:
            initial_index=list_of_files[0]
            initial_index=initial_index[:-20]
            temp_list=[]
            temp_list.clear()
            for file_name in list_of_files:
                if initial_index in file_name:
                    temp_list.append(file_name)
            for delete_files in temp_list:
                list_of_files.remove(delete_files)
            res = ''.join(random.choices(string.ascii_uppercase +string.digits, k = 7))
            dict_files[res]= temp_list
            size_of_list=len(list_of_files)
        file_keys=dict_files.keys()
        print(dict_files)
        Multiple_data_with_lat_lon={}
        for latitude_longitude in co_ordinats_latand_lon:
            all_dataframe=[]
            for values in file_keys:
                values_of_file=dict_files.get(values)
                file_data=[]
                for files_split in values_of_file:
                    file_path=files_split.replace('_','/')
                    file_path=file_path.replace('.nc','')
                    s3 = s3fs.S3FileSystem(anon=False)
                    val=boto3.resource('s3').Bucket('arup-climate-global-656165081472-aws-config').Object(file_path+'/'+files_split).content_length
                    if val !=0:
                        s3path = 's3://arup-climate-global-656165081472-aws-config/'+file_path+'/'+files_split
                        file_data.append(s3path)
                    else:
                        pass
                print(file_data)
                try:
                    if len(file_data) !=0:
                        fileset = [s3.open(file) for file in file_data]
                        data = xr.open_mfdataset(fileset)
                        latvar=data.lat.values
                        lonvar= data.lon.values
                        lat0=latitude_longitude[0]
                        lon0=latitude_longitude[1]
                        latvals = latvar[:]
                        lonvals = lonvar[:]
                        dist_sq = (latvals-lat0)**2 + (lonvals-lon0)**2
                        # 1D index of min element
                        minindex_flattened = dist_sq.argmin() 
                        iy_min,ix_min = np.unravel_index(minindex_flattened,
                                                         latvals.shape)
                        temp = data.variables[self.user_var]
                        # Creating an empty pandas dataframe
                        try:
                            data['time'] = data.indexes['time'].normalize()
                        except:
                            data['time'] = data.indexes['time'].\
                                        to_datetimeindex()
                        index_time = data["time"].to_series()
                        res_val = ''.join(random.choices(string.ascii_uppercase, k = 7))
                        df = pd.DataFrame(0, columns = [res_val], 
                                          index = index_time)
                        dt = np.arange(0, data.variables['time'].size)
                        for time_index in dt:
                            df.iloc[time_index] = temp[time_index,iy_min ,
                                                       ix_min]
                        df.index = pd.to_datetime(df.index).\
                                        strftime('%Y-%m-%d')
                        random_df_name = ''.join(random.choices(
                            string.ascii_uppercase+string.digits, k = 7))
                        random_df_name=df
                        all_dataframe.append(random_df_name)
                    else:
                        pass
                except:
                    pass
            try:
                combing_data = pd.concat(all_dataframe,axis=1)
                combing_data[self.user_var]=combing_data.iloc[:,:].mean(axis=1)
                combing_data=combing_data.filter([self.user_var])
                combing_data['Time'] = combing_data.index
                combing_data.reset_index(drop=True, inplace=True)
                co_ordinates=str(latitude_longitude[0])+","+\
                            str(latitude_longitude[1])
                value_of_dict={'{}'.format(self.user_var):\
                    combing_data[self.user_var].values.tolist(),'time':combing_data["Time"].values.tolist()}
                Multiple_data_with_lat_lon[co_ordinates]=value_of_dict
            except:
                Multiple_data_with_lat_lon[co_ordinates]="something went wrong"
        return Multiple_data_with_lat_lon
