import requests
import json
from django.conf import settings

class GlobalSearch():

    def __init__(self,parameters,token,search_flag):
        self.parameters = parameters
        self.token = token
        self.headers = {
                        'Content-Type': 'application/json',
                        'Authorization':'{}'.format(self.token)
                        }
        self.search_flag = search_flag
        self.domain = settings.CORDEX_DATA_DOMAIN

    def cordex(self):
        # upload data  
        region=self.parameters["region"]["name"]
        variable=self.parameters["variable"]
        experiment=self.parameters["experiment"]
        cordex_parameters = {
                    "project":[self.parameters["datasource"]],
                    "product":None,
                    "domain":[region],
                    "institute":None,
                    "driving_model":None,
                    "experiment":[experiment],
                    "experiment_family":None,
                    "ensemble":None,
                    "rcm_model":None,
                    "rcm_version":None,
                    "time_frequency":["day"],
                    "variable":variable,
                    "variable_long_name":None,
                    "cf_standard_name":None,
                    "datanode":None 
                    }
        data = json.dumps(cordex_parameters)
        url_upload_response = requests.\
                                post(self.domain+"/dataset/cordex/search",
                                data=data,
                                headers = self.headers)
        return url_upload_response.json()

    def cordex_filter(self):
        #filter data    
        region=self.parameters["region"]["name"]
        variable=self.parameters["variable"]
        experiment=self.parameters["experiment"]
        data = {
                "from_date":self.parameters["from_date"],
                "to_date":self.parameters["to_date"],
                "project":[self.parameters["datasource"]],
                "domain":[region],
                #"ensemble":["r1i1p1"],
                "experiment":[experiment],
                #"experiment_family":["All"],
                #"rcm_version":["v1"],
                "time_frequency":["day"],
                "variable":variable}
        data = json.dumps(data)
        filter_response = requests.post(self.domain+"/dataset/cordex/list",
                                         data = data,
                                         headers = self.headers)
        return filter_response.json()

    def run(self):
        message = {"message":"datasource not found"}
        if self.search_flag == False:
            data = None
            if self.parameters["datasource"] == "CORDEX":
                message = self.cordex()
                data = self.cordex_filter()
            return message,data
        else: 
            message = {"message":"urls are already uploaded successfully"}
            data = self.cordex_filter()
            return message,data
