from django.urls import path
from . import views

# url redirection
urlpatterns = [
    path("auth/login",views.login,name='login'),
    path("userlist",views.list_users,name='list_users'),
    path("auth/login-sso",views.login_sso,name='login_sso'),
]
