from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view,permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAuthenticated
from rest_framework import status
from .serializers import UserSerializer
import requests
import secrets
import random
import json

@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    """ 
    POST Method: login
    """
    if request.method == "POST":
        email = request.data.get("email",None)
        password = request.data.get("password",None)
        if email is None or password is None:
            data = {'error':'please provide email and password'}
            return Response(data,status = status.HTTP_400_BAD_REQUEST)
        user = authenticate(username = email, password = password)
        if user is not None:
            token,_ = Token.objects.get_or_create(user = user)
            if token is not None:
                token.delete()
            token, created = Token.objects.get_or_create(user= user)
            data = {'username':user.get_username(),'token':token.key}
            return Response(data,status = status.HTTP_200_OK)
        data = {"error":"Invalid credentials"}
        return Response(data, status = status.HTTP_404_NOT_FOUND)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def list_users(request):
    """ 
    GET Method: List all the users
    """
    users = User.objects.all()
    serializer = UserSerializer(users,many=True)
    return Response(serializer.data)

@api_view(["POST"])
@permission_classes((AllowAny,))
def login_sso(request):
    """ 
    POST Method: SSO based login
    """
    token = request.data.get("token",None)
    #token = "EwBwA8l6BAAU6k7+XVQzkGyMv7VHB/h4cHbJYRAAAfDTxC0PpZ9TtjMsS+DSBFxjEBbE7HswghFvilsnkJtQwJdO/2RA/iJn/bpsRhKMURaBAQxKCGftO20Inl1kPw3PRr3l3DfFG+dQzzqmbGMP2aDP1Bs1SsEY9qDWwvYFqX4RKBscuDw4NOpaQi41kFlmtSyu5MfBQ+dSPFkAXpF9o58KR30+5F5nTHEoDlWDmH5EMlvQuqTzL+s41wJhw88b8MoPWpaOZrYV22zSoIF+N6dvb4gbyiu5BkmahyX97p9QIkvvVqwmKFMWex0GaFTc6ZUjdHbpvNW4HbLd7/C7EWEMDpnuqiWH5XgJq2jpsymAVKxRgvodF7OwWrT+KksDZgAACC5KLm95AHfxQAJweSxU6qIu8YD10ZjeML57C4ZTyof5J8R8go7Onl4AXNNQ8DIOcparnMKJCvNJy8VCutMYVkhZ3RMBvnHDrMGepL/W/aMiim09rQEP+nr0LyVSSC57XxjGTYAB3f61BRoOLJ8HK4GQ5dCtHe0qfKfUJf3/wEJwfZYyV1OA/CopKkKfMKfDm4WgkGSS4talUY1m3Wvr2pM5RCK1sMr2d6cwa8U74wEjDUVcsoEFFiUnLfGb1XENufpoi08t+CvfrVlnqCVfOHDDy5evFRTdsx42+W3WvDw4ezIdTHvb4eUj01ughs3QwfIRSHEzVFzbTPzmJSC2itLtQFrKxQzuEzNlYo44B1MstGBkSTHE9UcAfV768PcqGypOeP8VQQabUjFEJiVWxOIrcOXX5DWtb+x+xEWnca7eGhUWKzwc4Jsllyt7yCfdo5OkUfoeOZUe3qbmof8SfHBAebiCi8GihIkjLwPlTk0P/wBHoP3bjS6TWzZMsFlAg5zWtH/o/MbC/AUzeVry6alaPo6WPhm/6tB82iiNCZRphj7TERmwK8pwzlM4l+fLQu1Pk9ThDwQa8ysXDDRTXCgexnLvRREIeQzW9NzB3ygDbb+T0MDMZDvqYwhiRfKSvOjnR8kF96B6a2cpENbLJo3rcCaM0QAuMGj9++x5UvxtgyqIsmLb5VGAOeBRxltWhpimtZr5KLYwoeX7A7BRF57KFKPK3TCR4CS/TZskMC3JHp3eB9TliiSotu1BQ/a8FV7LH67pVoJYxf+HAg=="
    url = "https://graph.microsoft.com/v1.0/me"
    payload={}
    headers = {
    'Authorization': 'Bearer '+token
    }
    try:
        response = requests.request("GET", url, headers=headers, data=payload)
        metadata = json.loads(response.text)
        email = metadata["userPrincipalName"]
        first_name = metadata["givenName"]
    except:
        data = {"details":"Invalid token"}
        return Response(data,status = status.HTTP_200_OK)
    secret_token = secrets.token_hex(15)
    start = random.randint(5,10)
    end = random.randint(10,15)
    token = secret_token+token[start:end]
    try:
        user_obj = User.objects.get(username=email)
    except User.DoesNotExist:
        if first_name != None:
            user_obj= User.objects.create(username = email,email=email,first_name=first_name)
            user_obj.save()
        else:
            user_obj= User.objects.create(username = email,email=email)
            user_obj.save()
    token_obj = Token.objects.filter(user=user_obj)
    if not token_obj:
        token_obj = Token.objects.create(user=user_obj)
    token_obj = Token.objects.filter(user=user_obj).update(key=token)
    data = {"status":"success","token":token}
    return Response(data,status = status.HTTP_200_OK)
