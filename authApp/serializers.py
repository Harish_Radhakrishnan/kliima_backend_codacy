from rest_framework import serializers
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    """ 
    User serializer to check the
    user input fields
    """
    class Meta:
        model = User
        fields = ("id","username","email")
