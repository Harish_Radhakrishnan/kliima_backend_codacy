CORDEX = r"""
(?P<variable>[a-zA-Z0-9]+)_
(?P<domain>[A-Z]{3}-(11|22|44)i?)_
(?P<GCMsource>[a-zA-Z0-9-]+)_
(?P<scenario>(historical|rcp\d{2}))_
(?P<member>r\d+i\d+p\d+(f\d+)?)_
(?P<RCMsource>[a-zA-Z0-9-]+)_
(?P<RCMversion>[a-zA-Z0-9-]+)_
(?P<frequency>(3hr|6hr|day|mon|seas|sem|ann))_
(?P<start>\d{6,})-
(?P<end>\d{6,})
_?
(?P<fid>[a-zA-Z0-9-]+)?
.nc
"""

HADUK = r"""
(?P<variable>[a-zA-Z0-9]+)_
hadukgrid_
(?P<domain>uk)_
(?P<resolution>(1km|2.2km|5km|12km|25km|60km)+)_
(?P<frequency>(3hr|6hr|day|mon|seas|sem|ann))_
(?P<start>\d{6,})-
(?P<end>\d{6,})
_?
(?P<fid>[a-zA-Z0-9]+)?
.nc
"""

UKCP18 = r"""
(?P<variable>[a-zA-Z0-9]+)_
(?P<scenario>rcp\d{2})_
(?P<collection>[a-zA-Z0-9-]+)_
(?P<domain>uk)_
(?P<resolution>(1km|2.2km|5km|12km|25km|60km)+)_
(?P<member>\d{2})_
(?P<frequency>(1hr|3hr|6hr|day|mon|seas|sem|ann))_
(?P<start>\d{6,})-
(?P<end>\d{6,})
_?
(?P<fid>[a-zA-Z0-9]+)?
.nc
"""

UKCP18PROB = r"""
(?P<variable>[a-zA-Z0-9]+)_
(?P<scenario>(sres-a1b|rcp\d{2}))_
(?P<collection>[a-zA-Z0-9-]+)_
(?P<domain>uk)_
(?P<resolution>(1km|2.2km|5km|12km|25km|60km)+)_
(?P<filetype>(cdf|pdf|sample))_
(?P<baseline>b(6190|8100|8110))_
(?P<nyear>\d+y)_
(?P<frequency>(3hr|6hr|day|mon|seas|sem|ann))_
(?P<start>\d{6,})-
(?P<end>\d{6,})
_?
(?P<fid>[a-zA-Z0-9]+)?
.nc
"""

LOCA = r"""
(?P<variable>[a-zA-Z0-9]+)_
(?P<frequency>(3hr|6hr|day|mon|seas|sem|ann))_
(?P<source>[a-zA-Z-0-9-]+)_
(?P<scenario>(historical|rcp\d{2}))_
(?P<member>r\d+i\d+p\d+(f\d+)?)_
(?P<start>\d{6,})-
(?P<end>\d{6,})
.LOCA_
(?P<version>\d{4}-\d{2}-\d{2}).
(?P<resolution>[a-zA-Z0-9-]+)
_?
(?P<fid>[a-zA-Z0-9]+)?
.nc
"""

CMIP6 = r"""
(?P<variable>[a-zA-Z0-9]+)_
(?P<frequency>(3hr|6hr|day|mon|seas|sem|ann))_
(?P<source>[a-zA-Z-0-9-]+)_
(?P<scenario>(historical|ssp\d{3}))_
(?P<member>r\d+i\d+p\d+(f\d+)?)_
(?P<grid>g[mnr][a-zA-Z0-9]+)_
(?P<start>\d{6,})-
(?P<end>\d{6,})
_?
(?P<fid>[a-zA-Z0-9]+)?
.nc
"""
