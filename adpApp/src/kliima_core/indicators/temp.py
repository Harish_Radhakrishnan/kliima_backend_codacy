from typing import Callable

import xclim

from .common import register_indicator

xclim.set_options(data_validation="log", cf_compliance="log")

TEMP_FUNCS: dict[str, Callable] = {}


@register_indicator("Maximum temperature", TEMP_FUNCS)
def tasmax_max(tasmax, *, freq):
    return tasmax.resample(time=freq).max(dim="time")


@register_indicator("Average daily temperature", TEMP_FUNCS)
def tas_mean(tas, *, freq):
    return tas.resample(time=freq).mean(dim="time")


@register_indicator("Average daily maximum temperature", TEMP_FUNCS)
def tasmax_mean(tasmax, *, freq):
    return tasmax.resample(time=freq).mean(dim="time")


@register_indicator("Average daily minimum temperature", TEMP_FUNCS)
def tasmin_mean(tasmin, *, freq):
    return tasmin.resample(time=freq).mean(dim="time")


@register_indicator("Temperature threshold more than", TEMP_FUNCS)
def tasmax_days_above(tasmax, *, thresh, freq):
    return (tasmax > thresh).resample(time=freq).sum(dim="time")


@register_indicator("Temperature threshold less than", TEMP_FUNCS)
def tasmin_days_below(tasmin, *, thresh, freq):
    return (tasmin < thresh).resample(time=freq).sum(dim="time")


@register_indicator("Average summer temperature", TEMP_FUNCS)
def tas_mean_summer(tas, *, freq):
    return tas.groupby("time.season")["JJA"]


@register_indicator("Average winter temperature", TEMP_FUNCS)
def tas_mean_winter(tas, *, freq):
    return tas.groupby("time.season")["DJF"]


@register_indicator("Average summer maximum temperature", TEMP_FUNCS)
def tasmax_mean_summer(tasmax, *, freq):
    return tasmax.groupby("time.season")["JJA"]


@register_indicator("Average winter minimum temperature", TEMP_FUNCS)
def tasmin_mean_winter(tasmin, *, freq):
    return tasmin.groupby("time.season")["DJF"]
