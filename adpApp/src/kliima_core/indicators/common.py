from typing import Callable


def register_indicator(name: str, funcs: dict[str, Callable]) -> Callable:
    def _register_inner(func):
        funcs[name] = func
        return func

    return _register_inner
