from pathlib import Path
from typing import Callable

import xarray as xr

from .parse import parse, ParseError
from . import _patterns

# TODO: write a get_dataloader wrapper function
DATALOADERS: dict[str, (Callable, str)] = {}


def register_dataloader(name: str, freq: str) -> Callable:
    def _register_inner(func):
        DATALOADERS[name] = (func, freq)
        return func

    return _register_inner


@register_dataloader("ukcp18-high-res", "AS-DEC")
def ukcp18_loader(ds):
    fname = Path(ds.encoding["source"])
    parsed = parse(fname, _patterns.UKCP18)
    ds = (
        ds.squeeze()
        .reset_coords("ensemble_member", drop=True)
        .expand_dims(
            dim={
                "scenario": [parsed["scenario"]],
                "model": [parsed["member"]],
                "fid": [parsed["fid"]],
            },
            axis=[0, 1, 2],
        )
    )
    return ds


@register_dataloader("ukcp18-prob", "AS-DEC")
def ukcp18_prob_loader(ds):
    fname = Path(ds.encoding["source"])
    parsed = parse(fname, _patterns.UKCP18PROB)
    ds = (
        ds.squeeze()
        .expand_dims(
            dim={
                "scenario": [parsed["scenario"]],
                "fid": [parsed["fid"]],
            },
            axis=[0, 1],
        )
        .sel(percentile=[10, 50, 90])
        .assign_coords(percentile=["10th percentile", "50th percentile", "90th percentile"])
        .rename({"percentile": "stat"})
    )
    return ds


@register_dataloader("haduk", "AS-DEC")
def haduk_loader(ds):
    fname = Path(ds.encoding["source"])
    parsed = parse(fname, _patterns.HADUK)
    return (
        ds.squeeze()
        .expand_dims(
            dim={
                "scenario": ["observations"],
                "model": ["hadukgrid"],
                "fid": [parsed["fid"]],
            },
            axis=[0, 1, 2],
        )
    )


@register_dataloader("loca", "AS-JAN")
def loca_loader(ds):
    fname = Path(ds.encoding["source"])
    parsed = parse(fname, _patterns.LOCA)
    ds = (
        ds.squeeze()
        .expand_dims(
            dim={
                "scenario": [parsed["scenario"]],
                "model": [f"{parsed['source']}_{parsed['member']}"],
                "fid": [parsed["fid"]],
            },
            axis=[0, 1, 2],
        )
    )
    if parsed["scenario"] == "historical":
        ds = ds.squeeze("scenario")
    return ds
