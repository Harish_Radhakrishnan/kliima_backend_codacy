import os
import json
from pathlib import Path

from .process import process_variable
from .utils import (
    describe,
    multiindex_to_dict,
    ADDITIVE,
    MULTIPLICATIVE,
    PERCENTAGE,
)


def run_from_config(config: dict | str | os.PathLike):
    """TODO: docstring"""
    if isinstance(config, (str, os.PathLike)):
        config = Path(config).expanduser()
        with config.open("r", encoding="utf-8") as f:
            config = json.load(f)

    data = config.get("data")
    indicator = config.get("indicator")
    bias = config.get("bias")

    processed = {}
    for key, values in data.items():
        processed[key] = process_variable(
            name=indicator.get("name"),
            **indicator.get("args", {}),
            **values,
        )

    if bias:
        kind = bias.get("kind")
        sim = processed["future"]
        ref = processed["historical"]
        if kind in ADDITIVE:
            results = sim - ref
        elif kind in MULTIPLICATIVE:
            results = sim / ref
        elif kind in PERCENTAGE:
            results = (sim - ref) / ref * 100
        else:
            raise ValueError("kind must be one of '+' or '*' or '%'")
    else:
        results = processed.popitem()[1]

    results = results.compute()
    if "model" in results.dims:
        results = describe(results, "model")
        
    df_out = results.to_dataframe(
        dim_order=["fid", "scenario", "stat"]
    )[results.name]
    
    return multiindex_to_dict(df_out)
