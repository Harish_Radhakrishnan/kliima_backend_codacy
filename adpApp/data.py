from django.conf import settings
import requests
import json
import time
import subprocess
import os
from zipfile import ZipFile
from tqdm import tqdm
import threading
from .models import UkcpResponseTracker
from .utils import AwsSecretManager
from django.db import close_old_connections
from adpApp import logger
from arup.utils import LogUtils
from rest_framework import status
from rest_framework.response import Response

class AdpData(AwsSecretManager):
    """ 
    Data downloading from adp api
    """
    def __init__(self, primary_key,hist_request_data,
                 proj_request_data,
                 request):
        """
        constructor

        Args:
            primary_key (int): ukcp table primary key
            hist_request_data (dict): history request data
            proj_request_data (dict): projection request data
            request: request data
        """
        super().__init__()
        self.primary_key = primary_key
        self.base_dir = settings.BASE_DIR
        self.job_create_api = settings.JOB_CREATE_API
        self.hist_request_data = hist_request_data
        self.proj_request_data = proj_request_data
        self.adp_secret = self.get_secret_values()
        self.request = request
    
    @classmethod
    def initialize(cls, primary_key, hist_request_data, 
                   proj_request_data, request):
        """
        initalize the scopes

        Args:
            primary_key (int): ukcp table primary key
            hist_request_data (dict): history request data
            proj_request_data (dict): projection request data
        """
        return cls(primary_key, hist_request_data, proj_request_data,
                   request)
    
    def job_status_api(self,job_id):
        """
        creating job status api url

        Args:
            job_id (str): job id
        """
        return f"{self.job_create_api.format(self.adp_secret['tenant_id'])}/{job_id}/status"
    
    def create_job(self,request_body_data):
        """
        creating job

        Args:
            request_body_data (dict): request body raw data
        """
        payload = json.dumps(request_body_data)
        headers = {'Content-Type': 'application/json',
                   'Authorization': f"Basic {self.adp_secret['api_token']}"}
        response = requests.request("POST",
                    self.job_create_api.format(self.adp_secret['tenant_id']), 
                    headers=headers, data=payload)
        return response.json()
    
    def getting_urls(self, job_id):
        """
        checking job status

        Args:
            job_id (str): job id to check the status
        """
        payload={}
        headers = {'Content-Type': 'application/json',
                   'Authorization': f"Basic {self.adp_secret['api_token']}"}
        job_status_api = self.job_status_api(job_id)
        response = requests.request("GET", job_status_api, 
                                    headers=headers, data=payload)
        return response.json()
    
    def extract(self,body_data,index):
        """
        extracting data urls
        """
        logger.info(LogUtils.load_message(**self.request.log_utils, 
                    message="process has been started .............."))
        print("process has been started ..............")
        try:
            response = self.create_job(body_data)
            job_id = response['jobId']
            self.save_ukcp_tracker_object(kind="job_id",
                                                set_data=job_id,
                                                index=index,
                                                primary_key=self.primary_key)
            break_time = 0
            while True:
                response = self.getting_urls(job_id)
                try:
                    logger.info(LogUtils.\
                    load_message(**self.request.log_utils,
                    message=f"{job_id} status={response['status']}......."))
                    print(f"{job_id} status={response['status']}.......")
                    if response['status'] == "jobComplete":
                        logger.info(LogUtils.\
                        load_message(**self.request.log_utils,
                        message=f"{job_id} status completed......."))
                        print(f"{job_id} status completed.......")
                        break
                    elif response['status'] == "jobFailed":
                        self.save_ukcp_tracker_object\
                        (kind="data_present_update",
                        set_data=False,
                        index=index,
                        primary_key=self.primary_key)
                        return
                    time.sleep(30)
                except KeyError:
                    logger.info(LogUtils.\
                    load_message(**self.request.log_utils,
                    message="job id status not getting yet....."))
                    print("job id status not getting yet.....")
                    continue
                if break_time >= 7200:
                    break
                break_time +=30
            if response['result']['downloadUrls']:
                self.save_ukcp_tracker_object(kind="data_present_update",
                                                set_data=True,
                                                index=index,
                                                primary_key=self.primary_key)
                urls = response['result']['downloadUrls']
                var = body_data['jobMetadata']['filter']\
                        ['variables'][0]['name']
                file_path = os.path.join(self.base_dir,
                                f"UKCP/{var}/{job_id}")
                os.makedirs(file_path)
                try:
                    self.save_ukcp_tracker_object(kind="data_download_update",
                                                set_data="inprogress",
                                                index=index,
                                                primary_key=self.primary_key)
                    for url in tqdm(urls):
                        filename = self.get_filename(file_path,url)
                        subprocess.run(["wget","-O",filename,url])
                    self.extract_zip(filename)
                    logger.info(LogUtils.\
                    load_message(**self.request.log_utils,
                    message="process has been finished .............."))
                    print("process has been finished ..............")
                    self.save_ukcp_tracker_object(kind="data_download_update",
                                                set_data="completed",
                                                index=index,
                                                primary_key=self.primary_key)
                except:
                    self.save_ukcp_tracker_object(kind="data_download_update",
                                                set_data="failed",
                                                index=index,
                                                primary_key=self.primary_key)
            else:
                self.save_ukcp_tracker_object(kind="data_download_update",
                                                set_data="failed",
                                                index=index,
                                                primary_key=self.primary_key)
        except Exception as e:
            logger.exception(LogUtils.load_message(**self.request.log_utils, 
                    message="process has been failed .............."))
            print("process has been failed ..............")
            self.set_obj_status(self.primary_key, "failed")
            data = {"status":"failed",
                    "message":"The request was not successfully completed"}
            return Response(data=data, 
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            
    def run(self):
        """ 
        run the data downloading pipeline
        
        returns: primary key
        """
        hist_thread = threading.Thread(target=self.extract,
                                       args=(self.hist_request_data, 0))
        future_thread = threading.Thread(target=self.extract,
                                         args=(self.proj_request_data, 1))
        hist_thread.start()
        time.sleep(10)
        future_thread.start()
        
        hist_thread.join()
        future_thread.join()
        return self.primary_key
    
    @staticmethod
    def get_filename(file_path,url):
        """
        creating file directory path

        Args:
            file_path (str): root path
            url (str): aws signed url

        Returns:
            str: file directory path
        """
        filename = url.split("//")
        filename = filename[-1].split("/")
        filename = filename[-1].split("?")
        filename = os.path.join(file_path,filename[0])
        return filename

    @staticmethod
    def set_obj_status(primary_key,status):
        """
        set status

        Args:
            primary_key (int): primary key of the obj
            status (str): success or failed
        """
        close_old_connections()
        obj = UkcpResponseTracker.objects.get(id=primary_key)
        obj.status = status
        obj.save()
    
    @staticmethod
    def save_ukcp_tracker_object(primary_key,kind,set_data,index):
        """
        helper method to update ukcp tracker object

        Args:
            primary_key (int): ukcp tracker primary key
            kind (str): type of updation
            set_data (any): update data
            index (int): 0 or 1, 0 => hist, 1=> proj
        """
        close_old_connections()
        obj = UkcpResponseTracker.objects.get(id=primary_key)
        if kind == "data_present_update":
            if index == 0:
                obj.is_hist_data_present = set_data
            else:
                obj.is_future_data_present = set_data
        elif kind == "data_download_update":
            if index == 0:
                obj.hist_file_download_status = set_data
            else:
                obj.future_file_download_status = set_data
        elif kind == "job_id":
            if index == 0:
                obj.hist_job_id = set_data
            else:
                obj.future_job_id = set_data 
        obj.save()
    
    @staticmethod
    def extract_zip(path):
        """
        extract zip files

        Args:
            path (Path): path of the zip
        """
        with ZipFile(path,'r') as zip:
            store_path = path.split("/")
            store_path = "/".join(store_path[:len(store_path)-1])
            zip.extractall(store_path)
        os.remove(path)
