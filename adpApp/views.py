from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from .pipeline import Pipeline
from adpApp import logger
from arup.utils import LogUtils
# Create your views here.

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def ukcp_analyse(request):
    """ 
    POST method: analysing the ukcp
    """
    request.log_utils = LogUtils.load_dict(request.data['primary_key'])
    logger.info(LogUtils.load_message(**request.log_utils, message="started"))
    print("started")
    pipeline = Pipeline.initialize(request)
    output = pipeline.run()
    print("completed")
    logger.info(LogUtils.load_message(**request.log_utils,
                                      message="completed"))
    if output == "something went wrong":
        data = {"status":"failed","message":output}
        return Response(data,status=status.HTTP_200_OK)
    else:
        data = {"status":"success","message":output}
        return Response(data,status=status.HTTP_200_OK)
