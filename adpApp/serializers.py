from rest_framework import serializers
from .models import UkcpResponseTracker

class UkcpResponseTrackerSerializer(serializers.ModelSerializer):
    """ 
    Validating the UkcpResponseTracker Model fields
    """
    class Meta:
        model = UkcpResponseTracker
        fields = "__all__"
