from django.contrib import admin
from .models import UkcpResponseTracker

# Register your models here.
admin.site.register(UkcpResponseTracker)