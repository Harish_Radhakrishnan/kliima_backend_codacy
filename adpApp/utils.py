from copy import deepcopy
import random
import string
import boto3
import json

class Utils():
    """ 
    Utility class for adp app
    """
    @staticmethod
    def polygon_request_data(request):
        """ 
        create adp-api body json data for request
        """
        coordinates = [[lo,la] for la,lo in \
                   zip(request.data['lat'],request.data['lon'])]
        
        variable = [
            {
                "type":"variable",
                "name": request.data['variable']
            }]
        param = [{"type":"param","name":param} 
                 for param in request.data['param']]
        variable.extend(param)
        data = {
                "projectCode": request.data['project_code'],
                "consumerId": request.data['consumer_id'],
                "jobMetadata": {
                    "datasetId": request.data['dataset_id'],
                    "datasetVersion": request.data['dataset_version'],
                    "filter": {
                        "filterType":"Climate",
                        "areaOfInterest": {
                        "type": "FeatureCollection",
                        "features": [
                            {
                            "id": 1,
                            "type": "Feature",
                            "properties": {"fid": "".join\
                                (random.choices(string.ascii_uppercase,k=3)\
                                +random.choices(string.digits,k=3))},
                            "geometry": {
                                "type": request.data['geometry_type'],
                                "coordinates": [coordinates]}
                                }]},
                        "variables": variable,
                        "whereClause": ""},
                    "dataExtract": { "format": "nc"}}}
        hist_request_data = deepcopy(data)
        proj_request_data = deepcopy(data)
        hist_request_data['jobMetadata']['filter']['startDate'] = \
            request.data['historical_from_date']
        hist_request_data['jobMetadata']['filter']['endDate'] = \
            request.data['historical_to_date']
        proj_request_data['jobMetadata']['filter']['startDate'] = \
            request.data['projectional_from_date']
        proj_request_data['jobMetadata']['filter']['endDate'] = \
            request.data['projectional_to_date']
        return hist_request_data, proj_request_data
    
    @staticmethod
    def point_request_data(request):
        """ 
        create adp-api body json data for request
        """
        coordinates = [ {
                            "type": "Feature",
                            "properties": {
                                "fid":  "".join\
                                (random.choices(string.ascii_uppercase,k=3)\
                                +random.choices(string.digits,k=3))
                            },
                            "id": id_,
                            "geometry": {
                                "coordinates": [
                                    lo,la
                                ],
                                "type": request.data['geometry_type']
                            }
                        }   for id_,la,lo in \
                   zip(range(0,len(request.data['lat'])+1),\
                       request.data['lat'],request.data['lon'])]
        # fid mappings
        fid_map = {}
        for lat,lon,fid in zip( request.data['lat'],request.data['lon'],
                               coordinates):
            fid_map[fid["properties"]['fid']] = f'{lat}/{lon}'
        variable = [
            {
                "type":"variable",
                "name": request.data['variable']
            }]
        param = [{"type":"param","name":param} 
                 for param in request.data['param']]
        variable.extend(param)
        data = {
                "projectCode": request.data['project_code'],
                "consumerId": request.data['consumer_id'],
                "jobMetadata": {
                    "datasetId": request.data['dataset_id'],
                    "datasetVersion": request.data['dataset_version'],
                    "filter": {
                        "filterType":"Climate",
                        "areaOfInterest": {
                            "type": "FeatureCollection",
                            "features": coordinates
                        },
                        "variables": variable,
                        "whereClause": ""
                    },
                    "dataExtract": {
                        "format": "nc"
                    }
                }
            }
        hist_request_data = deepcopy(data)
        proj_request_data = deepcopy(data)
        hist_request_data['jobMetadata']['filter']['startDate'] = \
            request.data['historical_from_date']
        hist_request_data['jobMetadata']['filter']['endDate'] = \
            request.data['historical_to_date']
        proj_request_data['jobMetadata']['filter']['startDate'] = \
            request.data['projectional_from_date']
        proj_request_data['jobMetadata']['filter']['endDate'] = \
            request.data['projectional_to_date']
        return hist_request_data, proj_request_data, fid_map

class AwsSecretManager():
    """
    AWS Secret Manager access
    """
    def __init__(self):
        """
        Intializing the Secret Manager
        """
        self.secret_id = "preprod/kliima/adp-api-key"
        self.client = boto3.client('secretsmanager',region_name="eu-west-2")
    
    def get_secret_values(self):
        """
        Get the secret values

        Returns:
            dict: tenant_id, api_token
        """
        response = self.client.\
                    get_secret_value(SecretId=self.secret_id)
        data = json.loads(response["SecretString"])
        return data

class FidtoCoordinates():
    """
    converting fid to coordinates
    """
    @staticmethod
    def run(fid_map, output):
        """
        run method 
        
        fid_map(dict)
        output(dict)
        """
        result = {}
        for key in output.keys():
            result[fid_map[key]] = output[key]
        return result