"""
UKCP process pipeline module
"""
import os
import shutil
import json
from django.conf import settings
from .models import UkcpResponseTracker
from django.db import close_old_connections
from .utils import Utils, FidtoCoordinates
from .data import AdpData
from .src.kliima_core.run import run_from_config
from arup.utils import LogUtils
from adpApp import logger

class Pipeline():
    """ 
    ukcp pipeline
    """
    def __init__(self,request):
        self.base_dir = settings.BASE_DIR
        self.request = request
        
    @classmethod
    def initialize(cls,request):
        """
        intialize the object 

        Args:
            request (Request): request data

        Returns:
            object : Pipeline object
        """
        return cls(request)
    
    def run(self):
        if self.request.data['geometry_type'].lower() \
            == "polygon":
            hist_request_data, proj_request_data = \
                Utils.polygon_request_data(self.request)
        elif self.request.data['geometry_type'].lower() \
            == "point":
            hist_request_data, proj_request_data, fid_map = \
                Utils.point_request_data(self.request)
        logger.info(LogUtils.load_message(**self.request.log_utils, 
                     message="request data has been generated"))
        print("hist request body ->",hist_request_data)
        print("proj request body ->",proj_request_data)
        logger.info(LogUtils.load_message(**self.request.log_utils, 
        message=f"hist request body -> {json.dumps(hist_request_data)}"))
        logger.info(LogUtils.load_message(**self.request.log_utils, 
        message=f"proj request body -> {json.dumps(proj_request_data)}"))
        print("request data has been generated")
        adp_data = AdpData.initialize(self.request.data['primary_key'],
                                hist_request_data,
                                proj_request_data,
                                self.request)
        primary_key = adp_data.run()
        logger.info(LogUtils.load_message(**self.request.log_utils, 
        message="data download have been completed successfully"))
        print("data download have been completed successfully")
        config = Pipeline.get_config_data(self.base_dir,primary_key)
        if config != None:
            try:
                print("config --->",config)
                logger.info(LogUtils.load_message(**self.request.log_utils, 
                message=f"config -> {json.dumps(config)}"))
                output = run_from_config(config)
                output = FidtoCoordinates.run(fid_map, output)
                self.status_update(self.base_dir,"success", 
                                   output, primary_key)
            except Exception as e:
                logger.exception(LogUtils.load_message(**self.request.log_utils, message="something went wrong..."))
                output = {"details":"something went wrong"}
                self.status_update(self.base_dir, "failed", 
                                   output, primary_key)
                print(e, "something went wrong")
        else:
            output = {"details":"data not present"}
            self.status_update(self.base_dir,"failed", 
                               output, primary_key)
        return output

    @staticmethod
    def status_update(base_dir, status, output, primary_key):
        """
        status updation

        Args:
            status (str): status of the request
            output (dict | str): output data
            primary_key (int): primary key of the request
        """
        close_old_connections()
        obj = UkcpResponseTracker.objects.get(id=primary_key)
        obj.status = status
        obj.display_data = output
        obj.save()
        hist_file_path = os.path.join(base_dir,
                                f"UKCP/{obj.variable}/{obj.hist_job_id}")
        proj_file_path = os.path.join(base_dir,
                                f"UKCP/{obj.variable}/{obj.future_job_id}")
        try:
            shutil.rmtree(hist_file_path)
            shutil.rmtree(proj_file_path)
        except OSError as exc:
            print(exc)
            pass
            
    @staticmethod
    def get_config_data(base_dir, primary_key):
        """
        generate config data for core process

        Args:
            base_dir (str): base dir path
            primary_key (int): UKCP tracker primary key id
        """
        close_old_connections()
        obj = UkcpResponseTracker.objects.get(id=primary_key)
        # args thresh selection
        args = {}
        if obj.threshold_value_less_than != None:
            args['thresh'] = obj.threshold_value_less_than
        elif obj.threshold_value_more_than != None:
            args['thresh'] = obj.threshold_value_more_than
        else:pass
        # bias anomaly selection
        additive= ["tas","tasmin","tasmax"]
        multiplicative=["pr"]
        if (obj.datasource in ("ukcp18-high-res","loca"))\
            and (obj.variable in additive):
            bias = {
                    "method": "anomaly",
                    "kind": "+"
                }
        elif (obj.datasource in ("ukcp18-high-res","loca"))\
            and (obj.variable in multiplicative):
            bias = {
                    "method": "anomaly",
                    "kind": "*"
                }
        else:
            bias = {}
        # config generate
        if obj.hist_file_download_status == "completed" \
            and obj.future_file_download_status == "completed":
            config = {
                "data": {
                    "historical": {
                        "dataset": obj.datasource.lower(),
                        "variable": obj.variable,
                        "start": obj.his_from_date,
                        "end": obj.his_to_date,
                        "path": os.path.join(base_dir,
                                    f"UKCP/{obj.variable}/{obj.hist_job_id}")
                    },
                    "future": {
                        "dataset": obj.datasource.lower(),
                        "variable": obj.variable,
                        "start": obj.future_from_date,
                        "end": obj.future_to_date,
                        "path": os.path.join(base_dir,
                                f"UKCP/{obj.variable}/{obj.future_job_id}")
                    }
                },
                "indicator": {
                    "name": obj.variable_long_name[0],
                    "args": args
                },
                "bias":bias
            }
        else:
            config = None
        return config
