from django.db import models

# Create your models here.

class UkcpResponseTracker(models.Model):
    """ 
    UKCP response tracker model
    """
    user_name = models.CharField(default=None,null=True,max_length=50)
    project_name = models.CharField(default=None,null=True,max_length=100)
    project_code = models.CharField(default=None,null=True,max_length=100)
    consumer_id = models.CharField(default=None,null=True,max_length=100)
    datasource = models.CharField(default=None,null=True,max_length=100)
    dataset_id = models.CharField(default=None,null=True,max_length=100)
    dataset_version = models.CharField(default=None,null=True,max_length=100)
    geometry_type = models.CharField(default=None,null=True,max_length=100)
    lat = models.JSONField(null=True,default=None)
    lon = models.JSONField(null=True,default=None)
    region_long_name = models.CharField(default=None,null=True,max_length=100)
    region = models.CharField(default=None,null=True,max_length=20)
    country = models.CharField(default=None,null=True,max_length=40)
    main_variable = models.CharField(default=None,null=True,max_length=100) 
    variable_long_name = models.JSONField(null=True,default=None)
    variable = models.CharField(null=True,default=None,max_length=100)
    param = models.JSONField(null=True,default=None)
    his_from_date= models.CharField(default=None,null=True,max_length=100)
    his_to_date= models.CharField(default=None,null=True,max_length=100)
    future_from_date= models.CharField(default=None,null=True,max_length=100)
    future_to_date = models.CharField(default=None,null=True,max_length=100)
    history_display_data= models.JSONField(null=True,default=None)
    display_data= models.JSONField(null=True,default=None)
    model_used= models.JSONField(null=True,default=None)
    text_display_data= models.JSONField(null=True,default=None)
    current_item_status_date=models.DateTimeField(null=True,default=None)
    hist_file_download_status = models.CharField(default=None,null=True,
                                           max_length=50)
    future_file_download_status = models.CharField(default=None,null=True,
                                           max_length=50)
    threshold_value_less_than=models.FloatField(null = True,default=None)
    threshold_value_more_than=models.FloatField(null = True,default=None)
    is_hist_data_present =models.BooleanField(null=True)
    is_future_data_present =models.BooleanField(null=True)
    hist_job_id = models.CharField(default=None,null=True,max_length=100)
    future_job_id = models.CharField(default=None,null=True,max_length=100)
    status= models.CharField(default=None,null=True,max_length=50)