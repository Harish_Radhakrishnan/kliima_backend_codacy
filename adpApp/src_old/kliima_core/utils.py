import os
import datetime
import calendar
import warnings
import inspect
from collections.abc import Iterable, Sequence, Callable

import pandas as pd
import numpy as np
import xarray as xr

from .parse import parse, dateparse


def reduce(da, op, dim):
    if op is None:
        return da
    allowed_ops = ("mean", "max", "min", "sum")
    if op not in allowed_ops:
        warnings.warn(
            f"Reduction operation '{op}' not allowed, must be in {allowed_ops}.", 
            RuntimeWarning
        )
        return da
    if op == "mean":
        return da.mean(dim)
    if op == "max":
        return da.max(dim)
    if op == "min":
        return da.min(dim)
    if op == "sum":
        return da.sum(dim)


def multiindex_to_dict(df):
    if (df.index.nlevels == 1):
        return df.to_dict() if isinstance(df, pd.Series) else df.to_dict("index")
    return {level: multiindex_to_dict(df.xs(level)) for level in df.index.levels[0]}


def describe(da, dims=None, quantiles=None, method="linear", stddev=False, ddof=0, **kwargs):
    """TODO: docstring
    """
    minn = da.min(dims, **kwargs).expand_dims({"stat": ["min"]})
    mean = da.mean(dims, **kwargs).expand_dims({"stat": ["mean"]})
    medi = da.median(dims, **kwargs).expand_dims({"stat": ["median"]})
    maxx = da.max(dims, **kwargs).expand_dims({"stat": ["max"]})

    stats = xr.concat([minn, mean, medi, maxx], "stat")

    if quantiles:
        quantiles = np.sort(np.atleast_1d(np.asarray(quantiles, dtype=np.float64)))
        qtil = (
            da.quantile(quantiles, dims, method=method, **kwargs)
              .assign_coords(quantile=[f"{q*100}th" for q in quantiles])
              .rename({"quantile": "stat"})
        )
        stats = xr.concat([stats, qtil], "stat")

    if stddev:
        stdd = da.std(dims, ddof=ddof, **kwargs).expand_dims({"stat": ["std"]})
        stats = xr.concat([stats, stdd], "stat")

    return stats


def filter_files(
    files: Iterable[str | os.PathLike], 
    dataset: str, 
    **filters: str | Sequence[str]
):
    """TODO: docstring
    """
    filtered = []
    for file in files:
        parsed = parse(file, dataset)
        vals = [parsed.get(k) for k in filters.keys()]
        if None in vals:
            continue
        # TODO: this probably isn't as robust as it needs to be
        #   - e.g. case sensitivity
        #   - e.g. multiple filter values for one key (multiple scenarios for example)
        # TODO: consider comparing sets instead of sorted lists?
        cond = sorted(vals) == sorted(filters.values())
        if cond:
            filtered.append(file)
    return filtered


def filter_dates(
    files: Iterable[str | os.PathLike],
    *,
    start: datetime.datetime | None,
    end: datetime.datetime | None,
) -> list[str | os.PathLike]:
    """TODO: docstring
    """
    # TODO: update to work with multiple pairs of start and end dates, where the output
    # is a nested list with each inner list for each (start, end) pair.
    start = datetime.datetime.min if start is None else start
    end = datetime.datetime.max if end is None else end
    if start > end:
        raise ValueError(f"{start} is after {end}")
    filtered = []
    for file in files:
        fstart, fend = dateparse(file)
        cond = max(start, fstart) <= min(end, fend)
        if cond:
            filtered.append(file)
    return filtered


def offset_period(
    start: str, end: str, fmt: str, offset_alias: str
) -> tuple[datetime.datetime, datetime.datetime]:
    """TODO: docstring
    """
    if start is None and end is None:
        return None, None
    # TODO: use cftime.datetime instead of datetime.datetime? cftime has functionality for non-standard calendars.
    #       alternatively if cftime calendars are not essential, look into using pendulum library.
    start = datetime.datetime.strptime(str(start), fmt)
    end = datetime.datetime.strptime(str(end), fmt)
    # TODO: this can be made more general through clever use of pd.tseries and pd.offsets
    # see documentation:
    # - https://pandas.pydata.org/docs/user_guide/timeseries.html#dateoffset-objects
    # - https://pandas.pydata.org/docs/reference/offset_frequency.html
    end += datetime.timedelta(days=365) if calendar.isleap(end.year) else datetime.timedelta(days=364) 
    if offset_alias != "YS":
        mon = datetime.datetime.strptime(offset_alias.split("-")[-1], "%b").month
        offset = pd.offsets.DateOffset(months=(1-mon) % 12)
        start = start - offset
        end = end - offset
    return start, end


def kwargs_filter(kwargs: dict, func: Callable) -> dict:
    sig = inspect.signature(func)
    keys = [param.name for param in sig.parameters.values() if param.kind == param.KEYWORD_ONLY]
    return {key: kwargs[key] for key in keys if key in kwargs}
