from pathlib import Path
from typing import Callable

import xarray as xr

from .parse import parse

# TODO: write a get_dataloader wrapper function
DATALOADERS: dict[str, Callable] = {}

def register_dataloader(name: str) -> Callable:
    def _register_inner(func):
        DATALOADERS[name] = func
        return func
    return _register_inner


@register_dataloader("UKCP18")
def ukcp18_loader(ds):
    fname = Path(ds.encoding["source"])
    parsed = parse(fname, "UKCP18")
    return ds.squeeze().reset_coords("ensemble_member", drop=True).expand_dims(
            dim={
                "scenario": [parsed["scenario"]],
                "model": [parsed["member"]],
                "fid": [parsed["fid"]],
            },
            axis=[0, 1, 2],
    )
