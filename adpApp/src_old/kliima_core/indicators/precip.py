from typing import Callable

import xclim
xclim.set_options(data_validation="log", cf_compliance="log")

from .common import register_indicator


PRECIP_FUNCS: dict[str, Callable] = {}


@register_indicator("Average daily precipitation", PRECIP_FUNCS)
def pr_mean(pr, *, freq="YS"):
    return pr.resample(time=freq).mean(dim="time")


@register_indicator("Precipitation threshold more than", PRECIP_FUNCS)
def pr_days_above(pr, *, thresh, freq="YS"):
    return (pr > thresh).resample(time=freq).sum(dim="time")


@register_indicator("Precipitation threshold less than", PRECIP_FUNCS)
def pr_days_below(pr, *, thresh, freq="YS"):
    return (pr < thresh).resample(time=freq).sum(dim="time", skipna=False)


@register_indicator("Maximum 1 day rainfall", PRECIP_FUNCS)
def pr_max_1_day(pr, *, freq="YS"):
    return pr.resample(time=freq).max(dim="time")


@register_indicator("Maximum 5 day rainfall", PRECIP_FUNCS)  # TODO: generalise 5 to N
def pr_max_5_day(pr, *, window=5, freq="YS"):
    return pr.rolling(time=5).sum().resample(time=freq).max(dim="time")


@register_indicator("Consecutive dry days", PRECIP_FUNCS)
def pr_cumulative_dry_days(pr, *, thresh, freq="YS"):
    return xclim.indices.maximum_consecutive_dry_days(
        pr, thresh = str(thresh) + "mm/day", freq=freq
    )


@register_indicator("Average summer precipitation", PRECIP_FUNCS)
def pr_mean_summer(pr):
    return pr.groupby("time.season").mean().sel(season="JJA")


@register_indicator("Average winter precipitation", PRECIP_FUNCS)
def pr_mean_winter(pr):
    return pr.groupby("time.season").mean().sel(season="DJF")
