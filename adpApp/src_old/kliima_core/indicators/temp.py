from typing import Callable

import xclim
xclim.set_options(data_validation="log", cf_compliance="log")

from .common import register_indicator


TEMP_FUNCS: dict[str, Callable] = {}


@register_indicator("Maximum temperature", TEMP_FUNCS)
def tasmax_max(tasmax, *, freq="YS"):
    return tasmax.resample(time=freq).max(dim="time")


@register_indicator("Average daily temperature", TEMP_FUNCS)
def tas_mean(tas, *, freq="YS"):
    return tas.resample(time=freq).mean(dim="time")


@register_indicator("Average daily maximum temperature", TEMP_FUNCS)
def tasmax_mean(tasmax, *, freq="YS"):
    return tasmax.resample(time=freq).mean(dim="time")


@register_indicator("Average daily minimum temperature", TEMP_FUNCS)
def tasmin_mean(tasmin, *, freq="YS"):
    return tasmin.resample(time=freq).mean(dim="time")


@register_indicator("Temperature threshold more than", TEMP_FUNCS)
def tasmax_days_above(tasmax, *, thresh, freq="YS"):
    return (tasmax > thresh).resample(time=freq).sum(dim="time")


@register_indicator("Temperature threshold less than", TEMP_FUNCS)
def tasmin_days_below(tasmin, *, thresh, freq="YS"):
    return (tasmin < thresh).resample(time=freq).sum(dim="time")


@register_indicator("Average summer temperature", TEMP_FUNCS)
def tas_mean_summer(tas):
    return tas.groupby("time.season").mean().sel(season="JJA")


@register_indicator("Average winter temperature", TEMP_FUNCS)
def tas_mean_winter(tas):
    return tas.groupby("time.season").mean().sel(season="DJF")


@register_indicator("Average summer maximum temperature", TEMP_FUNCS)
def tasmax_mean_summer(tasmax):
    return tasmax.groupby("time.season").mean().sel(season="JJA")


@register_indicator("Average winter minimum temperature", TEMP_FUNCS)
def tasmin_mean_winter(tasmin):
    return tasmin.groupby("time.season").mean().sel(season="DJF")
