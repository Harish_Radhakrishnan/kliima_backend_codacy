from typing import Callable

from .temp import TEMP_FUNCS
from .precip import PRECIP_FUNCS


INDICATOR_FUNCS: dict[str, Callable] = (
    TEMP_FUNCS | 
    PRECIP_FUNCS
)
