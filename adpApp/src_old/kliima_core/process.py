import os
from pathlib import Path

import xarray as xr

from .indicators import INDICATOR_FUNCS
from .loaders import DATALOADERS
from .utils import reduce, kwargs_filter


def open_variable(path, dataset, variable):
    """TODO: docstring
    """
    files = Path(path).expanduser().rglob("*.nc")
    ds = xr.open_mfdataset(files, preprocess=DATALOADERS[dataset])
    return ds.get(variable)


def calculate_indicator(data, name, *, freq, reduction=None, **kwargs):
    """TODO: docstring
    """
    func = INDICATOR_FUNCS[name]
    return reduce(
        func(data, freq=freq, **kwargs_filter(kwargs, func)), 
        reduction, 
        dim="time"
    ).compute()


def process_variable(
    *,
    path: str | os.PathLike,
    dataset: str,
    variable: str,
    name: str,
    start: str,
    end: str,
    **kwargs,
):
    """TODO: docstring
    """
    data = open_variable(path, dataset, variable)
    indicator = calculate_indicator(data, name, **kwargs)
    return indicator
