import os
import json
from pathlib import Path

from .process import process_variable
from .utils import describe, multiindex_to_dict


def run_from_config(config: dict | str | os.PathLike):
    """TODO: docstring
    """
    if isinstance(config, (str, os.PathLike)):
        config = Path(config).expanduser()
        with config.open("r", encoding="utf-8") as f:
            config = json.load(f)

    data = config.get("data")
    indicator = config.get("indicator")
    params = config.get("params")

    historical = process_variable(
        name=indicator.get("name"), 
        **indicator.get("args"),
        **data.get("historical"), 
    )
    future = process_variable(
        name=indicator.get("name"), 
        **indicator.get("args"),
        **data.get("future"), 
    )

    anom_op = params.get("anomaly")
    if anom_op == "additive":
        anom = future - historical
    elif anom_op == "multiplicative":
        anom = future / historical
    elif anom_op == "percentage":
        anom = (future - historical) / historical * 100
    else:
        raise ValueError(
            "anomaly must be one of 'additive' or 'multiplicative' or 'percentage'"
        )

    df_anom = describe(anom, "model").to_dataframe(dim_order=["fid", "scenario", "stat"])[anom.name]
    return multiindex_to_dict(df_anom)
