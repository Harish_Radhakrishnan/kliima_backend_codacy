import os
import re
import datetime
from pathlib import Path

from . import _patterns


_formats = {
    name: re.compile(getattr(_patterns, name), flags=re.VERBOSE) 
    for name in dir(_patterns) 
    if not (name.endswith("__") and name.startswith("__"))
}


class ParseError(Exception): ...


def _to_datetime(date_string: str, fmt: str | None = None) -> datetime.datetime:
    if fmt is None:
        cleaned_string = re.sub(r"[^\d]+", "", date_string)
        if len(cleaned_string) == 8:
            fmt = "%Y%m%d"
        elif len(cleaned_string) == 6:
            fmt = "%Y%m"
        else:
            raise NotImplementedError
    else:
        cleaned_string = date_string

    try:
        dt = datetime.datetime.strptime(cleaned_string, fmt)
        return dt
    except ValueError:
        raise ParseError(f"{cleaned_string} is not cannot be parsed with format '{fmt}'")    


def parse(file: str | os.PathLike, dataset: str, to_datetime: bool=False) -> dict:
    """TODO: docstring
    """
    filename = Path(file).with_suffix(".nc").name
    try:
        pattern = _formats[dataset]
    except KeyError:
        raise ValueError(f"dataset {dataset} not found in {list(_formats.keys())}")
    parsed = pattern.match(filename)
    if parsed is None:
        raise ParseError(f"file {file} failed to parse for dataset {dataset}")
    result = parsed.groupdict()
    if to_datetime:
        result["start"] = _to_datetime(result["start"])
        result["end"] = _to_datetime(result["end"])
    return result


def dateparse(
    file: str | os.PathLike, *, pat: str | None = None, fmt: str | None = None
) -> tuple[datetime.datetime, datetime.datetime]:
    """TODO: docstring
    """
    filename = Path(file).with_suffix(".nc").name
    if pat is None:
        pat = r"[0-9]{6,}-[0-9]{6,}"
    match = re.findall(pat, filename)
    if not match:
        raise ParseError(f"pattern '{pat}' not found in '{filename}'")
    if len(match) > 1:
        pass  # TODO: display/log a warning
    dates = match[-1].split("-")
    start, end = map(lambda x: _to_datetime(x, fmt), dates)
    return start, end
